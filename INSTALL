﻿* This document written in UTF-8

地球流体電脳ライブラリー (dcl-6.5) インストール手順

2011/12/26	塩谷 雅人, 鍵本 崇, 乙部 直人, 神代 剛, 辻野 智紀
2014/06/03	乙部直人

-. フォントについて

  マーカー用TrueTypeフォントのインストールについては
  まだ，環境ごとにconfig出来るようにはなっていません．
  Windowsでは src/env1/sysfont の ttfファイルを C:\Windows\Fonts
  にコピーして下さい．
  Linux では同じファイルを ~/.fonts 以下か /usr/local/share/fontsの
  下にコピーしてfc-cache を実行して下さい．もしくは./configureした後
  Mkinclude の下の方の debfontdir を編集すればいいはずです．

0. はじめに

ここでは, 「電脳ライブラリ」をインストールする手順について述べる.

このバージョンでは図形出力媒体として, CRT (GTK), EPS (エン
カプセルドポストスクリプト), PDF (ポータブルドキュメントフォーマット),
 png (ビットマップ画像) の4つをサポートしている.
ウインドウシステムとして X11R4 (以上) が正しくインストールされていなけ
ればならない. また、gtk+3(gtk+2)とcairo が利用できる必要がある。

「電脳ライブラリ」をインストール際, それぞれのプラットフォームに対して
適切な Mkinclude ファイルを用意しなければならない. dcl-5.1 以降では,
configure スクリプトを用いることによって, Mkinclude ファイルを自動的に
生成できるようになっている.

比較的一般的なプラットフォームについては, configure スクリプトを用いれ
ば容易にライブラリの構築は可能である. configure スクリプトを用いたイン
ストール方法については「1. configure によるインストール」を参照された
い. configure スクリプトによるインストールがうまくいかない場合は,
「2. パスの設定」以降を参照されたい. Windows環境については文末に別に
記述したのでそちらを参照されたい．

1. configure によるインストール

configure スクリプトによる初期設定の自動化は, 現在, GNU のソフトウェア
はもちろんのこと, さまざまなフリーソフトウェアで実現されている. この
confiugre スクリプトにはいくつかのオプションが用意されており, ユーザ自
身でインストール環境をカスタマイズすることも可能である. 具体的なオプショ
ンについては以下のようなものがある.

< オプションの説明 >

  % ./configure [options]

  [options]

  --help                  ヘルプメッセージの出力
  --quiet, --silent       実行状態の非表示
  --prefix=PREFIX         インストールディレクトリを PREFIX に設定
                          デフォルトは /usr/local
  --with-gtk2             グラフィックライブラリをgtk+2.0に変更

さて, この configure スクリプトを用いた「電脳ライブラリ」の構築方法は
以下の通りである.

  0) 必要なソフトウェアをインストールする

	FortranとCのコンパイラをインストールする。XやGTKの開発用ライブラリをインストールする。
	最近のLinuxなどではパッケージで用意されていると思われるので、そのような物を
	適宜インストールするのが良い.

  1) dcl 本体を展開する

	% zcat dcl-7.5.0.tar.gz | tar xvf -
	% cd dcl-7.5.0

  2) configure を実行する

     (例1) 資源を /usr/local/dcl-7.5.0 に構築する

	% ./configure --prefix=/usr/local/dcl-7.5.0

     (例2) configure が捜し出すコンパイラとは異なるものを使用する

	% export  CC=gcc
	% export  FC=gfortran
	% ./configure

     (例3) 使用するgtkを 3ではなく2にする

	% ./configure --with-gtk2

  3) あとは make; make install で dcl 本体がインストールされる

なお configure に関連するファイルとしては以下のようなものがある.

  Mkinclude.in		: Mkinclude を作成するためのプロトタイプ
  aclocal.m4		: DCL 用 autoconf マクロ
  configure		: configure スクリプト
  configure.in		: configure スクリプト作成用プロトタイプ

ライブラリを構築する作業は make でおこなうが, make が用いる(各所に散ら
ばる) Makefile の中では Mkinclude というファイルを取り込んでいる.
Mkinclude ファイルはこの INSTALL ファイルと同じ電脳ライブラリのトップ
ディレクトリにあって, システムに依存しうるいくつかのマクロ名が定義され
ている. configure はこの Mkinclude ファイルを自動的に生成する. もしも
configure が生成する Mkinclude ファイルを使ってもうまくインストールで
きない場合は, この Mkinclude ファイルを適切に書き換えてやれば
(「3. Mkinclude ファイル」参照), 以下に述べるような手順でインストール
作業がおこなえるはずである.


2. パスの設定

make でコンパイルおよびインストールをするために, これから移植しようと
する電脳ライブラリが置かれているディレクトリ(この例ではとあるユーザー
のホームディレクトリに電脳ライブラリが展開されているとする)を環境変数
DCLDIR に設定しなければならない. ただし, configure によって Mkinclude
ファイルが正しく生成されている場合は, その中で DCLDIR は定義されている
ので, ふつうはユーザーが環境変数 DCLDIR を設定する必要はない.

configure によって Mkinclude ファイルが正しく生成されなかった場合は,
Mkinclude ファイルの中の DCLDIR を適切に書くか, 以下のように環境変数と
してパスの設定をおこなう必要がある.

  % export DCLDIR=~/dcl-7.5.0


3. Mkinclude ファイル

トップディレクトリ (すなわち cd $DCLDIR ではいったところ) に Mkinclude
というファイルがあっていくつかのマクロ名が定義されており, $DCLDIR 以下
の Makefile ではこの情報を include して使っている.

また, Mkinclude ファイルの先頭部分にはサフィックスルールが記述されてい
る. (ふつうこれらはコメントアウトされていても動作する. )

Mkinclude ファイルの中で注意すべきマクロ名として次のものがある.

a) コマンド, ライブラリ等を格納する場所を指定するマクロ

  BINDIR   : dclfrt などのコマンドを格納するディレクトリ
  LIBDIR   : ライブラリを格納するためのディレクトリ
  INCDIR   : include ファイルを格納するためのディレクトリ
  DBASEDIR : データベース関連のファイルを格納するためのディレクトリ

これらのマクロ名は初期設定ではそれぞれ, $(prefix)/bin, $(prefix)/lib,
$(prefix)/include, $(LIBDIR)/dcldbase となっている. なおこれらとは別に,
上記のディレクトリを一時的に置くための場所を指定するために, LBINDIR,
LLIBDIR, LINCDIR, LDBASEDIR などがある.

  DCLVERSION : 電脳ライブラリのバージョン (x.x 形式)
  DCLVERNUM  : 電脳ライブラリのバージョン (xx 形式)
  DCLLANG    : どの言語に対するパッケージかを示す

configure スクリプトを用いていれば, ここには適切な値が入っているはずで
あ. 手作業で Mkinclude を編集しようとして, もしもここに値が定義されて
いない場合は, バージョン名(dcl-5.3 なら 53)を書いておいた方がライブラ
リの整合性を取りやすい.

ｂ）gtkに関するマクロ

Gtk に関するマクロとして, GTKVERSION, GTK_CFLAGS, GTK_LIBS
などがある.

c) 図形出力装置の数に関するマクロ

  DCLNWS : 図形出力装置の数

通常の構成では, DISP, Files の2つの出力装置をサポートしている. この場
合, DCLNWS は 2 となる.

d) ライブラリ構築に際するいくつかの選択に関するマクロ

  MISC1EXT : misc1 において, 他では用いられないライブラリ名

MISC1EXT には src/misc1 の下にあって, 現在のところ他のライブラリからは
用いられることのないライブラリ名が列記されている. 「0. はじめに」に書
かれているような環境では, これらのライブラリもすべてコンパイル・インス
トールが可能であるが, システムによっては MISC1EXT のうちのいくつかがう
まく移植できないことがあるかもしれない. そのような場合は, うまく移植で
きないライブラリをこのリストから外してやればよい. 例えば, fiolib の移
植がうまくいかない場合,

  MISC1EXT = clcklib randlib hexlib reallib

のように fiolib をリストから外せばよい.

e) コマンドおよびそのオプションに関するマクロ

  CC      : C コンパイラ名
  CFLAGS  : C コンパイラが解釈するオプション
  FC      : Fortran コンパイラ名
  FFLAGS  : Fortran コンパイラが解釈するオプション
  RANLIB  : ランダムライブラリを作成するコマンド名

C および Fortran のコマンド名およびコンパイラオプションはそれぞれのシ
ステムで適切なものを指定すること. システムによってはライブラリをリンク
する際に, ライブラリをランダムライブラリ化しなければならない.  RANLIB
にはそのコマンド名を指定する. この操作が必要でないシステムについては
RANLIB として, たとえば touch コマンドなどを指定しておけばよい.

(注意) Fortran と C の結合規約

システムに依存するルーチンや X 関連のルーチンは C 言語を用いて書かれて
いる. Fortran と C との結合に際しては, SunFORTRAN で使われているように
「C 側の関数名に '_' (アンダースコアー)を付け加えることで, Fortran 側
からは '_' なしのサブルーチン(あるいは関数)として呼ぶことができる」と
いうルールに従っている.

これは言い換えると Fortran プログラムのコンパイル時に, ルーチン名に対
してアンダースコアーを付け加えることにほかならない. SunFORTRAN で使わ
れているこのような結合規約に従っていないシステムもあるが, そのようなシ
ステムでも多くの場合 Fortran のコンパイラオプションとしてアンダースコ
アーを付加するオプション(たとえば IBM AIX XL Fortran/6000 (xlf) では
-qextname) が用意されていることが多いので, マニュアルなどを注意深く参
照されたい.

Fortran 側にアンダースコアーを付加するオプションがない場合は, C 側の関
数名からアンダースコアーを取ってやればよい.

f) math1/syslib/glpget.f で用いられるマクロ

  INTMAX
  REALMAX
  REALMIN
  REPSL

これらの詳細については src/math1/syslib の NOTE を参照されたい.

g) misc1/clcklib/clckst.c で用いられるマクロ

  CLK_PER_SEC (CLOCKS_PER_SEC)
  CLK_RSL_TCK (CLK_TCK)

これらはカッコ内に示された識別子に対応する. ANSI 対応の C コンパイラで
はふつう, カッコ内に示された識別子が定義されているのでこれらのマクロを
指定する必要はない.

詳しくは src/misc1/clcklib の NOTE を参照されたい.

h) math1/oslib で用いられるマクロ

  OSQARN
  OSGARG

双方ともに、Fortran2003 が使用可能な場合にはF2003が指定される。

使用できない場合には, OSQARN については iargc または dummy の 2 つが指定可能で
ある. システムが iargc を持つ場合は iargc を, 持たない場合は dummy を
指定する.

OSGARG については getarg または dummy の 2 つが指定可能である. システ
ムが getarg を持つ場合は getarg を, 持たない場合は dummy を指定する.

これの詳細については src/math1/oslib の NOTE を参照されたい.

i) grph1/csgi で用いられるマクロ

  CSGI

ICHAR が0 から 255 の間の整数値を返す場合 general, -127 から 127 の間
の値を返す場合 other を指定する.

詳しくは src/misc1/clcklib の NOTE を参照されたい.

j) grph1/swpack で用いられるマクロ

  IWIDTH  : X で表示するウインドウの横幅
  IHEIGHT : X で表示するウインドウの縦幅

初期値はそれぞれ 900, 650 となっている.

k) grph2/uwpack で用いられるマクロ

  MAXNGRID : 作業領域の大きさ

初期値は 4000 となっている.


4. make - プログラムのコンパイル

ここまで準備したら, トップディレクトリで (つまり cd $DCLDIR ではいった
ところ)

  % make

とすれば以下の作業が行われる. (カッコ内には, make の具体的なターゲット
名を記す.) コンパイル(object), ライブラリの作成(library), データベースの
作成(dbase), , および $DCLDIR/bin ディレクトリにある基本コマンドの作成(script)

env2 にあるアプリケーションプログラムの作成(exec),exec は明示的に指定する必要がある.

 % make exec

5. make - インストール

コンパイルがすんだらインストールをおこなう.

  % make install

とすれば, ライブラリ, データベース, アプリケーションプログラムとコマン
ドコマンド群を適当なディレクトリにコピーする.

6. デモプログラムの作成

ここまでで必要な作業は終わっているので, 次に $DCLDIR/demo に移動してデ
モプログラムを作成する.

  % cd $DCLDIR/demo
  % make

make 終了後, 各ディレクトリを下がってデモプログラムを実行してみるとよ
い. グラフィック出力を含むものの実行例は次節参照のこと.


7. 使用例

電脳ライブラリを利用するには dclfrt コマンドを用いる. これは基本的に
Mkinclude のマクロ名 FC で指定した Fortran コンパイラのコマンドと同じ
で, ここまでの操作によって作成されたライブラリをあらかじめリンクするよ
うになっている. このコマンドを利用するためには, Mkinclude のなかの
BINDIR で指定されているディレクトリ名をシェル変数 $path に書き加えてお
かなければならない. これは .cshrc などの設定ファイルに書き加えておけば
簡単である. たとえば test01.f というグラフィックを含む次のようなプログ
ラムがあったとすると(ふつうの Fortran コンパイラでは -o オプションで実
行形式ファイル名を指定する),

      PROGRAM TEST01

      WRITE(*,*) 'WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)
      CALL SGFRM
      CALL SGSVPT(0.1,0.9,0.1,0.9)
      CALL SGSWND(0.0,10.0,0.0,10.0)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL SLPWWR(1)
      CALL SLPVPR(1)
      CALL SGTXZU(5.0,5.0,'TEST FOR GRPH1',0.03,0,0,3)
      CALL SGCLS

      END

  % dclfrt -o test01 test01.f

によって test01 という実行ファイルが作られる. (以下は標準ライブラリに
おける例である.)

  % ./test01

といれると

  WORKSTATION ID (I) ? ;
  1:X, 2:PS, 3:Tek ;

ときいてくる (ワークステーション名のリストは SGPWNS によって出力してい
る) ので, X ウインドウシステムが起動されている状態で1を入力するとウイ
ンドウが現れ, ウインドウの位置を確定すると描画がはじまる. 改ページ, 図
形表示の終了はクリックでおこなう.  2 (PS)を指定すると, dcl.ps というファ
イルがカレントディレクトリにできるので, 適当な出力コマンドを用いてプリ
ンターに出力する. たとえば,

  % lpr dcl.ps

とする. また3 (Tek)を指定するとテクトロ端末での描画が可能である.


8. 後始末

ディスクスペースに余裕がなければ, インストール途中に作成されたオブジェ
クトファイル, あるいはデモプログラムを消去する. トップディレクトリで

  % make clean

とすると不用なファイルが消去される.

なお,

  % make distclean

とすると, ローカルに生成したライブラリなどもすべて消去される.


A. Windowsでのインストール

パッケージを任意の場所に展開する．
展開されたトップディレクトリにある dclfrtvar.bat をダブルクリックする．
スタートメニューに登録するとよい．
必要な環境が設定されるはずであるが、されない場合は setenv.bat 内を確認する．
Intel Fortran のディレクトリなどが標準的でないか確認する．

A.1 Windows 上での DCL の Build

Microsoft Visual C++ がある場合は、nmake -f Makefile.win とすると
ビルドされ、 nmake -f Makefile.win install とすることでインストールされる．
インストール先などは トップディレクトリの Mkinclude.win に記述する．

A.2 Mingw環境

config.h がうまく作成されないので
cp config.h.mingw config.h を手動で行う必要がある．

B. CHECK

インストールが正しくおこなわれているかどうかは, たとえば以下の点に注意
する. (システムが用意する電脳ライブラリのコマンド類と区別するために,
それぞれのディレクトリに移って作業すること. )

B.1. $BINDIR (Mkinclude 参照) にあるいくつかのコマンドを実行してみる.

	% ./dclclr
		カラーマップが正しく表示されるか

	% ./dcldate; date
		日付情報が正しく表示されるか(date コマンドの出力と同じ
		か)

	% ./dcldbs
		データベースファイルのありかは正しいか

	% ./dclfont
		フォントのリストが正しく表示されるか

	% ./dclfont -sg:ifont=2
		コマンドラインオプションが通るか

	% ./dclver
		電脳ライブラリのバージョンは正しいか

	% ./repsl
		Mkinclude で指定した REPSL の値に近いか

B.2. $DCLDIR/demo/math1/oslib のチェック

	% ./oslb01
		osgenv が正しく動作しているか(printenv HOME と同じパス
		を示すか)

	% ./oslb02
		osexec が正しく動作しているか(ls の結果と同じか)

B.3. $DCLDIR/demo/misc1/clcklib のチェック

	% time ./clck01
		clckst.c が正しく動作しているか(time で出力される実行
		時間とだいたい等しいか - CPU の遅いマシンだと時間がか
		かるので注意!)

B.4. $DCLDIR/demo/misc1/randlib のチェック

	% ./rand01
		rngu0.c が正しく動作しているか(円周率 - 3.14 - に近い
		値が出力されるか)

B.5. $DCLDIR/demo/grph2/umpack のチェック

	% ./test05
		すべての地図投影が正しく表示されるか
