/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static real c_b7 = 11.2f;
static real c_b12 = 9.f;
static real c_b17 = -1.2f;
static integer c__11 = 11;
static integer c__4 = 4;
static integer c_n7 = -7;
static integer c__3 = 3;
static real c_b40 = 11.f;
static real c_b41 = 4.f;
static real c_b46 = -1.f;
static real c_b47 = 2.5f;
static real c_b55 = .080000000000000016f;
static real c_b60 = .8f;
static integer c__10 = 10;
static integer c_n1 = -1;
static real c_b77 = 180.f;
static real c_b82 = 3.1415926535f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    extern real rd2r_(real *), rr2d_(real *);
    extern integer imod_(integer *, integer *);
    extern real rfpi_(void), rmod_(real *, real *);
    extern integer igus_(real *);
    extern real rexp_(real *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, "(A)", 0 };
    static cilist io___2 = { 0, 6, 0, "(A,I2)", 0 };
    static cilist io___3 = { 0, 6, 0, "(A,I2)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,I2)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,I2)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,I2)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,F4.1)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A,F4.1)", 0 };
    static cilist io___11 = { 0, 6, 0, "(A)", 0 };
    static cilist io___12 = { 0, 6, 0, "(A,G16.8)", 0 };
    static cilist io___13 = { 0, 6, 0, "(A,G16.8)", 0 };
    static cilist io___14 = { 0, 6, 0, "(A)", 0 };
    static cilist io___15 = { 0, 6, 0, "(A,G16.8)", 0 };
    static cilist io___16 = { 0, 6, 0, "(A)", 0 };
    static cilist io___17 = { 0, 6, 0, "(A,G16.8)", 0 };
    static cilist io___18 = { 0, 6, 0, "(A,G16.8)", 0 };


    s_wsfe(&io___1);
    do_fio(&c__1, " *** TEST FOR IGUS", (ftnlen)18);
    e_wsfe();
    s_wsfe(&io___2);
    do_fio(&c__1, " IGUS(11.2) = ", (ftnlen)14);
    i__1 = igus_(&c_b7);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___3);
    do_fio(&c__1, " IGUS( 9.0) = ", (ftnlen)14);
    i__1 = igus_(&c_b12);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " IGUS(-1.2) = ", (ftnlen)14);
    i__1 = igus_(&c_b17);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " *** TEST FOR IMOD", (ftnlen)18);
    e_wsfe();
    s_wsfe(&io___6);
    do_fio(&c__1, " IMOD(11,4) = ", (ftnlen)14);
    i__1 = imod_(&c__11, &c__4);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___7);
    do_fio(&c__1, " IMOD(-7,3) = ", (ftnlen)14);
    i__1 = imod_(&c_n7, &c__3);
    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, " *** TEST FOR RMOD", (ftnlen)18);
    e_wsfe();
    s_wsfe(&io___9);
    do_fio(&c__1, " RMOD(11.0,4.0) = ", (ftnlen)18);
    r__1 = rmod_(&c_b40, &c_b41);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___10);
    do_fio(&c__1, " RMOD(-1.0,2.5) = ", (ftnlen)18);
    r__1 = rmod_(&c_b46, &c_b47);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___11);
    do_fio(&c__1, " *** TEST FOR REXP", (ftnlen)18);
    e_wsfe();
    s_wsfe(&io___12);
    do_fio(&c__1, " 0.8*10.0**(-1)  = ", (ftnlen)19);
    do_fio(&c__1, (char *)&c_b55, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___13);
    do_fio(&c__1, " REXP(0.8,10,-1) = ", (ftnlen)19);
    r__1 = rexp_(&c_b60, &c__10, &c_n1);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___14);
    do_fio(&c__1, " *** TEST FOR RFPI", (ftnlen)18);
    e_wsfe();
    s_wsfe(&io___15);
    do_fio(&c__1, " RFPI  = ", (ftnlen)9);
    r__1 = rfpi_();
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___16);
    do_fio(&c__1, " *** TEST FOR RD2R & RR2D", (ftnlen)25);
    e_wsfe();
    s_wsfe(&io___17);
    do_fio(&c__1, " RD2R(180.0)  = ", (ftnlen)16);
    r__1 = rd2r_(&c_b77);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_wsfe(&io___18);
    do_fio(&c__1, " RR2D(PI) = ", (ftnlen)12);
    r__1 = rr2d_(&c_b82);
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int fncl01_ () { MAIN__ (); return 0; }
