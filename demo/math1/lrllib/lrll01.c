/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real x[3] = { 1.2f,3.f,3.f };
    static real y[3] = { 1.5f,3.f,3.000003f };

    /* System generated locals */
    logical L__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__;
    extern logical lrge_(real *, real *), lrle_(real *, real *), lrne_(real *,
	     real *), lreq_(real *, real *), lrgt_(real *, real *), lrlt_(
	    real *, real *);
    extern /* Subroutine */ int gllset_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, "(2(A,G16.7),A)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___6 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A,L4)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A,L4)", 0 };


    gllset_("LEPSL", &c_true, (ftnlen)5);
    for (i__ = 1; i__ <= 3; ++i__) {
	s_wsfe(&io___4);
	do_fio(&c__1, "X = ", (ftnlen)4);
	do_fio(&c__1, (char *)&x[i__ - 1], (ftnlen)sizeof(real));
	do_fio(&c__1, "; Y = ", (ftnlen)6);
	do_fio(&c__1, (char *)&y[i__ - 1], (ftnlen)sizeof(real));
	do_fio(&c__1, ";", (ftnlen)1);
	e_wsfe();
	s_wsfe(&io___5);
	do_fio(&c__1, " LREQ(X,Y)=", (ftnlen)11);
	L__1 = lreq_(&x[i__ - 1], &y[i__ - 1]);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___6);
	do_fio(&c__1, " LRNE(X,Y)=", (ftnlen)11);
	L__1 = lrne_(&x[i__ - 1], &y[i__ - 1]);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___7);
	do_fio(&c__1, " LRLT(X,Y)=", (ftnlen)11);
	L__1 = lrlt_(&x[i__ - 1], &y[i__ - 1]);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___8);
	do_fio(&c__1, " LRLE(X,Y)=", (ftnlen)11);
	L__1 = lrle_(&x[i__ - 1], &y[i__ - 1]);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___9);
	do_fio(&c__1, " LRGT(X,Y)=", (ftnlen)11);
	L__1 = lrgt_(&x[i__ - 1], &y[i__ - 1]);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
	s_wsfe(&io___10);
	do_fio(&c__1, " LRGE(X,Y)=", (ftnlen)11);
	L__1 = lrge_(&x[i__ - 1], &y[i__ - 1]);
	do_fio(&c__1, (char *)&L__1, (ftnlen)sizeof(logical));
	e_wsfe();
/* L10: */
    }
    return 0;
} /* MAIN__ */

/* Main program alias */ int lrll01_ () { MAIN__ (); return 0; }
