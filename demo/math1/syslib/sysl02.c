/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__8 = 8;
static integer c_n999 = -999;
static integer c__3 = 3;
static real c_b18 = -999.f;
static integer c__4 = 4;
static integer c__60 = 60;
static integer c__5 = 5;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    static char cmsg[120];
    static integer imiss;
    static logical lmiss;
    static real rmiss;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), gllget_(
	    char *, logical *, ftnlen), glrget_(char *, real *, ftnlen), 
	    msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static integer nlines, maxmsg;
    extern /* Subroutine */ int glistx_(char *, integer *, ftnlen), gllstx_(
	    char *, logical *, ftnlen), glrstx_(char *, real *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };


    gllstx_("LMISS   ", &c_true, (ftnlen)8);
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    s_wsle(&io___2);
    do_lio(&c__9, &c__1, "--- LMISS    WAS CHANGED TO ", (ftnlen)28);
    do_lio(&c__8, &c__1, (char *)&lmiss, (ftnlen)sizeof(logical));
    e_wsle();
    glistx_("IMISS   ", &c_n999, (ftnlen)8);
    gliget_("IMISS   ", &imiss, (ftnlen)8);
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, "--- IMISS    WAS CHANGED TO ", (ftnlen)28);
    do_lio(&c__3, &c__1, (char *)&imiss, (ftnlen)sizeof(integer));
    e_wsle();
    glrstx_("RMISS   ", &c_b18, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "--- RMISS    WAS CHANGED TO ", (ftnlen)28);
    do_lio(&c__4, &c__1, (char *)&rmiss, (ftnlen)sizeof(real));
    e_wsle();
    glistx_("NLNSIZE ", &c__60, (ftnlen)8);
    gliget_("NLNSIZE ", &nlines, (ftnlen)8);
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "--- NLNSIZE  WAS CHANGED TO ", (ftnlen)28);
    do_lio(&c__3, &c__1, (char *)&nlines, (ftnlen)sizeof(integer));
    e_wsle();
    glistx_("MAXMSG  ", &c__5, (ftnlen)8);
    gliget_("MAXMSG  ", &maxmsg, (ftnlen)8);
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "--- MAXMSG   WAS CHANGED TO ", (ftnlen)28);
    do_lio(&c__3, &c__1, (char *)&maxmsg, (ftnlen)sizeof(integer));
    e_wsle();
    s_copy(cmsg, "1234567890123456789012345678901234567890123456789012345678"
	    "90123456789012345678901234567890123456789012345678901234567890", (
	    ftnlen)120, (ftnlen)120);
    for (n = 1; n <= 5; ++n) {
	msgdmp_("M", "TMSGDM", "TEST PROGRAM FOR MSGDMP.", (ftnlen)1, (ftnlen)
		6, (ftnlen)24);
	msgdmp_("W", "TMSGDM", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)120);
/* L10: */
    }
/*     CALL MSGDMP('E','TMSGDM','TEST PROGRAM FOR MSGDMP.') */
    return 0;
} /* MAIN__ */

/* Main program alias */ int sysl02_ () { MAIN__ (); return 0; }
