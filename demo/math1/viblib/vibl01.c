/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static integer ix[10] = { 2,1,4,6,7,-4,12,999,23,-5 };
    static integer iy[10] = { 4,-1,2,999,3,7,1,4,3,-1 };

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer iz[10];
    extern integer ifnb_();
    extern /* Subroutine */ int viadd_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), vifnb_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *, 
	    I_fp), vidiv_(integer *, integer *, integer *, integer *, integer 
	    *, integer *, integer *), visub_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), vimlt_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     gllset_(char *, logical *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___4 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___5 = { 0, 6, 0, "(A)", 0 };
    static cilist io___7 = { 0, 6, 0, "(A)", 0 };
    static cilist io___8 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___9 = { 0, 6, 0, "(A)", 0 };
    static cilist io___10 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___11 = { 0, 6, 0, "(A)", 0 };
    static cilist io___12 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___13 = { 0, 6, 0, "(A)", 0 };
    static cilist io___14 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___15 = { 0, 6, 0, "(A)", 0 };
    static cilist io___16 = { 0, 6, 0, "(A)", 0 };
    static cilist io___17 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___18 = { 0, 6, 0, "(A)", 0 };
    static cilist io___19 = { 0, 6, 0, "(A)", 0 };
    static cilist io___20 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___21 = { 0, 6, 0, "(A)", 0 };
    static cilist io___22 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___23 = { 0, 6, 0, "(A)", 0 };
    static cilist io___24 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___25 = { 0, 6, 0, "(A)", 0 };
    static cilist io___26 = { 0, 6, 0, "(A,10I6)", 0 };
    static cilist io___27 = { 0, 6, 0, "(A)", 0 };
    static cilist io___28 = { 0, 6, 0, "(A)", 0 };
    static cilist io___29 = { 0, 6, 0, "(A,10I6)", 0 };


    s_wsfe(&io___3);
    do_fio(&c__1, " LIST OF IX : ", (ftnlen)14);
    do_fio(&c__10, (char *)&ix[0], (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___4);
    do_fio(&c__1, " LIST OF IY : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iy[0], (ftnlen)sizeof(integer));
    e_wsfe();
    s_wsfe(&io___5);
    do_fio(&c__1, " *** OPTION LMISS = .FALSE.", (ftnlen)27);
    e_wsfe();
    viadd_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___7);
    do_fio(&c__1, " AFTER CALLING VIADD(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    visub_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___9);
    do_fio(&c__1, " AFTER CALLING VISUB(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___10);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    vimlt_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___11);
    do_fio(&c__1, " AFTER CALLING VIMLT(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___12);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    vidiv_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___13);
    do_fio(&c__1, " AFTER CALLING VIDIV(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___14);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    vifnb_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1, (I_fp)ifnb_);
    s_wsfe(&io___15);
    do_fio(&c__1, " AFTER CALLING VIFNB(IX,IY,IZ,10,1,1,1,IFNB)", (ftnlen)44);
    e_wsfe();
    s_wsfe(&io___16);
    do_fio(&c__1, " IFNB(I,J)=MIN(I,J)", (ftnlen)19);
    e_wsfe();
    s_wsfe(&io___17);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    gllset_("LMISS", &c_true, (ftnlen)5);
    s_wsfe(&io___18);
    do_fio(&c__1, " *** OPTION LMISS = .TRUE.", (ftnlen)26);
    e_wsfe();
    viadd_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___19);
    do_fio(&c__1, " AFTER CALLING VIADD(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___20);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    visub_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___21);
    do_fio(&c__1, " AFTER CALLING VISUB(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___22);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    vimlt_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___23);
    do_fio(&c__1, " AFTER CALLING VIMLT(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___24);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    vidiv_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1);
    s_wsfe(&io___25);
    do_fio(&c__1, " AFTER CALLING VIDIV(IX,IY,IZ,10,1,1,1)", (ftnlen)39);
    e_wsfe();
    s_wsfe(&io___26);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    vifnb_(ix, iy, iz, &c__10, &c__1, &c__1, &c__1, (I_fp)ifnb_);
    s_wsfe(&io___27);
    do_fio(&c__1, " AFTER CALLING VIFNB(IX,IY,IZ,10,1,1,1,IFNB)", (ftnlen)44);
    e_wsfe();
    s_wsfe(&io___28);
    do_fio(&c__1, " IFNB(I,J)=MIN(I,J)", (ftnlen)19);
    e_wsfe();
    s_wsfe(&io___29);
    do_fio(&c__1, " LIST OF IZ : ", (ftnlen)14);
    do_fio(&c__10, (char *)&iz[0], (ftnlen)sizeof(integer));
    e_wsfe();
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
integer ifnb_(integer *i__, integer *j)
{
    /* System generated locals */
    integer ret_val;

    ret_val = min(*i__,*j);
    return ret_val;
} /* ifnb_ */

/* Main program alias */ int vibl01_ () { MAIN__ (); return 0; }
