/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b9 = 1600.f;
static real c_b10 = 2e3f;
static real c_b11 = 11.f;
static real c_b12 = 15.f;
static real c_b13 = .15f;
static real c_b14 = .95f;
static real c_b16 = .65f;
static integer c__401 = 401;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real x[401], y[401], y0;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uulin_(integer *, real *, real *), sglset_(char *, logical *, 
	    ftnlen), grswnd_(real *, real *, real *, real *), grstrf_(void), 
	    usdaxs_(void), uspfit_(void), sgpwsn_(void), grsvpt_(real *, real 
	    *, real *, real *), ussttl_(char *, char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


/* -- データ ---- */
    y0 = .5f;
    for (n = 1; n <= 401; ++n) {
	x[n - 1] = (n - 1) * 400.f / 400 + 1600.f;
	y[n - 1] = y0 * 5.f + 10.f;
	y0 = y0 * 3.7f * (1.f - y0);
/* L10: */
    }
/* -- グラフ ---- */
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    sglset_("LFULL", &c_true, (ftnlen)5);
    grfrm_();
    grswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    grsvpt_(&c_b13, &c_b14, &c_b13, &c_b16);
    uspfit_();
    grstrf_();
    ussttl_("TIME", "YEAR", "TEMPERATURE", "DEG", (ftnlen)4, (ftnlen)4, (
	    ftnlen)11, (ftnlen)3);
    usdaxs_();
    uulin_(&c__401, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int lay2_ () { MAIN__ (); return 0; }
