/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;
static integer c__51 = 51;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j, n;
    static real y[401], y0;
    static integer ij;
    static real yy[51];
    static integer iws;
    static real xmin, xmax;
    static integer ijmin;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), sldiv_(char *, 
	    integer *, integer *, ftnlen), gropn_(integer *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), grswnd_(real 
	    *, real *, real *, real *), usgrph_(integer *, real *, real *), 
	    sgpwsn_(void), ussttl_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


/* -- データ ---- */
    y0 = .5f;
    for (n = 1; n <= 401; ++n) {
	y[n - 1] = y0 * 5.f + 10.f;
	y0 = y0 * 3.7f * (1.f - y0);
/* L10: */
    }
    glrget_("RUNDEF", &rundef, (ftnlen)6);
/* -- グラフ ---- */
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    sldiv_("Y", &c__3, &c__2, (ftnlen)1);
    for (i__ = 1; i__ <= 8; ++i__) {
	grfrm_();
	ijmin = (i__ - 1) * 50 + 1;
	ij = ijmin;
	for (j = 1; j <= 51; ++j) {
	    yy[j - 1] = y[ij - 1];
	    ++ij;
/* L30: */
	}
	xmin = (real) (ijmin + 1599);
	xmax = xmin + 50;
	grswnd_(&xmin, &xmax, &rundef, &rundef);
	ussttl_("TIME", "YEAR", "TEMPERATURE", "DEG", (ftnlen)4, (ftnlen)4, (
		ftnlen)11, (ftnlen)3);
	usgrph_(&c__51, &rundef, yy);
/* L20: */
    }
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int lay1_ () { MAIN__ (); return 0; }
