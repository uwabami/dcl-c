/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static doublereal c_b1 = 2.;
static integer c__100 = 100;
static real c_b9 = 0.f;
static real c_b10 = 1.f;
static real c_b13 = .2f;
static integer c__300 = 300;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b28 = -1.f;
static real c_b32 = .1f;
static real c_b33 = .9f;
static real c_b44 = .5f;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *), exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real b[10000]	/* was [100][100] */, g[10000]	/* was [100][
	    100] */;
    static integer i__, j;
    static real r__[10000]	/* was [100][100] */, bc[10000]	/* was [100][
	    100] */, gc[10000]	/* was [100][100] */, rc[10000]	/* was [100][
	    100] */;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uipda3_(real *, real *, real *, integer *, integer *, integer *),
	     prccls_(char *, ftnlen), prcopn_(char *, ftnlen), grswnd_(real *,
	     real *, real *, real *), grstrf_(void), rnorml_(real *, real *, 
	    integer *, integer *, real *, real *), swiset_(char *, integer *, 
	    ftnlen), grstrn_(integer *), uxaxdv_(char *, real *, real *, 
	    ftnlen), sgpwsn_(void), uyaxdv_(char *, real *, real *, ftnlen), 
	    grsvpt_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 100; ++i__) {
	for (j = 1; j <= 100; ++j) {
	    d__1 = (doublereal) (i__ - 50.f);
	    d__2 = (doublereal) (j - 50.f);
	    r__[i__ + j * 100 - 101] = exp(-(pow_dd(&d__1, &c_b1) + pow_dd(&
		    d__2, &c_b1)) / 5e3f);
	    d__1 = (doublereal) (i__ - 90.909090909090907f);
	    d__2 = (doublereal) (j - 83.333333333333343f);
	    g[i__ + j * 100 - 101] = exp(-(pow_dd(&d__1, &c_b1) + pow_dd(&
		    d__2, &c_b1)) / 1e3f);
	    d__1 = (doublereal) (i__ - 33.333333333333336f);
	    d__2 = (doublereal) (j - 20.f);
	    b[i__ + j * 100 - 101] = exp(-(pow_dd(&d__1, &c_b1) + pow_dd(&
		    d__2, &c_b1)) / 3e3f);
	}
    }
    rnorml_(r__, rc, &c__100, &c__100, &c_b9, &c_b10);
    rnorml_(g, gc, &c__100, &c__100, &c_b13, &c_b10);
    rnorml_(b, bc, &c__100, &c__100, &c_b9, &c_b10);
    swiset_("WINDOW_HEIGHT", &c__300, (ftnlen)13);
    swiset_("WINDOW_WIDTH", &c__300, (ftnlen)12);
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___10);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b28, &c_b10, &c_b28, &c_b10);
    grsvpt_(&c_b32, &c_b33, &c_b32, &c_b33);
    grstrn_(&c__1);
    grstrf_();
    prcopn_("DclPaintGrid3", (ftnlen)13);
    uipda3_(rc, gc, bc, &c__100, &c__100, &c__100);
    prccls_("DclPaintGrid3", (ftnlen)13);
    uxaxdv_("T", &c_b32, &c_b44, (ftnlen)1);
    uxaxdv_("B", &c_b32, &c_b44, (ftnlen)1);
    uyaxdv_("L", &c_b32, &c_b44, (ftnlen)1);
    uyaxdv_("R", &c_b32, &c_b44, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int cslib01_ () { MAIN__ (); return 0; }
