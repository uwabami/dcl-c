/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__500 = 500;
static integer c_n80 = -80;
static integer c__120 = 120;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer id, jd, nd, im, jm, iy, jy, itd, jtd, idate, jdate;
    extern /* Subroutine */ int datef1_(integer *, integer *, integer *), 
	    dateg1_(integer *, integer *, integer *), datef2_(integer *, 
	    integer *, integer *, integer *, integer *), dateg2_(integer *, 
	    integer *, integer *, integer *, integer *), datef3_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *),
	     dateg3_(integer *, integer *, integer *, integer *, integer *, 
	    integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 6, 0, 0, 0 };
    static cilist io___11 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 6, 0, 0, 0 };
    static cilist io___17 = { 0, 6, 0, 0, 0 };
    static cilist io___18 = { 0, 6, 0, 0, 0 };
    static cilist io___19 = { 0, 6, 0, 0, 0 };
    static cilist io___20 = { 0, 6, 0, 0, 0 };
    static cilist io___21 = { 0, 6, 0, 0, 0 };
    static cilist io___24 = { 0, 6, 0, 0, 0 };
    static cilist io___27 = { 0, 6, 0, 0, 0 };
    static cilist io___28 = { 0, 6, 0, 0, 0 };
    static cilist io___29 = { 0, 6, 0, 0, 0 };
    static cilist io___30 = { 0, 6, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "*** TEST FOR DATE COUNT", (ftnlen)23);
    e_wsle();
    s_wsle(&io___2);
    do_lio(&c__9, &c__1, "*** TYPE-1", (ftnlen)10);
    e_wsle();
    idate = 19890215;
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, "IDATE = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&idate, (ftnlen)sizeof(integer));
    e_wsle();
    datef1_(&c__500, &idate, &jdate);
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEF1(500,IDATE,JDATE)", (ftnlen)37);
    e_wsle();
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, "JDATE = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&jdate, (ftnlen)sizeof(integer));
    e_wsle();
    dateg1_(&nd, &idate, &jdate);
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEG1(ND,IDATE,JDATE)", (ftnlen)36);
    e_wsle();
    s_wsle(&io___10);
    do_lio(&c__9, &c__1, "ND = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&nd, (ftnlen)sizeof(integer));
    e_wsle();
    s_wsle(&io___11);
    do_lio(&c__9, &c__1, "*** TYPE-2", (ftnlen)10);
    e_wsle();
    iy = 1989;
    itd = 46;
    s_wsle(&io___14);
    do_lio(&c__9, &c__1, "IY = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&iy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : ITD = ", (ftnlen)9);
    do_lio(&c__3, &c__1, (char *)&itd, (ftnlen)sizeof(integer));
    e_wsle();
    datef2_(&c_n80, &iy, &itd, &jy, &jtd);
    s_wsle(&io___17);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEF2(-80,IY,ITD,JY,JTD)", (ftnlen)
	    39);
    e_wsle();
    s_wsle(&io___18);
    do_lio(&c__9, &c__1, "JY = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&jy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : JTD = ", (ftnlen)9);
    do_lio(&c__3, &c__1, (char *)&jtd, (ftnlen)sizeof(integer));
    e_wsle();
    dateg2_(&nd, &iy, &itd, &jy, &jtd);
    s_wsle(&io___19);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEG1(ND,IY,ITD,JY,JTD)", (ftnlen)38)
	    ;
    e_wsle();
    s_wsle(&io___20);
    do_lio(&c__9, &c__1, "ND = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&nd, (ftnlen)sizeof(integer));
    e_wsle();
    s_wsle(&io___21);
    do_lio(&c__9, &c__1, "*** TYPE-3", (ftnlen)10);
    e_wsle();
    iy = 1989;
    im = 2;
    id = 15;
    s_wsle(&io___24);
    do_lio(&c__9, &c__1, "IY = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&iy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : IM = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&im, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : ID = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&id, (ftnlen)sizeof(integer));
    e_wsle();
    datef3_(&c__120, &iy, &im, &id, &jy, &jm, &jd);
    s_wsle(&io___27);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEF3(120,IY,IM,ID,JY,JM,JD)", (
	    ftnlen)43);
    e_wsle();
    s_wsle(&io___28);
    do_lio(&c__9, &c__1, "JY = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&jy, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : JM = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&jm, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : JD = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&jd, (ftnlen)sizeof(integer));
    e_wsle();
    dateg3_(&nd, &iy, &im, &id, &jy, &jm, &jd);
    s_wsle(&io___29);
    do_lio(&c__9, &c__1, "AFTER CALLING DATEG3(ND,IY,IM,ID,JY,JM,JD)", (
	    ftnlen)42);
    e_wsle();
    s_wsle(&io___30);
    do_lio(&c__9, &c__1, "ND = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&nd, (ftnlen)sizeof(integer));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int date02_ () { MAIN__ (); return 0; }
