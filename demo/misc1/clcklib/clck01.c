/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);
    double sin(doublereal);

    /* Local variables */
    static integer i__;
    static real x, sx, dt0, time;
    extern real rngu1_(integer *);
    static integer iseed;
    extern /* Subroutine */ int clckdt_(real *), clckgt_(real *), clckst_(
	    void);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "*** TEST FOR MEASURING RUNTIME", (ftnlen)30);
    e_wsle();
    clckst_();
    iseed = 1;
    x = rngu1_(&iseed);
    for (i__ = 1; i__ <= 10000000; ++i__) {
	x = rngu1_(&iseed);
	sx = sin(x);
/* L10: */
    }
    clckgt_(&time);
    clckdt_(&dt0);
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "*** EXECUTION TIME = ", (ftnlen)21);
    do_lio(&c__4, &c__1, (char *)&time, (ftnlen)sizeof(real));
    e_wsle();
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "*** PRECISION = ", (ftnlen)16);
    do_lio(&c__4, &c__1, (char *)&dt0, (ftnlen)sizeof(real));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int clck01_ () { MAIN__ (); return 0; }
