/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer ih, im, is, itt;
    extern /* Subroutine */ int time12_(integer *, integer *), time21_(
	    integer *, integer *), time13_(integer *, integer *, integer *, 
	    integer *), time31_(integer *, integer *, integer *, integer *);
    static integer itime, jtime, ktime;

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___13 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 6, 0, 0, 0 };
    static cilist io___16 = { 0, 6, 0, 0, 0 };
    static cilist io___17 = { 0, 6, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, "*** TEST FOR TIME CONVERSION", (ftnlen)28);
    e_wsle();
    itime = 100930;
    s_wsle(&io___3);
    do_lio(&c__9, &c__1, "ITIME = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&itime, (ftnlen)sizeof(integer));
    e_wsle();
    time12_(&itime, &itt);
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, "AFTER CALLING TIME12(ITIME,ITT)", (ftnlen)31);
    e_wsle();
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, "ITT = ", (ftnlen)6);
    do_lio(&c__3, &c__1, (char *)&itt, (ftnlen)sizeof(integer));
    e_wsle();
    time21_(&jtime, &itt);
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, "AFTER CALLING TIME21(JTIME,ITT)", (ftnlen)31);
    e_wsle();
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, "JTIME = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&jtime, (ftnlen)sizeof(integer));
    e_wsle();
    time13_(&jtime, &ih, &im, &is);
    s_wsle(&io___13);
    do_lio(&c__9, &c__1, "AFTER CALLING TIME13(JTIME,IH,IM,IS)", (ftnlen)36);
    e_wsle();
    s_wsle(&io___14);
    do_lio(&c__9, &c__1, "IH = ", (ftnlen)5);
    do_lio(&c__3, &c__1, (char *)&ih, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : IM = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&im, (ftnlen)sizeof(integer));
    do_lio(&c__9, &c__1, " : IS = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&is, (ftnlen)sizeof(integer));
    e_wsle();
    time31_(&ktime, &ih, &im, &is);
    s_wsle(&io___16);
    do_lio(&c__9, &c__1, "AFTER CALLING TIME31(KTIME,IH,IM,IS)", (ftnlen)36);
    e_wsle();
    s_wsle(&io___17);
    do_lio(&c__9, &c__1, "KTIME = ", (ftnlen)8);
    do_lio(&c__3, &c__1, (char *)&ktime, (ftnlen)sizeof(integer));
    e_wsle();
    return 0;
} /* MAIN__ */

/* Main program alias */ int time01_ () { MAIN__ (); return 0; }
