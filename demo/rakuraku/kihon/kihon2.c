/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__41 = 41;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, n;
    static real x[41], y[164]	/* was [41][4] */, dt;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplv_(integer *, real *, real *), sgspli_(integer *), sgsplt_(
	    integer *), sgpwsn_(void), slpvpr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


    dt = .31415899999999997f;
    for (n = 0; n <= 40; ++n) {
	x[n] = (real) n / 40.f;
	for (i__ = 1; i__ <= 4; ++i__) {
	    y[n + i__ * 41 - 41] = sin(n * dt) * .2f + 1.f - i__ * .2f;
/* L10: */
	}
/* L20: */
    }
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
/* -- ラインインデクス: frame 1 -- */
    sgfrm_();
    slpvpr_(&c__1);
    sgplv_(&c__41, x, y);
    for (i__ = 2; i__ <= 4; ++i__) {
	i__1 = i__ << 1;
	sgspli_(&i__1);
	sgplv_(&c__41, x, &y[i__ * 41 - 41]);
/* L30: */
    }
    sgspli_(&c__1);
/* -- ラインタイプ: frame 2 -- */
    sgfrm_();
    slpvpr_(&c__1);
    sgplv_(&c__41, x, y);
    for (i__ = 2; i__ <= 4; ++i__) {
	sgsplt_(&i__);
	sgplv_(&c__41, x, &y[i__ * 41 - 41]);
/* L40: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int kihon2_ () { MAIN__ (); return 0; }
