/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b8 = 1950.f;
static real c_b9 = 2e3f;
static integer c__51 = 51;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real r__, y[51];
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), grswnd_(real 
	    *, real *, real *, real *), usgrph_(integer *, real *, real *), 
	    sgpwsn_(void), ussttl_(char *, char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___5 = { 0, 5, 0, 0, 0 };


    r__ = 3.7f;
    y[0] = .5f;
    for (n = 0; n <= 49; ++n) {
	y[n + 1] = r__ * y[n] * (1.f - y[n]);
/* L10: */
    }
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___5);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    grswnd_(&c_b8, &c_b9, &rundef, &rundef);
    ussttl_("TIME", "YEAR", "HEAT FLUX", "W/m\\^2\"", (ftnlen)4, (ftnlen)4, (
	    ftnlen)9, (ftnlen)7);
    usgrph_(&c__51, &rundef, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspac3_ () { MAIN__ (); return 0; }
