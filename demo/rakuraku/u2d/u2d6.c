/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = -1.f;
static real c_b8 = 1.f;
static real c_b11 = .2f;
static real c_b12 = .8f;
static logical c_false = FALSE_;
static real c_b19 = .5f;
static real c_b21 = .05f;
static logical c_true = TRUE_;
static real c_b25 = .1f;
static real c_b29 = .06f;
static integer c__11 = 11;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real u[121]	/* was [11][11] */, v[121]	/* was [11][11] */, x,
	     y;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     ugsut_(char *, char *, ftnlen, ftnlen), ugvect_(real *, integer *
	    , real *, integer *, integer *, integer *), uglset_(char *, 
	    logical *, ftnlen), grswnd_(real *, real *, real *, real *), 
	    grstrf_(void), usdaxs_(void), ugrset_(char *, real *, ftnlen), 
	    grstrn_(integer *), sgpwsn_(void), grsvpt_(real *, real *, real *,
	     real *);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 11; ++j) {
	for (i__ = 1; i__ <= 11; ++i__) {
	    x = (i__ - 1) * 2.f / 10 - 1.f;
	    y = (j - 1) * 2.f / 10 - 1.f;
	    u[i__ + j * 11 - 12] = x * .1f;
	    v[i__ + j * 11 - 12] = -y;
/* L10: */
	}
    }
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b7, &c_b8, &c_b7, &c_b8);
    grsvpt_(&c_b11, &c_b12, &c_b11, &c_b12);
    grstrn_(&c__1);
    grstrf_();
    usdaxs_();
    uglset_("LNRMAL", &c_false, (ftnlen)6);
    ugrset_("XFACT1", &c_b19, (ftnlen)6);
    ugrset_("YFACT1", &c_b21, (ftnlen)6);
    uglset_("LUNIT", &c_true, (ftnlen)5);
    ugrset_("VXUNIT", &c_b25, (ftnlen)6);
    ugrset_("VYUNIT", &c_b25, (ftnlen)6);
    ugrset_("VXUOFF", &c_b29, (ftnlen)6);
    ugsut_("X", "U", (ftnlen)1, (ftnlen)1);
    ugsut_("Y", "V", (ftnlen)1, (ftnlen)1);
    ugvect_(u, &c__11, v, &c__11, &c__11, &c__11);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2d6_ () { MAIN__ (); return 0; }
