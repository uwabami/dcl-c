/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b11 = 120.f;
static real c_b12 = 150.f;
static real c_b13 = 20.f;
static real c_b14 = 50.f;
static real c_b15 = .1f;
static real c_b16 = .9f;
static integer c__10 = 10;
static real c_b22 = 10.f;
static real c_b24 = 2.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real u[256]	/* was [16][16] */, v[256]	/* was [16][16] */;
    static integer iws;
    static real alat[16], alon[16];
    extern /* Subroutine */ int grcls_(void), sglau_(real *, real *, real *, 
	    real *), grfrm_(void), gropn_(integer *), sgslai_(integer *), 
	    umpglb_(void), umpmap_(char *, ftnlen), sglset_(char *, logical *,
	     ftnlen), grswnd_(real *, real *, real *, real *), umpfit_(void), 
	    grstrf_(void), grstrn_(integer *), umrset_(char *, real *, ftnlen)
	    , sgpwsn_(void), grsvpt_(real *, real *, real *, real *), slpvpr_(
	    integer *);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 16; ++i__) {
	alon[i__ - 1] = (i__ - 1) * 30.f / 15 + 120.f;
/* L10: */
    }
    for (j = 1; j <= 16; ++j) {
	alat[j - 1] = (j - 1) * 30.f / 15 + 20.f;
/* L20: */
    }
    for (j = 1; j <= 16; ++j) {
	for (i__ = 1; i__ <= 16; ++i__) {
	    u[i__ + (j << 4) - 17] = (j - 1 - 7.5f) * .2f;
	    v[i__ + (j << 4) - 17] = -(i__ - 1 - 7.5f) * .2f;
/* L30: */
	}
    }
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    grswnd_(&c_b11, &c_b12, &c_b13, &c_b14);
    grsvpt_(&c_b15, &c_b16, &c_b15, &c_b16);
    grstrn_(&c__10);
    umpfit_();
    grstrf_();
    slpvpr_(&c__3);
    umrset_("DGRIDMJ", &c_b22, (ftnlen)7);
    umrset_("DGRIDMN", &c_b24, (ftnlen)7);
    umpmap_("coast_world", (ftnlen)11);
    umpglb_();
    sgslai_(&c__3);
    for (j = 1; j <= 16; ++j) {
	for (i__ = 1; i__ <= 16; ++i__) {
	    r__1 = alon[i__ - 1] + u[i__ + (j << 4) - 17];
	    r__2 = alat[j - 1] + v[i__ + (j << 4) - 17];
	    sglau_(&alon[i__ - 1], &alat[j - 1], &r__1, &r__2);
/* L40: */
	}
    }
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int map3d3_ () { MAIN__ (); return 0; }
