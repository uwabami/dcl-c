/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static real x[12]	/* was [3][4] */ = { 1.f,3.f,-1.f,2.f,-4.f,0.f,2.f,
	    1.f,3.f,2.f,1.f,0.f };

    /* Format strings */
    static char fmt_100[] = "(a,3f6.1)";

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__, j, nw1[3];
    static real wz1[6]	/* was [3][2] */;
    extern /* Subroutine */ int vs1din_(real *, integer *, integer *, real *),
	     vs1int_(real *, integer *, integer *), vs1out_(real *, integer *,
	     integer *);

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 6, 0, "(A)", 0 };
    static cilist io___6 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___8 = { 0, 6, 0, fmt_100, 0 };
    static cilist io___9 = { 0, 6, 0, fmt_100, 0 };


    s_wsfe(&io___2);
    do_fio(&c__1, " *** TEST FOR VS1INT / VS1DIN / VS1OUT", (ftnlen)38);
    e_wsfe();
    vs1int_(wz1, nw1, &c__3);
    for (j = 1; j <= 4; ++j) {
	s_wsfe(&io___6);
	do_fio(&c__1, " INPUT = ", (ftnlen)9);
	for (i__ = 1; i__ <= 3; ++i__) {
	    do_fio(&c__1, (char *)&x[i__ + j * 3 - 4], (ftnlen)sizeof(real));
	}
	e_wsfe();
	vs1din_(wz1, nw1, &c__3, &x[j * 3 - 3]);
/* L10: */
    }
    vs1out_(wz1, nw1, &c__3);
    s_wsfe(&io___8);
    do_fio(&c__1, " AVE.  = ", (ftnlen)9);
    for (i__ = 1; i__ <= 3; ++i__) {
	do_fio(&c__1, (char *)&wz1[i__ - 1], (ftnlen)sizeof(real));
    }
    e_wsfe();
    s_wsfe(&io___9);
    do_fio(&c__1, " VAR.  = ", (ftnlen)9);
    for (i__ = 1; i__ <= 3; ++i__) {
	do_fio(&c__1, (char *)&wz1[i__ + 2], (ftnlen)sizeof(real));
    }
    e_wsfe();
    return 0;
} /* MAIN__ */

/* Main program alias */ int vstl01_ () { MAIN__ (); return 0; }
