/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 0.f;
static real c_b8 = 1.f;
static real c_b9 = -.8f;
static real c_b10 = .2f;
static real c_b11 = .1f;
static real c_b12 = .9f;
static logical c_true = TRUE_;
static integer c__41 = 41;
static real c_b22 = -.7f;
static real c_b23 = .3f;
static integer c__2 = 2;
static real c_b28 = -.6f;
static real c_b29 = .4f;
static real c_b34 = -.5f;
static real c_b35 = .5f;
static integer c__4 = 4;
static real c_b40 = -.4f;
static real c_b41 = .6f;
static real c_b46 = -.3f;
static real c_b47 = .7f;
static real c_b54 = -.2f;
static real c_b55 = .8f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real x[41], y[41];
    static integer n1, n2;
    static real dt;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplu_(integer *, real *, real *), sgpmu_(integer *, real *, 
	    real *), gllset_(char *, logical *, ftnlen), sglset_(char *, 
	    logical *, ftnlen), sgspmi_(integer *), sgswnd_(real *, real *, 
	    real *, real *), sgstrf_(void), sgspmt_(integer *), sgstrn_(
	    integer *), sgpwsn_(void), sgsvpt_(real *, real *, real *, real *)
	    , slpvpr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 6, 0, 0, 0 };
    static cilist io___6 = { 0, 5, 0, 0, 0 };


    dt = .31415899999999997f;
    for (i__ = 1; i__ <= 41; ++i__) {
	y[i__ - 1] = sin(dt * (i__ - 1)) * .15f;
	x[i__ - 1] = (real) (i__ - 1) / 40.f;
/* L10: */
    }
    s_wsle(&io___5);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___6);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sgswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    sgsvpt_(&c_b11, &c_b12, &c_b11, &c_b12);
    sgstrn_(&c__1);
    sgstrf_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    slpvpr_(&c__1);
    sgpmu_(&c__41, x, y);
/* <-- マーカー描画 (1 */
    sgswnd_(&c_b7, &c_b8, &c_b22, &c_b23);
    sgstrf_();
    sgspmt_(&c__2);
/* <-- マーカー TYPE */
    sgpmu_(&c__41, x, y);
/* <-- マーカー描画 (2 */
    sgswnd_(&c_b7, &c_b8, &c_b28, &c_b29);
    sgstrf_();
    sgspmt_(&c__3);
/* <-- マーカー TYPE */
    sgpmu_(&c__41, x, y);
/* <-- マーカー描画 (3 */
    sgswnd_(&c_b7, &c_b8, &c_b34, &c_b35);
    sgstrf_();
    sgspmt_(&c__4);
/* <-- マーカー TYPE */
    sgpmu_(&c__41, x, y);
/* <-- マーカー描画 (4 */
    sgswnd_(&c_b7, &c_b8, &c_b40, &c_b41);
    sgstrf_();
    sgspmi_(&c__2);
/* <-- マーカー INDEX */
    sgpmu_(&c__41, x, y);
/* <-- マーカー描画 (5 */
    sgswnd_(&c_b7, &c_b8, &c_b46, &c_b47);
    sgstrf_();
    sgspmi_(&c__3);
    sgspmt_(&c__2);
    sgplu_(&c__41, x, y);
/* <-- */
    sgpmu_(&c__41, x, y);
/* <-- マーカー描画 (6 */
    n1 = 10;
    y[n1 - 2] = 999.f;
/* <-- */
    y[n1 - 1] = 999.f;
    y[n1] = 999.f;
    n2 = n1 * 3;
    y[n2 - 2] = 999.f;
    y[n2] = 999.f;
    sgswnd_(&c_b7, &c_b8, &c_b54, &c_b55);
    sgstrf_();
    gllset_("LMISS", &c_true, (ftnlen)5);
    sgspmi_(&c__2);
    sgspmt_(&c__3);
    sgplu_(&c__41, x, y);
/* <-- */
    sgpmu_(&c__41, x, y);
/* <-- マーカー描画 (7 */
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk04_ () { MAIN__ (); return 0; }
