/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 2.f;
static real c_b8 = 1.f;
static integer c__2 = 2;
static real c_b12 = .1f;
static real c_b17 = -1.f;
static real c_b19 = 100.f;
static real c_b21 = .9f;
static real c_b27 = 0.f;
static integer c__100 = 100;
static integer c__5 = 5;
static real c_b42 = .02f;
static real c_b43 = .5f;
static real c_b46 = .05f;
static integer c__0 = 0;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double exp(doublereal), sin(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real ux[100], uy[100];
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), slmgn_(real *, 
	    real *, real *, real *), sldiv_(char *, integer *, integer *, 
	    ftnlen), slrat_(real *, real *), sgopn_(integer *), gllset_(char *
	    , logical *, ftnlen), sgiset_(char *, integer *, ftnlen), sgswnd_(
	    real *, real *, real *, real *), sgstrf_(void), sgstrn_(integer *)
	    , sgpwsn_(void), sglnzu_(real *, real *, real *, real *, integer *
	    ), sgplzu_(integer *, real *, real *, integer *, integer *), 
	    sgpmzu_(integer *, real *, real *, integer *, integer *, real *), 
	    sgsvpt_(real *, real *, real *, real *), slpwwr_(integer *), 
	    sgtxzv_(real *, real *, char *, real *, integer *, integer *, 
	    integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 6, 0, 0, 0 };
    static cilist io___5 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 100; ++i__) {
	ux[i__ - 1] = (real) i__;
	uy[i__ - 1] = exp(-i__ * .03f) * sin(i__ * 3.14f / 180 * 20);
/* L10: */
    }
    s_wsle(&io___4);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___5);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    iws = abs(iws);
    sgopn_(&iws);
    slrat_(&c_b7, &c_b8);
    sldiv_("Y", &c__2, &c__1, (ftnlen)1);
    slmgn_(&c_b12, &c_b12, &c_b12, &c_b12);
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC. */
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC. */
/*     TRANSFORMATION FUNCTION : LINEAR*LINEAR */
    sgfrm_();
    sgswnd_(&c_b8, &c_b19, &c_b17, &c_b8);
    sgsvpt_(&c_b12, &c_b21, &c_b12, &c_b21);
    sgstrn_(&c__1);
    sgstrf_();
    slpwwr_(&c__1);
    sglnzu_(&c_b8, &c_b27, &c_b19, &c_b27, &c__1);
    sglnzu_(&c_b8, &c_b17, &c_b8, &c_b8, &c__1);
    sgplzu_(&c__100, ux, uy, &c__2, &c__3);
    sgpmzu_(&c__100, ux, uy, &c__5, &c__3, &c_b42);
    sgtxzv_(&c_b43, &c_b21, "TEST1", &c_b46, &c__0, &c__0, &c__3, (ftnlen)5);
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC. */
/*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC. */
/*     TRANSFORMATION FUNCTION : LOG*LINEAR */
    sgfrm_();
    sgswnd_(&c_b8, &c_b19, &c_b17, &c_b8);
    sgsvpt_(&c_b12, &c_b21, &c_b12, &c_b21);
    sgstrn_(&c__3);
    sgstrf_();
    slpwwr_(&c__1);
    gllset_("LMISS", &c_true, (ftnlen)5);
    sgiset_("NPMSKIP", &c__2, (ftnlen)7);
    ux[4] = 999.f;
    uy[19] = 999.f;
    sglnzu_(&c_b8, &c_b27, &c_b19, &c_b27, &c__1);
    sglnzu_(&c_b8, &c_b17, &c_b8, &c_b8, &c__1);
    sgplzu_(&c__100, ux, uy, &c__3, &c__3);
    sgpmzu_(&c__100, ux, uy, &c__2, &c__3, &c_b42);
    sgtxzv_(&c_b43, &c_b21, "TEST2", &c_b46, &c__0, &c__0, &c__3, (ftnlen)5);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgksx3_ () { MAIN__ (); return 0; }
