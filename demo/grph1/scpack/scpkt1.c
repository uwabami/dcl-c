/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b10 = 0.f;
static real c_b15 = 1.f;
static real c_b17 = -1.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    double cos(doublereal), sin(doublereal);

    /* Local variables */
    static integer i__;
    static real dt, th;
    static integer iws;
    extern /* Subroutine */ int cube_(void);
    extern real rfpi_(void);
    static real xeye, yeye, zeye;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     scsobj_(real *, real *, real *), scseye_(real *, real *, real *),
	     sglset_(char *, logical *, ftnlen), scsprj_(void), sgpwsn_(void),
	     slpwwr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION IS (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sglset_("LCLIP", &c_true, (ftnlen)5);
    dt = rfpi_() / 10.f;
    for (i__ = 1; i__ <= 21; ++i__) {
	sgfrm_();
	slpwwr_(&c__1);
	th = (i__ - 1) * dt;
	xeye = cos(th) * 10.f;
	yeye = sin(th) * 10.f;
	zeye = 2.f;
	scsobj_(&c_b10, &c_b10, &c_b10);
	scseye_(&xeye, &yeye, &zeye);
	scsprj_();
	cube_();
/* L10: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int cube_(void)
{
    /* Initialized data */

    static real x[5] = { -1.f,1.f,1.f,-1.f,-1.f };
    static real y[5] = { -1.f,-1.f,1.f,1.f,-1.f };

    static integer i__, i1, i2;
    extern /* Subroutine */ int szcll3_(void), szpll3_(real *, real *, real *)
	    , szopl3_(void), szmvl3_(real *, real *, real *);

    for (i__ = 2; i__ <= 5; ++i__) {
	i1 = i__ - 1;
	i2 = i__;
	szopl3_();
	szmvl3_(&x[i1 - 1], &y[i1 - 1], &c_b15);
	szpll3_(&x[i2 - 1], &y[i2 - 1], &c_b15);
	szpll3_(&x[i2 - 1], &y[i2 - 1], &c_b17);
	szpll3_(&x[i1 - 1], &y[i1 - 1], &c_b17);
	szcll3_();
/* L10: */
    }
    return 0;
} /* cube_ */

/* Main program alias */ int scpkt1_ () { MAIN__ (); return 0; }
