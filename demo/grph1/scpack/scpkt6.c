/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static real c_b11 = 1.f;
static real c_b12 = 0.f;
static real c_b16 = -5.f;
static real c_b17 = -3.f;
static real c_b18 = 2.f;
static integer c__2999 = 2999;
static integer c__4999 = 4999;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real p[1369]	/* was [37][37] */, xp[3], yp[3], zp[3];
    static integer iws;
    static real alat[37], alon[37];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     scplu_(integer *, real *, real *, real *), sctnu_(real *, real *,
	     real *), scsobj_(real *, real *, real *), scseye_(real *, real *,
	     real *), scspli_(integer *), scsorg_(real *, real *, real *, 
	    real *), sglset_(char *, logical *, ftnlen), scstrf_(void), 
	    scsprj_(void), scstnp_(integer *, integer *), scstrn_(integer *), 
	    sgpwsn_(void);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


    for (i__ = 1; i__ <= 37; ++i__) {
	alon[i__ - 1] = (i__ - 1) * 360.f / 36 + 0.f;
/* L10: */
    }
    for (j = 1; j <= 37; ++j) {
	alat[j - 1] = (j - 1) * 180.f / 36 + 0.f;
/* L20: */
    }
    for (j = 1; j <= 37; ++j) {
	for (i__ = 1; i__ <= 37; ++i__) {
	    p[i__ + j * 37 - 38] = 1.f;
/* L30: */
	}
    }
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sglset_("LDEG", &c_true, (ftnlen)4);
/* ---------------- 3-D ------------------ */
    scsorg_(&c_b11, &c_b12, &c_b12, &c_b12);
    scstrn_(&c__3);
    scstrf_();
    scseye_(&c_b16, &c_b17, &c_b18);
    scsobj_(&c_b12, &c_b12, &c_b12);
    scsprj_();
    scspli_(&c__1);
    scstnp_(&c__2999, &c__4999);
    for (j = 36; j >= 1; --j) {
	for (i__ = 36; i__ >= 1; --i__) {
	    xp[0] = alon[i__ - 1];
	    yp[0] = alat[j - 1];
	    zp[0] = p[i__ + j * 37 - 38];
	    xp[1] = alon[i__ - 1];
	    yp[1] = alat[j];
	    zp[1] = p[i__ + (j + 1) * 37 - 38];
	    xp[2] = alon[i__];
	    yp[2] = alat[j];
	    zp[2] = p[i__ + 1 + (j + 1) * 37 - 38];
	    sctnu_(zp, yp, xp);
	    scplu_(&c__3, zp, yp, xp);
	    xp[0] = alon[i__];
	    yp[0] = alat[j];
	    zp[0] = p[i__ + 1 + (j + 1) * 37 - 38];
	    xp[1] = alon[i__];
	    yp[1] = alat[j - 1];
	    zp[1] = p[i__ + 1 + j * 37 - 38];
	    xp[2] = alon[i__ - 1];
	    yp[2] = alat[j - 1];
	    zp[2] = p[i__ + j * 37 - 38];
	    sctnu_(zp, yp, xp);
	    scplu_(&c__3, zp, yp, xp);
/* L40: */
	}
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int scpkt6_ () { MAIN__ (); return 0; }
