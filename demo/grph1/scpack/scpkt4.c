/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__2999 = 2999;
static integer c__3999 = 3999;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);
    double sin(doublereal), cos(doublereal);

    /* Local variables */
    static real a;
    static integer i__;
    static real t1, t2, x1, x2, y1, y2, dt;
    extern /* Subroutine */ int box_(real *, real *, real *, real *);
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     szl3cl_(void), szt3cl_(void), szl3op_(integer *), szt3op_(
	    integer *, integer *), sglset_(char *, logical *, ftnlen), 
	    scsprj_(void), sgstrf_(void), sgpwsn_(void);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    sglset_("L2TO3", &c_true, (ftnlen)5);
/*     CALL SCSEYE( -0.5, -0.5, 2.0) */
/*     CALL SCSOBJ(  0.5,  0.5, 0.5) */
    scsprj_();
    sgstrf_();
    dt = .78539749999999997f;
    a = .5f;
    szl3op_(&c__1);
    szt3op_(&c__2999, &c__3999);
    for (i__ = 1; i__ <= 8; ++i__) {
	t1 = dt * (i__ - 1);
	t2 = dt * i__;
	x1 = a * sin(t1) + .5f;
	x2 = a * sin(t2) + .5f;
	y1 = a * cos(t1) + .5f;
	y2 = a * cos(t2) + .5f;
	box_(&x1, &y1, &x2, &y2);
/* L10: */
    }
    szl3cl_();
    szt3cl_();
    sgcls_();
    return 0;
} /* MAIN__ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int box_(real *x1, real *y1, real *x2, real *y2)
{
    static real x[3], y[3], z__[3];
    extern /* Subroutine */ int szl3zv_(integer *, real *, real *, real *), 
	    szt3zv_(real *, real *, real *);

    x[0] = *x1;
    x[1] = *x1;
    x[2] = *x2;
    y[0] = *y1;
    y[1] = *y1;
    y[2] = *y2;
    z__[0] = .1f;
    z__[1] = -.1f;
    z__[2] = -.1f;
    szt3zv_(x, y, z__);
    szl3zv_(&c__3, x, y, z__);
    x[0] = *x2;
    x[1] = *x2;
    x[2] = *x1;
    y[0] = *y2;
    y[1] = *y2;
    y[2] = *y1;
    z__[0] = -.1f;
    z__[1] = .1f;
    z__[2] = .1f;
    szt3zv_(x, y, z__);
    szl3zv_(&c__3, x, y, z__);
    return 0;
} /* box_ */

/* Main program alias */ int scpkt4_ () { MAIN__ (); return 0; }
