/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c_n1 = -1;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__100 = 100;
static real c_b15 = 0.f;
static real c_b16 = 1.f;
static integer c__5999 = 5999;
static integer c__4999 = 4999;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[5];
    static integer i__, j;
    static real t, y1[100], y2[100];
    static integer jj;
    static real dt, pi, yy;
    static integer iws;
    extern /* Subroutine */ int uhdif_(integer *, real *, real *, real *), 
	    grcls_(void), grfrm_(void), uvdif_(integer *, real *, real *, 
	    real *), gropn_(integer *), uulin_(integer *, real *, real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglset_(char 
	    *, logical *, ftnlen), grswnd_(real *, real *, real *, real *), 
	    grstrf_(void), usdaxs_(void), uspfit_(void), uusarp_(integer *, 
	    integer *), sgpwsn_(void), usspnt_(integer *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___12 = { 0, 6, 0, 0, 0 };
    static cilist io___13 = { 0, 5, 0, 0, 0 };


/* ----------------------------------------------------------------------- */
    dt = .010101010101010102f;
    pi = 3.14159f;
    for (j = 1; j <= 5; ++j) {
	jj = (j << 1) - 1;
	a[j - 1] = pow_ii(&c_n1, &j) * 2.f / (jj * pi);
/* L50: */
    }
    for (i__ = 1; i__ <= 100; ++i__) {
	t = dt * (i__ - 1) * 2 * pi;
	y1[i__ - 1] = a[0] * cos(t);
	y2[i__ - 1] = 0.f;
	for (j = 1; j <= 5; ++j) {
	    jj = (j << 1) - 1;
	    yy = a[j - 1] * cos(jj * t);
	    y2[i__ - 1] += yy;
/* L150: */
	}
/* L100: */
    }
/* ----------------------------------------------------------------------- */
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    s_wsle(&io___12);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___13);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    usspnt_(&c__100, &rundef, y1);
    usspnt_(&c__100, &rundef, y2);
    grswnd_(&c_b15, &c_b16, &rundef, &rundef);
    uspfit_();
    grstrf_();
    usdaxs_();
    uusarp_(&c__5999, &c__4999);
    uvdif_(&c__100, &rundef, y1, y2);
    uulin_(&c__100, &rundef, y1);
    uulin_(&c__100, &rundef, y2);
    grfrm_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    usspnt_(&c__100, y1, &rundef);
    usspnt_(&c__100, y2, &rundef);
    grswnd_(&rundef, &rundef, &c_b15, &c_b16);
    uspfit_();
    grstrf_();
    usdaxs_();
    uhdif_(&c__100, y1, y2, &rundef);
    uulin_(&c__100, y1, &rundef);
    uulin_(&c__100, y2, &rundef);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uupk06_ () { MAIN__ (); return 0; }
