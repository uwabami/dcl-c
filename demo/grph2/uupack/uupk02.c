/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c_n1 = -1;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_true = TRUE_;
static integer c__20 = 20;
static integer c__33 = 33;
static integer c__4 = 4;
static integer c__21 = 21;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[5];
    static integer i__, j;
    static real t, x0[20], x1[20], x2[20], y0[20], y1[20], y2[20];
    static integer jj;
    static real dt, pi, dy, yy;
    static integer iws;
    extern /* Subroutine */ int uherb_(integer *, real *, real *, real *), 
	    grcls_(void), grfrm_(void), uverb_(integer *, real *, real *, 
	    real *), gropn_(integer *), uumrk_(integer *, real *, real *), 
	    uusebi_(integer *), sglset_(char *, logical *, ftnlen), grstrf_(
	    void), usdaxs_(void), uusebt_(integer *), uspfit_(void), uusmki_(
	    integer *), sgpwsn_(void), uusmkt_(integer *), usspnt_(integer *, 
	    real *, real *);

    /* Fortran I/O blocks */
    static cilist io___16 = { 0, 6, 0, 0, 0 };
    static cilist io___17 = { 0, 5, 0, 0, 0 };


/* ----------------------------------------------------------------------- */
    dt = .052631578947368418f;
    pi = 3.14159f;
    for (j = 1; j <= 5; ++j) {
	jj = (j << 1) - 1;
	a[j - 1] = pow_ii(&c_n1, &j) * 2.f / (jj * pi);
/* L50: */
    }
    for (i__ = 1; i__ <= 20; ++i__) {
	t = dt * (i__ - 1) * 2 * pi;
	x0[i__ - 1] = dt * (i__ - 1);
	y1[i__ - 1] = a[0] * cos(t);
	y2[i__ - 1] = 0.f;
	for (j = 1; j <= 5; ++j) {
	    jj = (j << 1) - 1;
	    yy = a[j - 1] * cos(jj * t);
	    y2[i__ - 1] += yy;
/* L150: */
	}
	y0[i__ - 1] = (y1[i__ - 1] + y2[i__ - 1]) / 2.f;
	dy = (r__1 = y1[i__ - 1] - y2[i__ - 1], abs(r__1));
	x1[i__ - 1] = x0[i__ - 1] - dy / 5;
	x2[i__ - 1] = x0[i__ - 1] + dy / 5;
/* L100: */
    }
/* ----------------------------------------------------------------------- */
    s_wsle(&io___16);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___17);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    sglset_("LCLIP", &c_true, (ftnlen)5);
    usspnt_(&c__20, x0, y0);
    usspnt_(&c__20, x1, y1);
    usspnt_(&c__20, x2, y2);
    uspfit_();
    grstrf_();
    usdaxs_();
    uverb_(&c__20, x0, y1, y2);
    uusebt_(&c__3);
    uusebi_(&c__33);
    uherb_(&c__20, x1, x2, y0);
    uusmkt_(&c__4);
    uusmki_(&c__21);
    uumrk_(&c__20, x0, y0);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uupk02_ () { MAIN__ (); return 0; }
