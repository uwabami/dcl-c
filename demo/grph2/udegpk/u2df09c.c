/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__5 = 5;
static logical c_false = FALSE_;
static real c_b11 = -10.f;
static real c_b12 = 10.f;
static real c_b15 = .2f;
static real c_b16 = .8f;
static integer c__21 = 21;
static real c_b25 = 1.f;
static real c_b26 = 5.f;
static real c_b32 = 0.f;
static real c_b49 = .85f;
static real c_b50 = .9f;
static real c_b51 = .45f;
static real c_b52 = .75f;
static integer c__2 = 2;
static integer c__6 = 6;
static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j, k;
    static real p[441]	/* was [21][21] */, x, y, dp, pi[12]	/* was [2][6] 
	    */;
    static integer iws, ipat;
    static real tlev1, tlev2;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     sgscmn_(integer *), uzfact_(real *), uetonc_(real *, integer *, 
	    integer *, integer *), uetone_(real *, integer *, integer *, 
	    integer *), udlset_(char *, logical *, ftnlen), sglset_(char *, 
	    logical *, ftnlen), grswnd_(real *, real *, real *, real *), 
	    grstrf_(void), grstrn_(integer *), uxaxdv_(char *, real *, real *,
	     ftnlen), sgpwsn_(void), uestlv_(real *, real *, integer *), 
	    uyaxdv_(char *, real *, real *, ftnlen), grsvpt_(real *, real *, 
	    real *, real *), slpvpr_(integer *), uzlset_(char *, logical *, 
	    ftnlen), uysfmt_(char *, ftnlen), uxmttl_(char *, char *, real *, 
	    ftnlen, ftnlen), uxsttl_(char *, char *, real *, ftnlen, ftnlen), 
	    uysttl_(char *, char *, real *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 21; ++j) {
	for (i__ = 1; i__ <= 21; ++i__) {
	    x = (i__ - 1) * 20.f / 20 - 10.f;
	    y = (j - 1) * 20.f / 20 - 10.f;
/* Computing 2nd power */
	    r__1 = x;
/* Computing 2nd power */
	    r__2 = y;
	    p[i__ + j * 21 - 22] = exp(-(r__1 * r__1) / 64 - r__2 * r__2 / 25)
		    ;
/* L15: */
	}
/* L10: */
    }
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgscmn_(&c__5);
    gropn_(&iws);
    sglset_("LSOFTF", &c_false, (ftnlen)6);
    grfrm_();
    grswnd_(&c_b11, &c_b12, &c_b11, &c_b12);
    grsvpt_(&c_b15, &c_b16, &c_b15, &c_b16);
    grstrn_(&c__1);
    grstrf_();
    dp = .20000000000000001f;
    for (k = 1; k <= 5; ++k) {
	tlev1 = (k - 1) * dp;
	tlev2 = tlev1 + dp;
	ipat = (99 - k * 15) * 1000 + 999;
	uestlv_(&tlev1, &tlev2, &ipat);
/* L20: */
    }
    uetonc_(p, &c__21, &c__21, &c__21);
    uxaxdv_("B", &c_b25, &c_b26, (ftnlen)1);
    uxaxdv_("T", &c_b25, &c_b26, (ftnlen)1);
    uxsttl_("B", "X-axis", &c_b32, (ftnlen)1, (ftnlen)6);
    uxmttl_("T", "FIGURE TITLE", &c_b32, (ftnlen)1, (ftnlen)12);
    uyaxdv_("L", &c_b25, &c_b26, (ftnlen)1);
    uyaxdv_("R", &c_b25, &c_b26, (ftnlen)1);
    uysttl_("L", "Y-axis", &c_b32, (ftnlen)1, (ftnlen)6);
/* -- トーンバー ---- */
    grswnd_(&c_b32, &c_b25, &c_b32, &c_b25);
    grsvpt_(&c_b49, &c_b50, &c_b51, &c_b52);
    grstrn_(&c__1);
    grstrf_();
    for (k = 1; k <= 6; ++k) {
	pi[(k << 1) - 2] = (k - 1) * dp + 0.f;
	pi[(k << 1) - 1] = (k - 1) * dp + 0.f;
/* L30: */
    }
    udlset_("LMSG", &c_false, (ftnlen)4);
    uetone_(pi, &c__2, &c__2, &c__6);
/*     CALL UDCNTR( PI, 2, 2, KMAX+1 ) */
    slpvpr_(&c__3);
    uzlset_("LABELYR", &c_true, (ftnlen)7);
    uzfact_(&c_b16);
    uysfmt_("(F4.1)", (ftnlen)6);
    uyaxdv_("R", &dp, &dp, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2df09c_ () { MAIN__ (); return 0; }
