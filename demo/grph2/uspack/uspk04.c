/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c_n1 = -1;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__200 = 200;
static integer c__5 = 5;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    double cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[5];
    static integer i__, j;
    static real t, x[200], y0[200], y1[200], y2[200];
    static integer jj;
    static real dt, pi, yy;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     usgrph_(integer *, real *, real *), uuslni_(integer *), sgpwsn_(
	    void), uulinz_(integer *, real *, real *, integer *, integer *), 
	    usspnt_(integer *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___13 = { 0, 6, 0, 0, 0 };
    static cilist io___14 = { 0, 5, 0, 0, 0 };


/* ----------------------------------------------------------------------- */
    dt = .0050251256281407036f;
    pi = 3.14159f;
    for (j = 1; j <= 5; ++j) {
	jj = (j << 1) - 1;
	a[j - 1] = pow_ii(&c_n1, &j) * 2.f / (jj * pi);
/* L50: */
    }
    for (i__ = 1; i__ <= 200; ++i__) {
	t = dt * (i__ - 1) * 2 * pi;
	x[i__ - 1] = dt * (i__ - 1);
	y2[i__ - 1] = 0.f;
	for (j = 1; j <= 5; ++j) {
	    jj = (j << 1) - 1;
	    yy = a[j - 1] * cos(jj * t);
	    y2[i__ - 1] += yy;
/* L150: */
	}
	y1[i__ - 1] = a[0] * cos(t);
	if (t < pi / 2.f || t >= pi * 3.f / 2.f) {
	    y0[i__ - 1] = -.5f;
	} else {
	    y0[i__ - 1] = .5f;
	}
/* L100: */
    }
/* ----------------------------------------------------------------------- */
    s_wsle(&io___13);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___14);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    usspnt_(&c__200, x, y1);
    usspnt_(&c__200, x, y2);
    uuslni_(&c__5);
    usgrph_(&c__200, x, y0);
    uulinz_(&c__200, x, y1, &c__3, &c__1);
    uulinz_(&c__200, x, y2, &c__2, &c__2);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk04_ () { MAIN__ (); return 0; }
