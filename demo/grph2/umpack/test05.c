/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static logical c_false = FALSE_;
static real c_b16 = 75.f;
static real c_b18 = 60.f;
static real c_b22 = 30.f;
static real c_b24 = 0.f;
static real c_b26 = 165.f;
static real c_b29 = .1f;
static real c_b30 = .9f;
static real c_b34 = 360.f;
static real c_b35 = -90.f;
static real c_b36 = 90.f;
static real c_b38 = -180.f;
static real c_b39 = 180.f;
static logical c_true = TRUE_;
static real c_b50 = .5f;
static real c_b51 = .95f;
static real c_b52 = .03f;
static integer c__0 = 0;
static integer c__73 = 73;
static integer c__37 = 37;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char ctr[3*14] = "CYL" "MER" "MWD" "HMR" "EK6" "KTD" "CON" "COA" 
	    "COC" "BON" "OTG" "PST" "AZM" "AZA";
    static real fct[14] = { .12f,.12f,.14f,.14f,.14f,.14f,.11f,.16f,.12f,.12f,
	    .4f,.12f,.12f,.17f };

    /* System generated locals */
    integer i__1;
    olist o__1;
    cllist cl__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), f_open(olist *), 
	    s_rsfe(cilist *), do_fio(integer *, char *, ftnlen), e_rsfe(void),
	     f_clos(cllist *), s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, j;
    static real p[2701]	/* was [73][37] */, r__;
    static integer ir, idx, iws;
    static real amin, amax;
    static char cttl[32];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     umpglb_(void);
    extern integer isgtrc_(char *, ftnlen);
    extern /* Subroutine */ int uetone_(real *, integer *, integer *, integer 
	    *), udcntr_(real *, integer *, integer *, integer *), sglset_(
	    char *, logical *, ftnlen), umpmap_(char *, ftnlen), sgssim_(real 
	    *, real *, real *), sgsmpl_(real *, real *, real *), sgrset_(char 
	    *, real *, ftnlen), sgswnd_(real *, real *, real *, real *), 
	    umlset_(char *, logical *, ftnlen), sgstrf_(void), sgtrnl_(
	    integer *, char *, ftnlen), umrset_(char *, real *, ftnlen), 
	    sgstrn_(integer *), sgpwsn_(void), uestlv_(real *, real *, 
	    integer *), sgsvpt_(real *, real *, real *, real *), slpvpr_(
	    integer *), slpwwr_(integer *), sgstxy_(real *, real *, real *, 
	    real *), sgtxzr_(real *, real *, char *, real *, integer *, 
	    integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };
    static cilist io___7 = { 0, 11, 0, "(10F8.3)", 0 };


    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    o__1.oerr = 0;
    o__1.ounit = 11;
    o__1.ofnmlen = 11;
    o__1.ofnm = "t811231.dat";
    o__1.orl = 0;
    o__1.osta = 0;
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    for (j = 1; j <= 37; ++j) {
	s_rsfe(&io___7);
	for (i__ = 1; i__ <= 73; ++i__) {
	    do_fio(&c__1, (char *)&p[i__ + j * 73 - 74], (ftnlen)sizeof(real))
		    ;
	}
	e_rsfe();
/* L10: */
    }
    cl__1.cerr = 0;
    cl__1.cunit = 11;
    cl__1.csta = 0;
    f_clos(&cl__1);
    for (ir = 190; ir <= 245; ir += 5) {
	r__ = (real) ir;
	amin = r__;
	amax = r__ + 5;
/*       IDX=(R-180)*1.4*1000+999 */
	idx = (integer) ((r__ - 170) * 1.25f) * 1000 + 999;
	uestlv_(&amin, &amax, &idx);
/* L20: */
    }
    sgopn_(&iws);
    sglset_("LSOFTF", &c_false, (ftnlen)6);
    sgrset_("STLAT1", &c_b16, (ftnlen)6);
    sgrset_("STLAT2", &c_b18, (ftnlen)6);
    umlset_("LGRIDMJ", &c_false, (ftnlen)7);
    umrset_("DGRIDMN", &c_b22, (ftnlen)7);
    for (i__ = 1; i__ <= 14; ++i__) {
	sgfrm_();
	sgssim_(&fct[i__ - 1], &c_b24, &c_b24);
	sgsmpl_(&c_b26, &c_b18, &c_b24);
	sgsvpt_(&c_b29, &c_b30, &c_b29, &c_b30);
	sgswnd_(&c_b24, &c_b34, &c_b35, &c_b36);
	if (s_cmp(ctr + (i__ - 1) * 3, "OTG", (ftnlen)3, (ftnlen)3) == 0) {
	    sgstxy_(&c_b38, &c_b39, &c_b24, &c_b36);
	} else {
	    sgstxy_(&c_b38, &c_b39, &c_b35, &c_b36);
	}
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	sgstrn_(&i__1);
	sgstrf_();
	sglset_("LCLIP", &c_true, (ftnlen)5);
	slpwwr_(&c__1);
	slpvpr_(&c__1);
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	sgtrnl_(&i__1, cttl, (ftnlen)32);
	sgtxzr_(&c_b50, &c_b51, cttl, &c_b52, &c__0, &c__0, &c__3, (ftnlen)32)
		;
	uetone_(p, &c__73, &c__73, &c__37);
	udcntr_(p, &c__73, &c__73, &c__37);
	umpmap_("coast_world", (ftnlen)11);
	umpglb_();
	slpvpr_(&c__1);
/* L30: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int test05_ () { MAIN__ (); return 0; }
