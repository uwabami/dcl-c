/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 45.f;
static real c_b9 = 30.f;
static real c_b11 = 0.f;
static real c_b14 = 90.f;
static real c_b16 = .1f;
static real c_b17 = .9f;
static real c_b21 = -180.f;
static real c_b22 = 180.f;
static real c_b27 = -90.f;
static logical c_true = TRUE_;
static real c_b33 = .5f;
static real c_b34 = .95f;
static real c_b35 = .03f;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static char ctr[3*20] = "CYL" "MER" "MWD" "HMR" "EK6" "KTD" "MIL" "RBS" 
	    "SIN" "VDG" "CON" "COA" "COC" "BON" "PLC" "OTG" "PST" "AZM" "AZA" 
	    "GNO";
    static real fct[20] = { .12f,.12f,.14f,.14f,.14f,.14f,.12f,.12f,.12f,.12f,
	    .11f,.16f,.12f,.12f,.12f,.4f,.12f,.12f,.17f,.18f };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void), s_cmp(char *, char *
	    , ftnlen, ftnlen);

    /* Local variables */
    static integer i__, iws;
    static char cttl[32];
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     umpglb_(void);
    extern integer isgtrc_(char *, ftnlen);
    extern /* Subroutine */ int umpmap_(char *, ftnlen), sglset_(char *, 
	    logical *, ftnlen), sgssim_(real *, real *, real *), sgsmpl_(real 
	    *, real *, real *), sgrset_(char *, real *, ftnlen), sgstrf_(void)
	    , sgtrnl_(integer *, char *, ftnlen), sgstrn_(integer *), sgpwsn_(
	    void), sgsvpt_(real *, real *, real *, real *), slpvpr_(integer *)
	    , slpwwr_(integer *), sgstxy_(real *, real *, real *, real *), 
	    sgtxzr_(real *, real *, char *, real *, integer *, integer *, 
	    integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 6, 0, 0, 0 };
    static cilist io___4 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___3);
    do_lio(&c__9, &c__1, " WORKSTATION IS (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___4);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgrset_("STLAT1", &c_b7, (ftnlen)6);
    sgrset_("STLAT2", &c_b9, (ftnlen)6);
    for (i__ = 1; i__ <= 20; ++i__) {
	sgfrm_();
	sgssim_(&fct[i__ - 1], &c_b11, &c_b11);
	sgsmpl_(&c_b11, &c_b14, &c_b11);
	sgsvpt_(&c_b16, &c_b17, &c_b16, &c_b17);
	if (s_cmp(ctr + (i__ - 1) * 3, "OTG", (ftnlen)3, (ftnlen)3) == 0) {
	    sgstxy_(&c_b21, &c_b22, &c_b11, &c_b14);
	} else {
	    sgstxy_(&c_b21, &c_b22, &c_b27, &c_b14);
	}
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	sgstrn_(&i__1);
	sgstrf_();
	sglset_("LCLIP", &c_true, (ftnlen)5);
	slpwwr_(&c__1);
	slpvpr_(&c__1);
	i__1 = isgtrc_(ctr + (i__ - 1) * 3, (ftnlen)3);
	sgtrnl_(&i__1, cttl, (ftnlen)32);
	sgtxzr_(&c_b33, &c_b34, cttl, &c_b35, &c__0, &c__0, &c__3, (ftnlen)32)
		;
	umpmap_("coast_world", (ftnlen)11);
	umpglb_();
/* L10: */
    }
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int test04_ () { MAIN__ (); return 0; }
