/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b10 = 0.f;
static real c_b11 = 360.f;
static real c_b12 = -90.f;
static real c_b13 = 90.f;
static real c_b14 = .2f;
static real c_b15 = .8f;
static integer c__37 = 37;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sin(doublereal), sqrt(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real p[1369]	/* was [37][37] */;
    static integer iws;
    static real alat, alon, slat;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), gllset_(char 
	    *, logical *, ftnlen), uetone_(real *, integer *, integer *, 
	    integer *), udcntr_(real *, integer *, integer *, integer *), 
	    grswnd_(real *, real *, real *, real *), grstrf_(void), usdaxs_(
	    void), grstrn_(integer *), sgpwsn_(void), grsvpt_(real *, real *, 
	    real *, real *);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllset_("LMISS", &c_true, (ftnlen)5);
    for (j = 1; j <= 37; ++j) {
	for (i__ = 1; i__ <= 37; ++i__) {
	    alon = ((i__ - 1) * 360.f / 36 + 0.f) * .017453277777777776f;
	    alat = ((j - 1) * 180.f / 36 - 90.f) * .017453277777777776f;
	    slat = sin(alat);
/* Computing 2nd power */
	    r__1 = slat;
/* Computing 2nd power */
	    r__2 = slat;
	    p[i__ + j * 37 - 38] = sqrt(1 - r__1 * r__1) * 3 * slat * cos(
		    alon) - (r__2 * r__2 * 3 - 1) * .5f;
	    if (i__ == 6 && j == 6) {
		p[i__ + j * 37 - 38] = rmiss;
	    }
	    if (8 <= i__ && i__ <= 24 && j == 30) {
		p[i__ + j * 37 - 38] = rmiss;
	    }
	    if (22 <= i__ && i__ <= 30 && (12 <= j && j <= 20)) {
		p[i__ + j * 37 - 38] = rmiss;
	    }
/* L10: */
	}
    }
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b10, &c_b11, &c_b12, &c_b13);
    grsvpt_(&c_b14, &c_b15, &c_b14, &c_b15);
    grstrn_(&c__1);
    grstrf_();
    uetone_(p, &c__37, &c__37, &c__37);
    usdaxs_();
    udcntr_(p, &c__37, &c__37, &c__37);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int miss2_ () { MAIN__ (); return 0; }
