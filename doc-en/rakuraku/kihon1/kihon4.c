/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = .1f;
static real c_b8 = .9f;
static real c_b9 = .5f;
static real c_b10 = .05f;
static real c_b12 = .95f;
static integer c__5 = 5;
static real c_b22 = .03f;
static real c_b25 = .07f;
static integer c_n1 = -1;
static integer c__0 = 0;
static integer c__10 = 10;
static integer c_n10 = -10;

/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real y[9];
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglnv_(real *, real *, real *, real *), sgtxv_(real *, real *, 
	    char *, ftnlen), sgstxc_(integer *), sgpwsn_(void), sgstxi_(
	    integer *), sgstxr_(integer *), sgstxs_(real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
/* -- 罫線 ---- */
    for (n = 1; n <= 9; ++n) {
	y[n - 1] = (10 - n) * .1f;
	sglnv_(&c_b7, &y[n - 1], &c_b8, &y[n - 1]);
/* L10: */
    }
    sglnv_(&c_b9, &c_b10, &c_b9, &c_b12);
/* -- デフォルト ---- */
    sgtxv_(&c_b9, y, "SGTXV", (ftnlen)5);
/* -- 文字のラインインデクス ---- */
    sgstxi_(&c__3);
    sgtxv_(&c_b9, &y[1], "INDEX3", (ftnlen)6);
    sgstxi_(&c__5);
    sgtxv_(&c_b9, &y[2], "INDEX5", (ftnlen)6);
    sgstxi_(&c__1);
/* -- 文字の大きさ ---- */
    sgstxs_(&c_b22);
    sgtxv_(&c_b9, &y[3], "SMALL", (ftnlen)5);
    sgstxs_(&c_b25);
    sgtxv_(&c_b9, &y[4], "LARGE", (ftnlen)5);
    sgstxs_(&c_b10);
/* -- 文字列のセンタリングオプション ---- */
    sgstxc_(&c_n1);
    sgtxv_(&c_b9, &y[5], "LEFT", (ftnlen)4);
    sgstxc_(&c__1);
    sgtxv_(&c_b9, &y[6], "RIGHT", (ftnlen)5);
    sgstxc_(&c__0);
/* -- 文字列の回転角 ---- */
    sgstxr_(&c__10);
    sgtxv_(&c_b9, &y[7], "ROTATION+", (ftnlen)9);
    sgstxr_(&c_n10);
    sgtxv_(&c_b9, &y[8], "ROTATION-", (ftnlen)9);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int kihon4_ () { MAIN__ (); return 0; }
