/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__300 = 300;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b10 = 0.f;
static real c_b11 = 256.f;
static real c_b14 = .1f;
static real c_b15 = .9f;
static integer c__2 = 2;
static integer c__0 = 0;
static real c_b24 = 16.f;
static real c_b25 = 64.f;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[2], b[2];
    static integer i__, j, iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    extern integer isgrgb_(integer *, integer *, integer *);
    extern /* Subroutine */ int grswnd_(real *, real *, real *, real *), 
	    grstrf_(void), swiset_(char *, integer *, ftnlen), grstrn_(
	    integer *), uxaxdv_(char *, real *, real *, ftnlen), sgpwsn_(void)
	    , sgplxu_(integer *, real *, real *, integer *, integer *, 
	    integer *), uyaxdv_(char *, real *, real *, ftnlen), grsvpt_(real 
	    *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    swiset_("WINDOW_HEIGHT", &c__300, (ftnlen)13);
    swiset_("WINDOW_WIDTH", &c__300, (ftnlen)12);
    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I) ? ;", (ftnlen)23);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b10, &c_b11, &c_b10, &c_b11);
    grsvpt_(&c_b14, &c_b15, &c_b14, &c_b15);
    grstrn_(&c__1);
    grstrf_();
    for (i__ = 1; i__ <= 255; ++i__) {
	a[0] = i__ * 1.f;
	a[1] = i__ * 1.f;
	for (j = 1; j <= 255; ++j) {
	    b[0] = (j - 1) * 1.f;
	    b[1] = j * 1.f;
	    i__1 = isgrgb_(&i__, &j, &c__0);
	    sgplxu_(&c__2, a, b, &c__1, &c__1, &i__1);
	}
    }
    uxaxdv_("T", &c_b24, &c_b25, (ftnlen)1);
    uxaxdv_("B", &c_b24, &c_b25, (ftnlen)1);
    uyaxdv_("L", &c_b24, &c_b25, (ftnlen)1);
    uyaxdv_("R", &c_b24, &c_b25, (ftnlen)1);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uipk01_ () { MAIN__ (); return 0; }
