/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b8 = .05f;
static real c_b9 = .95f;
static integer c__2 = 2;
static integer c__4 = 4;
static real c_b16 = .03f;
static real c_b18 = .07f;
static integer c__5 = 5;
static integer c_n1 = -1;
static integer c__0 = 0;
static integer c__20 = 20;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real y[9], x1, x2, xc;
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sglnv_(real *, real *, real *, real *), sgtxv_(real *, real *, 
	    char *, ftnlen), sgslni_(integer *), sgstxc_(integer *), sgpwsn_(
	    void), sgstxi_(integer *), sgstxr_(integer *), sgstxs_(real *);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 6, 0, 0, 0 };
    static cilist io___2 = { 0, 5, 0, 0, 0 };


    s_wsle(&io___1);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___2);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    x1 = .1f;
    x2 = .9f;
    xc = .5f;
    sgslni_(&c__1);
    for (i__ = 1; i__ <= 9; ++i__) {
	y[i__ - 1] = (10 - i__) * .1f;
	sglnv_(&x1, &y[i__ - 1], &x2, &y[i__ - 1]);
/* L10: */
    }
    sglnv_(&xc, &c_b8, &xc, &c_b9);
    sgtxv_(&xc, y, "SGTXV TEST", (ftnlen)10);
/* <-- テキスト 1 */
    sgstxi_(&c__2);
/* <-- テキスト INDEX */
    sgtxv_(&xc, &y[1], "INDEX2", (ftnlen)6);
/* <-- テキスト 2 */
    sgstxi_(&c__3);
/* <-- テキスト INDEX */
    sgtxv_(&xc, &y[2], "INDEX3", (ftnlen)6);
/* <-- テキスト 3 */
    sgstxi_(&c__4);
    sgstxs_(&c_b16);
/* <-- テキスト SIZE 設定 (小) */
    sgtxv_(&xc, &y[3], "SMALL", (ftnlen)5);
/* <-- テキスト 4 */
    sgstxs_(&c_b18);
/* <-- テキスト SIZE 設定 (大) */
    sgtxv_(&xc, &y[4], "LARGE", (ftnlen)5);
/* <-- テキスト 5 */
    sgstxs_(&c_b8);
    sgstxi_(&c__5);
    sgstxc_(&c_n1);
/* <-- */
    sgtxv_(&xc, &y[5], "LEFT", (ftnlen)4);
/* <-- テキスト 6 */
    sgstxc_(&c__0);
/* <-- */
    sgtxv_(&xc, &y[6], "CENTER", (ftnlen)6);
/* <-- テキスト 7 */
    sgstxc_(&c__1);
/* <-- */
    sgtxv_(&xc, &y[7], "RIGHT", (ftnlen)5);
/* <-- テキスト 8 */
    sgstxc_(&c__0);
    sgstxi_(&c__4);
    sgstxr_(&c__20);
/* <-- */
    sgtxv_(&xc, &y[8], "ROTATION", (ftnlen)8);
/* <-- テキスト 9 */
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int sgpk05_ () { MAIN__ (); return 0; }
