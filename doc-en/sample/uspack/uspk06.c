/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__200 = 200;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Initialized data */

    static doublereal x = 0.;
    static doublereal y = 1.;
    static doublereal z__ = 1.;
    static doublereal s = 10.;
    static doublereal r__ = 26.;
    static doublereal b = 2.6;
    static doublereal dt = .01;

    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a[200];
    static integer i__, j;
    static real t[200];
    static doublereal dx, dy, dz;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     usgrph_(integer *, real *, real *), sgpwsn_(void), ussttl_(char *
	    , char *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___15 = { 0, 6, 0, 0, 0 };
    static cilist io___16 = { 0, 5, 0, 0, 0 };


/* ----------------------------------------------------------------------- */
    for (i__ = 1; i__ <= 200; ++i__) {
	for (j = 1; j <= 8; ++j) {
	    dx = -s * x + s * y;
	    dy = -x * z__ + r__ * x - y;
	    dz = x * y - b * z__;
	    x += dx * dt;
	    y += dy * dt;
	    z__ += dz * dt;
/* L20: */
	}
	t[i__ - 1] = (real) ((i__ - 1) * 1000);
	a[i__ - 1] = y + 20.f;
/* L10: */
    }
/* ----------------------------------------------------------------------- */
    s_wsle(&io___15);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___16);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    ussttl_("TIME", "SEC", "HEAT FLUX", "W/m|2\"", (ftnlen)4, (ftnlen)3, (
	    ftnlen)9, (ftnlen)6);
    usgrph_(&c__200, t, a);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk06_ () { MAIN__ (); return 0; }
