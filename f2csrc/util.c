/* Copyright (C) by GFD-Dennou Club, 1999-2000.  All rights reserved. */
#include "sys/types.h"
#include "sys/stat.h"
#include "signal.h"
#include "libtinyf2c.h"
#undef abs
#undef min
#undef max
#include "math.h"
#include "stdio.h"
#include "stdlib.h"

extern void f_exit(void);

/* void getarg_(ftnint *n, register char *s, ftnlen ls); */
/* ftnint iargc_(void); */
void exit_(integer *rc);
int s_stop(char *s, ftnlen n);
int s_stop(char *s, ftnlen n);
long f__inode(char *a, int *dev);

/* exit the program with code of (*rc)
*/
void exit_(integer *rc)
{
	exit(*rc);
}

/*
 * subroutine getarg(k, c)
 * returns the kth unix command argument in fortran character
 * variable argument c
*/
/* 
void getarg_(ftnint *n, register char *s, ftnlen ls)
{
	extern int xargc;
	extern char **xargv;
	register char *t;
	register int i;

	if(*n>=0 && *n<xargc)
		t = xargv[*n];
	else
		t = "";

	for(i = 0; i<ls && *t!='\0' ; ++i)
		*s++ = *t++;
	for( ; i<ls ; ++i)
		*s++ = ' ';
}
*/

/* return the argument count except the program name.
 */
/*
ftnint iargc_(void)
{
	extern int xargc;
	return ( xargc - 1 );
}
 */


#ifndef SIGIOT
#ifdef SIGABRT
#define SIGIOT SIGABRT
#endif
#endif
/* stop the excution of the program up to the statement of s
 */
int s_stop(char *s, ftnlen n)
{
	int i;

	if(n > 0)
	{
		fprintf(stderr, "STOP ");
		for(i = 0; i<n ; ++i)
			putc(*s++, stderr);
		fprintf(stderr, " statement executed\n");
	}
	exit(0);

	return 0; /* NOT REACHED */
}

/* abort or exit the excution of the program by some kind of error.
 */
void sig_die(register char *s, int kill)
{
	/* print error message, then clear buffers */
	fprintf(stderr, "%s\n", s);

	if(kill){
		fflush(stderr);
		f_exit();
		fflush(stderr);
		/* now get a core */
#ifdef SIGIOT
		signal(SIGIOT, SIG_DFL);
#endif
		abort();
	}
	else {
		exit(1);
	}
}

/* retrieve the device info of the file with name of a.
*/
long f__inode(char *a, int *dev)
{
	struct stat x;
	
	if(stat(a,&x)<0)
		return(-1);
	*dev = x.st_dev;
	
	return(x.st_ino);
}
