/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     HEXDIC */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int hexdic_(integer *ip, char *cp, ftnlen cp_len)
{
    /* Initialized data */

    static char chex[1*16] = "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "A" 
	    "B" "C" "D" "E" "F";
    static logical lfst = TRUE_;

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, nh, idx, ipx, nhex;
    extern /* Subroutine */ int gbyte_(integer *, integer *, integer *, 
	    integer *);
    static integer nbits, iskip;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);

    if (lfst) {
	gliget_("NBITSPW", &nbits, (ftnlen)7);
	nhex = nbits / 4;
	lfst = FALSE_;
    }
    nh = i_len(cp, cp_len);
    s_copy(cp, " ", nh, (ftnlen)1);
    i__1 = min(nhex,nh);
    for (i__ = 1; i__ <= i__1; ++i__) {
	iskip = nbits - (i__ << 2);
	idx = nh - i__ + 1;
	gbyte_(ip, &ipx, &iskip, &c__4);
	*(unsigned char *)&cp[idx - 1] = *(unsigned char *)&chex[ipx];
/* L10: */
    }
    return 0;
} /* hexdic_ */

