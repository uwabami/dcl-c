/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     HEXDCI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int hexdci_(char *cp, integer *ip, ftnlen cp_len)
{
    /* Initialized data */

    static char chex[1*16] = "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "A" 
	    "B" "C" "D" "E" "F";
    static logical lfst = TRUE_;

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen);

    /* Local variables */
    static integer i__, j, nh, idx, ipx, nhex;
    static logical lchar;
    static integer nbits, iskip;
    extern /* Subroutine */ int sbyte_(integer *, integer *, integer *, 
	    integer *), gliget_(char *, integer *, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    if (lfst) {
	gliget_("NBITSPW", &nbits, (ftnlen)7);
	nhex = nbits / 4;
	lfst = FALSE_;
    }
    nh = i_len(cp, cp_len);
    *ip = 0;
    i__1 = min(nhex,nh);
    for (i__ = 1; i__ <= i__1; ++i__) {
	iskip = nbits - (i__ << 2);
	idx = nh - i__ + 1;
	lchar = FALSE_;
	for (j = 0; j <= 15; ++j) {
	    if (lchreq_(cp + (idx - 1), chex + j, (ftnlen)1, (ftnlen)1)) {
		ipx = j;
		lchar = TRUE_;
		goto L20;
	    }
/* L15: */
	}
L20:
	if (! lchar) {
	    msgdmp_("E", "HEXDCI", "INVALID HEXADECIMAL CHARACTER.", (ftnlen)
		    1, (ftnlen)6, (ftnlen)30);
	}
	sbyte_(ip, &ipx, &iskip, &c__4);
/* L10: */
    }
    return 0;
} /* hexdci_ */

