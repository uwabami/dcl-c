/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CMON */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Character */ VOID cmon_(char *ret_val, ftnlen ret_val_len, integer *im)
{
    /* Initialized data */

    static char mon[9*12] = "JANUARY  " "FEBRUARY " "MARCH    " "APRIL    " 
	    "MAY      " "JUNE     " "JULY     " "AUGUST   " "SEPTEMBER" "OCT"
	    "OBER  " "NOVEMBER " "DECEMBER ";

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    if (! (1 <= *im && *im <= 12)) {
	msgdmp_("E", "CMON  ", "IM IS OUT OF RANGE (1-12).", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    s_copy(ret_val, mon + (*im - 1) * 9, ret_val_len, (ftnlen)9);
} /* cmon_ */

