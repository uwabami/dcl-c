/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DATEG2 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int dateg2_(integer *n, integer *iy, integer *itd, integer *
	ny, integer *ntd)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer j, jy;
    extern integer ndyear_(integer *);

    *n = 0;
    jy = *ny - *iy;
    if (jy > 0) {
	i__1 = jy;
	for (j = 1; j <= i__1; ++j) {
	    i__2 = *iy + j - 1;
	    *n += ndyear_(&i__2);
/* L10: */
	}
	*n = *n + *ntd - *itd;
    } else if (jy < 0) {
	i__1 = -jy;
	for (j = 1; j <= i__1; ++j) {
	    i__2 = *iy - j;
	    *n -= ndyear_(&i__2);
/* L15: */
	}
	*n = *n - *itd + *ntd;
    } else {
	*n = *ntd - *itd;
    }
    return 0;
} /* dateg2_ */

