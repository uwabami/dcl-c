/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     CWEEK */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Character */ VOID cweek_(char *ret_val, ftnlen ret_val_len, integer *iw)
{
    /* Initialized data */

    static char week[9*7] = "SUNDAY   " "MONDAY   " "TUESDAY  " "WEDNESDAY" 
	    "THURSDAY " "FRIDAY   " "SATURDAY ";

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    if (! (1 <= *iw && *iw <= 7)) {
	msgdmp_("E", "CWEEK ", "IW IS OUT OF RANGE (1-7).", (ftnlen)1, (
		ftnlen)6, (ftnlen)25);
    }
    s_copy(ret_val, week + (*iw - 1) * 9, ret_val_len, (ftnlen)9);
} /* cweek_ */

