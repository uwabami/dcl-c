/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TIME12 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int time12_(integer *itime, integer *itt)
{
    static integer ih, im, is;
    extern /* Subroutine */ int time13_(integer *, integer *, integer *, 
	    integer *), time32_(integer *, integer *, integer *, integer *);

/*     ITIME : TIME (IH*10000+IM*100+IS)                         (I/ ) */
/*     ITT   : TOTAL TIME                                        ( /O) */
    time13_(itime, &ih, &im, &is);
    time32_(&ih, &im, &is, itt);
    return 0;
} /* time12_ */

