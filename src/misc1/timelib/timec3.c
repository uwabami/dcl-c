/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;

/* ----------------------------------------------------------------------- */
/*     TIMEC3 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int timec3_(char *cform, integer *ih, integer *im, integer *
	is, ftnlen cform_len)
{
    /* Initialized data */

    static char cd[1*3] = "H" "M" "S";

    /* System generated locals */
    integer i__1;
    icilist ici__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    , pow_ii(integer *, integer *);

    /* Local variables */
    static integer i__, jd[3], mc, ncm, idx1, idx2;
    extern integer lenc_(char *, ftnlen);
    static char cfmt[6];
    extern integer indxcf_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen), indxcl_(char *, integer *, integer *, char *, ftnlen, 
	    ftnlen);

    /* Fortran I/O blocks */
    static icilist io___9 = { 0, cfmt, 0, "(A2,I1,A1,I1,A1)", 6, 1 };


    mc = lenc_(cform, cform_len);
    jd[0] = *ih;
    jd[1] = *im;
    jd[2] = *is;
    for (i__ = 1; i__ <= 3; ++i__) {
	idx1 = indxcf_(cform, &mc, &c__1, cd + (i__ - 1), cform_len, (ftnlen)
		1);
	idx2 = indxcl_(cform, &mc, &c__1, cd + (i__ - 1), cform_len, (ftnlen)
		1);
	if (idx1 > 0) {
	    ncm = idx2 - idx1 + 1;
	    s_wsfi(&io___9);
	    do_fio(&c__1, "(I", (ftnlen)2);
	    do_fio(&c__1, (char *)&ncm, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ".", (ftnlen)1);
	    do_fio(&c__1, (char *)&ncm, (ftnlen)sizeof(integer));
	    do_fio(&c__1, ")", (ftnlen)1);
	    e_wsfi();
	    ici__1.icierr = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = idx2 - (idx1 - 1);
	    ici__1.iciunit = cform + (idx1 - 1);
	    ici__1.icifmt = cfmt;
	    s_wsfi(&ici__1);
	    i__1 = jd[i__ - 1] % pow_ii(&c__10, &ncm);
	    do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
	    e_wsfi();
	}
/* L10: */
    }
    return 0;
} /* timec3_ */

