/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     TIMEQ2 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int timeq2_(integer *itt)
{
    static integer ih, im, is;
    extern /* Subroutine */ int time32_(integer *, integer *, integer *, 
	    integer *), timeq3_(integer *, integer *, integer *);

/*     ITT   : TOTAL TIME                                        ( /O) */
    timeq3_(&ih, &im, &is);
    time32_(&ih, &im, &is, itt);
    return 0;
} /* timeq2_ */

