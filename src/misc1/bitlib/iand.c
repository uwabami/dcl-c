#include "../../../config.h"
/*
 *	iand
 */
#ifndef WINDOWS
DCL_INT iand_(DCL_INT *i, DCL_INT *j)
#else
DCL_INT IAND(DCL_INT *i, DCL_INT *j)
#endif
{
    return (*i & *j);
}
