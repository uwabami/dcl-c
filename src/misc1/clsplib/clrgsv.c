/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b4 = 360.f;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int clrgsv_(integer *r__, integer *g, integer *b, real *h__, 
	real *s, real *v, integer *n, integer *m)
{
    /* System generated locals */
    integer r_dim1, r_offset, g_dim1, g_offset, b_dim1, b_offset, h_dim1, 
	    h_offset, s_dim1, s_offset, v_dim1, v_offset, i__1, i__2;

    /* Builtin functions */
    double r_mod(real *, real *);

    /* Local variables */
    static integer i__, j, min__, max__;

    /* Parameter adjustments */
    v_dim1 = *n;
    v_offset = 1 + v_dim1;
    v -= v_offset;
    s_dim1 = *n;
    s_offset = 1 + s_dim1;
    s -= s_offset;
    h_dim1 = *n;
    h_offset = 1 + h_dim1;
    h__ -= h_offset;
    b_dim1 = *n;
    b_offset = 1 + b_dim1;
    b -= b_offset;
    g_dim1 = *n;
    g_offset = 1 + g_dim1;
    g -= g_offset;
    r_dim1 = *n;
    r_offset = 1 + r_dim1;
    r__ -= r_offset;

    /* Function Body */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = *m;
	for (j = 1; j <= i__2; ++j) {
	    if (r__[i__ + j * r_dim1] >= g[i__ + j * g_dim1] && r__[i__ + j * 
		    r_dim1] >= b[i__ + j * b_dim1]) {
		max__ = r__[i__ + j * r_dim1];
	    } else if (g[i__ + j * g_dim1] >= b[i__ + j * b_dim1]) {
		max__ = g[i__ + j * g_dim1];
	    } else {
		max__ = b[i__ + j * b_dim1];
	    }
	    if (r__[i__ + j * r_dim1] <= g[i__ + j * g_dim1] && r__[i__ + j * 
		    r_dim1] <= b[i__ + j * b_dim1]) {
		min__ = r__[i__ + j * r_dim1];
	    } else if (g[i__ + j * g_dim1] <= b[i__ + j * b_dim1]) {
		min__ = g[i__ + j * g_dim1];
	    } else {
		min__ = b[i__ + j * b_dim1];
	    }
	    if (max__ == r__[i__ + j * r_dim1]) {
		h__[i__ + j * h_dim1] = (g[i__ + j * g_dim1] - b[i__ + j * 
			b_dim1]) * 60.f / (max__ - min__);
	    } else if (max__ == g[i__ + j * g_dim1]) {
		h__[i__ + j * h_dim1] = (b[i__ + j * b_dim1] - r__[i__ + j * 
			r_dim1]) * 60.f / (max__ - min__) + 120.f;
	    } else {
		h__[i__ + j * h_dim1] = (r__[i__ + j * r_dim1] - g[i__ + j * 
			g_dim1]) * 60.f / (max__ - min__) + 240.f;
	    }
	    h__[i__ + j * h_dim1] = r_mod(&h__[i__ + j * h_dim1], &c_b4);
	    s[i__ + j * s_dim1] = (real) ((max__ - min__) / max__);
	    v[i__ + j * v_dim1] = (r__[i__ + j * r_dim1] + g[i__ + j * g_dim1]
		     + b[i__ + j * b_dim1]) / 3.f;
/* L20: */
	}
/* L10: */
    }
    return 0;
} /* clrgsv_ */

