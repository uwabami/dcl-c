/*
 *    rngu0 (written in C)
 *
 *    Copyright (C) 2000-2004 GFD Dennou Club. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include "../../../config.h"

#ifndef LONG_MAX
#define LONG_MAX 0x7FFFFFFF
#endif

#ifndef RAND_MAX
#define RAND_MAX LONG_MAX
#endif

#define TRUE   1             /* numeric value for true  */
#define FALSE  0             /* numeric value for false */

static int lfirst = TRUE;

DCL_REAL rngu0_(DCL_INT *iseed)
{
    if (*iseed != 0){
	*iseed = abs(*iseed) + 1;
	srand((unsigned int) *iseed);
	*iseed = 0;
	lfirst = FALSE;
	return((float)rand()/(float)RAND_MAX);
    }
    else {
	if (lfirst){
	    fprintf (stderr, "*** Error in rngu0 : ");
	    fprintf (stderr, "Iseed must be > 0 for 1st call.\n");
	    exit(1);
	}
	return((float)rand()/(float)RAND_MAX);
    }
}
