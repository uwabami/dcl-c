/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__31 = 31;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     PRESISION OF REAL VARIABLE                             (94/09/17) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double atan(doublereal), sin(doublereal);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static integer i__, i0, i1;
    static real s0, s1, pi, th, eps;
    static integer iou;
    static real emin, emax, delta;
    extern /* Subroutine */ int sbyte_(real *, integer *, integer *, integer *
	    ), gliget_(char *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___13 = { 0, 0, 0, "(A,1P,E10.3)", 0 };


    pi = atan(1.f) * 4;
    emin = 1.f;
    emax = 0.f;
    for (i__ = 1; i__ <= 179; ++i__) {
	th = pi / 180 * i__;
	s0 = sin(th);
	s1 = s0;
	i0 = 0;
	i1 = 1;
	sbyte_(&s0, &i0, &c__31, &c__1);
	sbyte_(&s1, &i1, &c__31, &c__1);
	delta = s1 - s0;
	eps = (r__1 = delta / s1, abs(r__1));
	emax = max(eps,emax);
	emin = min(eps,emin);
/* L100: */
    }
    gliget_("IOUNIT", &iou, (ftnlen)6);
/*     WRITE(IOU,*) ' MAX ERROR = ',EMAX */
/*     WRITE(IOU,*) ' MIN ERROR = ',EMIN */
    io___13.ciunit = iou;
    s_wsfe(&io___13);
    do_fio(&c__1, "< REPSL > IN GLPGET/GLPSET SHOULD BE ", (ftnlen)37);
    r__1 = emax * 10;
    do_fio(&c__1, (char *)&r__1, (ftnlen)sizeof(real));
    e_wsfe();
    s_stop("", (ftnlen)0);
    return 0;
} /* MAIN__ */

/* Main program alias */ int repsl_ () { MAIN__ (); return 0; }
