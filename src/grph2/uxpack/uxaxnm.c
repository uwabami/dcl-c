/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UXAXNM : PLOT X-AXIS (SPECIFY NUMBERS) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxaxnm_(char *cside, real *ux1, integer *n1, real *ux2, 
	integer *n2, ftnlen cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    char ch__1[7];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static logical label;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    extern /* Subroutine */ int uxroff_(void), uxsoff_(void), uzlget_(char *, 
	    logical *, ftnlen), uxpaxs_(char *, integer *, ftnlen), uxptmk_(
	    char *, integer *, real *, integer *, ftnlen), uxpnum_(char *, 
	    integer *, real *, integer *, ftnlen);

    /* Parameter adjustments */
    --ux2;
    --ux1;

    /* Function Body */
    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UXAXNM", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    uxsoff_();
    uxpaxs_(cside, &c__2, (ftnlen)1);
    if (*n1 >= 1) {
	uxptmk_(cside, &c__1, &ux1[1], n1, (ftnlen)1);
    }
    if (*n2 >= 1) {
	uxptmk_(cside, &c__2, &ux2[1], n2, (ftnlen)1);
/* Writing concatenation */
	i__1[0] = 6, a__1[0] = "LABELX";
	i__1[1] = 1, a__1[1] = cside;
	s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)7);
	uzlget_(ch__1, &label, (ftnlen)7);
	if (label) {
	    uxpnum_(cside, &c__1, &ux2[1], n2, (ftnlen)1);
	}
    }
    uxroff_();
    return 0;
} /* uxaxnm_ */

