/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     BASIC ROUTINES */
/* ----------------------------------------------------------------------- */
/*     UXPTMZ : PLOT TICKMARKS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxptmz_(real *ux, integer *n, real *upy, real *roffx, 
	real *rtick, integer *index)
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Local variables */
    static integer i__;
    static real vpx, vpy;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static logical lclipz;
    extern /* Subroutine */ int sglset_(char *, logical *, ftnlen), szlncl_(
	    void), stftrf_(real *, real *, real *, real *), szlnop_(integer *)
	    , szlnzv_(real *, real *, real *, real *);

    /* Parameter adjustments */
    --ux;

    /* Function Body */
    if (*n <= 0) {
	msgdmp_("E", "UXPTMZ", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*index <= 0) {
	msgdmp_("E", "UXPTMZ", "LINE INDEX IS INVALID.", (ftnlen)1, (ftnlen)6,
		 (ftnlen)22);
    }
    sglget_("LCLIP", &lclipz, (ftnlen)5);
    sglset_("LCLIP", &c_false, (ftnlen)5);
    szlnop_(index);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	stftrf_(&ux[i__], upy, &vpx, &vpy);
	vpy += *roffx;
	r__1 = vpy + *rtick;
	szlnzv_(&vpx, &vpy, &vpx, &r__1);
/* L10: */
    }
    szlncl_();
    sglset_("LCLIP", &lclipz, (ftnlen)5);
    return 0;
} /* uxptmz_ */

