/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UXAXDV : PLOT X-AXIS (SPECIFY DIVISION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxaxdv_(char *cside, real *dx1, real *dx2, ftnlen 
	cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    real r__1;
    char ch__1[7];

    /* Builtin functions */
    integer i_nint(real *);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    static real x;
    static integer nn;
    static real rx, ux[200], dz1, dz2, frc;
    extern integer irle_(real *);
    extern logical lrle_(real *, real *), lreq_(real *, real *);
    static real uxmn, uymn, uxmx, uymx;
    static logical label, lepsl;
    static real repsl, uxmnw, uxmxw;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), glrget_(
	    char *, real *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), gllset_(char *, logical *, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    extern /* Subroutine */ int sgqwnd_(real *, real *, real *, real *), 
	    uxroff_(void), uxsoff_(void), uzlget_(char *, logical *, ftnlen), 
	    uxpaxs_(char *, integer *, ftnlen), uxptmk_(char *, integer *, 
	    real *, integer *, ftnlen), uxpnum_(char *, integer *, real *, 
	    integer *, ftnlen);

/*     / CHECK ARGUMENTS / */
    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UXAXDV", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*dx1 <= 0.f) {
	msgdmp_("E", "UXAXDV", "MINOR DIVISION IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (*dx2 <= 0.f) {
	msgdmp_("E", "UXAXDV", "MAJOR DIVISION IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (*dx1 <= *dx2) {
	dz1 = *dx1;
	dz2 = *dx2;
    } else {
	msgdmp_("W", "UXAXDV", "MINOR DIVISION IS GREATER THAN MAJOR DIVISIO"
		"N.", (ftnlen)1, (ftnlen)6, (ftnlen)46);
	dz1 = *dx2;
	dz2 = *dx1;
	msgdmp_("M", "-CNT.-", "DX1 AND DX2 WERE SWITCHED.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    r__1 = dz2 / dz1;
    frc = dz2 / dz1 - i_nint(&r__1);
    if (abs(frc) > 1e-4f) {
	msgdmp_("W", "UXAXDV", "MAJOR DIVISION IS NOT MULTIPLE OF MINOR DIVI"
		"SION.", (ftnlen)1, (ftnlen)6, (ftnlen)49);
    }
/*     / GET & SET INTERNAL PARAMETERS / */
    gllget_("LEPSL", &lepsl, (ftnlen)5);
    glrget_("REPSL", &repsl, (ftnlen)5);
    gllset_("LEPSL", &c_true, (ftnlen)5);
/*     / SET SCALING FOR OFFSET / */
    uxsoff_();
/*     / INQUIRE NORMALIZATION TRANSFORMATION / */
    sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
    uxmnw = min(uxmn,uxmx);
    uxmxw = max(uxmn,uxmx);
/*     / PLOT HOLIZONTAL AXIS / */
    uxpaxs_(cside, &c__2, (ftnlen)1);
/*     / GENERATE NUMBERS FOR MINOR TICKMARKS / */
    nn = 0;
    r__1 = uxmnw / dz1;
    rx = irle_(&r__1) * dz1;
    if (lreq_(&uxmnw, &rx)) {
	x = rx;
    } else {
	x = rx + dz1;
    }
L11:
    if (! lrle_(&x, &uxmxw)) {
	goto L10;
    }
    ++nn;
    if (abs(x) < dz1 * repsl * nn) {
	x = 0.f;
    }
    ux[nn - 1] = x;
    x += dz1;
    goto L11;
L10:
    if (nn == 0) {
	msgdmp_("W", "UXAXDV", "THERE IS NO TICKMARK / LABEL.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	goto L100;
    } else if (nn > 200) {
	msgdmp_("E", "UXAXDV", "WORKING AREA IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
/*     / IF DZ1.NE.DZ2 THEN PLOT MINOR TICKMARKS */
/*       ELSE SKIP TO THE PROCESS FOR MAJOR TICKMARKS / */
    if (dz2 == dz1) {
	goto L20;
    }
    uxptmk_(cside, &c__1, ux, &nn, (ftnlen)1);
/*     / GENERATE NUMBERS FOR MAJOR TICKMARKS AND LABELS / */
    nn = 0;
    r__1 = uxmnw / dz2;
    rx = irle_(&r__1) * dz2;
    if (lreq_(&uxmnw, &rx)) {
	x = rx;
    } else {
	x = rx + dz2;
    }
L16:
    if (! lrle_(&x, &uxmxw)) {
	goto L15;
    }
    ++nn;
    if (abs(x) < dz2 * repsl * nn) {
	x = 0.f;
    }
    ux[nn - 1] = x;
    x += dz2;
    goto L16;
L15:
L20:
    uxptmk_(cside, &c__2, ux, &nn, (ftnlen)1);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "LABELX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)7);
    uzlget_(ch__1, &label, (ftnlen)7);
    if (label) {
	uxpnum_(cside, &c__1, ux, &nn, (ftnlen)1);
    }
/*     / RESET INTERNAL PARAMETER / */
L100:
    gllset_("LEPSL", &lepsl, (ftnlen)5);
/*     / RESET SCALING FOR OFFSET / */
    uxroff_();
    return 0;
} /* uxaxdv_ */

