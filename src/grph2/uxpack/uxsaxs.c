/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UXSAXS : OFFSET FOR NEW AXIS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uxsaxs_(char *cside, ftnlen cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    real r__1;
    char ch__1[6];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer i_sign(integer *, integer *);

    /* Local variables */
    static real pad;
    static integer jsgn, iflag, inner;
    static real rtick, roffx;
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luxchk_(char *, ftnlen);
    static real rsizec;
    extern /* Subroutine */ int uziget_(char *, integer *, ftnlen), uzrget_(
	    char *, real *, ftnlen);
    static real rsizet;
    extern /* Subroutine */ int uxsaxz_(char *, real *, ftnlen);

    if (! luxchk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UXSAXS", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFX";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &roffx, (ftnlen)6);
    uzrget_("RSIZET2", &rsizet, (ftnlen)7);
    uzrget_("RSIZEC2", &rsizec, (ftnlen)7);
    uzrget_("PAD2", &pad, (ftnlen)4);
    uziget_("INNER", &inner, (ftnlen)5);
    jsgn = i_sign(&c__1, &inner);
    if (! lchreq_(cside, "U", (ftnlen)1, (ftnlen)1)) {
	if (lchreq_(cside, "B", (ftnlen)1, (ftnlen)1)) {
	    iflag = -1;
	} else {
	    iflag = 1;
	}
    } else {
	uziget_("IFLAG", &iflag, (ftnlen)5);
	iflag = i_sign(&c__1, &iflag);
    }
    rtick = -rsizet * jsgn * iflag;
    if (iflag >= 0) {
/* Computing MAX */
	r__1 = roffx - rtick;
	roffx = max(r__1,roffx) + rsizec * pad;
    } else {
/* Computing MIN */
	r__1 = roffx - rtick;
	roffx = min(r__1,roffx) - rsizec * pad;
    }
    uxsaxz_(cside, &roffx, (ftnlen)1);
    return 0;
} /* uxsaxs_ */

