/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UILGET / UILSET / UILSTX */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uilget_0_(int n__, char *cp, logical *lpara, ftnlen 
	cp_len)
{
    static char cl[40], cx[8];
    static logical lp;
    static integer idx;
    extern /* Subroutine */ int uilqid_(char *, integer *, ftnlen), uilqcl_(
	    integer *, char *, ftnlen), rllget_(char *, logical *, integer *, 
	    ftnlen), uilqcp_(integer *, char *, ftnlen), rtlget_(char *, char 
	    *, logical *, integer *, ftnlen, ftnlen), uilqvl_(integer *, 
	    logical *), uilsvl_(integer *, logical *);

    switch(n__) {
	case 1: goto L_uilset;
	case 2: goto L_uilstx;
	}

    uilqid_(cp, &idx, cp_len);
    uilqvl_(&idx, lpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uilset:
    uilqid_(cp, &idx, cp_len);
    uilsvl_(&idx, lpara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uilstx:
    lp = *lpara;
    uilqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    uilqcp_(&idx, cx, (ftnlen)8);
    rtlget_("UE", cx, &lp, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    uilqcl_(&idx, cl, (ftnlen)40);
    rllget_(cl, &lp, &c__1, (ftnlen)40);
    uilsvl_(&idx, &lp);
    return 0;
} /* uilget_ */

/* Subroutine */ int uilget_(char *cp, logical *lpara, ftnlen cp_len)
{
    return uilget_0_(0, cp, lpara, cp_len);
    }

/* Subroutine */ int uilset_(char *cp, logical *lpara, ftnlen cp_len)
{
    return uilget_0_(1, cp, lpara, cp_len);
    }

/* Subroutine */ int uilstx_(char *cp, logical *lpara, ftnlen cp_len)
{
    return uilget_0_(2, cp, lpara, cp_len);
    }

