/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int uicrgb_0_(int n__, real *val, real *cmask, integer *irgb,
	 real *zmin, real *zmax, real *clrlvl, integer *icolor, integer *n, 
	char *cdsn, real *shdlvl, real *shade, ftnlen cdsn_len)
{
    /* Initialized data */

    static integer nlev = 7;
    static real rcmax = 999.f;
    static real rsmin = 999.f;
    static real rsmax = 999.f;
    static real sd[256] = { -.2f,-.2f,.2f,.2f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f };
    static real rs0[256] = { 0.f,.5f,.5f,1.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f };
    static integer nshade = 4;
    static logical lcset = FALSE_;
    static logical lsset = FALSE_;
    static integer ir[256] = { 255,0,0,0,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0 };
    static integer ig[256] = { 0,0,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0 };
    static integer ib[256] = { 255,255,255,0,0,0,255,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	    0,0,0,0,0,0 };
    static real rc0[256] = { 0.f,.1667f,.3333f,.5f,.6667f,.8333f,1.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,0.f,0.f,0.f,0.f };
    static real rcmin = 999.f;

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    real r__1, r__2;
    olist o__1;
    cllist cl__1;

    /* Builtin functions */
    double r_mod(real *, real *);
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer f_open(olist *), s_rsfe(cilist *), do_fio(integer *, char *, 
	    ftnlen), e_rsfe(void), f_clos(cllist *);

    /* Local variables */
    static integer i__;
    static real x0, x1, x2, cc;
    static integer jb, kb, jg, kg;
    static real rc[256], vc;
    static integer jr, kr, iu, nn;
    static real vm, rs[256];
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    static integer iclr;
    static real rmin, rmax;
    static integer iost;
    static real range;
    static char cdsnx[1024];
    static logical lshade;
    extern /* Subroutine */ int uifpac_(integer *, integer *, integer *, 
	    integer *), uiipac_(integer *, integer *, integer *, integer *);
    static logical lcycle;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uilget_(char *, logical *, ftnlen), uiqfnm_(char 
	    *, char *, ftnlen, ftnlen);
    extern integer iufopn_(void);
    static real xlevel;

    /* Fortran I/O blocks */
    static cilist io___36 = { 0, 0, 0, "(I3)", 0 };
    static cilist io___39 = { 1, 0, 1, "(G6.4, Z8)", 0 };
    static cilist io___44 = { 0, 0, 0, "(I3)", 0 };
    static cilist io___45 = { 1, 0, 1, "(F6.4,F8.3)", 0 };


    /* Parameter adjustments */
    if (clrlvl) {
	--clrlvl;
	}
    if (icolor) {
	--icolor;
	}
    if (shdlvl) {
	--shdlvl;
	}
    if (shade) {
	--shade;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_uicini;
	case 2: goto L_uiscrg;
	case 3: goto L_uismrg;
	case 4: goto L_uiqcrg;
	case 5: goto L_uiqmrg;
	case 6: goto L_uiscsq;
	case 7: goto L_uiscfl;
	case 8: goto L_uismsq;
	case 9: goto L_uismfl;
	}

    if (lcycle) {
	range = rc[nlev - 1] - rc[0];
	r__2 = *val - rc[0];
	r__1 = r_mod(&r__2, &range) + range;
	vc = r_mod(&r__1, &range) + rc[0];
    } else {
	vc = *val;
    }
    if (vc <= rc[0]) {
	jr = ir[0];
	jg = ig[0];
	jb = ib[0];
	goto L100;
    }
    i__1 = nlev;
    for (i__ = 2; i__ <= i__1; ++i__) {
	if (vc <= rc[i__ - 1]) {
	    x0 = rc[i__ - 1] - rc[i__ - 2];
	    x1 = rc[i__ - 1] - vc;
	    x2 = vc - rc[i__ - 2];
	    jr = (x2 * ir[i__ - 1] + x1 * ir[i__ - 2]) / x0;
	    jg = (x2 * ig[i__ - 1] + x1 * ig[i__ - 2]) / x0;
	    jb = (x2 * ib[i__ - 1] + x1 * ib[i__ - 2]) / x0;
	    goto L100;
	}
/* L10: */
    }
    jr = ir[nlev - 1];
    jg = ig[nlev - 1];
    jb = ib[nlev - 1];
L100:
    if (lshade) {
	range = rs[nshade - 1] - rs[0];
	r__2 = *val - rs[0];
	r__1 = r_mod(&r__2, &range) + range;
	vm = r_mod(&r__1, &range) + rs[0];
	i__1 = nshade;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    if (vm <= rs[i__ - 1]) {
		x0 = rs[i__ - 1] - rs[i__ - 2];
		x1 = rs[i__ - 1] - vm;
		x2 = vm - rs[i__ - 2];
		cc = (x2 * sd[i__ - 1] + x1 * sd[i__ - 2]) / x0;
		goto L120;
	    }
/* L110: */
	}
L120:
	;
    } else {
	cc = *cmask;
    }
    if (cc > 0.f) {
	kr = jr + (255 - jr) * cc;
	kg = jg + (255 - jg) * cc;
	kb = jb + (255 - jb) * cc;
    } else {
	kr = jr * (r__1 = cc + 1.f, abs(r__1));
	kg = jg * (r__1 = cc + 1.f, abs(r__1));
	kb = jb * (r__1 = cc + 1.f, abs(r__1));
    }
    uifpac_(&kr, &kg, &kb, irgb);
    return 0;
/* ----------------------------------------------------------------------- */

L_uicini:
    uilget_("LCYCLE", &lcycle, (ftnlen)6);
    uilget_("LMASK", &lshade, (ftnlen)5);
    if (! lcset) {
	rcmin = *zmin;
	rcmax = *zmax;
    }
    if (! lsset) {
	rsmin = rcmin;
	rsmax = rcmin + (rcmax - rcmin) / 10.f;
    }
    i__1 = nlev;
    for (i__ = 1; i__ <= i__1; ++i__) {
	rc[i__ - 1] = rc0[i__ - 1] * (rcmax - rcmin) + rcmin;
/* L200: */
    }
    i__1 = nshade;
    for (i__ = 1; i__ <= i__1; ++i__) {
	rs[i__ - 1] = rs0[i__ - 1] * (rsmax - rsmin) + rsmin;
/* L210: */
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uiscrg:
    if (*zmin >= *zmax) {
	msgdmp_("E", "UISCRG", "ZMIN > ZMAX.", (ftnlen)1, (ftnlen)6, (ftnlen)
		12);
    }
    rcmin = *zmin;
    rcmax = *zmax;
    lcset = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_uismrg:
    if (*zmin >= *zmax) {
	msgdmp_("E", "UISMRG", "ZMIN > ZMAX.", (ftnlen)1, (ftnlen)6, (ftnlen)
		12);
    }
    rsmin = *zmin;
    rsmax = *zmax;
    lsset = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiqcrg:
    *zmin = rcmin;
    *zmax = rcmax;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiqmrg:
    *zmin = rsmin;
    *zmax = rsmax;
    return 0;
/* ----------------------------------------------------------------------- */

L_uiscsq:
    if (*n > 256) {
	msgdmp_("M", "UISCSQ", "TOO MANY LEVEL (N).", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    nlev = min(*n,256);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	rc0[i__ - 1] = (clrlvl[i__] - clrlvl[1]) / (clrlvl[*n] - clrlvl[1]);
	uiipac_(&icolor[i__], &ir[i__ - 1], &ig[i__ - 1], &ib[i__ - 1]);
/* L20: */
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uiscfl:
/*     / INQUIRE OUTLINE FILE / */
    uiqfnm_(cdsn, cdsnx, cdsn_len, (ftnlen)1024);
    if (s_cmp(cdsnx, " ", (ftnlen)1024, (ftnlen)1) == 0) {
/* Writing concatenation */
	i__2[0] = 17, a__1[0] = "COLOR MAP FILE = ";
	i__2[1] = lenc_(cdsn, cdsn_len), a__1[1] = cdsn;
	s_cat(cmsg, a__1, i__2, &c__2, (ftnlen)80);
	msgdmp_("M", "UISCFL", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	msgdmp_("E", "UISCFL", "COLOR MAP FILE DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
/*     / OPEN OUTLINE FILE / */
    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cdsnx;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    io___36.ciunit = iu;
    s_rsfe(&io___36);
    do_fio(&c__1, (char *)&nn, (ftnlen)sizeof(integer));
    e_rsfe();
    if (nn > 256) {
	msgdmp_("M", "UISCFL", "TOO MANY LEVEL.", (ftnlen)1, (ftnlen)6, (
		ftnlen)15);
    }
    nlev = min(nn,256);
    i__1 = nlev;
    for (i__ = 1; i__ <= i__1; ++i__) {
	io___39.ciunit = iu;
	iost = s_rsfe(&io___39);
	if (iost != 0) {
	    goto L100001;
	}
	iost = do_fio(&c__1, (char *)&xlevel, (ftnlen)sizeof(real));
	if (iost != 0) {
	    goto L100001;
	}
	iost = do_fio(&c__1, (char *)&iclr, (ftnlen)sizeof(integer));
	if (iost != 0) {
	    goto L100001;
	}
	iost = e_rsfe();
L100001:
	if (iost != 0) {
	    msgdmp_("E", "UISCFL", "ERROR IN FILE.", (ftnlen)1, (ftnlen)6, (
		    ftnlen)14);
	}
	rc0[i__ - 1] = xlevel;
	uiipac_(&iclr, &ir[i__ - 1], &ig[i__ - 1], &ib[i__ - 1]);
/* L30: */
    }
    rmin = rc0[0];
    rmax = rc0[nlev - 1];
    i__1 = nlev;
    for (i__ = 1; i__ <= i__1; ++i__) {
	rc0[i__ - 1] = (rc0[i__ - 1] - rmin) / (rmax - rmin);
/* L40: */
    }
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_uismsq:
    if (*n > 256) {
	msgdmp_("M", "UISMSQ", "TOO MANY LEVEL (N).", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    nshade = min(*n,256);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	rs0[i__ - 1] = (shdlvl[i__] - shdlvl[1]) / (shdlvl[*n] - shdlvl[1]);
	sd[i__ - 1] = shade[i__];
/* L50: */
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uismfl:
/*     / INQUIRE OUTLINE FILE / */
    uiqfnm_(cdsn, cdsnx, cdsn_len, (ftnlen)1024);
    if (s_cmp(cdsnx, " ", (ftnlen)1024, (ftnlen)1) == 0) {
/* Writing concatenation */
	i__2[0] = 17, a__1[0] = "COLOR MAP FILE = ";
	i__2[1] = lenc_(cdsn, cdsn_len), a__1[1] = cdsn;
	s_cat(cmsg, a__1, i__2, &c__2, (ftnlen)80);
	msgdmp_("M", "UISMFL", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	msgdmp_("E", "UISMFL", "MASK FILE DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)25);
    }
/*     / OPEN OUTLINE FILE / */
    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cdsnx;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    io___44.ciunit = iu;
    s_rsfe(&io___44);
    do_fio(&c__1, (char *)&nn, (ftnlen)sizeof(integer));
    e_rsfe();
    if (nn > 256) {
	msgdmp_("M", "UISMFL", "TOO MANY LEVEL.", (ftnlen)1, (ftnlen)6, (
		ftnlen)15);
    }
    nshade = min(nn,256);
    i__1 = nshade;
    for (i__ = 1; i__ <= i__1; ++i__) {
	io___45.ciunit = iu;
	iost = s_rsfe(&io___45);
	if (iost != 0) {
	    goto L100002;
	}
	iost = do_fio(&c__1, (char *)&rs0[i__ - 1], (ftnlen)sizeof(real));
	if (iost != 0) {
	    goto L100002;
	}
	iost = do_fio(&c__1, (char *)&sd[i__ - 1], (ftnlen)sizeof(real));
	if (iost != 0) {
	    goto L100002;
	}
	iost = e_rsfe();
L100002:
	if (iost != 0) {
	    msgdmp_("E", "UISMFL", "ERROR IN FILE.", (ftnlen)1, (ftnlen)6, (
		    ftnlen)14);
	}
/* L60: */
    }
    rmin = rs0[0];
    rmax = rs0[nshade - 1];
    i__1 = nshade;
    for (i__ = 1; i__ <= i__1; ++i__) {
	rs0[i__ - 1] = (rs0[i__ - 1] - rmin) / (rmax - rmin);
/* L70: */
    }
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* uicrgb_ */

/* Subroutine */ int uicrgb_(real *val, real *cmask, integer *irgb)
{
    return uicrgb_0_(0, val, cmask, irgb, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (char *)0, (real *)0, (real *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uicini_(real *zmin, real *zmax)
{
    return uicrgb_0_(1, (real *)0, (real *)0, (integer *)0, zmin, zmax, (real 
	    *)0, (integer *)0, (integer *)0, (char *)0, (real *)0, (real *)0, 
	    (ftnint)0);
    }

/* Subroutine */ int uiscrg_(real *zmin, real *zmax)
{
    return uicrgb_0_(2, (real *)0, (real *)0, (integer *)0, zmin, zmax, (real 
	    *)0, (integer *)0, (integer *)0, (char *)0, (real *)0, (real *)0, 
	    (ftnint)0);
    }

/* Subroutine */ int uismrg_(real *zmin, real *zmax)
{
    return uicrgb_0_(3, (real *)0, (real *)0, (integer *)0, zmin, zmax, (real 
	    *)0, (integer *)0, (integer *)0, (char *)0, (real *)0, (real *)0, 
	    (ftnint)0);
    }

/* Subroutine */ int uiqcrg_(real *zmin, real *zmax)
{
    return uicrgb_0_(4, (real *)0, (real *)0, (integer *)0, zmin, zmax, (real 
	    *)0, (integer *)0, (integer *)0, (char *)0, (real *)0, (real *)0, 
	    (ftnint)0);
    }

/* Subroutine */ int uiqmrg_(real *zmin, real *zmax)
{
    return uicrgb_0_(5, (real *)0, (real *)0, (integer *)0, zmin, zmax, (real 
	    *)0, (integer *)0, (integer *)0, (char *)0, (real *)0, (real *)0, 
	    (ftnint)0);
    }

/* Subroutine */ int uiscsq_(real *clrlvl, integer *icolor, integer *n)
{
    return uicrgb_0_(6, (real *)0, (real *)0, (integer *)0, (real *)0, (real *
	    )0, clrlvl, icolor, n, (char *)0, (real *)0, (real *)0, (ftnint)0)
	    ;
    }

/* Subroutine */ int uiscfl_(char *cdsn, ftnlen cdsn_len)
{
    return uicrgb_0_(7, (real *)0, (real *)0, (integer *)0, (real *)0, (real *
	    )0, (real *)0, (integer *)0, (integer *)0, cdsn, (real *)0, (real 
	    *)0, cdsn_len);
    }

/* Subroutine */ int uismsq_(real *shdlvl, real *shade, integer *n)
{
    return uicrgb_0_(8, (real *)0, (real *)0, (integer *)0, (real *)0, (real *
	    )0, (real *)0, (integer *)0, n, (char *)0, shdlvl, shade, (ftnint)
	    0);
    }

/* Subroutine */ int uismfl_(char *cdsn, ftnlen cdsn_len)
{
    return uicrgb_0_(9, (real *)0, (real *)0, (integer *)0, (real *)0, (real *
	    )0, (real *)0, (integer *)0, (integer *)0, cdsn, (real *)0, (real 
	    *)0, cdsn_len);
    }

