/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__7 = 7;

/* ----------------------------------------------------------------------- */
/*     UILQNP / UILQID / UILQCP / UILQVL / UILSVL */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uilqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	logical *lpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*7] = "LCELLX  " "LCELLY  " "LCORNER " "LCYCLE  " 
	    "LMASK   " "LEMBOSS " "LSPHERE ";
    static logical lx[7] = { FALSE_,FALSE_,FALSE_,FALSE_,FALSE_,FALSE_,FALSE_ 
	    };
    static char cparal[40*7] = "CELL_MODE_X                             " 
	    "CELL_MODE_Y                             " "FILL_CORNER         "
	    "                    " "CYCLE_COLOR                             " 
	    "MASK_COLOR                              " "EMBOSS              "
	    "                    " "SPHERICAL_MAPPING                       ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rllget_(char *, logical *, integer *, ftnlen), 
	    rtlget_(char *, char *, logical *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uilqid;
	case 2: goto L_uilqcp;
	case 3: goto L_uilqcl;
	case 4: goto L_uilqvl;
	case 5: goto L_uilsvl;
	case 6: goto L_uilqin;
	}

/*     / LONG NAME / */
    *ncp = 7;
    return 0;
/* ----------------------------------------------------------------------- */

L_uilqid:
    for (n = 1; n <= 7; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UILQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uilqcp:
    if (1 <= *idx && *idx <= 7) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UILQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uilqcl:
    if (1 <= *idx && *idx <= 7) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UILQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uilqvl:
    if (lfirst) {
	rtlget_("UE", cparas, lx, &c__7, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__7, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 7) {
	*lpara = lx[*idx - 1];
    } else {
	msgdmp_("E", "UILQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uilsvl:
    if (lfirst) {
	rtlget_("UE", cparas, lx, &c__7, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__7, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 7) {
	lx[*idx - 1] = *lpara;
    } else {
	msgdmp_("E", "UILSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uilqin:
    for (n = 1; n <= 7; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uilqnp_ */

/* Subroutine */ int uilqnp_(integer *ncp)
{
    return uilqnp_0_(0, ncp, (char *)0, (integer *)0, (logical *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int uilqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uilqnp_0_(1, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uilqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uilqnp_0_(2, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uilqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uilqnp_0_(3, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uilqvl_(integer *idx, logical *lpara)
{
    return uilqnp_0_(4, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uilsvl_(integer *idx, logical *lpara)
{
    return uilqnp_0_(5, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uilqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uilqnp_0_(6, (integer *)0, cp, (integer *)0, (logical *)0, in, 
	    cp_len);
    }

