/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     CONTROL ROUTINES */
/* ----------------------------------------------------------------------- */
/*     UYSAXZ : OFFSET FOR AXIS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uysaxz_(char *cside, real *roffy, ftnlen cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    char ch__1[6];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    extern logical luychk_(char *, ftnlen);
    extern /* Subroutine */ int uzrset_(char *, real *, ftnlen);

    if (! luychk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UYSAXZ", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFY";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrset_(ch__1, roffy, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFGY";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrset_(ch__1, roffy, (ftnlen)6);
    return 0;
} /* uysaxz_ */

