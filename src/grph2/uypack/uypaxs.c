/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     LOWER LEVEL APPLICATIONS */
/* ----------------------------------------------------------------------- */
/*     UYPAXS : PLOT VERTICAL LINE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uypaxs_(char *cside, integer *islct, ftnlen cside_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    real r__1, r__2;
    char ch__1[6], ch__2[7];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static real vx1, vy1, vx2, vy2, uxmn, uymn, posx, uxmx, uymx;
    static integer index;
    static char cslct[1];
    static real roffy, rofgy;
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static logical lclipz;
    extern logical luychk_(char *, ftnlen);
    extern /* Subroutine */ int sglset_(char *, logical *, ftnlen), sgqwnd_(
	    real *, real *, real *, real *), uziget_(char *, integer *, 
	    ftnlen), stftrf_(real *, real *, real *, real *), uzrget_(char *, 
	    real *, ftnlen), sglnzv_(real *, real *, real *, real *, integer *
	    );

    /* Fortran I/O blocks */
    static icilist io___4 = { 0, cslct, 0, "(I1)", 1, 1 };


    if (! luychk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UYPAXS", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! (0 <= *islct && *islct <= 2)) {
	msgdmp_("E", "UYPAXS", "'ISLCT' IS INVALID", (ftnlen)1, (ftnlen)6, (
		ftnlen)18);
    }
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFFY";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &roffy, (ftnlen)6);
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ROFGY";
    i__1[1] = 1, a__1[1] = cside;
    s_cat(ch__1, a__1, i__1, &c__2, (ftnlen)6);
    uzrget_(ch__1, &rofgy, (ftnlen)6);
    if (roffy != rofgy) {
	return 0;
    }
    s_wsfi(&io___4);
    do_fio(&c__1, (char *)&(*islct), (ftnlen)sizeof(integer));
    e_wsfi();
    sglget_("LCLIP", &lclipz, (ftnlen)5);
    sglset_("LCLIP", &c_false, (ftnlen)5);
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "INDEXT";
    i__1[1] = 1, a__1[1] = cslct;
    s_cat(ch__2, a__1, i__1, &c__2, (ftnlen)7);
    uziget_(ch__2, &index, (ftnlen)7);
    sgqwnd_(&uxmn, &uxmx, &uymn, &uymx);
    if (! lchreq_(cside, "U", (ftnlen)1, (ftnlen)1)) {
	if (lchreq_(cside, "L", (ftnlen)1, (ftnlen)1)) {
	    posx = uxmn;
	} else {
	    posx = uxmx;
	}
    } else {
	uzrget_("UXUSER", &posx, (ftnlen)6);
    }
    stftrf_(&posx, &uymn, &vx1, &vy1);
    stftrf_(&posx, &uymx, &vx2, &vy2);
    r__1 = vx1 + rofgy;
    r__2 = vx2 + rofgy;
    sglnzv_(&r__1, &vy1, &r__2, &vy2, &index);
    sglset_("LCLIP", &lclipz, (ftnlen)5);
    return 0;
} /* uypaxs_ */

