/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

struct {
    logical lclip;
} szbtn3_;

#define szbtn3_1 szbtn3_

/* Table of constant values */

static real c_b21 = 0.f;
static real c_b24 = 1.f;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uvbraz_(integer *n, real *upx, real *upy1, real *upy2, 
	integer *itpat1, integer *itpat2, real *rsize)
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static real dx, rx0, ry0, rx1, ry1, rx2, ry2, vy1, vy2, rot, uxx, vxx, 
	    vyy;
    static logical lyc1, lyc2;
    static real uyy1, uyy2;
    static char cobj[80];
    static real uref;
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lmiss;
    static real rmiss;
    static logical lxuni;
    static real uxmin, uxmax;
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *), 
	    gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), stftrf_(real *, 
	    real *, real *, real *), swocls_(char *, ftnlen), uurget_(char *, 
	    real *, ftnlen), uuqidv_(real *, real *), swoopn_(char *, char *, 
	    ftnlen, ftnlen), szcltv_(void), szstni_(integer *), szoptv_(void),
	     szsttv_(real *, real *);

    /* Fortran I/O blocks */
    static icilist io___12 = { 0, cobj, 0, "(2I8,F8.5)", 80, 1 };


    /* Parameter adjustments */
    --upy2;
    --upy1;
    --upx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "UVBRAZ", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itpat1 == 0 || *itpat2 == 0) {
	msgdmp_("M", "UVBRAZ", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	return 0;
    }
    if (*itpat1 < 0 || *itpat2 < 0) {
	msgdmp_("E", "UVBRAZ", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    if (*rsize == 0.f) {
	msgdmp_("M", "UVBRAZ", "BAR SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "UVBRAZ", "BAR SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    sglget_("LCLIP", &szbtn3_1.lclip, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    stfpr2_(&c_b21, &c_b21, &rx0, &ry0);
    stfpr2_(&c_b21, &c_b24, &rx1, &ry1);
    stfpr2_(&c_b24, &c_b21, &rx2, &ry2);
    rot = (rx2 - rx0) * (ry1 - ry0) - (ry2 - ry0) * (rx1 - rx0);
    szbtn2_1.irmode = 0;
    if (rot > 0.f) {
	szbtn2_1.irmodr = szbtn2_1.irmode;
    } else {
	szbtn2_1.irmodr = (szbtn2_1.irmode + 1) % 2;
    }
    s_wsfi(&io___12);
    do_fio(&c__1, (char *)&(*itpat1), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*itpat2), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rsize), (ftnlen)sizeof(real));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UVBRAZ", cobj, (ftnlen)6, (ftnlen)80);
    lxuni = upx[1] == rundef;
    lyc1 = upy1[1] == rundef;
    lyc2 = upy2[1] == rundef;
    if (lxuni) {
	uuqidv_(&uxmin, &uxmax);
	if (uxmin == rundef) {
	    sgrget_("UXMIN", &uxmin, (ftnlen)5);
	}
	if (uxmax == rundef) {
	    sgrget_("UXMAX", &uxmax, (ftnlen)5);
	}
	dx = (uxmax - uxmin) / (*n - 1);
    }
    if (lyc1 || lyc2) {
	uurget_("UREF", &uref, (ftnlen)4);
    }
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lxuni) {
	    uxx = uxmin + dx * (i__ - 1);
	} else {
	    uxx = upx[i__];
	}
	if (lyc1) {
	    uyy1 = uref;
	} else {
	    uyy1 = upy1[i__];
	}
	if (lyc2) {
	    uyy2 = uref;
	} else {
	    uyy2 = upy2[i__];
	}
	if (! ((uxx == rmiss || uyy1 == rmiss || uyy2 == rmiss) && lmiss)) {
	    stftrf_(&uxx, &uyy1, &vxx, &vy1);
	    stftrf_(&uxx, &uyy2, &vxx, &vy2);
	    if (uyy2 > uyy1) {
		szstni_(itpat1);
	    } else {
		szstni_(itpat2);
	    }
	    if (vy1 > vy2) {
		vyy = vy1;
		vy1 = vy2;
		vy2 = vyy;
	    }
	    szoptv_();
	    r__1 = vxx - *rsize / 2.f;
	    szsttv_(&r__1, &vy1);
	    r__1 = vxx + *rsize / 2.f;
	    szsttv_(&r__1, &vy1);
	    r__1 = vxx + *rsize / 2.f;
	    szsttv_(&r__1, &vy2);
	    r__1 = vxx - *rsize / 2.f;
	    szsttv_(&r__1, &vy2);
	    r__1 = vxx - *rsize / 2.f;
	    szsttv_(&r__1, &vy1);
	    szcltv_();
	}
/* L20: */
    }
    swocls_("UVBRAZ", (ftnlen)6);
    return 0;
} /* uvbraz_ */

