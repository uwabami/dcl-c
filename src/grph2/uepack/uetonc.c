/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real tl1[100], tl2[100];
    integer ipt[100], nt;
    logical lascnd;
} ueblk1_;

#define ueblk1_1 ueblk1_

/* Table of constant values */

static real c_b13 = 360.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uetonc_(real *z__, integer *mx, integer *nx, integer *ny)
{
    /* System generated locals */
    integer z_dim1, z_offset, i__1, i__2, i__3, i__4;

    /* Local variables */
    static integer i__, j, ix, iy;
    static real rx, ry, ux, vx, wx, wy, vy, uy, zz;
    static integer ix1, iy1, ix2, iy2, ix3, iy3, ix4, iy4;
    static real wx1, wy1, wx2, wy2, wx3, wy3, wx4, wy4;
    static integer itr;
    static real tux, tuy;
    static logical limc;
    extern real rmod_(real *, real *);
    static real tuxz, tuyz;
    static integer image[4000], istat, ixmin, iymin;
    static logical lmiss;
    static integer ixmax, iymax;
    static real rmiss, txmin, tymin, txmax, tymax;
    extern integer iuwgx_(real *), iuwgy_(real *);
    static real vxmin, vxmax, vymin, vymax;
    extern /* Subroutine */ int stfpr2_(real *, real *, real *, real *), 
	    stipr2_(real *, real *, real *, real *);
    static integer ibgcli, iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    static integer ihight;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), sgiget_(
	    char *, integer *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), stirad_(real 
	    *, real *, real *, real *), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static integer iwidth;
    extern /* Subroutine */ int uezchk_(real *, integer *, integer *, integer 
	    *, char *, integer *, ftnlen), swidat_(integer *, integer *);
    static logical lexten;
    extern integer iueton_(real *);
    extern /* Subroutine */ int swqimc_(logical *), uwdflt_(integer *, 
	    integer *), swicls_(void), swfint_(real *, real *, integer *, 
	    integer *), stitrf_(real *, real *, real *, real *), swiint_(
	    integer *, integer *, real *, real *), sgqtrn_(integer *), 
	    swiopn_(integer *, integer *, integer *, integer *, real *, real *
	    , real *, real *, real *, real *, real *, real *), stitrn_(real *,
	     real *, real *, real *), sgqvpt_(real *, real *, real *, real *),
	     stfwtr_(real *, real *, real *, real *), stiwtr_(real *, real *, 
	    real *, real *), sgqtxy_(real *, real *, real *, real *);

    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;

    /* Function Body */
    sgiget_("IBGCLI", &ibgcli, (ftnlen)6);
/*     / CHECK IMAGE CAPABILITY / */
    swqimc_(&limc);
    if (! limc) {
	msgdmp_("E", "UETONC", "NO IMAGE CAPABILITY.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
/*     / GET INTERNAL PARAMETERS / */
    glrget_("RUNDEF  ", &rundef, (ftnlen)8);
    gliget_("IUNDEF  ", &iundef, (ftnlen)8);
    gllget_("LMISS   ", &lmiss, (ftnlen)8);
    glrget_("RMISS   ", &rmiss, (ftnlen)8);
/*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET / */
    uwdflt_(nx, ny);
/*     / CHECK Z VALUES AND TONE LEVEL / */
    uezchk_(&z__[z_offset], mx, nx, ny, "UETONC", &istat, (ftnlen)6);
    if (istat != 0) {
	return 0;
    }
/*     / INITIALIZE / */
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    stfpr2_(&vxmin, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx1, &wy1);
    swfint_(&wx1, &wy1, &ix1, &iy1);
    stfpr2_(&vxmax, &vymin, &rx, &ry);
    stfwtr_(&rx, &ry, &wx2, &wy2);
    swfint_(&wx2, &wy2, &ix2, &iy2);
    stfpr2_(&vxmax, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx3, &wy3);
    swfint_(&wx3, &wy3, &ix3, &iy3);
    stfpr2_(&vxmin, &vymax, &rx, &ry);
    stfwtr_(&rx, &ry, &wx4, &wy4);
    swfint_(&wx4, &wy4, &ix4, &iy4);
/* Computing MIN */
    i__1 = min(ix1,ix2), i__1 = min(i__1,ix3);
    ixmin = min(i__1,ix4);
/* Computing MIN */
    i__1 = min(iy1,iy2), i__1 = min(i__1,iy3);
    iymin = min(i__1,iy4);
/* Computing MAX */
    i__1 = max(ix1,ix2), i__1 = max(i__1,ix3);
    ixmax = max(i__1,ix4);
/* Computing MAX */
    i__1 = max(iy1,iy2), i__1 = max(i__1,iy3);
    iymax = max(i__1,iy4);
    iwidth = ixmax - ixmin + 1;
    ihight = iymax - iymin + 1;
    swiopn_(&ixmin, &iymin, &iwidth, &ihight, &wx1, &wy1, &wx2, &wy2, &wx3, &
	    wy3, &wx4, &wy4);
/*     / LOOP FOR EACH PIXEL / */
    sgqtrn_(&itr);
    sgqtxy_(&txmin, &txmax, &tymin, &tymax);
    i__1 = ihight;
    for (j = 1; j <= i__1; ++j) {
	i__2 = iwidth;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    i__3 = i__ + ixmin - 1;
	    i__4 = j + iymin - 1;
	    swiint_(&i__3, &i__4, &wx, &wy);
	    stiwtr_(&wx, &wy, &rx, &ry);
	    stipr2_(&rx, &ry, &vx, &vy);
	    if (vx < vxmin || vx > vxmax || vy < vymin || vy > vymax) {
		ux = rundef;
	    } else {
		stitrf_(&vx, &vy, &ux, &uy);
	    }
	    if (ux == rundef) {
		image[i__ - 1] = 0;
	    } else {
		ix = iuwgx_(&ux);
		iy = iuwgy_(&uy);
		tux = rmod_(&ux, &c_b13) - 180.f;
		stitrn_(&vx, &vy, &tuxz, &tuyz);
		stirad_(&tuxz, &tuyz, &tux, &tuy);
/*          TUX=RMOD(UX,360.)-180. */
		if (txmin <= txmax) {
		    if (tux >= txmin && tux <= txmax) {
			lexten = FALSE_;
		    } else {
			lexten = TRUE_;
		    }
		} else {
		    if (tux >= txmin || tux <= txmax) {
			lexten = FALSE_;
		    } else {
			lexten = TRUE_;
		    }
		}
		if (tuy >= tymax || tuy <= tymin) {
		    lexten = TRUE_;
		}
		if (lexten && itr >= 5) {
		    image[i__ - 1] = 0;
		} else if (ix == iundef || iy == iundef) {
		    image[i__ - 1] = 0;
		} else {
		    zz = z__[ix + iy * z_dim1];
		    if (lmiss && zz == rmiss) {
			image[i__ - 1] = 0;
		    } else {
			if (ueblk1_1.tl1[0] <= zz && zz <= ueblk1_1.tl2[
				ueblk1_1.nt - 1]) {
			    image[i__ - 1] = iueton_(&zz) / 1000;
			    if (image[i__ - 1] == ibgcli) {
				image[i__ - 1] = 0;
			    }
			} else {
			    image[i__ - 1] = 0;
			}
		    }
		}
	    }
/* L20: */
	}
	swidat_(image, &iwidth);
/* L30: */
    }
    swicls_();
    return 0;
} /* uetonc_ */

