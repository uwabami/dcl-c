/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UEPGET / UEPSET / UEPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uepget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int ueiqid_(char *, integer *, ftnlen), uelqid_(
	    char *, integer *, ftnlen), rliget_(char *, integer *, integer *, 
	    ftnlen), uepqid_(char *, integer *, ftnlen), uepqcl_(integer *, 
	    char *, ftnlen), rllget_(char *, integer *, integer *, ftnlen), 
	    uerqid_(char *, integer *, ftnlen), uepqcp_(integer *, char *, 
	    ftnlen), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), rlrget_(char *, integer *, integer *, ftnlen), rtlget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), uepqit_(
	    integer *, integer *), ueisvl_(integer *, integer *), rtrget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), uelsvl_(
	    integer *, integer *), uepqvl_(integer *, integer *), uepsvl_(
	    integer *, integer *), uersvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_uepset;
	case 2: goto L_uepstx;
	}

    uepqid_(cp, &idx, cp_len);
    uepqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uepset:
    uepqid_(cp, &idx, cp_len);
    uepsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_uepstx:
    ip = *ipara;
    uepqid_(cp, &idx, cp_len);
    uepqit_(&idx, &it);
    uepqcp_(&idx, cx, (ftnlen)8);
    uepqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UE", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	ueiqid_(cp, &idx, cp_len);
	ueisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UE", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	uelqid_(cp, &idx, cp_len);
	uelsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UE", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	uerqid_(cp, &idx, cp_len);
	uersvl_(&idx, &ip);
    }
    return 0;
} /* uepget_ */

/* Subroutine */ int uepget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uepget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int uepset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uepget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int uepstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uepget_0_(2, cp, ipara, cp_len);
    }

