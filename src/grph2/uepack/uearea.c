/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__4 = 4;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uearea_0_(int n__, real *xp, real *yp, real *zp, real *
	vlvm, real *ax, real *ay, integer *np, integer *ni, integer *ng)
{
    /* System generated locals */
    integer i__1;
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static integer m, n, n1, n2;
    static real fc;
    static integer mm[2], nl, mp, nq, nx;
    static logical lbm[8]	/* was [4][2] */, lcm[8]	/* was [4][2] 
	    */;
    static real xcm[8]	/* was [4][2] */;
    static integer nnm[2];
    static real ycm[8]	/* was [4][2] */;
    static logical lpm[8]	/* was [4][2] */;
    static integer mmp, npt;
    static real rtx;
    static logical lcrnr;
    static real rtmin, zpmin, zpmax;
    static logical lstrp;
    extern /* Subroutine */ int vrset0_(real *, real *, integer *, integer *, 
	    integer *), glrget_(char *, real *, ftnlen);
    static real realmx;

/*     / FIND MIN. & MAX. VALUES AND CHECK STATUS OF EACH CORNER / */
    /* Parameter adjustments */
    if (xp) {
	--xp;
	}
    if (yp) {
	--yp;
	}
    if (zp) {
	--zp;
	}
    if (vlvm) {
	--vlvm;
	}
    if (ax) {
	--ax;
	}
    if (ay) {
	--ay;
	}
    if (np) {
	--np;
	}
    if (ni) {
	--ni;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_ueaint;
	}

    zpmax = -realmx;
    zpmin = realmx;
    for (n = 1; n <= 4; ++n) {
	if (zp[n] > zpmax) {
	    zpmax = zp[n];
	}
	if (zp[n] < zpmin) {
	    zpmin = zp[n];
	}
	lpm[n - 1] = zp[n] >= vlvm[1];
	lpm[n + 3] = zp[n] <= vlvm[2];
/* L20: */
    }
    for (m = 1; m <= 2; ++m) {
/*       / CALCULATE CROSSING POINTS / */
	nnm[m - 1] = 0;
	for (n = 1; n <= 4; ++n) {
	    n1 = n;
	    n2 = n % 4 + 1;
	    if (lpm[n1 + (m << 2) - 5] != lpm[n2 + (m << 2) - 5]) {
		lcm[n + (m << 2) - 5] = TRUE_;
		++nnm[m - 1];
		fc = (vlvm[m] - zp[n1]) / (zp[n2] - zp[n1]);
		xcm[n + (m << 2) - 5] = xp[n1] + (xp[n2] - xp[n1]) * fc;
		ycm[n + (m << 2) - 5] = yp[n1] + (yp[n2] - yp[n1]) * fc;
	    } else {
		lcm[n + (m << 2) - 5] = FALSE_;
	    }
/* L30: */
	}
/*       / CHECK STATUS OF EACH LINE / */
	for (n = 1; n <= 4; ++n) {
	    n1 = n;
	    n2 = (n + 2) % 4 + 1;
	    lbm[n + (m << 2) - 5] = ! (lcm[n1 + (m << 2) - 5] && lcm[n2 + (m 
		    << 2) - 5]);
/* L40: */
	}
/*       / TREAT THE CONDITION WITH 4 LINES / */
	if (nnm[m - 1] == 4) {
	    nx = 0;
	    rtmin = realmx;
	    for (n = 1; n <= 4; ++n) {
		n1 = n;
		n2 = n % 4 + 1;
/* Computing 2nd power */
		r__1 = xcm[n2 + (m << 2) - 5] - xcm[n1 + (m << 2) - 5];
/* Computing 2nd power */
		r__2 = ycm[n2 + (m << 2) - 5] - ycm[n1 + (m << 2) - 5];
		rtx = sqrt(r__1 * r__1 + r__2 * r__2);
		if (rtx < rtmin) {
		    rtmin = rtx;
		    nx = n;
		}
/* L50: */
	    }
	    nl = (nx - 1) % 2 + 1;
	    lbm[nl + (m << 2) - 5] = TRUE_;
	    lbm[nl + 2 + (m << 2) - 5] = TRUE_;
	}
/* L60: */
    }
/*     / SET CROSSING POINTS / */
    nq = 0;
    if (nnm[0] == 0 && nnm[1] == 0) {
	if (vlvm[1] <= zpmin && zpmax <= vlvm[2]) {
	    vrset0_(&xp[1], &ax[1], &c__4, &c__1, &c__1);
	    vrset0_(&yp[1], &ay[1], &c__4, &c__1, &c__1);
	    nq = 4;
	}
    } else {
	for (n = 1; n <= 4; ++n) {
	    if (lpm[n - 1] && lpm[n + 3]) {
		++nq;
		ax[nq] = xp[n];
		ay[nq] = yp[n];
	    }
	    mmp = 0;
	    if (lcm[n - 1] && lcm[n + 3]) {
		mmp = 2;
		if (lpm[n - 1]) {
		    mm[0] = 2;
		    mm[1] = 1;
		} else {
		    mm[0] = 1;
		    mm[1] = 2;
		}
	    } else if (lcm[n - 1]) {
		mmp = 1;
		mm[0] = 1;
	    } else if (lcm[n + 3]) {
		mmp = 1;
		mm[0] = 2;
	    }
	    i__1 = mmp;
	    for (mp = 1; mp <= i__1; ++mp) {
		++nq;
		ax[nq] = xcm[n + (mm[mp - 1] << 2) - 5];
		ay[nq] = ycm[n + (mm[mp - 1] << 2) - 5];
/* L70: */
	    }
/* L80: */
	}
    }
/*     / FIND THE NUMBER OF AREAS AND DO SOME TREATMENT / */
    *ng = 0;
    if (nnm[0] == 4 || nnm[1] == 4) {
	if (lbm[1] && lbm[3] && lbm[5] && lbm[7]) {
	    lcrnr = lpm[0] && lpm[4];
	    lstrp = lpm[0] != lpm[4] && (lcm[0] && lcm[4]);
	    if (lcrnr || lstrp) {
		if (lcrnr) {
		    npt = 3;
		} else if (lstrp) {
		    npt = 4;
		}
		*ng = 2;
		np[1] = nq - npt;
		np[2] = npt;
		ni[1] = 3;
		ni[2] = ni[1] + np[1];
		ax[nq + 1] = ax[1];
		ax[nq + 2] = ax[2];
		ay[nq + 1] = ay[1];
		ay[nq + 2] = ay[2];
	    }
	} else if (lbm[0] && lbm[2] && lbm[4] && lbm[6]) {
	    lcrnr = lpm[1] && lpm[5];
	    lstrp = lpm[1] != lpm[5] && (lcm[0] && lcm[4]);
	    if (lcrnr || lstrp) {
		if (lcrnr) {
		    npt = 3;
		} else {
		    npt = 4;
		}
		*ng = 2;
		np[1] = npt;
		np[2] = nq - npt;
		ni[1] = 1;
		ni[2] = ni[1] + np[1];
	    }
	}
    }
    if (*ng == 0 && nq != 0) {
	*ng = 1;
	np[1] = nq;
	ni[1] = 1;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ueaint:
    glrget_("REALMAX ", &realmx, (ftnlen)8);
    return 0;
} /* uearea_ */

/* Subroutine */ int uearea_(real *xp, real *yp, real *zp, real *vlvm, real *
	ax, real *ay, integer *np, integer *ni, integer *ng)
{
    return uearea_0_(0, xp, yp, zp, vlvm, ax, ay, np, ni, ng);
    }

/* Subroutine */ int ueaint_(void)
{
    return uearea_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (real *)0,
	     (real *)0, (integer *)0, (integer *)0, (integer *)0);
    }

