/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__100 = 100;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uestln_(real *tlevn, integer *ipatn, integer *nton)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer it, nt;
    static char cmsg[80];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), ueqntl_(integer *), uestlv_(real *, real *, 
	    integer *);

    /* Fortran I/O blocks */
    static icilist io___3 = { 0, cmsg+40, 0, "(I3)", 3, 1 };


/*     / CHECK NUMBER OF TONE / */
    /* Parameter adjustments */
    --ipatn;
    --tlevn;

    /* Function Body */
    ueqntl_(&nt);
    if (nt + *nton > 100) {
	s_copy(cmsg, "NUMBER OF TONE IS IN EXCESS OF MAXIMUM (###).", (ftnlen)
		80, (ftnlen)45);
	s_wsfi(&io___3);
	do_fio(&c__1, (char *)&c__100, (ftnlen)sizeof(integer));
	e_wsfi();
	msgdmp_("E", "UESTLN", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    i__1 = *nton;
    for (it = 1; it <= i__1; ++it) {
/*       / CHECK IPAT / */
	if (ipatn[it] < 0) {
	    s_copy(cmsg, "TONE PATTERN NUMBER IS LESS THAN ZERO.", (ftnlen)80,
		     (ftnlen)38);
	    msgdmp_("E", "UESTLN", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	}
	uestlv_(&tlevn[it], &tlevn[it + 1], &ipatn[it]);
/* L10: */
    }
    return 0;
} /* uestln_ */

