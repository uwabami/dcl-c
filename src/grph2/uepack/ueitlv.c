/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real tl1[100], tl2[100];
    integer ipt[100], nt;
    logical lascnd;
} ueblk1_;

#define ueblk1_1 ueblk1_

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;
static integer c__100 = 100;

/* ----------------------------------------------------------------------- */
/*     UEITLV / UESTLV */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ueitlv_0_(int n__, real *tlev1, real *tlev2, integer *
	ipat, integer *iton, integer *nton, logical *lset)
{
    /* Initialized data */

    static logical lsetz = FALSE_;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static char cmsg[80];
    extern logical lreq1_(real *, real *);
    extern /* Subroutine */ int iset0_(integer *, integer *, integer *, 
	    integer *);
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen);
    static real realmx;

    /* Fortran I/O blocks */
    static icilist io___5 = { 0, cmsg+40, 0, "(I3)", 3, 1 };
    static icilist io___6 = { 0, cmsg+7, 0, "(I2)", 2, 1 };
    static icilist io___7 = { 0, cmsg+46, 0, "(I2)", 2, 1 };


    switch(n__) {
	case 1: goto L_uestlv;
	case 2: goto L_ueqtlv;
	case 3: goto L_ueqntl;
	case 4: goto L_uestlz;
	case 5: goto L_ueqtlz;
	}

    ueblk1_1.nt = 0;
    lsetz = FALSE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_uestlv:
/*     / INITIALIZATION / */
    if (! lsetz) {
	glrget_("RMISS   ", &rmiss, (ftnlen)8);
	glrget_("REALMAX ", &realmx, (ftnlen)8);
	iset0_(ueblk1_1.ipt, &ueblk1_1.nt, &c__1, &c__0);
	ueblk1_1.nt = 0;
	lsetz = TRUE_;
	ueblk1_1.lascnd = TRUE_;
    }
/*     / CHECK TONE LEVEL / */
    if (*tlev1 == rmiss || *tlev2 == rmiss) {
	if (*tlev1 == *tlev2) {
	    s_copy(cmsg, "TLEV1 AND TLEV2 ARE MISSING VALUES.", (ftnlen)80, (
		    ftnlen)35);
	    msgdmp_("E", "UESTLV", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	}
    } else {
	if (*tlev1 >= *tlev2) {
	    s_copy(cmsg, "TLEV1 IS GREATER THAN OR EQUAL TO TLEV2.", (ftnlen)
		    80, (ftnlen)40);
	    msgdmp_("E", "UESTLV", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	}
    }
/*     / CHECK IPAT / */
    if (*ipat < 0) {
	s_copy(cmsg, "TONE PATTERN NUMBER IS LESS THAN ZERO.", (ftnlen)80, (
		ftnlen)38);
	msgdmp_("E", "UESTLV", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
/*     / CHECK NUMBER OF TONE / */
    if (ueblk1_1.nt + 1 > 100) {
	s_copy(cmsg, "NUMBER OF TONE IS IN EXCESS OF MAXIMUM (###).", (ftnlen)
		80, (ftnlen)45);
	s_wsfi(&io___5);
	do_fio(&c__1, (char *)&c__100, (ftnlen)sizeof(integer));
	e_wsfi();
	msgdmp_("E", "UESTLV", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    ++ueblk1_1.nt;
    if (*tlev1 == rmiss) {
	ueblk1_1.tl1[ueblk1_1.nt - 1] = -realmx;
    } else {
	ueblk1_1.tl1[ueblk1_1.nt - 1] = *tlev1;
    }
    if (*tlev2 == rmiss) {
	ueblk1_1.tl2[ueblk1_1.nt - 1] = realmx;
    } else {
	ueblk1_1.tl2[ueblk1_1.nt - 1] = *tlev2;
    }
    ueblk1_1.ipt[ueblk1_1.nt - 1] = *ipat;
    if (ueblk1_1.nt != 1) {
	ueblk1_1.lascnd = ueblk1_1.lascnd && lreq1_(&ueblk1_1.tl1[ueblk1_1.nt 
		- 1], &ueblk1_1.tl2[ueblk1_1.nt - 2]);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ueqtlv:
    if (*iton < 1 || *iton > ueblk1_1.nt) {
	s_copy(cmsg, "LEVEL (##) OF TONE PATTERN IS OUT OF RANGE (1-##).", (
		ftnlen)80, (ftnlen)50);
	s_wsfi(&io___6);
	do_fio(&c__1, (char *)&(*iton), (ftnlen)sizeof(integer));
	e_wsfi();
	s_wsfi(&io___7);
	do_fio(&c__1, (char *)&ueblk1_1.nt, (ftnlen)sizeof(integer));
	e_wsfi();
	msgdmp_("E", "UEQTLV", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    if (ueblk1_1.tl1[*iton - 1] == -realmx) {
	*tlev1 = rmiss;
    } else {
	*tlev1 = ueblk1_1.tl1[*iton - 1];
    }
    if (ueblk1_1.tl2[*iton - 1] == realmx) {
	*tlev2 = rmiss;
    } else {
	*tlev2 = ueblk1_1.tl2[*iton - 1];
    }
    *ipat = ueblk1_1.ipt[*iton - 1];
    return 0;
/* ----------------------------------------------------------------------- */

L_ueqntl:
    *nton = ueblk1_1.nt;
    return 0;
/* ----------------------------------------------------------------------- */

L_uestlz:
    lsetz = *lset;
    return 0;
/* ----------------------------------------------------------------------- */

L_ueqtlz:
    *lset = lsetz;
    return 0;
} /* ueitlv_ */

/* Subroutine */ int ueitlv_(void)
{
    return ueitlv_0_(0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (logical *)0);
    }

/* Subroutine */ int uestlv_(real *tlev1, real *tlev2, integer *ipat)
{
    return ueitlv_0_(1, tlev1, tlev2, ipat, (integer *)0, (integer *)0, (
	    logical *)0);
    }

/* Subroutine */ int ueqtlv_(real *tlev1, real *tlev2, integer *ipat, integer 
	*iton)
{
    return ueitlv_0_(2, tlev1, tlev2, ipat, iton, (integer *)0, (logical *)0);
    }

/* Subroutine */ int ueqntl_(integer *nton)
{
    return ueitlv_0_(3, (real *)0, (real *)0, (integer *)0, (integer *)0, 
	    nton, (logical *)0);
    }

/* Subroutine */ int uestlz_(logical *lset)
{
    return ueitlv_0_(4, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, lset);
    }

/* Subroutine */ int ueqtlz_(logical *lset)
{
    return ueitlv_0_(5, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, lset);
    }

