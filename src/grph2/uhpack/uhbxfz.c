/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbls2_;

#define szbls2_1 szbls2_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uhbxfz_(integer *n, real *upx1, real *upx2, real *upy, 
	integer *itype, integer *index)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static real dy, ux1, uy1, uy2, ux2;
    static logical lxc1, lxc2;
    static char cobj[80];
    static real uref;
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lmiss;
    static real rmiss;
    static logical lyuni;
    static real uymin, uymax;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), swocls_(char *, 
	    ftnlen), uurget_(char *, real *, ftnlen), szcllu_(void), uuqidv_(
	    real *, real *), swoopn_(char *, char *, ftnlen, ftnlen), szslti_(
	    integer *, integer *), szpllu_(real *, real *), szoplu_(void), 
	    szmvlu_(real *, real *);

    /* Fortran I/O blocks */
    static icilist io___5 = { 0, cobj, 0, "(2I8)", 80, 1 };


    /* Parameter adjustments */
    --upy;
    --upx2;
    --upx1;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "UHBXFZ", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "UHBXFZ", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "UHBXFZ", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "UHBXFZ", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    sglget_("LCLIP", &szbls2_1.lclip, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    s_wsfi(&io___5);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UHBXFZ", cobj, (ftnlen)6, (ftnlen)80);
    szslti_(itype, index);
    lyuni = upy[1] == rundef;
    lxc1 = upx1[1] == rundef;
    lxc2 = upx2[1] == rundef;
    if (lyuni) {
	uuqidv_(&uymin, &uymax);
	if (uymin == rundef) {
	    sgrget_("UYMIN", &uymin, (ftnlen)5);
	}
	if (uymax == rundef) {
	    sgrget_("UYMAX", &uymax, (ftnlen)5);
	}
	dy = (uymax - uymin) / *n;
    }
    if (lxc1 || lxc2) {
	uurget_("UREF", &uref, (ftnlen)4);
    }
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lyuni) {
	    uy1 = uymin + dy * (i__ - 1);
	    uy2 = uymin + dy * i__;
	} else {
	    uy1 = upy[i__];
	    uy2 = upy[i__ + 1];
	}
	if (lxc1) {
	    ux1 = uref;
	} else {
	    ux1 = upx1[i__];
	}
	if (lxc2) {
	    ux2 = uref;
	} else {
	    ux2 = upx2[i__];
	}
	if (! ((ux1 == rmiss || ux1 == rmiss || uy1 == rmiss || uy2 == rmiss) 
		&& lmiss)) {
	    szoplu_();
	    szmvlu_(&ux1, &uy2);
	    szpllu_(&ux2, &uy2);
	    szpllu_(&ux2, &uy1);
	    szpllu_(&ux1, &uy1);
	    szpllu_(&ux1, &uy2);
	    szcllu_();
	}
/* L20: */
    }
    swocls_("UHBXFZ", (ftnlen)6);
    return 0;
} /* uhbxfz_ */

