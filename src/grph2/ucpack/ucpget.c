/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UCPGET / UCPSET / UCPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ucpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int uciqid_(char *, integer *, ftnlen), uclqid_(
	    char *, integer *, ftnlen), ucpqid_(char *, integer *, ftnlen), 
	    rliget_(char *, integer *, integer *, ftnlen), ucpqcl_(integer *, 
	    char *, ftnlen), rllget_(char *, integer *, integer *, ftnlen), 
	    ucrqid_(char *, integer *, ftnlen), ucpqcp_(integer *, char *, 
	    ftnlen), rtiget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), rlrget_(char *, integer *, integer *, ftnlen), rtlget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), ucpqit_(
	    integer *, integer *), ucisvl_(integer *, integer *), rtrget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), uclsvl_(
	    integer *, integer *), ucpqvl_(integer *, integer *), ucpsvl_(
	    integer *, integer *), ucrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_ucpset;
	case 2: goto L_ucpstx;
	}

    ucpqid_(cp, &idx, cp_len);
    ucpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpset:
    ucpqid_(cp, &idx, cp_len);
    ucpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_ucpstx:
    ip = *ipara;
    ucpqid_(cp, &idx, cp_len);
    ucpqit_(&idx, &it);
    ucpqcp_(&idx, cx, (ftnlen)8);
    ucpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UC", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	uciqid_(cp, &idx, cp_len);
	ucisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UC", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	uclqid_(cp, &idx, cp_len);
	uclsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UC", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	ucrqid_(cp, &idx, cp_len);
	ucrsvl_(&idx, &ip);
    }
    return 0;
} /* ucpget_ */

/* Subroutine */ int ucpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ucpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int ucpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ucpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int ucpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ucpget_0_(2, cp, ipara, cp_len);
    }

