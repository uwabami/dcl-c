/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     UCYACL : PLOT CALENDAR AXIS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ucyacl_(char *cside, integer *jd0, integer *nd, ftnlen 
	cside_len)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), ucyamn_(char *, integer *, integer *, ftnlen), 
	    ucyady_(char *, integer *, integer *, ftnlen);
    extern logical luychk_(char *, ftnlen);
    extern /* Subroutine */ int ucyayr_(char *, integer *, integer *, ftnlen),
	     uypaxs_(char *, integer *, ftnlen);

    if (! luychk_(cside, (ftnlen)1)) {
	msgdmp_("E", "UCYACL", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*jd0 < 0) {
	msgdmp_("E", "UCYACL", "FIRST DATE IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (*nd <= 0) {
	msgdmp_("E", "UCYACL", "DATE LENGTH IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    uypaxs_(cside, &c__2, (ftnlen)1);
    ucyady_(cside, jd0, nd, (ftnlen)1);
    ucyamn_(cside, jd0, nd, (ftnlen)1);
    ucyayr_(cside, jd0, nd, (ftnlen)1);
    return 0;
} /* ucyacl_ */

