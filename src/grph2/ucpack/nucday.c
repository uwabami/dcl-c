/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     NUCDAY */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer nucday_(char *cs, integer *nd, ftnlen cs_len)
{
    /* Initialized data */

    static integer ndx[5] = { 1,2,5,10,15 };

    /* System generated locals */
    integer ret_val;

    /* Local variables */
    static integer n;
    static real wd, vx1, vx2, vy1, vy2;
    static integer nday, maxl;
    static real dfact, rsize;
    static integer iundef;
    extern /* Subroutine */ int uciget_(char *, integer *, ftnlen), ucrget_(
	    char *, real *, ftnlen), uzrget_(char *, real *, ftnlen), sgqvpt_(
	    real *, real *, real *, real *);

    uciget_("IUNDEF", &iundef, (ftnlen)6);
    uciget_("NDAY  ", &nday, (ftnlen)6);
    if (nday != iundef) {
	ret_val = nday;
    } else {
	ucrget_("DFACT  ", &dfact, (ftnlen)7);
	uzrget_("RSIZEL1", &rsize, (ftnlen)7);
	sgqvpt_(&vx1, &vx2, &vy1, &vy2);
	if (*(unsigned char *)cs == 'X') {
	    wd = vx2 - vx1;
	} else if (*(unsigned char *)cs == 'Y') {
	    wd = vy2 - vy1;
	}
	maxl = wd / (rsize * 2 * dfact);
	for (n = 1; n <= 5; ++n) {
	    if (*nd / ndx[n - 1] <= maxl) {
		ret_val = ndx[n - 1];
		return ret_val;
	    }
/* L10: */
	}
	ret_val = 0;
    }
    return ret_val;
} /* nucday_ */

