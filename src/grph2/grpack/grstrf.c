/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int grstrf_(void)
{
    static integer itr;
    static real plx, ply, vxoff, vyoff, txmin, uxmin, vxmin, vymin, vxmax, 
	    vymax, uxmax, uymin, uymax, plrot, txmax, tymin, tymax, simfac;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), msgdmp_(char 
	    *, char *, char *, ftnlen, ftnlen, ftnlen), sgrget_(char *, real *
	    , ftnlen), sgstrf_(void);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    sgiget_("ITR", &itr, (ftnlen)3);
    sgrget_("VXMIN", &vxmin, (ftnlen)5);
    if (vxmin == rundef) {
	msgdmp_("E", "GRSTRF", "VXMIN IS NOT DEFINED.", (ftnlen)1, (ftnlen)6, 
		(ftnlen)21);
    }
    sgrget_("VXMAX", &vxmax, (ftnlen)5);
    if (vxmax == rundef) {
	msgdmp_("E", "GRSTRF", "VXMAX IS NOT DEFINED.", (ftnlen)1, (ftnlen)6, 
		(ftnlen)21);
    }
    sgrget_("VYMIN", &vymin, (ftnlen)5);
    if (vymin == rundef) {
	msgdmp_("E", "GRSTRF", "VYMIN IS NOT DEFINED.", (ftnlen)1, (ftnlen)6, 
		(ftnlen)21);
    }
    sgrget_("VYMAX", &vymax, (ftnlen)5);
    if (vymax == rundef) {
	msgdmp_("E", "GRSTRF", "VYMAX IS NOT DEFINED.", (ftnlen)1, (ftnlen)6, 
		(ftnlen)21);
    }
    if (1 <= itr && itr <= 4) {
	sgrget_("UXMIN", &uxmin, (ftnlen)5);
	if (uxmin == rundef) {
	    msgdmp_("E", "GRSTRF", "UXMIN IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("UXMAX", &uxmax, (ftnlen)5);
	if (uxmax == rundef) {
	    msgdmp_("E", "GRSTRF", "UXMAX IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("UYMIN", &uymin, (ftnlen)5);
	if (uymin == rundef) {
	    msgdmp_("E", "GRSTRF", "UYMIN IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("UYMAX", &uymax, (ftnlen)5);
	if (uymax == rundef) {
	    msgdmp_("E", "GRSTRF", "UYMAX IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
    } else if (5 <= itr && itr <= 7) {
	sgrget_("SIMFAC", &simfac, (ftnlen)6);
	if (simfac == rundef) {
	    msgdmp_("E", "GRSTRF", "SIMFAC IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	}
	sgrget_("VXOFF", &vxoff, (ftnlen)5);
	if (vxoff == rundef) {
	    msgdmp_("E", "GRSTRF", "VXOFF IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("VYOFF", &vyoff, (ftnlen)5);
	if (vyoff == rundef) {
	    msgdmp_("E", "GRSTRF", "VYOFF IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
    } else if (10 <= itr && itr <= 24 || 30 <= itr && itr <= 34) {
	sgrget_("SIMFAC", &simfac, (ftnlen)6);
	if (simfac == rundef) {
	    msgdmp_("E", "GRSTRF", "SIMFAC IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	}
	sgrget_("VXOFF", &vxoff, (ftnlen)5);
	if (vxoff == rundef) {
	    msgdmp_("E", "GRSTRF", "VXOFF IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("VYOFF", &vyoff, (ftnlen)5);
	if (vyoff == rundef) {
	    msgdmp_("E", "GRSTRF", "VYOFF IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("PLX", &plx, (ftnlen)3);
	if (plx == rundef) {
	    msgdmp_("E", "GRSTRF", "PLX IS NOT DEFINED.", (ftnlen)1, (ftnlen)
		    6, (ftnlen)19);
	}
	sgrget_("PLY", &ply, (ftnlen)3);
	if (ply == rundef) {
	    msgdmp_("E", "GRSTRF", "PLY IS NOT DEFINED.", (ftnlen)1, (ftnlen)
		    6, (ftnlen)19);
	}
	sgrget_("PLROT", &plrot, (ftnlen)5);
	if (plrot == rundef) {
	    msgdmp_("E", "GRSTRF", "PLROT IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("TXMIN", &txmin, (ftnlen)5);
	if (txmin == rundef) {
	    msgdmp_("E", "GRSTRF", "TXMIN IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("TXMAX", &txmax, (ftnlen)5);
	if (txmax == rundef) {
	    msgdmp_("E", "GRSTRF", "TXMAX IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("TYMIN", &tymin, (ftnlen)5);
	if (tymin == rundef) {
	    msgdmp_("E", "GRSTRF", "TYMIN IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
	sgrget_("TYMAX", &tymax, (ftnlen)5);
	if (tymax == rundef) {
	    msgdmp_("E", "GRSTRF", "TYMAX IS NOT DEFINED.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)21);
	}
    }
    sgstrf_();
    return 0;
} /* grstrf_ */

