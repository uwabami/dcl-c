/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int gropn_0_(int n__, integer *iws)
{
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), grinit_(void), 
	    sginit_(void), uminit_(void), usinit_(void), uuinit_(void), 
	    uwinit_(void), sgistx_(char *, integer *, ftnlen), uzinit_(void);

/*     / OPEN / */
    switch(n__) {
	case 1: goto L_grfrm;
	case 2: goto L_grfig;
	case 3: goto L_grcls;
	}

    sgistx_("IWS", iws, (ftnlen)3);
    sginit_();
    return 0;
/* ----------------------------------------------------------------------- */

L_grfrm:
/*     / NEW FRAME / */
    sgfrm_();
    grinit_();
    usinit_();
    uzinit_();
    uminit_();
    uuinit_();
    uwinit_();
    return 0;
/* ----------------------------------------------------------------------- */

L_grfig:
/*     / NEW FIGURE / */
    grinit_();
    usinit_();
    uzinit_();
    uminit_();
    uuinit_();
    uwinit_();
    return 0;
/* ----------------------------------------------------------------------- */

L_grcls:
/*     / CLOSE / */
    sgcls_();
    return 0;
} /* gropn_ */

/* Subroutine */ int gropn_(integer *iws)
{
    return gropn_0_(0, iws);
    }

/* Subroutine */ int grfrm_(void)
{
    return gropn_0_(1, (integer *)0);
    }

/* Subroutine */ int grfig_(void)
{
    return gropn_0_(2, (integer *)0);
    }

/* Subroutine */ int grcls_(void)
{
    return gropn_0_(3, (integer *)0);
    }

