/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UGPGET / UGPSET / UGPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int ugiqid_(char *, integer *, ftnlen), uglqid_(
	    char *, integer *, ftnlen), rliget_(char *, integer *, integer *, 
	    ftnlen), ugpqid_(char *, integer *, ftnlen), rllget_(char *, 
	    integer *, integer *, ftnlen), ugpqcl_(integer *, char *, ftnlen),
	     ugrqid_(char *, integer *, ftnlen), rtiget_(char *, char *, 
	    integer *, integer *, ftnlen, ftnlen), ugpqcp_(integer *, char *, 
	    ftnlen), rlrget_(char *, integer *, integer *, ftnlen), rtlget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), rtrget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), ugpqit_(
	    integer *, integer *), ugisvl_(integer *, integer *), uglsvl_(
	    integer *, integer *), ugpqvl_(integer *, integer *), ugpsvl_(
	    integer *, integer *), ugrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_ugpset;
	case 2: goto L_ugpstx;
	}

    ugpqid_(cp, &idx, cp_len);
    ugpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpset:
    ugpqid_(cp, &idx, cp_len);
    ugpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugpstx:
    ip = *ipara;
    ugpqid_(cp, &idx, cp_len);
    ugpqit_(&idx, &it);
    ugpqcp_(&idx, cx, (ftnlen)8);
    ugpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("UG", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	ugiqid_(cp, &idx, cp_len);
	ugisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("UG", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	uglqid_(cp, &idx, cp_len);
	uglsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("UG", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	ugrqid_(cp, &idx, cp_len);
	ugrsvl_(&idx, &ip);
    }
    return 0;
} /* ugpget_ */

/* Subroutine */ int ugpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ugpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int ugpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ugpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int ugpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return ugpget_0_(2, cp, ipara, cp_len);
    }

