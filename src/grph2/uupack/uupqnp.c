/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UUPQNP / UUPQID / UUPQCP / UUPQVL / UUPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uupqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*3] = "UMIN    " "UMAX    " "UREF    ";
    static integer itype[3] = { 3,3,3 };
    static char cparal[12*3] = "****UMIN    " "****UMAX    " "****UREF    ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uuiqid_(char *, integer *, ftnlen), uulqid_(char 
	    *, integer *, ftnlen), uurqid_(char *, integer *, ftnlen), 
	    uuiqvl_(integer *, integer *), uulqvl_(integer *, integer *), 
	    uuisvl_(integer *, integer *), uulsvl_(integer *, integer *), 
	    uurqvl_(integer *, integer *), uursvl_(integer *, integer *);

/* ---- SHORT NAME ---- */
    switch(n__) {
	case 1: goto L_uupqid;
	case 2: goto L_uupqcp;
	case 3: goto L_uupqcl;
	case 4: goto L_uupqit;
	case 5: goto L_uupqvl;
	case 6: goto L_uupsvl;
	case 7: goto L_uupqin;
	}

/* ---- LONG NAME ---- */
    *ncp = 3;
    return 0;
/* ----------------------------------------------------------------------- */

L_uupqid:
    for (n = 1; n <= 3; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 12, cp_len, (ftnlen)12)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UUPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uupqcp:
    if (1 <= *idx && *idx <= 3) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UUPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uupqcl:
    if (1 <= *idx && *idx <= 3) {
	s_copy(cp, cparal + (*idx - 1) * 12, cp_len, (ftnlen)12);
    } else {
	msgdmp_("E", "UUPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uupqit:
    if (1 <= *idx && *idx <= 3) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "UUPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uupqvl:
    if (1 <= *idx && *idx <= 3) {
	if (itype[*idx - 1] == 1) {
	    uuiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uuiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uulqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uulqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    uurqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uurqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UUPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uupsvl:
    if (1 <= *idx && *idx <= 3) {
	if (itype[*idx - 1] == 1) {
	    uuiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uuisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uulqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uulsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    uurqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uursvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UUPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uupqin:
    for (n = 1; n <= 3; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 12, cp_len, (ftnlen)12)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uupqnp_ */

/* Subroutine */ int uupqnp_(integer *ncp)
{
    return uupqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int uupqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uupqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uupqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uupqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uupqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uupqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uupqit_(integer *idx, integer *itp)
{
    return uupqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uupqvl_(integer *idx, integer *ipara)
{
    return uupqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uupsvl_(integer *idx, integer *ipara)
{
    return uupqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uupqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uupqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }

