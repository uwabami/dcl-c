/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UUIQNP / UUIQID / UUIQCP / UUIQVL / UUISVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uuiqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);

    /* Local variables */
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_uuiqid;
	case 2: goto L_uuiqcp;
	case 3: goto L_uuiqcl;
	case 4: goto L_uuiqvl;
	case 5: goto L_uuisvl;
	case 6: goto L_uuiqin;
	}

    *ncp = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuiqid:
    *idx = 0;
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UUIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uuiqcp:
    msgdmp_("E", "UUIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_uuiqcl:
    msgdmp_("E", "UUIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_uuiqvl:
    *ipara = 0;
    msgdmp_("E", "UUIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_uuisvl:
    msgdmp_("E", "UUISVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
	    ftnlen)20);
    return 0;
/* ----------------------------------------------------------------------- */

L_uuiqin:
    *in = 0;
    return 0;
} /* uuiqnp_ */

/* Subroutine */ int uuiqnp_(integer *ncp)
{
    return uuiqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int uuiqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uuiqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uuiqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uuiqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uuiqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uuiqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uuiqvl_(integer *idx, integer *ipara)
{
    return uuiqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uuisvl_(integer *idx, integer *ipara)
{
    return uuiqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uuiqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uuiqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }

