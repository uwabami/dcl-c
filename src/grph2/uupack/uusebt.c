/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uusebt_0_(int n__, integer *itype, integer *index, real *
	rsize)
{
    /* Initialized data */

    static integer itypez = 1;
    static integer indexz = 1;
    static real rsizez = .02f;

    switch(n__) {
	case 1: goto L_uuqebt;
	case 2: goto L_uusebi;
	case 3: goto L_uuqebi;
	case 4: goto L_uusebs;
	case 5: goto L_uuqebs;
	}

    itypez = *itype;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqebt:
    *itype = itypez;
    return 0;
/* ----------------------------------------------------------------------- */

L_uusebi:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqebi:
    *index = indexz;
    return 0;
/* ----------------------------------------------------------------------- */

L_uusebs:
    rsizez = *rsize;
    return 0;
/* ----------------------------------------------------------------------- */

L_uuqebs:
    *rsize = rsizez;
    return 0;
} /* uusebt_ */

/* Subroutine */ int uusebt_(integer *itype)
{
    return uusebt_0_(0, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int uuqebt_(integer *itype)
{
    return uusebt_0_(1, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int uusebi_(integer *index)
{
    return uusebt_0_(2, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int uuqebi_(integer *index)
{
    return uusebt_0_(3, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int uusebs_(real *rsize)
{
    return uusebt_0_(4, (integer *)0, (integer *)0, rsize);
    }

/* Subroutine */ int uuqebs_(real *rsize)
{
    return uusebt_0_(5, (integer *)0, (integer *)0, rsize);
    }

