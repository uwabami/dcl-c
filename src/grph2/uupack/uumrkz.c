/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbtx3_;

#define szbtx3_1 szbtx3_

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uumrkz_(integer *n, real *upx, real *upy, integer *itype,
	 integer *index, real *rsize)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1;
    char ch__1[1];

    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static real dx, dy, pmf;
    static integer npm;
    static real uxx, uyy;
    static char cobj[80];
    extern /* Character */ VOID csgi_(char *, ftnlen, integer *);
    extern /* Subroutine */ int cdblk_(char *, ftnlen);
    static logical lflag;
    static char cmark[1];
    static logical lmiss;
    static real rmiss;
    static logical lxuni, lyuni;
    static real uxmin, uxmax, uymin, uymax;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), sgiget_(
	    char *, integer *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), sglget_(char 
	    *, logical *, ftnlen), msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), swocls_(char *, 
	    ftnlen), uuqidv_(real *, real *), swoopn_(char *, char *, ftnlen, 
	    ftnlen), sztxcl_(void), sztxop_(real *, integer *, integer *, 
	    integer *), sztxzu_(real *, real *, char *, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___8 = { 0, cobj, 0, "(2I8,F8.5)", 80, 1 };


    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "UUMRKZ", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "UUMRKZ", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "UUMRKZ", "MARKER INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "UUMRKZ", "MARKER INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*rsize == 0.f) {
	msgdmp_("M", "UUMRKZ", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "UUMRKZ", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sglget_("LCLIP", &szbtx3_1.lclip, (ftnlen)5);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    gllget_("LMISS", &lmiss, (ftnlen)5);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    sgrget_("PMFACT", &pmf, (ftnlen)6);
    sgiget_("NPMSKIP", &npm, (ftnlen)7);
    csgi_(ch__1, (ftnlen)1, itype);
    *(unsigned char *)cmark = *(unsigned char *)&ch__1[0];
    s_wsfi(&io___8);
    do_fio(&c__1, (char *)&(*itype), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*index), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rsize), (ftnlen)sizeof(real));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("UUMRKZ", cobj, (ftnlen)6, (ftnlen)80);
    r__1 = *rsize * pmf;
    sztxop_(&r__1, &c__0, &c__0, index);
    lxuni = upx[1] == rundef;
    lyuni = upy[1] == rundef;
    if (lxuni) {
	uuqidv_(&uxmin, &uxmax);
	if (uxmin == rundef) {
	    sgrget_("UXMIN", &uxmin, (ftnlen)5);
	}
	if (uxmax == rundef) {
	    sgrget_("UXMAX", &uxmax, (ftnlen)5);
	}
	dx = (uxmax - uxmin) / (*n - 1);
    }
    if (lyuni) {
	uuqidv_(&uymin, &uymax);
	if (uymin == rundef) {
	    sgrget_("UYMIN", &uymin, (ftnlen)5);
	}
	if (uymax == rundef) {
	    sgrget_("UYMAX", &uymax, (ftnlen)5);
	}
	dy = (uymax - uymin) / (*n - 1);
    }
    i__1 = *n;
    i__2 = npm;
    for (i__ = 1; i__2 < 0 ? i__ >= i__1 : i__ <= i__1; i__ += i__2) {
	if (lxuni) {
	    uxx = uxmin + dx * (i__ - 1);
	} else {
	    uxx = upx[i__];
	}
	if (lyuni) {
	    uyy = uymin + dy * (i__ - 1);
	} else {
	    uyy = upy[i__];
	}
	lflag = lmiss && (uxx == rmiss || uyy == rmiss);
	if (! lflag) {
	    sztxzu_(&uxx, &uyy, cmark, (ftnlen)1);
	}
/* L10: */
    }
    sztxcl_();
    swocls_("UUMRKZ", (ftnlen)6);
    return 0;
} /* uumrkz_ */

