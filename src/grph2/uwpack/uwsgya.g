/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical leqdyz;
    integer nyz;
    real uyminz, uymaxz, dyz, ypz[@MAXNGRID];
} uwblky_;

#define uwblky_1 uwblky_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     UWSGYA / UWSGYB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uwsgya_0_(int n__, real *yp, integer *ny, real *uymin, 
	real *uymax, logical *lsety)
{
    /* Initialized data */

    static logical lsetyz = FALSE_;

    extern /* Subroutine */ int vrset_(real *, real *, integer *, integer *, 
	    integer *), msgdmp_(char *, char *, char *, ftnlen, ftnlen, 
	    ftnlen), uwigyi_(void);

    /* Parameter adjustments */
    if (yp) {
	--yp;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_uwqgya;
	case 2: goto L_uwsgyb;
	case 3: goto L_uwqgyb;
	case 4: goto L_uwsgyz;
	case 5: goto L_uwqgyz;
	}

    if (*ny < 2) {
	msgdmp_("E", "UWSGYA", "NUMBER OF POINTS IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*ny > @MAXNGRID) {
	msgdmp_("E", "UWSGYA", "WORKING AREA IS NOT ENOUGH.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
    }
    uwblky_1.leqdyz = FALSE_;
    uwblky_1.nyz = *ny;
    vrset_(&yp[1], uwblky_1.ypz, ny, &c__1, &c__1);
    uwblky_1.uyminz = yp[1];
    uwblky_1.uymaxz = yp[*ny];
    lsetyz = TRUE_;
    uwigyi_();
    return 0;
/* ----------------------------------------------------------------------- */

L_uwqgya:
    *ny = uwblky_1.nyz;
    vrset_(uwblky_1.ypz, &yp[1], ny, &c__1, &c__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_uwsgyb:
    if (*uymin == *uymax) {
	msgdmp_("E", "UWSGYB", "UYMIN = UYMAX.", (ftnlen)1, (ftnlen)6, (
		ftnlen)14);
    }
    uwblky_1.leqdyz = TRUE_;
    uwblky_1.nyz = *ny;
    uwblky_1.uyminz = *uymin;
    uwblky_1.uymaxz = *uymax;
    uwblky_1.dyz = (*uymax - *uymin) / (*ny - 1);
    lsetyz = TRUE_;
    uwigyi_();
    return 0;
/* ----------------------------------------------------------------------- */

L_uwqgyb:
    *ny = uwblky_1.nyz;
    *uymin = uwblky_1.uyminz;
    *uymax = uwblky_1.uymaxz;
    return 0;
/* ----------------------------------------------------------------------- */

L_uwsgyz:
    lsetyz = *lsety;
    return 0;
/* ----------------------------------------------------------------------- */

L_uwqgyz:
    *lsety = lsetyz;
    return 0;
} /* uwsgya_ */

/* Subroutine */ int uwsgya_(real *yp, integer *ny)
{
    return uwsgya_0_(0, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int uwqgya_(real *yp, integer *ny)
{
    return uwsgya_0_(1, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int uwsgyb_(real *uymin, real *uymax, integer *ny)
{
    return uwsgya_0_(2, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int uwqgyb_(real *uymin, real *uymax, integer *ny)
{
    return uwsgya_0_(3, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int uwsgyz_(logical *lsety)
{
    return uwsgya_0_(4, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }

/* Subroutine */ int uwqgyz_(logical *lsety)
{
    return uwsgya_0_(5, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }

