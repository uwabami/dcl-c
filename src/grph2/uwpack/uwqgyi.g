/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical leqdyz;
    integer nyz;
    real uyminz, uymaxz, dyz, ypz[@MAXNGRID];
} uwblky_;

#define uwblky_1 uwblky_

/* ----------------------------------------------------------------------- */
/*     UWQGYI / UWIGYI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uwqgyi_0_(int n__, real *uy, integer *iuy, real *frac)
{
    /* Initialized data */

    static integer iy = 1;

    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer i__;
    static real ynorm;
    static logical lascnd;
    static integer iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    static real uymina, uymaxa;

    switch(n__) {
	case 1: goto L_uwigyi;
	}

    if (! (uymina <= *uy && *uy <= uymaxa)) {
	*iuy = iundef;
	*frac = 0.f;
	return 0;
    }
    if (uwblky_1.leqdyz) {
	ynorm = (*uy - uwblky_1.uyminz) / uwblky_1.dyz;
/* Computing MIN */
	i__1 = (integer) ynorm + 1, i__2 = uwblky_1.nyz - 1;
	*iuy = min(i__1,i__2);
	*frac = ynorm - *iuy + 1;
    } else {
	if (lascnd) {
	    if (*uy > uwblky_1.ypz[iy - 1]) {
		i__1 = uwblky_1.nyz - 2;
		for (i__ = iy; i__ <= i__1; ++i__) {
		    if (*uy <= uwblky_1.ypz[i__]) {
			goto L30;
		    }
/* L10: */
		}
	    } else {
		for (i__ = iy; i__ >= 2; --i__) {
		    if (*uy > uwblky_1.ypz[i__ - 1]) {
			goto L30;
		    }
/* L20: */
		}
	    }
L30:
	    ;
	} else {
	    if (*uy > uwblky_1.ypz[iy - 1]) {
		for (i__ = iy - 1; i__ >= 1; --i__) {
		    if (*uy <= uwblky_1.ypz[i__ - 1]) {
			goto L130;
		    }
/* L110: */
		}
	    } else {
		i__1 = uwblky_1.nyz - 2;
		for (i__ = iy; i__ <= i__1; ++i__) {
		    if (*uy > uwblky_1.ypz[i__]) {
			goto L130;
		    }
/* L120: */
		}
	    }
L130:
	    ;
	}
	iy = i__;
	*iuy = i__;
	*frac = (*uy - uwblky_1.ypz[i__ - 1]) / (uwblky_1.ypz[i__] - 
		uwblky_1.ypz[i__ - 1]);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uwigyi:
    iy = 1;
    gliget_("IUNDEF", &iundef, (ftnlen)6);
    if (! uwblky_1.leqdyz) {
	lascnd = uwblky_1.ypz[uwblky_1.nyz - 1] > uwblky_1.ypz[0];
    }
    uymaxa = max(uwblky_1.uyminz,uwblky_1.uymaxz);
    uymina = min(uwblky_1.uyminz,uwblky_1.uymaxz);
    return 0;
} /* uwqgyi_ */

/* Subroutine */ int uwqgyi_(real *uy, integer *iuy, real *frac)
{
    return uwqgyi_0_(0, uy, iuy, frac);
    }

/* Subroutine */ int uwigyi_(void)
{
    return uwqgyi_0_(1, (real *)0, (integer *)0, (real *)0);
    }

