/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical leqdxz;
    integer nxz;
    real uxminz, uxmaxz, dxz, xpz[@MAXNGRID];
} uwblkx_;

#define uwblkx_1 uwblkx_

/* ----------------------------------------------------------------------- */
/*     UWQGXI / UWIGXI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uwqgxi_0_(int n__, real *ux, integer *iux, real *frac)
{
    /* Initialized data */

    static integer ix = 1;

    /* System generated locals */
    integer i__1, i__2;
    real r__1;

    /* Local variables */
    static integer i__;
    static real uxx;
    static logical ldeg, lmap;
    extern real rfpi_(void), rmod_(real *, real *);
    static real umod, xnorm;
    static logical lascnd;
    static integer iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), sglget_(
	    char *, logical *, ftnlen);
    static real uxmina, uxmaxa;
    extern /* Subroutine */ int stqtrf_(logical *);

    switch(n__) {
	case 1: goto L_uwigxi;
	}

    if (lmap) {
	r__1 = *ux - uxmina;
	uxx = rmod_(&r__1, &umod) + uxmina;
    } else {
	uxx = *ux;
    }
    if (! (uxmina <= uxx && uxx <= uxmaxa)) {
	*iux = iundef;
	*frac = 0.f;
	return 0;
    }
    if (uwblkx_1.leqdxz) {
	xnorm = (uxx - uwblkx_1.uxminz) / uwblkx_1.dxz;
/* Computing MIN */
	i__1 = (integer) xnorm + 1, i__2 = uwblkx_1.nxz - 1;
	*iux = min(i__1,i__2);
	*frac = xnorm - *iux + 1;
    } else {
	if (lascnd) {
	    if (uxx > uwblkx_1.xpz[ix - 1]) {
		i__1 = uwblkx_1.nxz - 2;
		for (i__ = ix; i__ <= i__1; ++i__) {
		    if (uxx <= uwblkx_1.xpz[i__]) {
			goto L30;
		    }
/* L10: */
		}
	    } else {
		for (i__ = ix; i__ >= 2; --i__) {
		    if (uxx > uwblkx_1.xpz[i__ - 1]) {
			goto L30;
		    }
/* L20: */
		}
	    }
L30:
	    ;
	} else {
	    if (uxx > uwblkx_1.xpz[ix - 1]) {
		for (i__ = ix - 1; i__ >= 1; --i__) {
		    if (uxx <= uwblkx_1.xpz[i__ - 1]) {
			goto L130;
		    }
/* L110: */
		}
	    } else {
		i__1 = uwblkx_1.nxz - 2;
		for (i__ = ix; i__ <= i__1; ++i__) {
		    if (uxx > uwblkx_1.xpz[i__]) {
			goto L130;
		    }
/* L120: */
		}
	    }
L130:
	    ;
	}
	ix = i__;
	*iux = i__;
	*frac = (uxx - uwblkx_1.xpz[i__ - 1]) / (uwblkx_1.xpz[i__] - 
		uwblkx_1.xpz[i__ - 1]);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uwigxi:
    ix = 1;
    gliget_("IUNDEF", &iundef, (ftnlen)6);
    sglget_("LDEG", &ldeg, (ftnlen)4);
    stqtrf_(&lmap);
    if (ldeg) {
	umod = 360.f;
    } else {
	umod = rfpi_() * 2;
    }
    if (! uwblkx_1.leqdxz) {
	lascnd = uwblkx_1.xpz[uwblkx_1.nxz - 1] > uwblkx_1.xpz[0];
    }
    uxmaxa = max(uwblkx_1.uxminz,uwblkx_1.uxmaxz);
    uxmina = min(uwblkx_1.uxminz,uwblkx_1.uxmaxz);
    return 0;
} /* uwqgxi_ */

/* Subroutine */ int uwqgxi_(real *ux, integer *iux, real *frac)
{
    return uwqgxi_0_(0, ux, iux, frac);
    }

/* Subroutine */ int uwigxi_(void)
{
    return uwqgxi_0_(1, (real *)0, (integer *)0, (real *)0);
    }

