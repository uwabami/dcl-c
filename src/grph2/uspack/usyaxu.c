/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USPACK DRAW Y-AXIS (UNIFORM)                     DCL 5.0 95/09/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usyaxu_(char *cys, ftnlen cys_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    real r__1;
    char ch__1[32];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rsfi(icilist *), do_fio(integer *, char *, ftnlen), e_rsfi(void)
	    ;

    /* Local variables */
    static integer i__;
    static char cp[8];
    static real dyl;
    static integer itr;
    static real dyt;
    static integer nys;
    static logical lab1;
    static real yfac, dyla;
    static integer ndgt;
    static real dyta, yoff;
    static char cpos[1];
    extern integer lenz_(char *, ftnlen);
    static real xmin, ymin, xmax, ymax;
    static char cfmt0[8];
    static real ymina, ymaxa;
    static char cyfmt[8], cysub[32];
    extern /* Character */ VOID csblbl_(char *, ftnlen, real *, real *, char *
	    , ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uscget_(char *, char *, ftnlen, ftnlen), sgqwnd_(
	    real *, real *, real *, real *), sgswnd_(real *, real *, real *, 
	    real *), sgstrf_(void), usrget_(char *, real *, ftnlen), uzlget_(
	    char *, logical *, ftnlen);
    static char cyunit[32];
    extern /* Subroutine */ int sgqtrn_(integer *), uyaxdv_(char *, real *, 
	    real *, ftnlen), uyqfmt_(char *, ftnlen), uysfmt_(char *, ftnlen),
	     usysub_(char *, char *, char *, real *, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static icilist io___23 = { 0, cyfmt, 0, "(T3,I1)", 8, 1 };


/* ---------------------------- PARAMETERS ------------------------------- */
    usrget_("DYT", &dyt, (ftnlen)3);
    usrget_("DYL", &dyl, (ftnlen)3);
    usrget_("YFAC", &yfac, (ftnlen)4);
    usrget_("YOFF", &yoff, (ftnlen)4);
    uscget_("CYFMT", cyfmt, (ftnlen)5, (ftnlen)8);
    uscget_("CYUNIT", cyunit, (ftnlen)6, (ftnlen)32);
    if (dyt <= 0.f || dyl <= 0.f) {
	msgdmp_("E", "USYAXU", "DYT OR DYL IS NEGATIVE.", (ftnlen)1, (ftnlen)
		6, (ftnlen)23);
    }
/* ----------------------------------------------------------------------- */
    sgqtrn_(&itr);
    if (! (itr == 1 || itr == 3)) {
	msgdmp_("E", "USYAXU", "INVALID TRANSFORMATION NUMBER.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    sgqwnd_(&xmin, &xmax, &ymin, &ymax);
    dyta = dyt / yfac;
    dyla = dyl / yfac;
    ymina = (ymin - yoff) * yfac;
    ymaxa = (ymax - yoff) * yfac;
    sgswnd_(&xmin, &xmax, &ymina, &ymaxa);
    sgstrf_();
/* ---------------------------- Y-AXIS ----------------------------------- */
    uyqfmt_(cfmt0, (ftnlen)8);
    uysfmt_(cyfmt, (ftnlen)8);
/* Computing MIN */
    i__1 = lenz_(cys, cys_len);
    nys = min(i__1,2);
    i__1 = nys;
    for (i__ = 1; i__ <= i__1; ++i__) {
	uyaxdv_(cys + (i__ - 1), &dyta, &dyla, (ftnlen)1);
/* Writing concatenation */
	i__2[0] = 6, a__1[0] = "LABELY";
	i__2[1] = 1, a__1[1] = cys + (i__ - 1);
	s_cat(cp, a__1, i__2, &c__2, (ftnlen)8);
	uzlget_(cp, &lab1, (ftnlen)8);
	if (lab1) {
	    csblbl_(ch__1, (ftnlen)32, &yfac, &yoff, cyunit, (ftnlen)32);
	    s_copy(cysub, ch__1, (ftnlen)32, (ftnlen)32);
	    if (lenz_(cysub, (ftnlen)32) != 0) {
		uscget_("CYSPOS", cpos, (ftnlen)6, (ftnlen)1);
		s_rsfi(&io___23);
		do_fio(&c__1, (char *)&ndgt, (ftnlen)sizeof(integer));
		e_rsfi();
		r__1 = (real) ndgt;
		usysub_(cys + (i__ - 1), cpos, cysub, &r__1, (ftnlen)1, (
			ftnlen)1, (ftnlen)32);
	    }
	}
/* L100: */
    }
    sgswnd_(&xmin, &xmax, &ymin, &ymax);
    sgstrf_();
    uysfmt_(cfmt0, (ftnlen)8);
    return 0;
} /* usyaxu_ */

