/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/*     USPACK AXIS (CALENDAR)                          S.Sakai  99/10/05 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usaxcl_(char *cside, integer *jd0, char *ctype, integer *
	nd, ftnlen cside_len, ftnlen ctype_len)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1;

    /* Builtin functions */
    integer i_nint(real *);

    /* Local variables */
    static integer i__, j;
    static char cs[1], ct[1];
    static integer nd0, ncs;
    static logical loff;
    extern integer lenz_(char *, ftnlen);
    static real xmin, ymin, xmax, ymax;
    static integer ntyp;
    static real factor, factot, offset;
    extern /* Subroutine */ int ucxamn_(char *, integer *, integer *, ftnlen),
	     ucyamn_(char *, integer *, integer *, ftnlen), ucxady_(char *, 
	    integer *, integer *, ftnlen), cupper_(char *, ftnlen), ucyady_(
	    char *, integer *, integer *, ftnlen), sgqwnd_(real *, real *, 
	    real *, real *), uzlget_(char *, logical *, ftnlen), ucxayr_(char 
	    *, integer *, integer *, ftnlen), ucyayr_(char *, integer *, 
	    integer *, ftnlen), uzlset_(char *, logical *, ftnlen), usxinz_(
	    char *, real *, real *, ftnlen), usyinz_(char *, real *, real *, 
	    ftnlen), usxtlz_(void), usytlz_(void);

    ncs = lenz_(cside, cside_len);
    uzlget_("LOFFSET", &loff, (ftnlen)7);
    uzlset_("LOFFSET", &c_false, (ftnlen)7);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'T' || *(unsigned char *)cs == 'B' || *(
		unsigned char *)cs == 'H') {
	    if (*(unsigned char *)cs == 'H') {
		*(unsigned char *)cs = 'U';
	    }
	    if (*nd == 0) {
		sgqwnd_(&xmin, &xmax, &ymin, &ymax);
		r__1 = xmax - xmin;
		nd0 = (i__2 = i_nint(&r__1), abs(i__2));
	    } else {
		nd0 = *nd;
	    }
	    usxinz_(cs, &factot, &offset, (ftnlen)1);
	    ntyp = lenz_(ctype, ctype_len);
	    i__2 = ntyp;
	    for (j = 1; j <= i__2; ++j) {
		*(unsigned char *)ct = *(unsigned char *)&ctype[j - 1];
		cupper_(ct, (ftnlen)1);
		if (*(unsigned char *)ct == 'Y') {
		    ucxayr_(cs, jd0, &nd0, (ftnlen)1);
		}
		if (*(unsigned char *)ct == 'M') {
		    ucxamn_(cs, jd0, &nd0, (ftnlen)1);
		}
		if (*(unsigned char *)ct == 'D') {
		    ucxady_(cs, jd0, &nd0, (ftnlen)1);
		}
/* L200: */
	    }
	    usxtlz_();
	} else if (*(unsigned char *)cs == 'L' || *(unsigned char *)cs == 'R' 
		|| *(unsigned char *)cs == 'V') {
	    if (*(unsigned char *)cs == 'V') {
		*(unsigned char *)cs = 'U';
	    }
	    if (*nd == 0) {
		sgqwnd_(&xmin, &xmax, &ymin, &ymax);
		r__1 = ymax - ymin;
		nd0 = (i__2 = i_nint(&r__1), abs(i__2));
	    } else {
		nd0 = *nd;
	    }
	    usyinz_(cs, &factor, &offset, (ftnlen)1);
	    ntyp = lenz_(ctype, ctype_len);
	    i__2 = ntyp;
	    for (j = 1; j <= i__2; ++j) {
		*(unsigned char *)ct = *(unsigned char *)&ctype[j - 1];
		cupper_(ct, (ftnlen)1);
		if (*(unsigned char *)ct == 'Y') {
		    ucyayr_(cs, jd0, &nd0, (ftnlen)1);
		}
		if (*(unsigned char *)ct == 'M') {
		    ucyamn_(cs, jd0, &nd0, (ftnlen)1);
		}
		if (*(unsigned char *)ct == 'D') {
		    ucyady_(cs, jd0, &nd0, (ftnlen)1);
		}
/* L250: */
	    }
	    usytlz_();
	}
/* L100: */
    }
    uzlset_("LOFFSET", &loff, (ftnlen)7);
    return 0;
} /* usaxcl_ */

