/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     USPACK DRAW DEFAULT AXIS                        S.Sakai  99/10/09 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usdaxs_(void)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static char cs[1];
    static integer ncs;
    extern integer lenz_(char *, ftnlen);
    static char cside[4];
    extern /* Subroutine */ int uscget_(char *, char *, ftnlen, ftnlen), 
	    cupper_(char *, ftnlen), usaxsc_(char *, ftnlen);

/* ----------------------------- X-AXIS ---------------------------------- */
    uscget_("CXSIDE", cside, (ftnlen)6, (ftnlen)4);
    ncs = lenz_(cside, (ftnlen)4);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'U') {
	    *(unsigned char *)cs = 'H';
	}
	usaxsc_(cs, (ftnlen)1);
/* L100: */
    }
/* ----------------------------- Y-AXIS ---------------------------------- */
    uscget_("CYSIDE", cside, (ftnlen)6, (ftnlen)4);
    ncs = lenz_(cside, (ftnlen)4);
    i__1 = ncs;
    for (i__ = 1; i__ <= i__1; ++i__) {
	*(unsigned char *)cs = *(unsigned char *)&cside[i__ - 1];
	cupper_(cs, (ftnlen)1);
	if (*(unsigned char *)cs == 'U') {
	    *(unsigned char *)cs = 'V';
	}
	usaxsc_(cs, (ftnlen)1);
/* L200: */
    }
    return 0;
} /* usdaxs_ */

