/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USIGET / USISET / USISTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usiget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer idx;
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     usiqid_(char *, integer *, ftnlen), rtiget_(char *, char *, 
	    integer *, integer *, ftnlen, ftnlen), usiqcl_(integer *, char *, 
	    ftnlen), usiqcp_(integer *, char *, ftnlen), usiqvl_(integer *, 
	    integer *), usisvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_usiset;
	case 2: goto L_usistx;
	}

    usiqid_(cp, &idx, cp_len);
    usiqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_usiset:
    usiqid_(cp, &idx, cp_len);
    usisvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_usistx:
    ip = *ipara;
    usiqid_(cp, &idx, cp_len);
/*     / SHORT NAME / */
    usiqcp_(&idx, cx, (ftnlen)8);
    rtiget_("US", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
/*     / LONG NAME / */
    usiqcl_(&idx, cl, (ftnlen)40);
    rliget_(cl, &ip, &c__1, (ftnlen)40);
    usisvl_(&idx, &ip);
    return 0;
} /* usiget_ */

/* Subroutine */ int usiget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return usiget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int usiset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return usiget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int usistx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return usiget_0_(2, cp, ipara, cp_len);
    }

