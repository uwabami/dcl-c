/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     USPACK AUTO SCALING ROUTINE (LOG)                DCL 5.0 95/09/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ususcl_(char *caxis, real *umin, real *umax, real *vmin, 
	real *vmax, ftnlen caxis_len)
{
    /* Initialized data */

    static real sc[4] = { 1.f,2.f,5.f,10.f };

    /* System generated locals */
    address a__1[2];
    integer i__1[2];
    real r__1;

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    double r_lg10(real *);

    /* Local variables */
    static char cp[8];
    static real cw, dv, ufac;
    extern /* Subroutine */ int gnge_(real *, real *, integer *), gnle_(real *
	    , real *, integer *);
    static integer nlbl;
    static real ufaca;
    static integer nlbla;
    static real bumin, bumax;
    static integer ipmin, ipmax, mxdgt, itype, nticka, iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), gnsblk_(
	    real *, integer *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), gnsave_(void)
	    , msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen);
    static integer itypea, nticks;
    extern /* Subroutine */ int usiget_(char *, integer *, ftnlen), gnrset_(
	    void), usrget_(char *, real *, ftnlen), usiset_(char *, integer *,
	     ftnlen), uzrget_(char *, real *, ftnlen), usrset_(char *, real *,
	     ftnlen);

/* ----------------------- ARGUMENT CHECK -------------------------------- */
    if (*(unsigned char *)caxis != 'X' && *(unsigned char *)caxis != 'Y') {
	msgdmp_("E", "USUSCL", "INVALID CAXIS", (ftnlen)1, (ftnlen)6, (ftnlen)
		13);
    }
    if (*vmin >= *vmax) {
	msgdmp_("E", "USUSCL", "VMIN>VMAX", (ftnlen)1, (ftnlen)6, (ftnlen)9);
    }
/* --------------------------- PARAMETERS -------------------------------- */
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "MXDGT";
    i__1[1] = 1, a__1[1] = caxis;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    usiget_(cp, &mxdgt, (ftnlen)8);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    gliget_("IUNDEF", &iundef, (ftnlen)6);
    uzrget_("RSIZEL1", &cw, (ftnlen)7);
/* --------------------------- ITYPE & UFAC ------------------------------ */
    gnsave_();
    gnsblk_(sc, &c__4);
    r__1 = max(*umax,*umin);
    gnge_(&r__1, &bumax, &ipmax);
    r__1 = min(*umin,*umax);
    gnle_(&r__1, &bumin, &ipmin);
    gnrset_();
    if (ipmax <= ipmin + 1) {
	itype = 1;
/*        IF(IPMAX+1.GT.MXDGT .OR. 2-IPMIN.GT.MXDGT) THEN */
/*          UFAC = 1.D1**IPMIN */
/*        ELSE */
/*        ENDIF */
    } else {
	itype = 2;
    }
    ufac = 1.f;

/* Writing concatenation */
    i__1[0] = 1, a__1[0] = caxis;
    i__1[1] = 3, a__1[1] = "FAC";
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    usrget_(cp, &ufaca, (ftnlen)8);
    if (ufaca == rundef) {
	usrset_(cp, &ufac, (ftnlen)8);
    }
/* Writing concatenation */
    i__1[0] = 5, a__1[0] = "ITYPE";
    i__1[1] = 1, a__1[1] = caxis;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    usiget_(cp, &itypea, (ftnlen)8);
    if (itypea == iundef) {
	usiset_(cp, &itype, (ftnlen)8);
    }
/* --------------------------- NLBL & NTISCKS ---------------------------- */
    dv = (*vmax - *vmin) / (r__1 = r_lg10(umax) - r_lg10(umin), abs(r__1)) / 
	    cw;
    if (dv >= 10.f) {
	nlbl = 3;
	nticks = 9;
    } else if (dv >= 5.f) {
	nlbl = 1;
	nticks = 9;
    } else if (dv >= 2.5f) {
	nlbl = 1;
	nticks = 5;
    } else {
	nlbl = 1;
	nticks = 2;
    }
/* Writing concatenation */
    i__1[0] = 4, a__1[0] = "NLBL";
    i__1[1] = 1, a__1[1] = caxis;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    usiget_(cp, &nlbla, (ftnlen)8);
    if (nlbla == iundef) {
	usiset_(cp, &nlbl, (ftnlen)8);
    }
/* Writing concatenation */
    i__1[0] = 6, a__1[0] = "NTICKS";
    i__1[1] = 1, a__1[1] = caxis;
    s_cat(cp, a__1, i__1, &c__2, (ftnlen)8);
    usiget_(cp, &nticka, (ftnlen)8);
    if (nticka == iundef) {
	usiset_(cp, &nticks, (ftnlen)8);
    }
    return 0;
} /* ususcl_ */

