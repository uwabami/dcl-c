/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__11 = 11;

/* ----------------------------------------------------------------------- */
/*     USCQNP */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uscqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	char *cval, integer *in, ftnlen cp_len, ftnlen cval_len)
{
    /* Initialized data */

    static char cparas[8*11] = "CXSIDE  " "CYSIDE  " "CXSPOS  " "CYSPOS  " 
	    "CBLKT   " "CXTTL   " "CYTTL   " "CXUNIT  " "CYUNIT  " "CXFMT   " 
	    "CYFMT   ";
    static char cx[80*11] = "BT                                             "
	    "                                 " "LR                          "
	    "                                                    " "R        "
	    "                                                                "
	    "       " "T                                                     "
	    "                          " "()                                 "
	    "                                             " "                "
	    "                                                                " 
	    "                                                               "
	    "                 " "                                            "
	    "                                    " "                         "
	    "                                                       " "      "
	    "                                                                "
	    "          " "                                                   "
	    "                             ";
    static char cparal[40*11] = "X_AXIS_POS                              " 
	    "Y_AXIS_POS                              " "X_SUBLABEL_POS      "
	    "                    " "Y_SUBLABEL_POS                          " 
	    "SUBLABEL_BRACKET                        " "****CXTTL           "
	    "                    " "****CYTTL                               " 
	    "****CXUNIT                              " "****CYUNIT          "
	    "                    " "****CXFMT                               " 
	    "****CYFMT                               ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rlcget_(char *, char *, integer *, ftnlen, 
	    ftnlen), msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), 
	    rtcget_(char *, char *, char *, integer *, ftnlen, ftnlen, ftnlen)
	    ;

/* ---- short name ---- */
    switch(n__) {
	case 1: goto L_uscqid;
	case 2: goto L_uscqcp;
	case 3: goto L_uscqcl;
	case 4: goto L_uscqvl;
	case 5: goto L_uscsvl;
	case 6: goto L_uscqin;
	}

/* ---- long name ---- */
    *ncp = 11;
    return 0;
/* ----------------------------------------------------------------------- */

L_uscqid:
    for (n = 1; n <= 11; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER \"";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "\" IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "USCQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uscqcp:
    if (1 <= *idx && *idx <= 11) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "USCQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uscqcl:
    if (1 <= *idx && *idx <= 11) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "USCQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uscqvl:
    if (lfirst) {
	rtcget_("US", cparas, cx, &c__11, (ftnlen)2, (ftnlen)8, (ftnlen)80);
	rlcget_(cparal, cx, &c__11, (ftnlen)40, (ftnlen)80);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 11) {
	s_copy(cval, cx + (*idx - 1) * 80, cval_len, (ftnlen)80);
    } else {
	msgdmp_("E", "USCQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uscsvl:
    if (lfirst) {
	rtcget_("US", cparas, cx, &c__11, (ftnlen)2, (ftnlen)8, (ftnlen)80);
	rlcget_(cparal, cx, &c__11, (ftnlen)40, (ftnlen)80);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 11) {
	s_copy(cx + (*idx - 1) * 80, cval, (ftnlen)80, cval_len);
    } else {
	msgdmp_("E", "USCSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uscqin:
    for (n = 1; n <= 11; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uscqnp_ */

/* Subroutine */ int uscqnp_(integer *ncp)
{
    return uscqnp_0_(0, ncp, (char *)0, (integer *)0, (char *)0, (integer *)0,
	     (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int uscqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uscqnp_0_(1, (integer *)0, cp, idx, (char *)0, (integer *)0, 
	    cp_len, (ftnint)0);
    }

/* Subroutine */ int uscqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uscqnp_0_(2, (integer *)0, cp, idx, (char *)0, (integer *)0, 
	    cp_len, (ftnint)0);
    }

/* Subroutine */ int uscqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uscqnp_0_(3, (integer *)0, cp, idx, (char *)0, (integer *)0, 
	    cp_len, (ftnint)0);
    }

/* Subroutine */ int uscqvl_(integer *idx, char *cval, ftnlen cval_len)
{
    return uscqnp_0_(4, (integer *)0, (char *)0, idx, cval, (integer *)0, (
	    ftnint)0, cval_len);
    }

/* Subroutine */ int uscsvl_(integer *idx, char *cval, ftnlen cval_len)
{
    return uscqnp_0_(5, (integer *)0, (char *)0, idx, cval, (integer *)0, (
	    ftnint)0, cval_len);
    }

/* Subroutine */ int uscqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uscqnp_0_(6, (integer *)0, cp, (integer *)0, (char *)0, in, cp_len,
	     (ftnint)0);
    }

