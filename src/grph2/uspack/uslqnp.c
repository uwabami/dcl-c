/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__6 = 6;

/* ----------------------------------------------------------------------- */
/*     USLQNP / USLQID / USLQCP / USLQVL / USLSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uslqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	logical *lpara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*6] = "LXINV   " "LYINV   " "LMATCH  " "LPRTCT  " 
	    "LXSUB   " "LYSUB   ";
    static logical lx[6] = { FALSE_,FALSE_,FALSE_,FALSE_,TRUE_,TRUE_ };
    static char cparal[40*6] = "INV_X_AXIS                              " 
	    "INV_Y_AXIS                              " "ENABLE_XY_MATCHING  "
	    "                    " "PROTECT_REGION                          " 
	    "DRAW_X_SUBLABEL                         " "DRAW_Y_SUBLABEL     "
	    "                    ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rllget_(char *, logical *, integer *, ftnlen), 
	    rtlget_(char *, char *, logical *, integer *, ftnlen, ftnlen);

/* ---- SHORT NAME ---- */
/*     / PARAMETERS FOR USDAXS / */
    switch(n__) {
	case 1: goto L_uslqid;
	case 2: goto L_uslqcp;
	case 3: goto L_uslqcl;
	case 4: goto L_uslqvl;
	case 5: goto L_uslsvl;
	case 6: goto L_uslqin;
	}

/*     / PARAMETERS FOR USXSUB & USYSUB / */
/* ---- LONG NAME ---- */
/*     / PARAMETERS FOR USDAXS / */
/*     / PARAMETERS FOR USXSUB & USYSUB / */
    *ncp = 6;
    return 0;
/* ----------------------------------------------------------------------- */

L_uslqid:
    for (n = 1; n <= 6; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "USLQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uslqcp:
    if (1 <= *idx && *idx <= 6) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "USLQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uslqcl:
    if (1 <= *idx && *idx <= 6) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "USLQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uslqvl:
    if (lfirst) {
	rtlget_("US", cparas, lx, &c__6, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__6, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 6) {
	*lpara = lx[*idx - 1];
    } else {
	msgdmp_("E", "USLQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uslsvl:
    if (lfirst) {
	rtlget_("US", cparas, lx, &c__6, (ftnlen)2, (ftnlen)8);
	rllget_(cparal, lx, &c__6, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 6) {
	lx[*idx - 1] = *lpara;
    } else {
	msgdmp_("E", "USLSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uslqin:
    for (n = 1; n <= 6; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uslqnp_ */

/* Subroutine */ int uslqnp_(integer *ncp)
{
    return uslqnp_0_(0, ncp, (char *)0, (integer *)0, (logical *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int uslqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uslqnp_0_(1, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uslqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uslqnp_0_(2, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uslqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uslqnp_0_(3, (integer *)0, cp, idx, (logical *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int uslqvl_(integer *idx, logical *lpara)
{
    return uslqnp_0_(4, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uslsvl_(integer *idx, logical *lpara)
{
    return uslqnp_0_(5, (integer *)0, (char *)0, idx, lpara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int uslqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uslqnp_0_(6, (integer *)0, cp, (integer *)0, (logical *)0, in, 
	    cp_len);
    }

