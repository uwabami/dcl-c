/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b22 = 0.f;
static logical c_false = FALSE_;
static real c_b76 = 9.524f;
static real c_b78 = 2.f;
static integer c__4 = 4;
static integer c__1 = 1;
static integer c__2 = 2;
static integer c__6 = 6;

/* ----------------------------------------------------------------------- */
/*     USINIT                                          DCL 5.0  95/09/04 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int usinit_(void)
{
    /* System generated locals */
    real r__1;

    /* Local variables */
    static real sizel;
    static integer iundef;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    static real offset;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static integer ireset;
    extern /* Subroutine */ int usiget_(char *, integer *, ftnlen), uzrget_(
	    char *, real *, ftnlen), usrset_(char *, real *, ftnlen), uscstx_(
	    char *, char *, ftnlen, ftnlen), usistx_(char *, integer *, 
	    ftnlen), uslstx_(char *, logical *, ftnlen), usrstx_(char *, real 
	    *, ftnlen);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    gliget_("IUNDEF", &iundef, (ftnlen)6);
    usiget_("IRESET", &ireset, (ftnlen)6);
    if (ireset < 0 || ireset > 2) {
	msgdmp_("E", "USINIT", "INVALID VALUE OF 'IRESET'.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
/* ----------------------------------------------------------------------- */
    uzrget_("RSIZEL1", &sizel, (ftnlen)7);
    offset = sizel * .86f;
    usrset_("SOFFXTR", &offset, (ftnlen)7);
    usrset_("SOFFXBR", &offset, (ftnlen)7);
    usrset_("SOFFXUR", &offset, (ftnlen)7);
    r__1 = -offset;
    usrset_("SOFFXTL", &r__1, (ftnlen)7);
    r__1 = -offset;
    usrset_("SOFFXBL", &r__1, (ftnlen)7);
    r__1 = -offset;
    usrset_("SOFFXUL", &r__1, (ftnlen)7);
    usrset_("SOFFYRT", &offset, (ftnlen)7);
    usrset_("SOFFYLT", &offset, (ftnlen)7);
    usrset_("SOFFYUT", &offset, (ftnlen)7);
    r__1 = -offset;
    usrset_("SOFFYRB", &r__1, (ftnlen)7);
    r__1 = -offset;
    usrset_("SOFFYLB", &r__1, (ftnlen)7);
    r__1 = -offset;
    usrset_("SOFFYUB", &r__1, (ftnlen)7);
    usrset_("ROFFXT", &c_b22, (ftnlen)6);
    usrset_("ROFFXB", &c_b22, (ftnlen)6);
    usrset_("ROFFYR", &c_b22, (ftnlen)6);
    usrset_("ROFFYL", &c_b22, (ftnlen)6);
    usrstx_("XOFF", &rundef, (ftnlen)4);
    usrstx_("YOFF", &rundef, (ftnlen)4);
    usrstx_("XFAC", &rundef, (ftnlen)4);
    usrstx_("YFAC", &rundef, (ftnlen)4);
    usrstx_("DXT", &rundef, (ftnlen)3);
    usrstx_("DYT", &rundef, (ftnlen)3);
    usrstx_("DXL", &rundef, (ftnlen)3);
    usrstx_("DYL", &rundef, (ftnlen)3);
    usistx_("NLBLX", &iundef, (ftnlen)5);
    usistx_("NLBLY", &iundef, (ftnlen)5);
    usistx_("NTICKSX", &iundef, (ftnlen)7);
    usistx_("NTICKSY", &iundef, (ftnlen)7);
    usistx_("ITYPEX", &iundef, (ftnlen)6);
    usistx_("ITYPEY", &iundef, (ftnlen)6);
    usrset_("XDTMIN", &rundef, (ftnlen)6);
    usrset_("XDTMAX", &rundef, (ftnlen)6);
    usrset_("YDTMIN", &rundef, (ftnlen)6);
    usrset_("YDTMAX", &rundef, (ftnlen)6);
    uscstx_("CXFMT", " ", (ftnlen)5, (ftnlen)1);
    uscstx_("CYFMT", " ", (ftnlen)5, (ftnlen)1);
    if (ireset >= 1) {
	uscstx_("CXTTL", " ", (ftnlen)5, (ftnlen)1);
	uscstx_("CYTTL", " ", (ftnlen)5, (ftnlen)1);
	uscstx_("CXUNIT", " ", (ftnlen)6, (ftnlen)1);
	uscstx_("CYUNIT", " ", (ftnlen)6, (ftnlen)1);
    }
    if (ireset >= 2) {
	uscstx_("CXSIDE", "BT", (ftnlen)6, (ftnlen)2);
	uscstx_("CYSIDE", "LR", (ftnlen)6, (ftnlen)2);
	uscstx_("CXSPOS", "R ", (ftnlen)6, (ftnlen)2);
	uscstx_("CYSPOS", "T ", (ftnlen)6, (ftnlen)2);
	uscstx_("CBLKT ", "()", (ftnlen)6, (ftnlen)2);
/* -------------------- PARAMETERS FOR USDAXS ---------------------------- */
	uslstx_("LXINV", &c_false, (ftnlen)5);
	uslstx_("LYINV", &c_false, (ftnlen)5);
	uslstx_("LMATCH", &c_false, (ftnlen)6);
	usrstx_("RMRGN", &c_b76, (ftnlen)5);
/* -------------------- PARAMETERS FOR USUSCU ---------------------------- */
	usrstx_("TFACT", &c_b78, (ftnlen)5);
	usistx_("MXDGTX", &c__4, (ftnlen)6);
	usistx_("MXDGTY", &c__4, (ftnlen)6);
	usistx_("NBLANK1", &c__1, (ftnlen)7);
	usistx_("NBLANK2", &c__2, (ftnlen)7);
/* ---------------- PARAMETERS FOR USXSUB & USYSUB ----------------------- */
	usistx_("MXDGTSX", &c__6, (ftnlen)7);
	usistx_("MXDGTSY", &c__6, (ftnlen)7);
	uslstx_("LPRTCT", &c_false, (ftnlen)6);
    }
    return 0;
} /* usinit_ */

