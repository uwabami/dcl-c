/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__1 = 1;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     UDLINE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int udline_(real *z__, integer *mx, integer *kx, integer *ky,
	 integer *kk, real *cx, logical *lopen, integer *ibr)
{
    /* System generated locals */
    integer z_dim1, z_offset;
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static integer j, k, k0, k1, j1, j2, jk[4], jn, jp, np, ix, jx[4], jy[4], 
	    iy;
    static real sx, tx[4], ty[4], ux, uy, sy;
    static integer jm1, jm2, ix0, iy0, ix1, iy1, ixd, iyd;
    static logical lnp;
    static integer jrt;
    static logical ltx[4];
    static real rtx[4];
    static logical lend;
    static real rtmin;
    extern logical ludchk_(integer *, integer *, integer *, integer *, 
	    integer *);
    extern /* Subroutine */ int udbclr_(integer *, integer *, integer *, 
	    integer *, integer *), udgrdn_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *), glrget_(char *, real 
	    *, ftnlen), szcllu_(void), szpllu_(real *, real *), szoplu_(void),
	     szmvlu_(real *, real *), uduxuy_(real *, integer *, integer *, 
	    integer *, integer *, real *, real *, real *);

/*     / MAIN / */
    /* Parameter adjustments */
    z_dim1 = *mx;
    z_offset = 1 + z_dim1;
    z__ -= z_offset;
    --ibr;

    /* Function Body */
    ix = *kx;
    iy = *ky;
    k = *kk;
    k1 = 1 - k;
    ix0 = ix;
    iy0 = iy;
    k0 = k;
    uduxuy_(&z__[z_offset], mx, &ix, &iy, &k, cx, &ux, &uy);
    szoplu_();
    szmvlu_(&ux, &uy);
    if (*lopen) {
	udbclr_(&ix, &iy, &k, &c__0, &ibr[1]);
    }
    ix1 = ix + k;
    iy1 = iy + k1;
    if (ludchk_(&ix1, &iy1, &k, &c__1, &ibr[1])) {
	udgrdn_(&c__1, &ix, &iy, &k, jx, jy, jk);
    } else {
	udgrdn_(&c__2, &ix, &iy, &k, jx, jy, jk);
    }
L30:
    np = 0;
    if (ludchk_(&jx[2], &jy[2], &jk[2], &c__1, &ibr[1])) {
	ltx[0] = TRUE_;
	for (j = 1; j <= 3; ++j) {
	    if (ludchk_(&jx[j], &jy[j], &jk[j], &c__0, &ibr[1])) {
		++np;
		ltx[j] = TRUE_;
		jn = j;
	    } else {
		ltx[j] = FALSE_;
	    }
/* L10: */
	}
	if (np == 1) {
	    jp = jn;
	    uduxuy_(&z__[z_offset], mx, &jx[jp], &jy[jp], &jk[jp], cx, &sx, &
		    sy);
	} else if (np == 2) {
	    if (ltx[0] == ltx[1]) {
		jp = 1;
	    } else {
		jp = 3;
	    }
	    uduxuy_(&z__[z_offset], mx, &jx[jp], &jy[jp], &jk[jp], cx, &sx, &
		    sy);
	} else if (np == 3) {
	    for (j = 0; j <= 3; ++j) {
		uduxuy_(&z__[z_offset], mx, &jx[j], &jy[j], &jk[j], cx, &tx[j]
			, &ty[j]);
/* L15: */
	    }
	    glrget_("REALMAX", &rtmin, (ftnlen)7);
	    for (j = 0; j <= 3; ++j) {
		j1 = j;
		j2 = (j + 1) % 4;
/* Computing 2nd power */
		r__1 = tx[j1] - tx[j2];
/* Computing 2nd power */
		r__2 = ty[j1] - ty[j2];
		rtx[j] = sqrt(r__1 * r__1 + r__2 * r__2);
		if (rtx[j] < rtmin) {
		    rtmin = rtx[j];
		    jrt = j;
		}
/* L20: */
	    }
	    jm1 = jrt;
	    jm2 = (jrt + 1) % 4;
	    ltx[jm1] = FALSE_;
	    ltx[jm2] = FALSE_;
	    if (ltx[0] == ltx[1]) {
		jp = 1;
		sx = tx[1];
		sy = ty[1];
	    } else {
		jp = 3;
		sx = tx[3];
		sy = ty[3];
	    }
	}
    }
    lnp = np == 0;
    if (! lnp) {
	szpllu_(&sx, &sy);
	ixd = jx[jp] - ix;
	iyd = jy[jp] - iy;
	ix = jx[jp];
	iy = jy[jp];
	k = jk[jp];
	udbclr_(&ix, &iy, &k, &c__0, &ibr[1]);
	if (k == 0 && iyd == 1 || k == 1 && ixd == 1) {
	    udgrdn_(&c__1, &ix, &iy, &k, jx, jy, jk);
	} else {
	    udgrdn_(&c__2, &ix, &iy, &k, jx, jy, jk);
	}
    }
    lend = ix == ix0 && iy == iy0 && k == k0;
    if (! (lnp || lend)) {
	goto L30;
    }
    szcllu_();
    return 0;
} /* udline_ */

