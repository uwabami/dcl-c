/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__2 = 2;
static integer c__5 = 5;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     USPGET / USPSET (OLD INTERFACE) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uspget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    /* System generated locals */
    address a__1[3], a__2[2], a__3[5];
    integer i__1[3], i__2[2], i__3[5];

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer ip;
    static char cx[8], cpp[12];
    static integer idx;
    static char cmsg[80];
    extern /* Subroutine */ int glpget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), sgpget_(char *, 
	    integer *, ftnlen), glpset_(char *, integer *, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), rtlget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen), uspqid_(
	    char *, integer *, ftnlen), sgpset_(char *, integer *, ftnlen), 
	    rtrget_(char *, char *, integer *, integer *, ftnlen, ftnlen), 
	    uspqvl_(integer *, integer *), uspsvl_(integer *, integer *);

/*     ---------------------- */
/*     ---------------------- */
    switch(n__) {
	case 1: goto L_usiget;
	case 2: goto L_usrget;
	case 3: goto L_uslget;
	case 4: goto L_uspset;
	case 5: goto L_usiset;
	case 6: goto L_usrset;
	case 7: goto L_uslset;
	case 8: goto L_uspstx;
	case 9: goto L_usistx;
	case 10: goto L_usrstx;
	case 11: goto L_uslstx;
	}


L_usiget:

L_usrget:

L_uslget:
    if (s_cmp(cp, "RUNDEF", cp_len, (ftnlen)6) == 0 || s_cmp(cp, "IUNDEF", 
	    cp_len, (ftnlen)6) == 0) {
	glpget_(cp, ipara, cp_len);
/* Writing concatenation */
	i__1[0] = 23, a__1[0] = "THIS IS OLD INTERFACE (";
	i__1[1] = cp_len, a__1[1] = cp;
	i__1[2] = 15, a__1[2] = ") - USE GLPGET.";
	s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
	msgdmp_("W", "USPGET", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    } else if (s_cmp(cp, "VXMIN", cp_len, (ftnlen)5) == 0 || s_cmp(cp, "VYMIN"
	    , cp_len, (ftnlen)5) == 0 || s_cmp(cp, "VXMAX", cp_len, (ftnlen)5)
	     == 0 || s_cmp(cp, "VYMAX", cp_len, (ftnlen)5) == 0 || s_cmp(cp, 
	    "ITR", cp_len, (ftnlen)3) == 0) {
	sgpget_(cp, ipara, cp_len);
/* Writing concatenation */
	i__1[0] = 23, a__1[0] = "THIS IS OLD INTERFACE (";
	i__1[1] = cp_len, a__1[1] = cp;
	i__1[2] = 15, a__1[2] = ") - USE SGPGET.";
	s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
	msgdmp_("W", "USPGET", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    } else if (s_cmp(cp, "XMIN", cp_len, (ftnlen)4) == 0 || s_cmp(cp, "YMIN", 
	    cp_len, (ftnlen)4) == 0 || s_cmp(cp, "XMAX", cp_len, (ftnlen)4) ==
	     0 || s_cmp(cp, "YMAX", cp_len, (ftnlen)4) == 0) {
/* Writing concatenation */
	i__2[0] = 1, a__2[0] = "U";
	i__2[1] = cp_len, a__2[1] = cp;
	s_cat(cpp, a__2, i__2, &c__2, (ftnlen)12);
	sgpget_(cpp, ipara, (ftnlen)12);
/* Writing concatenation */
	i__3[0] = 23, a__3[0] = "THIS IS OLD INTERFACE (";
	i__3[1] = cp_len, a__3[1] = cp;
	i__3[2] = 17, a__3[2] = ") - USE SGPGET (U";
	i__3[3] = cp_len, a__3[3] = cp;
	i__3[4] = 2, a__3[4] = ").";
	s_cat(cmsg, a__3, i__3, &c__5, (ftnlen)80);
	msgdmp_("W", "USPGET", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    } else {
	uspqid_(cp, &idx, cp_len);
	uspqvl_(&idx, ipara);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uspset:
/*     ---------------------- */

L_usiset:

L_usrset:

L_uslset:
/*     ---------------------- */
    if (s_cmp(cp, "RUNDEF", cp_len, (ftnlen)6) == 0 || s_cmp(cp, "IUNDEF", 
	    cp_len, (ftnlen)6) == 0) {
	glpset_(cp, ipara, cp_len);
/* Writing concatenation */
	i__1[0] = 23, a__1[0] = "THIS IS OLD INTERFACE (";
	i__1[1] = cp_len, a__1[1] = cp;
	i__1[2] = 15, a__1[2] = ") - USE GLPSET.";
	s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
	msgdmp_("W", "USPSET", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    } else if (s_cmp(cp, "VXMIN", cp_len, (ftnlen)5) == 0 || s_cmp(cp, "VYMIN"
	    , cp_len, (ftnlen)5) == 0 || s_cmp(cp, "VXMAX", cp_len, (ftnlen)5)
	     == 0 || s_cmp(cp, "VYMAX", cp_len, (ftnlen)5) == 0 || s_cmp(cp, 
	    "ITR", cp_len, (ftnlen)3) == 0) {
	sgpset_(cp, ipara, cp_len);
/* Writing concatenation */
	i__1[0] = 23, a__1[0] = "THIS IS OLD INTERFACE (";
	i__1[1] = cp_len, a__1[1] = cp;
	i__1[2] = 15, a__1[2] = ") - USE SGPSET.";
	s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
	msgdmp_("W", "USPSET", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    } else if (s_cmp(cp, "XMIN", cp_len, (ftnlen)4) == 0 || s_cmp(cp, "YMIN", 
	    cp_len, (ftnlen)4) == 0 || s_cmp(cp, "XMAX", cp_len, (ftnlen)4) ==
	     0 || s_cmp(cp, "YMAX", cp_len, (ftnlen)4) == 0) {
/* Writing concatenation */
	i__2[0] = 1, a__2[0] = "U";
	i__2[1] = cp_len, a__2[1] = cp;
	s_cat(cpp, a__2, i__2, &c__2, (ftnlen)12);
	sgpset_(cpp, ipara, (ftnlen)12);
/* Writing concatenation */
	i__3[0] = 23, a__3[0] = "THIS IS OLD INTERFACE (";
	i__3[1] = cp_len, a__3[1] = cp;
	i__3[2] = 17, a__3[2] = ") - USE SGPSET (U";
	i__3[3] = cp_len, a__3[3] = cp;
	i__3[4] = 2, a__3[4] = ").";
	s_cat(cmsg, a__3, i__3, &c__5, (ftnlen)80);
	msgdmp_("W", "USPSET", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    } else {
	uspqid_(cp, &idx, cp_len);
	uspsvl_(&idx, ipara);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uspstx:
/*     ---------------------- */

L_usistx:
    s_copy(cx, cp, (ftnlen)8, cp_len);
    ip = *ipara;
    rtiget_("US", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
    goto L10;


L_usrstx:
    s_copy(cx, cp, (ftnlen)8, cp_len);
    ip = *ipara;
    rtrget_("US", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
    goto L10;


L_uslstx:
    s_copy(cx, cp, (ftnlen)8, cp_len);
    ip = *ipara;
    rtlget_("US", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
/*     ---------------------- */
L10:
    uspqid_(cx, &idx, (ftnlen)8);
    uspsvl_(&idx, &ip);
    return 0;
} /* uspget_ */

/* Subroutine */ int uspget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int usiget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int usrget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(2, cp, ipara, cp_len);
    }

/* Subroutine */ int uslget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(3, cp, ipara, cp_len);
    }

/* Subroutine */ int uspset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(4, cp, ipara, cp_len);
    }

/* Subroutine */ int usiset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(5, cp, ipara, cp_len);
    }

/* Subroutine */ int usrset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(6, cp, ipara, cp_len);
    }

/* Subroutine */ int uslset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(7, cp, ipara, cp_len);
    }

/* Subroutine */ int uspstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(8, cp, ipara, cp_len);
    }

/* Subroutine */ int usistx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(9, cp, ipara, cp_len);
    }

/* Subroutine */ int usrstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(10, cp, ipara, cp_len);
    }

/* Subroutine */ int uslstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return uspget_0_(11, cp, ipara, cp_len);
    }

