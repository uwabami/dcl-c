/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     UGSGYA / UGSGYB */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ugsgya_0_(int n__, real *yp, integer *ny, real *uymin, 
	real *uymax, logical *lsety)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uwqgya_(real *, integer *), uwqgyb_(real *, real 
	    *, integer *), uwsgya_(real *, integer *), uwsgyb_(real *, real *,
	     integer *), uwqgyz_(logical *), uwsgyz_(logical *);

    /* Parameter adjustments */
    if (yp) {
	--yp;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_ugqgya;
	case 2: goto L_ugsgyb;
	case 3: goto L_ugqgyb;
	case 4: goto L_ugsgyz;
	case 5: goto L_ugqgyz;
	}

    msgdmp_("M", "UGSGYA", "THIS IS OLD INTERFACE - USE UWSGYA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgya_(&yp[1], ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugqgya:
    msgdmp_("M", "UGQGYA", "THIS IS OLD INTERFACE - USE UWQGYA !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgya_(&yp[1], ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugsgyb:
    msgdmp_("M", "UGSGYB", "THIS IS OLD INTERFACE - USE UWSGYB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgyb_(uymin, uymax, ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugqgyb:
    msgdmp_("M", "UGQGYB", "THIS IS OLD INTERFACE - USE UWQGYB !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgyb_(uymin, uymax, ny);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugsgyz:
    msgdmp_("M", "UGSGYZ", "THIS IS OLD INTERFACE - USE UWSGYZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwsgyz_(lsety);
    return 0;
/* ----------------------------------------------------------------------- */

L_ugqgyz:
    msgdmp_("M", "UGQGYZ", "THIS IS OLD INTERFACE - USE UWQGYZ !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    uwqgyz_(lsety);
    return 0;
} /* ugsgya_ */

/* Subroutine */ int ugsgya_(real *yp, integer *ny)
{
    return ugsgya_0_(0, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int ugqgya_(real *yp, integer *ny)
{
    return ugsgya_0_(1, yp, ny, (real *)0, (real *)0, (logical *)0);
    }

/* Subroutine */ int ugsgyb_(real *uymin, real *uymax, integer *ny)
{
    return ugsgya_0_(2, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int ugqgyb_(real *uymin, real *uymax, integer *ny)
{
    return ugsgya_0_(3, (real *)0, ny, uymin, uymax, (logical *)0);
    }

/* Subroutine */ int ugsgyz_(logical *lsety)
{
    return ugsgya_0_(4, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }

/* Subroutine */ int ugqgyz_(logical *lsety)
{
    return ugsgya_0_(5, (real *)0, (integer *)0, (real *)0, (real *)0, lsety);
    }

