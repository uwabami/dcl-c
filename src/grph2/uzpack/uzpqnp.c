/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     UZPQNP / UZPQID / UZPQCP / UZPQVL / UZPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*68] = "UXUSER  " "UYUSER  " "ROFFXB  " "ROFFXT  " 
	    "ROFFXU  " "ROFFYL  " "ROFFYR  " "ROFFYU  " "ROFGXB  " "ROFGXT  " 
	    "ROFGXU  " "ROFGYL  " "ROFGYR  " "ROFGYU  " "LABELXB " "LABELXT " 
	    "LABELXU " "LABELYL " "LABELYR " "LABELYU " "IROTLXB " "IROTLXT " 
	    "IROTLXU " "IROTLYL " "IROTLYR " "IROTLYU " "IROTCXB " "IROTCXT " 
	    "IROTCXU " "IROTCYL " "IROTCYR " "IROTCYU " "ICENTXB " "ICENTXT " 
	    "ICENTXU " "ICENTYL " "ICENTYR " "ICENTYU " "INDEXT0 " "INDEXT1 " 
	    "INDEXT2 " "INDEXL0 " "INDEXL1 " "INDEXL2 " "RSIZET0 " "RSIZET1 " 
	    "RSIZET2 " "RSIZEL0 " "RSIZEL1 " "RSIZEL2 " "RSIZEC0 " "RSIZEC1 " 
	    "RSIZEC2 " "LOFFSET " "XOFFSET " "YOFFSET " "XFACT   " "YFACT   " 
	    "PAD1    " "PAD2    " "IFLAG   " "LBTWN   " "RBTWN   " "LBOUND  " 
	    "LBMSG   " "INNER   " "IUNDEF  " "RUNDEF  ";
    static integer itype[68] = { 3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,1,1,
	    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,2,3,
	    3,3,3,3,3,1,2,3,2,2,1,1,3 };
    static char cparal[40*68] = "++++UXUSER                              " 
	    "++++UYUSER                              " "****ROFFXB          "
	    "                    " "****ROFFXT                              " 
	    "****ROFFXU                              " "****ROFFYL          "
	    "                    " "****ROFFYR                              " 
	    "****ROFFYU                              " "****ROFGXB          "
	    "                    " "****ROFGXT                              " 
	    "****ROFGXU                              " "****ROFGYL          "
	    "                    " "****ROFGYR                              " 
	    "****ROFGYU                              " "DRAW_BOTTOM_LABEL   "
	    "                    " "DRAW_TOP_LABEL                          " 
	    "DRAW_HORIZONTAL_LABEL                   " "DRAW_LEFT_LABEL     "
	    "                    " "DRAW_RIGHT_LABEL                        " 
	    "DRAW_VERTICAL_LABEL                     " "BOTTOM_LABAL_ANGLE  "
	    "                    " "TOP_LABEL_ANGLE                         " 
	    "HORIZONTAL_LABEL_ANGLE                  " "LEFT_LABEL_ANGLE    "
	    "                    " "RIGHT_LABEL_ANGLE                       " 
	    "VERTICAL_LABEL_ANGLE                    " "BOTTOM_TITLE_ANGLE  "
	    "                    " "TOP_TITLE_ANGLE                         " 
	    "HORIZONTAL_TITLE_ANGLE                  " "LEFT_TITLE_ANGLE    "
	    "                    " "RIGHT_TITLE_ANGLE                       " 
	    "VERTICAL_TITLE_ANGLE                    " "BOTTOM_LABEL_CENTERI"
	    "NG                  " "TOP_LABEL_CENTERING                     " 
	    "HORIZONTAL_LABEL_CENTERING              " "LEFT_LABEL_CENTERING"
	    "                    " "RIGHT_LABEL_CENTERING                   " 
	    "VERTICAL_LABEL_CENTERING                " "AXIS_LINE_INDEX0    "
	    "                    " "AXIS_LINE_INDEX1                        " 
	    "AXIS_LINE_INDEX2                        " "LABEL_INDEX0        "
	    "                    " "LABEL_INDEX1                            " 
	    "LABEL_INDEX2                            " "TICK_LENGTH0        "
	    "                    " "TICK_LENGTH1                            " 
	    "TICK_LENGTH2                            " "LABEL_HEIGHT0       "
	    "                    " "LABEL_HEIGHT1                           " 
	    "LABEL_HEIGHT2                           " "TITLE_HEIGHT0       "
	    "                    " "TITLE_HEIGHT1                           " 
	    "TITLE_HEIGHT2                           " "LINEAR_OFFSET       "
	    "                    " "X_AXIS_OFFSET                           " 
	    "Y_AXIS_OFFSET                           " "X_AXIS_FACTOR       "
	    "                    " "Y_AXIS_FACTOR                           " 
	    "****PAD1                                " "****PAD2            "
	    "                    " "LABEL_SIDE_FOR_USER_AXIS                " 
	    "ENABLE_SPAN_LABELING                    " "SPAN_LABELING_CENTER"
	    "ING                 " "TITLE_OVER_VIEWPORT                     " 
	    "TITLE_OVER_VIEWPORT_MESSAGE             " "TICKMARK_SIDE       "
	    "                    " "----IUNDEF                              " 
	    "----RUNDEF                              ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), uziqid_(char *, integer *, ftnlen), uzlqid_(char 
	    *, integer *, ftnlen), uzrqid_(char *, integer *, ftnlen), 
	    uziqvl_(integer *, integer *), uzlqvl_(integer *, integer *), 
	    uzisvl_(integer *, integer *), uzlsvl_(integer *, integer *), 
	    uzrqvl_(integer *, integer *), uzrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uzpqid;
	case 2: goto L_uzpqcp;
	case 3: goto L_uzpqcl;
	case 4: goto L_uzpqit;
	case 5: goto L_uzpqvl;
	case 6: goto L_uzpsvl;
	case 7: goto L_uzpqin;
	}

/*     / LONG NAME / */
    *ncp = 68;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpqid:
    for (n = 1; n <= 68; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UZPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpqcp:
    if (1 <= *idx && *idx <= 68) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UZPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpqcl:
    if (1 <= *idx && *idx <= 68) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UZPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpqit:
    if (1 <= *idx && *idx <= 68) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "UZPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpqvl:
    if (1 <= *idx && *idx <= 68) {
	if (itype[*idx - 1] == 1) {
	    uziqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uziqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uzlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uzlqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    uzrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uzrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UZPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpsvl:
    if (1 <= *idx && *idx <= 68) {
	if (itype[*idx - 1] == 1) {
	    uziqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uzisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    uzlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uzlsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    uzrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    uzrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "UZPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzpqin:
    for (n = 1; n <= 68; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* uzpqnp_ */

/* Subroutine */ int uzpqnp_(integer *ncp)
{
    return uzpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uzpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzpqit_(integer *idx, integer *itp)
{
    return uzpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzpqvl_(integer *idx, integer *ipara)
{
    return uzpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzpsvl_(integer *idx, integer *ipara)
{
    return uzpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uzpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }

