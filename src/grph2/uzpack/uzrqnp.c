/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__31 = 31;

/* ----------------------------------------------------------------------- */
/*     UZRQNP / UZRQID / UZRQCP / UZRQVL / UZRSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int uzrqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	real *rpara, integer *in, integer *iu, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*31] = "UXUSER  " "UYUSER  " "ROFFXB  " "ROFFXT  " 
	    "ROFFXU  " "ROFFYL  " "ROFFYR  " "ROFFYU  " "ROFGXB  " "ROFGXT  " 
	    "ROFGXU  " "ROFGYL  " "ROFGYR  " "ROFGYU  " "RSIZET0 " "RSIZET1 " 
	    "RSIZET2 " "RSIZEL0 " "RSIZEL1 " "RSIZEL2 " "RSIZEC0 " "RSIZEC1 " 
	    "RSIZEC2 " "XOFFSET " "YOFFSET " "XFACT   " "YFACT   " "PAD1    " 
	    "PAD2    " "RBTWN   " "RUNDEF  ";
    static real rx[31] = { -999.f,-999.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,0.f,
	    0.f,0.f,0.f,-999.f,.007f,.014f,-999.f,.021f,.028f,-999.f,.028f,
	    .035f,0.f,0.f,1.f,1.f,.7f,1.5f,0.f,-999.f };
    static char cparal[40*31] = "X_INTERSECTION                          " 
	    "Y_INTERSECTION                          " "****ROFFXB          "
	    "                    " "****ROFFXT                              " 
	    "****ROFFXU                              " "****ROFFYL          "
	    "                    " "****ROFFYR                              " 
	    "****ROFFYU                              " "****ROFGXB          "
	    "                    " "****ROFGXT                              " 
	    "****ROFGXU                              " "****ROFGYL          "
	    "                    " "****ROFGYR                              " 
	    "****ROFGYU                              " "TICK_LENGTH0        "
	    "                    " "TICK_LENGTH1                            " 
	    "TICK_LENGTH2                            " "LABEL_HEIGHT0       "
	    "                    " "LABEL_HEIGHT1                           " 
	    "LABEL_HEIGHT2                           " "TITLE_HEIGHT0       "
	    "                    " "TITLE_HEIGHT1                           " 
	    "TITLE_HEIGHT2                           " "X_AXIS_OFFSET       "
	    "                    " "Y_AXIS_OFFSET                           " 
	    "X_AXIS_FACTOR                           " "Y_AXIS_FACTOR       "
	    "                    " "****PAD1                                " 
	    "****PAD2                                " "SPAN_LABELING_CENTER"
	    "ING                 " "----RUNDEF                              ";
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsue(cilist *), do_uio(integer *, char *, ftnlen), e_wsue(void),
	     s_rsue(cilist *), e_rsue(void);

    /* Local variables */
    static integer n, ios;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), rlrget_(char *, real *, integer *, ftnlen), 
	    rtrget_(char *, char *, real *, integer *, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___8 = { 1, 0, 0, 0, 0 };
    static cilist io___9 = { 1, 0, 1, 0, 0 };


/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_uzrqid;
	case 2: goto L_uzrqcp;
	case 3: goto L_uzrqcl;
	case 4: goto L_uzrqvl;
	case 5: goto L_uzrsvl;
	case 6: goto L_uzrqin;
	case 7: goto L_uzrsav;
	case 8: goto L_uzrrst;
	}

/*     / LONG NAME / */
    *ncp = 31;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrqid:
    for (n = 1; n <= 31; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "UZRQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrqcp:
    if (1 <= *idx && *idx <= 31) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "UZRQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrqcl:
    if (1 <= *idx && *idx <= 31) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "UZRQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrqvl:
    if (lfirst) {
	rtrget_("UZ", cparas, rx, &c__31, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__31, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 31) {
	*rpara = rx[*idx - 1];
    } else {
	msgdmp_("E", "UZRQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrsvl:
    if (lfirst) {
	rtrget_("UZ", cparas, rx, &c__31, (ftnlen)2, (ftnlen)8);
	rlrget_(cparal, rx, &c__31, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 31) {
	rx[*idx - 1] = *rpara;
    } else {
	msgdmp_("E", "UZRSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrqin:
    for (n = 1; n <= 31; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrsav:
    io___8.ciunit = *iu;
    ios = s_wsue(&io___8);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__31, (char *)&rx[0], (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    ios = e_wsue();
L100001:
    if (ios != 0) {
	msgdmp_("E", "UZRSAV", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_uzrrst:
    io___9.ciunit = *iu;
    ios = s_rsue(&io___9);
    if (ios != 0) {
	goto L100002;
    }
    ios = do_uio(&c__31, (char *)&rx[0], (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100002;
    }
    ios = e_rsue();
L100002:
    if (ios != 0) {
	msgdmp_("E", "UZRRST", "IOSTAT IS NOT ZERO.", (ftnlen)1, (ftnlen)6, (
		ftnlen)19);
    }
    return 0;
} /* uzrqnp_ */

/* Subroutine */ int uzrqnp_(integer *ncp)
{
    return uzrqnp_0_(0, ncp, (char *)0, (integer *)0, (real *)0, (integer *)0,
	     (integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzrqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return uzrqnp_0_(1, (integer *)0, cp, idx, (real *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzrqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzrqnp_0_(2, (integer *)0, cp, idx, (real *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzrqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return uzrqnp_0_(3, (integer *)0, cp, idx, (real *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzrqvl_(integer *idx, real *rpara)
{
    return uzrqnp_0_(4, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzrsvl_(integer *idx, real *rpara)
{
    return uzrqnp_0_(5, (integer *)0, (char *)0, idx, rpara, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int uzrqin_(char *cp, integer *in, ftnlen cp_len)
{
    return uzrqnp_0_(6, (integer *)0, cp, (integer *)0, (real *)0, in, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int uzrsav_(integer *iu)
{
    return uzrqnp_0_(7, (integer *)0, (char *)0, (integer *)0, (real *)0, (
	    integer *)0, iu, (ftnint)0);
    }

/* Subroutine */ int uzrrst_(integer *iu)
{
    return uzrqnp_0_(8, (integer *)0, (char *)0, (integer *)0, (real *)0, (
	    integer *)0, iu, (ftnint)0);
    }

