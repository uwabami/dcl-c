/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer itr;
    real rundef;
    integer iundef;
    real pi, cpr, cpd, cp;
} umwk1_;

#define umwk1_1 umwk1_

/* ----------------------------------------------------------------------- */
/*     UMSCOM : COMMON BLOCK SETTING */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umscom_(void)
{
    static logical ldeg;
    extern real rfpi_(void);
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), glrget_(
	    char *, real *, ftnlen), sglget_(char *, logical *, ftnlen), 
	    msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), sgqtrn_(
	    integer *);

    glrget_("RUNDEF", &umwk1_1.rundef, (ftnlen)6);
    gliget_("IUNDEF", &umwk1_1.iundef, (ftnlen)6);
    sglget_("LDEG", &ldeg, (ftnlen)4);
    umwk1_1.pi = rfpi_();
    umwk1_1.cp = umwk1_1.pi / 180;
    if (ldeg) {
	umwk1_1.cpd = 1.f;
	umwk1_1.cpr = 1 / umwk1_1.cp;
    } else {
	umwk1_1.cpd = umwk1_1.cp;
	umwk1_1.cpr = 1.f;
    }
/* ----------------------------- ITR (CHECK)------------------------------ */
    sgqtrn_(&umwk1_1.itr);
    if (! (10 <= umwk1_1.itr && umwk1_1.itr <= 24 || 30 <= umwk1_1.itr && 
	    umwk1_1.itr <= 34)) {
	msgdmp_("E", "UMSCOM", "INVALID TRANSFORMATION NUMBER.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    return 0;
} /* umscom_ */

