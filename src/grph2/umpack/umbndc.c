/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umbndc_(real *xmin, real *xmax, real *ymin, real *ymax)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double cos(doublereal), sin(doublereal), acos(doublereal), r_sign(real *, 
	    real *);

    /* Local variables */
    static integer i__;
    static real r__, dt, pi, th, rr, tx, ty, vx, vy, xx, psi, txc, uxc, uyc, 
	    tyc, xlm, psi0;
    extern real rfpi_(void);
    static real xcntr, ycntr, rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), stfrad_(real 
	    *, real *, real *, real *), umqcwd_(real *, real *, real *);
    extern real xmplon_(real *);
    extern /* Subroutine */ int stftrn_(real *, real *, real *, real *), 
	    stfrot_(real *, real *, real *, real *);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    umqcwd_(&xcntr, &ycntr, &r__);
    if (xcntr == rundef || ycntr == rundef || r__ == rundef) {
	return 0;
    }
    stfrad_(&r__, &r__, &rr, &rr);
    stfrad_(&xcntr, &ycntr, &uxc, &uyc);
    stfrot_(&uxc, &uyc, &txc, &tyc);
    if (*xmin == rundef) {
	stftrn_(&txc, &tyc, xmin, ymin);
	*xmax = *xmin;
	*ymax = *ymin;
    }
    pi = rfpi_();
    psi0 = pi / 2.f - tyc;
    dt = pi * 2 / 360;
    for (i__ = 1; i__ <= 360; ++i__) {
	th = dt * i__ - pi;
	xx = cos(psi0) * cos(rr) + sin(psi0) * sin(rr) * cos(th);
	if (xx >= 1.f) {
	    psi = 0.f;
	} else if (xx <= -1.f) {
	    psi = pi;
	} else {
	    psi = acos(xx);
	}
	if (psi0 == 0.f || psi0 == pi) {
	    xlm = th;
	} else if (psi == 0.f || psi == pi) {
	    xlm = 0.f;
	} else {
	    xx = (cos(rr) - cos(psi) * cos(psi0)) / (sin(psi) * sin(psi0));
	    if (xx >= 1.f) {
		xlm = 0.f;
	    } else if (xx <= -1.f) {
		xlm = pi;
	    } else {
		xlm = acos(xx);
	    }
	    xlm = r_sign(&xlm, &th);
	}
	r__1 = txc + xlm;
	tx = xmplon_(&r__1);
	ty = pi / 2.f - psi;
	stftrn_(&tx, &ty, &vx, &vy);
	*xmax = max(*xmax,vx);
	*xmin = min(*xmin,vx);
	*ymax = max(*ymax,vy);
	*ymin = min(*ymin,vy);
/* L10: */
    }
    return 0;
} /* umbndc_ */

