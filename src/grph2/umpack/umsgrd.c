/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     DETERMINE GRID SPACING */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umsgrd_(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static real r__, vr, vx1, vy1, vx2, vy2, uxc, uyc, dgrmj, dgrmn, vxmin, 
	    vymin, vxmax, vymax, rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    static real dgrmjx;
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen);
    static real dgrmnx;
    extern /* Subroutine */ int umrget_(char *, real *, ftnlen), stftrf_(real 
	    *, real *, real *, real *), stitrf_(real *, real *, real *, real *
	    ), umrset_(char *, real *, ftnlen), sgqvpt_(real *, real *, real *
	    , real *);

    umrget_("DGRIDMJ", &dgrmjx, (ftnlen)7);
    umrget_("DGRIDMN", &dgrmnx, (ftnlen)7);
    glrget_("RUNDEF", &rundef, (ftnlen)6);
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    r__1 = (vxmin + vxmax) / 2.f;
    r__2 = (vymin + vymax) / 2.f;
    stitrf_(&r__1, &r__2, &uxc, &uyc);
/* Computing 2nd power */
    r__1 = vxmax - vxmin;
/* Computing 2nd power */
    r__2 = vymax - vymin;
    vr = sqrt(r__1 * r__1 + r__2 * r__2) / sqrt(2.f);
    if (uxc != rundef && uyc != rundef && uyc < 89.f && uyc > -89.f) {
	r__1 = uxc - .5f;
	r__2 = uyc - .5f;
	stftrf_(&r__1, &r__2, &vx1, &vy1);
	r__1 = uxc + .5f;
	r__2 = uyc + .5f;
	stftrf_(&r__1, &r__2, &vx2, &vy2);
/* Computing 2nd power */
	r__1 = vx2 - vx1;
/* Computing 2nd power */
	r__2 = vy2 - vy1;
	r__ = sqrt(r__1 * r__1 + r__2 * r__2) / vr / .01234f;
    } else {
	sgrget_("SIMFAC", &r__, (ftnlen)6);
	r__ /= vr;
    }
    if (r__ <= 1.f) {
	dgrmj = 90.f;
	dgrmn = 30.f;
    } else if (r__ <= 2.f) {
	dgrmj = 45.f;
	dgrmn = 15.f;
    } else if (r__ <= 3.f) {
	dgrmj = 30.f;
	dgrmn = 10.f;
    } else if (r__ <= 4.f) {
	dgrmj = 20.f;
	dgrmn = 5.f;
    } else if (r__ <= 8.f) {
	dgrmj = 10.f;
	dgrmn = 2.f;
    } else if (r__ <= 15.f) {
	dgrmj = 5.f;
	dgrmn = 1.f;
    } else if (r__ <= 30.f) {
	dgrmj = 2.f;
	dgrmn = .5f;
    } else {
	dgrmj = 1.f;
	dgrmn = .2f;
    }
    if (dgrmjx == rundef) {
	umrset_("DGRIDMJ", &dgrmj, (ftnlen)7);
    }
    if (dgrmnx == rundef) {
	umrset_("DGRIDMN", &dgrmn, (ftnlen)7);
    }
    return 0;
} /* umsgrd_ */

