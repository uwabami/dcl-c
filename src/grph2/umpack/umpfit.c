/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b5 = 1.f;
static real c_b6 = 0.f;

/* ----------------------------------------------------------------------- */
/*     SET MAPPING PARAMETERS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umpfit_(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Local variables */
    static real wx, wy, rsat, xmin, ymin, xmax, ymax, vxoff, vyoff, vxmin, 
	    vymin, vxmax, vymax;
    static logical lglobe;
    extern /* Subroutine */ int umbndc_(real *, real *, real *, real *);
    static real rundef;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), umbndp_(real 
	    *, real *, real *, real *);
    static real radius;
    extern /* Subroutine */ int umbndr_(U_fp, S_fp, real *, real *, real *, 
	    real *), sgrget_(char *, real *, ftnlen), umlget_(char *, logical 
	    *, ftnlen), umspdf_(void);
    extern /* Subroutine */ int sgqwnd_();
    extern /* Subroutine */ int umscom_(void), sgqsim_(real *, real *, real *)
	    , sgrset_(char *, real *, ftnlen);
    extern /* Subroutine */ int stftrf_();
    extern /* Subroutine */ int sgssim_(real *, real *, real *), sgstrf_(void)
	    , umspct_(void), umspcw_(void), umspwd_(void);
    extern /* Subroutine */ int stftrn_();
    extern /* Subroutine */ int sgqvpt_(real *, real *, real *, real *), 
	    sgsvpt_(real *, real *, real *, real *), umsppt_(void);
    static real txminz, tyminz, vxminz, vyminz, vxmaxz, vymaxz, txmaxz, 
	    tymaxz;
    extern /* Subroutine */ int umqtxy_(real *, real *, real *, real *), 
	    umstvz_(void);

    glrget_("RUNDEF", &rundef, (ftnlen)6);
    umscom_();
    sgqvpt_(&vxmin, &vxmax, &vymin, &vymax);
    sgqsim_(&radius, &vxoff, &vyoff);
/* ----------------------- SET EARTH'S POSITION--------------------------- */
    umspct_();
    umspcw_();
    umspwd_();
    umsppt_();
    umspdf_();
    sgrget_("RSAT", &rsat, (ftnlen)4);
    if (rsat == rundef) {
	rsat = 0.f;
    }
    sgrset_("RSAT", &rsat, (ftnlen)4);
/* --------------------- SET TENTATIVE VIEW PORT-------------------------- */
    umstvz_();
    sgssim_(&c_b5, &c_b6, &c_b6);
    sgstrf_();
/* --------------------------- RESCALING --------------------------------- */
    umlget_("LGLOBE", &lglobe, (ftnlen)6);
    sgqvpt_(&vxminz, &vxmaxz, &vyminz, &vymaxz);
    umqtxy_(&txminz, &txmaxz, &tyminz, &tymaxz);
    xmin = rundef;
    if (! lglobe) {
	umbndc_(&xmin, &xmax, &ymin, &ymax);
	umbndr_((U_fp)stftrf_, (S_fp)sgqwnd_, &xmin, &xmax, &ymin, &ymax);
	umbndp_(&xmin, &xmax, &ymin, &ymax);
    }
    if (xmin == rundef) {
	umbndr_((U_fp)stftrn_, (S_fp)umqtxy_, &xmin, &xmax, &ymin, &ymax);
    }
/* ----------------------------------------------------------------------- */
    if (radius == rundef) {
/* Computing MIN */
	r__1 = (vxmaxz - vxminz) / (xmax - xmin), r__2 = (vymaxz - vyminz) / (
		ymax - ymin);
	radius = min(r__1,r__2);
    }
    if (vxoff == rundef) {
	vxoff = (vxmaxz + vxminz - (xmax + xmin)) / 2.f * radius;
    }
    if (vyoff == rundef) {
	vyoff = (vymaxz + vyminz - (ymax + ymin)) / 2.f * radius;
    }
    sgssim_(&radius, &vxoff, &vyoff);
/* ------------------------- RESET VIEW PORT ----------------------------- */
    wx = (xmax - xmin) * radius;
    wy = (ymax - ymin) * radius;
    if (vxmin == rundef && vxmax == rundef) {
	vxmin = (vxminz + vxmaxz - wx) / 2.f;
	vxmax = (vxminz + vxmaxz + wx) / 2.f;
    } else if (vxmin == rundef) {
	vxmin = vxmax - wx;
    } else if (vxmax == rundef) {
	vxmax = vxmin + wx;
    }
    if (vymin == rundef && vymax == rundef) {
	vymin = (vyminz + vymaxz - wy) / 2.f;
	vymax = (vyminz + vymaxz + wy) / 2.f;
    } else if (vymin == rundef) {
	vymin = vymax - wy;
    } else if (vymax == rundef) {
	vymax = vymin + wy;
    }
    sgsvpt_(&vxmin, &vxmax, &vymin, &vymax);
    return 0;
} /* umpfit_ */

