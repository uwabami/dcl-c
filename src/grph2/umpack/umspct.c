/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer itr;
    real rundef;
    integer iundef;
    real pi, cpr, cpd, cp;
} umwk1_;

#define umwk1_1 umwk1_

/* ----------------------------------------------------------------------- */
/*     BASIC ROUTINES */
/* ----------------------------------------------------------------------- */
/*     UMSPCT : CONTACT POINT MODE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umspct_(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sin(doublereal), cos(doublereal), acos(doublereal), r_sign(real *, 
	    real *);

    /* Local variables */
    static real a, b, c__, yc, st1, st2, plx, ply, rot, beta, alph, rotz, 
	    xctct, yctct, plrot;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgrget_(char *, real *, ftnlen), sgqmpl_(real *, 
	    real *, real *), sgsmpl_(real *, real *, real *), umqcnt_(real *, 
	    real *, real *), sgrset_(char *, real *, ftnlen);
    static real xctctz, yctctz;

    sgqmpl_(&plx, &ply, &plrot);
    if (plx != umwk1_1.rundef && ply != umwk1_1.rundef && plrot != 
	    umwk1_1.rundef) {
	return 0;
    }
    umqcnt_(&xctct, &yctct, &rot);
    if (xctct == umwk1_1.rundef || yctct == umwk1_1.rundef || rot == 
	    umwk1_1.rundef) {
	return 0;
    }
    xctctz = xctct / umwk1_1.cpr;
    yctctz = yctct / umwk1_1.cpr;
    rotz = rot / umwk1_1.cpr;
/* --------------------- CYLINDRICAL PROJECTION -------------------------- */
    if (10 <= umwk1_1.itr && umwk1_1.itr <= 19) {
	if (rotz != 0.f) {
	    a = umwk1_1.pi / 2.f - yctctz;
	    c__ = acos(sin(a) * cos(rotz));
	    r__1 = acos(cos(a) / sin(c__));
	    alph = r_sign(&r__1, &rotz);
	    r__1 = acos(-cos(alph) * cos(rotz));
	    beta = r_sign(&r__1, &rotz);
	    plx = (xctctz - beta) * umwk1_1.cpr;
	    ply = (umwk1_1.pi / 2.f - c__) * umwk1_1.cpr;
	    plrot = (umwk1_1.pi - alph) * umwk1_1.cpr;
	} else {
	    ply = umwk1_1.pi / 2.f * umwk1_1.cpr - abs(yctct);
	    if (yctct <= 0.f) {
		plx = xctct;
		plrot = 0.f;
	    } else {
		plx = xctct + umwk1_1.pi * umwk1_1.cpr;
		plrot = -umwk1_1.pi * umwk1_1.cpr;
	    }
	}
/* ----------------------- CONICAL PROJECTION ---------------------------- */
    } else if (20 <= umwk1_1.itr && umwk1_1.itr <= 24) {
	sgrget_("STLAT1", &st1, (ftnlen)6);
	if (umwk1_1.itr == 22) {
	    sgrget_("STLAT2", &st2, (ftnlen)6);
	    if (st1 == umwk1_1.rundef) {
/* Computing MAX */
		r__1 = yctct - umwk1_1.cpd * 5.f, r__2 = -umwk1_1.pi / 2.f;
		st1 = max(r__1,r__2);
		sgrset_("STLAT1", &st1, (ftnlen)6);
	    }
	    if (st2 == umwk1_1.rundef) {
/* Computing MIN */
		r__1 = yctct + umwk1_1.cpd * 5.f, r__2 = umwk1_1.pi / 2.f;
		st2 = min(r__1,r__2);
		sgrset_("STLAT2", &st2, (ftnlen)6);
	    }
	    yc = (st1 + st2) / 2.f / umwk1_1.cpr;
	} else {
	    if (st1 == umwk1_1.rundef) {
		st1 = yctct;
		sgrset_("STLAT1", &st1, (ftnlen)6);
	    }
	    yc = st1 / umwk1_1.cpr;
	}
	if (yc == 0.f) {
	    msgdmp_("E", "UMSCNT", "INVALID STANDARD LATITUDE", (ftnlen)1, (
		    ftnlen)6, (ftnlen)25);
	}
	if (rot != 0.f) {
	    a = umwk1_1.pi / 2.f - yctctz;
	    b = umwk1_1.pi / 2.f - yc;
	    c__ = acos(cos(a) * cos(b) + sin(a) * sin(b) * cos(rotz));
	    r__1 = acos((cos(a) - cos(c__) * cos(b)) / (sin(c__) * sin(b)));
	    alph = r_sign(&r__1, &rotz);
	    r__1 = acos((cos(b) - cos(c__) * cos(a)) / (sin(c__) * sin(a)));
	    beta = r_sign(&r__1, &rotz);
	    plx = (xctctz - beta) * umwk1_1.cpr;
	    ply = (umwk1_1.pi / 2.f - c__) * umwk1_1.cpr;
	    plrot = (umwk1_1.pi - alph) * umwk1_1.cpr;
	} else {
	    plx = xctct;
	    ply = umwk1_1.cpd * 90.f;
	    plrot = umwk1_1.cpd * 0.f;
	}
/* ---------------------- AZIMUTHAL PROJECTION --------------------------- */
    } else if (30 <= umwk1_1.itr && umwk1_1.itr <= 34) {
	plx = xctct;
	ply = yctct;
	plrot = rot;
    } else {
	msgdmp_("E", "UMSCNT", "INVALID ITR", (ftnlen)1, (ftnlen)6, (ftnlen)
		11);
    }
    sgsmpl_(&plx, &ply, &plrot);
    return 0;
} /* umspct_ */

