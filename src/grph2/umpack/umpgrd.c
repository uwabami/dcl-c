/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     DRAW LAT-LON GRIDS OF MAP */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umpgrd_(void)
{
    /* System generated locals */
    integer i__1;
    real r__1;

    /* Builtin functions */
    integer i_nint(real *);

    /* Local variables */
    static integer m, n;
    static real x[4], y[4];
    static integer m1, m2;
    static real rn;
    static integer nmj, nmn, ndx;
    static real dgmj;
    static integer idmj;
    static real dgmn;
    static integer idmn;
    static real dpmj, dpmn;
    extern logical lrne_(real *, real *);
    static integer itmj;
    extern integer irgt_(real *);
    static integer itmn;
    extern integer irlt_(real *);
    static logical lepsl;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen);
    static logical lgrdmj, lgcint, lgrdmn;
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), umiget_(char *, 
	    integer *, ftnlen), gllset_(char *, logical *, ftnlen), umlget_(
	    char *, logical *, ftnlen);
    static logical llnint;
    extern /* Subroutine */ int umsgrd_(void), sglset_(char *, logical *, 
	    ftnlen), umrget_(char *, real *, ftnlen), szplcl_(void), szplop_(
	    integer *, integer *), szplzu_(integer *, real *, real *);

/*     / GET INTERNAL PARAMETERS / */
    gllget_("LEPSL", &lepsl, (ftnlen)5);
    sglget_("LLNINT", &llnint, (ftnlen)6);
    sglget_("LGCINT", &lgcint, (ftnlen)6);
    umsgrd_();
    umrget_("DGRIDMJ", &dgmj, (ftnlen)7);
    umrget_("DGRIDMN", &dgmn, (ftnlen)7);
    umrget_("DGRPLMJ", &dpmj, (ftnlen)7);
    umrget_("DGRPLMN", &dpmn, (ftnlen)7);
    umlget_("LGRIDMJ", &lgrdmj, (ftnlen)7);
    umlget_("LGRIDMN", &lgrdmn, (ftnlen)7);
    umiget_("INDEXMJ", &idmj, (ftnlen)7);
    umiget_("INDEXMN", &idmn, (ftnlen)7);
    umiget_("ITYPEMJ", &itmj, (ftnlen)7);
    umiget_("ITYPEMN", &itmn, (ftnlen)7);
    if (! lgrdmj && ! lgrdmn) {
	return 0;
    }
/*     / SET INTERNAL PARAMETER / */
    gllset_("LEPSL", &c_true, (ftnlen)5);
    sglset_("LLNINT", &c_true, (ftnlen)6);
    sglset_("LGCINT", &c_true, (ftnlen)6);
/*     / MAJOR LINES / */
    if (lgrdmj) {
	rn = 360 / dgmj;
	nmj = i_nint(&rn);
	r__1 = (real) nmj;
	if (lrne_(&rn, &r__1)) {
	    msgdmp_("E", "UMPGRD", "MAJOR DIVISION IS NOT A COMMON MEASURE O"
		    "F 360.", (ftnlen)1, (ftnlen)6, (ftnlen)46);
	}
	szplop_(&itmj, &idmj);
	i__1 = nmj;
	for (m = 1; m <= i__1; ++m) {
	    for (n = 1; n <= 4; ++n) {
		x[n - 1] = (m - 1) * dgmj - 180.f;
		y[n - 1] = (n - 1) * (180.f - dpmj * 2) / 3 - (90.f - dpmj);
/* L10: */
	    }
	    szplzu_(&c__4, x, y);
/* L20: */
	}
	r__1 = -90 / dgmj;
	m1 = irgt_(&r__1);
	r__1 = 90 / dgmj;
	m2 = irlt_(&r__1);
	i__1 = m2;
	for (m = m1; m <= i__1; ++m) {
	    for (n = 1; n <= 4; ++n) {
		x[n - 1] = (n - 1) * 360.f / 3 - 180.f;
		y[n - 1] = dgmj * m;
/* L30: */
	    }
	    szplzu_(&c__4, x, y);
/* L40: */
	}
	szplcl_();
    }
/*     / MINOR LINES / */
    if (lgrdmn) {
	rn = 360 / dgmn;
	nmn = i_nint(&rn);
	r__1 = (real) nmn;
	if (lrne_(&rn, &r__1)) {
	    msgdmp_("E", "UMPGRD", "MINOR DIVISION IS NOT A COMMON MEASURE O"
		    "F 360.", (ftnlen)1, (ftnlen)6, (ftnlen)46);
	}
	if (lgrdmj) {
	    if (nmn % nmj != 0) {
		msgdmp_("E", "UMPGRD", "MAJOR DIVISION IS NOT MULTIPLE OF MI"
			"NOR DIVISION.", (ftnlen)1, (ftnlen)6, (ftnlen)49);
	    }
	    ndx = nmn / nmj;
	} else {
	    ndx = 1;
	}
	szplop_(&itmn, &idmn);
	i__1 = nmn;
	for (m = 1; m <= i__1; ++m) {
	    if (lgrdmj && (m - 1) % ndx == 0) {
		goto L60;
	    }
	    for (n = 1; n <= 4; ++n) {
		x[n - 1] = (m - 1) * dgmn - 180.f;
		y[n - 1] = (n - 1) * (180.f - dpmn * 2) / 3 - (90.f - dpmn);
/* L50: */
	    }
	    szplzu_(&c__4, x, y);
L60:
	    ;
	}
	r__1 = -90 / dgmn;
	m1 = irgt_(&r__1);
	r__1 = 90 / dgmn;
	m2 = irlt_(&r__1);
	i__1 = m2;
	for (m = m1; m <= i__1; ++m) {
	    if (lgrdmj && m % ndx == 0) {
		goto L80;
	    }
	    for (n = 1; n <= 4; ++n) {
		x[n - 1] = (n - 1) * 360.f / 3 - 180.f;
		y[n - 1] = dgmn * m;
/* L70: */
	    }
	    szplzu_(&c__4, x, y);
L80:
	    ;
	}
	szplcl_();
    }
/*     / RESET INTERNAL PARAMETER / */
    gllset_("LEPSL", &lepsl, (ftnlen)5);
    sglset_("LLNINT", &llnint, (ftnlen)6);
    sglset_("LGCINT", &lgcint, (ftnlen)6);
    return 0;
} /* umpgrd_ */

