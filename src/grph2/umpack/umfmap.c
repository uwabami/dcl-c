/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__2 = 2;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     FILL PATTERN ON MAP (GEOGRAPHIC COORDINATE) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int umfmap_(char *cdsn, ftnlen cdsn_len)
{
    /* System generated locals */
    address a__1[2];
    integer i__1[2], i__2;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer f_open(olist *), f_rew(alist *), s_rsue(cilist *), do_uio(integer 
	    *, char *, ftnlen), e_rsue(void), f_clos(cllist *);

    /* Local variables */
    static integer i__, iu, ios, igid;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    static real xlat[8192], xlon[8192];
    static integer npts;
    static char cdsnx[1024];
    static integer ibgcli;
    static logical lflake;
    static integer iplake, ipland;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), umiget_(char *, 
	    integer *, ftnlen), umlget_(char *, logical *, ftnlen), sgiset_(
	    char *, integer *, ftnlen);
    extern integer iufopn_(void);
    extern /* Subroutine */ int umqfnm_(char *, char *, ftnlen, ftnlen);
    static real xlatmn, xlonmn, xlatmx, xlonmx;
    extern /* Subroutine */ int sgtnzu_(integer *, real *, real *, integer *);

    /* Fortran I/O blocks */
    static cilist io___9 = { 1, 0, 1, 0, 0 };


/*     / SET INTERNAL PARAMETERS / */
    sgiset_("IRMODE", &c__0, (ftnlen)6);
/*     / GET INTERNAL PARAMETERS / */
    sgiget_("IBGCLI", &ibgcli, (ftnlen)6);
    umiget_("IPATLAND", &ipland, (ftnlen)8);
    umiget_("IPATLAKE", &iplake, (ftnlen)8);
    umlget_("LFILLAKE", &lflake, (ftnlen)8);
    if (! lflake) {
	iplake = ibgcli * 1000 + 999;
    }
/*     / INQUIRE OUTLINE FILE / */
    umqfnm_(cdsn, cdsnx, cdsn_len, (ftnlen)1024);
    if (s_cmp(cdsnx, " ", (ftnlen)1024, (ftnlen)1) == 0) {
/* Writing concatenation */
	i__1[0] = 15, a__1[0] = "OUTLINE FILE = ";
	i__1[1] = lenc_(cdsn, cdsn_len), a__1[1] = cdsn;
	s_cat(cmsg, a__1, i__1, &c__2, (ftnlen)80);
	msgdmp_("M", "UMPMAP", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	msgdmp_("E", "UMPMAP", "OUTLINE FILE DOES NOT EXIST.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
/*     / OPEN OUTLINE FILE / */
    iu = iufopn_();
    o__1.oerr = 0;
    o__1.ounit = iu;
    o__1.ofnmlen = 1024;
    o__1.ofnm = cdsnx;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "UNFORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = iu;
    f_rew(&al__1);
/*     / READ OUTLINE DATA AND PLOT / */
L10:
    io___9.ciunit = iu;
    ios = s_rsue(&io___9);
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&npts, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&igid, (ftnlen)sizeof(integer));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlatmx, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlatmn, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlonmx, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    ios = do_uio(&c__1, (char *)&xlonmn, (ftnlen)sizeof(real));
    if (ios != 0) {
	goto L100001;
    }
    i__2 = npts / 2;
    for (i__ = 1; i__ <= i__2; ++i__) {
	ios = do_uio(&c__1, (char *)&xlat[i__ - 1], (ftnlen)sizeof(real));
	if (ios != 0) {
	    goto L100001;
	}
	ios = do_uio(&c__1, (char *)&xlon[i__ - 1], (ftnlen)sizeof(real));
	if (ios != 0) {
	    goto L100001;
	}
    }
    ios = e_rsue();
L100001:
    if (ios == 0) {
	if (npts > 10 || xlon[0] == xlon[npts / 2 - 1] && xlat[0] == xlat[
		npts / 2 - 1]) {
	    if (igid == 1) {
		i__2 = npts / 2;
		sgtnzu_(&i__2, xlon, xlat, &ipland);
	    } else if (igid == 2) {
		i__2 = npts / 2;
		sgtnzu_(&i__2, xlon, xlat, &iplake);
	    }
	}
    }
    if (ios == 0) {
	goto L10;
    }
/* L20: */
    cl__1.cerr = 0;
    cl__1.cunit = iu;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* umfmap_ */

