/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__8 = 8;
static integer c__32 = 32;
static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     PROGRAM FOR BITMAP FILE CONVERSION FOR X               (94/05/07) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Format strings */
    static char fmt_100[] = "(i4,i4,i3,i3,tr1,a)";

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2], i__3, i__4;
    char ch__1[90];
    olist o__1;
    cllist cl__1;
    alist al__1;
    static char equiv_0[32];

    /* Builtin functions */
    integer s_rsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_rsle(void), f_open(olist *), f_rew(alist *), s_rsfe(cilist *), 
	    do_fio(integer *, char *, ftnlen), e_rsfe(void), s_wsfe(cilist *),
	     e_wsfe(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), e_wsfi(void);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer f_clos(cllist *);

    /* Local variables */
    static integer i__, j, k, n, ih1, ih2, idx;
#define cpat (equiv_0)
    static char cmsg[80];
    static integer nbit, npat, nhex, nbyt;
    static char cfmt1[8];
    static integer ipat1, ipat2;
    static char cline[256], cdsni[64];
    static integer iline;
    static char cdsno[64];
    static integer ibits;
#define cpatx (equiv_0)
    extern /* Subroutine */ int crvrs_(char *, ftnlen), hexdcc_(char *, char *
	    , ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 5, 0, 0, 0 };
    static cilist io___6 = { 0, 11, 0, "(I4)", 0 };
    static cilist io___8 = { 0, 21, 0, "(I4)", 0 };
    static cilist io___10 = { 0, 11, 0, "(4I4)", 0 };
    static icilist io___16 = { 0, cmsg+25, 0, "(I2)", 2, 1 };
    static icilist io___17 = { 0, cmsg+30, 0, "(I2)", 2, 1 };
    static cilist io___18 = { 0, 6, 0, "(A)", 0 };
    static icilist io___19 = { 0, cmsg+30, 0, "(I2)", 2, 1 };
    static cilist io___20 = { 0, 6, 0, "(A)", 0 };
    static icilist io___25 = { 0, cfmt1, 0, "(A,I2,A)", 8, 1 };
    static cilist io___27 = { 0, 11, 0, cfmt1, 0 };
    static cilist io___34 = { 0, 21, 0, fmt_100, 0 };


/*     / READ BITMAP FILE NAMES / */
/*     WRITE(*,*) ' INPUT  BITMAP FILE NAME (C) ? ;' */
/*     READ(*,*) CDSNI */
/*     WRITE(*,*) ' OUTPUT BITMAP FILE NAME (C) ? ;' */
/*     READ(*,*) CDSNO */
    s_rsle(&io___3);
    do_lio(&c__9, &c__1, cdsni, (ftnlen)64);
    do_lio(&c__9, &c__1, cdsno, (ftnlen)64);
    e_rsle();
/*     / OPEN BITMAP FILES / */
    o__1.oerr = 0;
    o__1.ounit = 11;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsni;
    o__1.orl = 0;
    o__1.osta = "OLD";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 11;
    f_rew(&al__1);
    o__1.oerr = 0;
    o__1.ounit = 21;
    o__1.ofnmlen = 64;
    o__1.ofnm = cdsno;
    o__1.orl = 0;
    o__1.osta = "NEW";
    o__1.oacc = 0;
    o__1.ofm = "FORMATTED";
    o__1.oblnk = 0;
    f_open(&o__1);
    al__1.aerr = 0;
    al__1.aunit = 21;
    f_rew(&al__1);
/*     / READ ASCII BITMAP IMAGE & WRITE HEXADECIMAL BITMAP IMAGE / */
    s_rsfe(&io___6);
    do_fio(&c__1, (char *)&npat, (ftnlen)sizeof(integer));
    e_rsfe();
    s_wsfe(&io___8);
    do_fio(&c__1, (char *)&npat, (ftnlen)sizeof(integer));
    e_wsfe();
    i__1 = npat;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_rsfe(&io___10);
	do_fio(&c__1, (char *)&ipat1, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ipat2, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ibits, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&iline, (ftnlen)sizeof(integer));
	e_rsfe();
	if (! (8 <= ibits && ibits <= 32)) {
	    s_copy(cmsg, "BIT LENGTH IS NOT WITHIN ## - ##.", (ftnlen)80, (
		    ftnlen)33);
	    s_wsfi(&io___16);
	    do_fio(&c__1, (char *)&c__8, (ftnlen)sizeof(integer));
	    e_wsfi();
	    s_wsfi(&io___17);
	    do_fio(&c__1, (char *)&c__32, (ftnlen)sizeof(integer));
	    e_wsfi();
	    s_wsfe(&io___18);
/* Writing concatenation */
	    i__2[0] = 10, a__1[0] = " ERROR -- ";
	    i__2[1] = 80, a__1[1] = cmsg;
	    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)90);
	    do_fio(&c__1, ch__1, (ftnlen)90);
	    e_wsfe();
	}
	if (ibits % 8 != 0) {
	    s_copy(cmsg, "BIT LENGTH IS NOT MULTIPLE OF ##.", (ftnlen)80, (
		    ftnlen)33);
	    s_wsfi(&io___19);
	    do_fio(&c__1, (char *)&c__8, (ftnlen)sizeof(integer));
	    e_wsfi();
	    s_wsfe(&io___20);
/* Writing concatenation */
	    i__2[0] = 10, a__1[0] = " ERROR -- ";
	    i__2[1] = 80, a__1[1] = cmsg;
	    s_cat(ch__1, a__1, i__2, &c__2, (ftnlen)90);
	    do_fio(&c__1, ch__1, (ftnlen)90);
	    e_wsfe();
	}
	nbit = ibits;
	nbyt = nbit / 8;
	nhex = nbyt << 1;
	s_wsfi(&io___25);
	do_fio(&c__1, "(A", (ftnlen)2);
	do_fio(&c__1, (char *)&nbit, (ftnlen)sizeof(integer));
	do_fio(&c__1, ")", (ftnlen)1);
	e_wsfi();
	i__3 = iline;
	for (j = 1; j <= i__3; ++j) {
	    s_rsfe(&io___27);
	    do_fio(&c__1, cpatx, nbit);
	    e_rsfe();
	    i__4 = nbyt;
	    for (k = 1; k <= i__4; ++k) {
		crvrs_(cpat + (k - 1 << 3), (ftnlen)8);
/* L30: */
	    }
	    i__4 = nhex;
	    for (n = 1; n <= i__4; ++n) {
		ih1 = (n - 1 << 2) + 1;
		ih2 = ih1 + 3;
		idx = nhex * (j - 1) + n;
		hexdcc_(cpatx + (ih1 - 1), cline + (idx - 1), ih2 - (ih1 - 1),
			 (ftnlen)1);
/* L40: */
	    }
/* L20: */
	}
	s_wsfe(&io___34);
	do_fio(&c__1, (char *)&ipat1, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ipat2, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ibits, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&iline, (ftnlen)sizeof(integer));
	do_fio(&c__1, cline, nhex * iline);
	e_wsfe();
/* L10: */
    }
/*     / CLOSE BITMAP FILE / */
    cl__1.cerr = 0;
    cl__1.cunit = 11;
    cl__1.csta = 0;
    f_clos(&cl__1);
    cl__1.cerr = 0;
    cl__1.cunit = 21;
    cl__1.csta = 0;
    f_clos(&cl__1);
    return 0;
} /* MAIN__ */

#undef cpatx
#undef cpat


/* ----------------------------------------------------------------------- */
/* Subroutine */ int crvrs_(char *chr, ftnlen chr_len)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer i_len(char *, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, lc;
    static char ch1[1];

    lc = i_len(chr, chr_len);
    i__1 = lc / 2;
    for (i__ = 1; i__ <= i__1; ++i__) {
	i__2 = lc - i__;
	s_copy(ch1, chr + i__2, (ftnlen)1, lc - i__ + 1 - i__2);
	i__2 = lc - i__;
	s_copy(chr + i__2, chr + (i__ - 1), lc - i__ + 1 - i__2, (ftnlen)1);
	*(unsigned char *)&chr[i__ - 1] = *(unsigned char *)ch1;
/* L10: */
    }
    return 0;
} /* crvrs_ */

/* ----------------------------------------------------------------------- */
/* Subroutine */ int hexdcc_(char *cp, char *ch, ftnlen cp_len, ftnlen ch_len)
{
    /* Initialized data */

    static char chex[1*16] = "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "A" 
	    "B" "C" "D" "E" "F";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer pow_ii(integer *, integer *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, nn;

    nn = 0;
    for (i__ = 4; i__ >= 1; --i__) {
	if (*(unsigned char *)&cp[i__ - 1] != '0') {
	    i__1 = 4 - i__;
	    nn += pow_ii(&c__2, &i__1);
	}
/* L10: */
    }
    s_copy(ch, chex + nn, ch_len, (ftnlen)1);
    return 0;
} /* hexdcc_ */

/* Main program alias */ int bmcx11_ () { MAIN__ (); return 0; }
