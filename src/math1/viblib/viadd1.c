/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int viadd1_(integer *ix, integer *iy, integer *iz, integer *
	n, integer *jx, integer *jy, integer *jz)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer j, kx, ky, kz, imiss;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen);

    /* Parameter adjustments */
    --iz;
    --iy;
    --ix;

    /* Function Body */
    gliget_("IMISS", &imiss, (ftnlen)5);
    kx = 1 - *jx;
    ky = 1 - *jy;
    kz = 1 - *jz;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	kx += *jx;
	ky += *jy;
	kz += *jz;
	if (ix[kx] != imiss && iy[ky] != imiss) {
	    iz[kz] = ix[kx] + iy[ky];
	} else {
	    iz[kz] = imiss;
	}
/* L10: */
    }
    return 0;
} /* viadd1_ */

