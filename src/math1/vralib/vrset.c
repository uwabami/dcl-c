/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     VRSET */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vrset_0_(int n__, real *rx, real *ry, integer *n, 
	integer *jx, integer *jy)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer j, kx, ky;

/*     --------------------------- */
/*     --------------------------- */
    /* Parameter adjustments */
    --rx;
    --ry;

    /* Function Body */
    switch(n__) {
	case 1: goto L_vrset0;
	case 2: goto L_vrset1;
	}


L_vrset0:

L_vrset1:
    kx = 1 - *jx;
    ky = 1 - *jy;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	kx += *jx;
	ky += *jy;
	ry[ky] = rx[kx];
/* L10: */
    }
    return 0;
} /* vrset_ */

/* Subroutine */ int vrset_(real *rx, real *ry, integer *n, integer *jx, 
	integer *jy)
{
    return vrset_0_(0, rx, ry, n, jx, jy);
    }

/* Subroutine */ int vrset0_(real *rx, real *ry, integer *n, integer *jx, 
	integer *jy)
{
    return vrset_0_(1, rx, ry, n, jx, jy);
    }

/* Subroutine */ int vrset1_(real *rx, real *ry, integer *n, integer *jx, 
	integer *jy)
{
    return vrset_0_(2, rx, ry, n, jx, jy);
    }

