/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     COORDINATE TRANSFORMATION (2D HYPERBOLIC -> CARTESIAN ) */
/*                                              93/02/18   S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ct2hc_(real *u, real *v, real *x, real *y)
{
    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static real uv;

    uv = sqrt(*u * *u + *v * *v);
    if (*v > 0.f) {
	*x = sqrt(*u + uv) / 2;
    } else {
	*x = -sqrt(*u + uv) / 2;
    }
    *y = sqrt(-(*u) + uv) / 2;
    return 0;
} /* ct2hc_ */

