/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     COORDINATE TRANSFORMATION (3D SPHERICAL -> CARTESIAN ) */
/*                                              93/02/18   S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int ct3sc_(real *r__, real *theta, real *phi, real *x, real *
	y, real *z__)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);

    *x = *r__ * sin(*theta) * cos(*phi);
    *y = *r__ * sin(*theta) * sin(*phi);
    *z__ = *r__ * cos(*theta);
    return 0;
} /* ct3sc_ */

