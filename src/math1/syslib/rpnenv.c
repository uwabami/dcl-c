/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     GET EXTERNAL PARAMETER FROM ENVIRONMENTAL VARIABLE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int rpnenv_0_(int n__, char *cpfix, char *cp, integer *ipara,
	 logical *lpara, real *rpara, char *cpara, ftnlen cpfix_len, ftnlen 
	cp_len, ftnlen cpara_len)
{
    /* Initialized data */

    static char csepx[1] = "_";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char cpx[16], carg[80];
    extern integer lenc_(char *, ftnlen);
    static char csep[1];
    extern integer ifromc_(char *, ftnlen);
    extern logical lfromc_(char *, ftnlen);
    extern real rfromc_(char *, ftnlen);
    extern /* Subroutine */ int osgenv_(char *, char *, ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_rtienv;
	case 2: goto L_rtlenv;
	case 3: goto L_rtrenv;
	case 4: goto L_rtcenv;
	case 5: goto L_rlienv;
	case 6: goto L_rllenv;
	case 7: goto L_rlrenv;
	case 8: goto L_rlcenv;
	}

    osgenv_("DCLENVCHAR", csep, (ftnlen)10, (ftnlen)1);
    if (*(unsigned char *)csep == ' ') {
	*(unsigned char *)csep = *(unsigned char *)&csepx[0];
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtienv:
/* Writing concatenation */
    i__1[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__1[1] = 1, a__1[1] = csep;
    i__1[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__1, &c__3, (ftnlen)16);
    osgenv_(cpx, carg, (ftnlen)16, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	*ipara = ifromc_(carg, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtlenv:
/* Writing concatenation */
    i__1[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__1[1] = 1, a__1[1] = csep;
    i__1[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__1, &c__3, (ftnlen)16);
    osgenv_(cpx, carg, (ftnlen)16, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	*lpara = lfromc_(carg, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtrenv:
/* Writing concatenation */
    i__1[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__1[1] = 1, a__1[1] = csep;
    i__1[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__1, &c__3, (ftnlen)16);
    osgenv_(cpx, carg, (ftnlen)16, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	*rpara = rfromc_(carg, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETER WITH SHORT NAME */
/* ----------------------------------------------------------------------- */

L_rtcenv:
/* Writing concatenation */
    i__1[0] = lenc_(cpfix, cpfix_len), a__1[0] = cpfix;
    i__1[1] = 1, a__1[1] = csep;
    i__1[2] = cp_len, a__1[2] = cp;
    s_cat(cpx, a__1, i__1, &c__3, (ftnlen)16);
    osgenv_(cpx, carg, (ftnlen)16, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	s_copy(cpara, carg, cpara_len, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET INTEGER PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlienv:
    osgenv_(cp, carg, cp_len, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	*ipara = ifromc_(carg, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET LOGICAL PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rllenv:
    osgenv_(cp, carg, cp_len, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	*lpara = lfromc_(carg, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET REAL PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlrenv:
    osgenv_(cp, carg, cp_len, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	*rpara = rfromc_(carg, (ftnlen)80);
    }
    return 0;
/* ----------------------------------------------------------------------- */
/*     GET CHARACTER PARAMETER WITH LONG NAME */
/* ----------------------------------------------------------------------- */

L_rlcenv:
    osgenv_(cp, carg, cp_len, (ftnlen)80);
    if (s_cmp(carg, " ", (ftnlen)80, (ftnlen)1) != 0) {
	s_copy(cpara, carg, cpara_len, (ftnlen)80);
    }
    return 0;
} /* rpnenv_ */

/* Subroutine */ int rpnenv_(void)
{
    return rpnenv_0_(0, (char *)0, (char *)0, (integer *)0, (logical *)0, (
	    real *)0, (char *)0, (ftnint)0, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int rtienv_(char *cpfix, char *cp, integer *ipara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnenv_0_(1, cpfix, cp, ipara, (logical *)0, (real *)0, (char *)0, 
	    cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtlenv_(char *cpfix, char *cp, logical *lpara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnenv_0_(2, cpfix, cp, (integer *)0, lpara, (real *)0, (char *)0, 
	    cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtrenv_(char *cpfix, char *cp, real *rpara, ftnlen 
	cpfix_len, ftnlen cp_len)
{
    return rpnenv_0_(3, cpfix, cp, (integer *)0, (logical *)0, rpara, (char *)
	    0, cpfix_len, cp_len, (ftnint)0);
    }

/* Subroutine */ int rtcenv_(char *cpfix, char *cp, char *cpara, ftnlen 
	cpfix_len, ftnlen cp_len, ftnlen cpara_len)
{
    return rpnenv_0_(4, cpfix, cp, (integer *)0, (logical *)0, (real *)0, 
	    cpara, cpfix_len, cp_len, cpara_len);
    }

/* Subroutine */ int rlienv_(char *cp, integer *ipara, ftnlen cp_len)
{
    return rpnenv_0_(5, (char *)0, cp, ipara, (logical *)0, (real *)0, (char *
	    )0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rllenv_(char *cp, logical *lpara, ftnlen cp_len)
{
    return rpnenv_0_(6, (char *)0, cp, (integer *)0, lpara, (real *)0, (char *
	    )0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rlrenv_(char *cp, real *rpara, ftnlen cp_len)
{
    return rpnenv_0_(7, (char *)0, cp, (integer *)0, (logical *)0, rpara, (
	    char *)0, (ftnint)0, cp_len, (ftnint)0);
    }

/* Subroutine */ int rlcenv_(char *cp, char *cpara, ftnlen cp_len, ftnlen 
	cpara_len)
{
    return rpnenv_0_(8, (char *)0, cp, (integer *)0, (logical *)0, (real *)0, 
	    cpara, (ftnint)0, cp_len, cpara_len);
    }

