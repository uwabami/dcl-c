/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     PARAMETER CONTROL (GENERIC) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int glpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*20] = "LMISS   " "IMISS   " "RMISS   " "IUNDEF  " 
	    "RUNDEF  " "LEPSL   " "REPSL   " "RFACT   " "INTMAX  " "REALMAX " 
	    "REALMIN " "NBITSPW " "NCHRSPW " "IIUNIT  " "IOUNIT  " "MSGUNIT " 
	    "MAXMSG  " "MSGLEV  " "NLNSIZE " "LLMSG   ";
    static integer itype[20] = { 2,1,3,1,3,2,3,3,1,3,3,1,1,1,1,1,1,1,1,2 };
    static char cparal[40*20] = "INTERPRET_MISSING_VALUE                 " 
	    "MISSING_INT                             " "MISSING_REAL        "
	    "                    " "UNDEFINED_INT                           " 
	    "UNDEFINED_REAL                          " "INTERPRET_TRUNCATION"
	    "                    " "TRUNCATION_ERROR                        " 
	    "TRUNCATION_FACTOR                       " "MAX_INT             "
	    "                    " "MAX_REAL                                " 
	    "MIN_REAL                                " "WORD_LENGTH_IN_BIT  "
	    "                    " "WORD_LENGTH_IN_CHAR                     " 
	    "INPUT_UNIT                              " "OUTPUT_UNIT         "
	    "                    " "MESSAGE_UNIT                            " 
	    "MAX_MESSAGE_NUMBER                      " "MESSAGE_LEVEL       "
	    "                    " "LINE_SIZE                               " 
	    "ENABLE_LONG_MESSAGE                     ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern /* Subroutine */ int gliqid_(char *, integer *, ftnlen), gllqid_(
	    char *, integer *, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int glrqid_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), gliqvl_(integer *
	    , integer *), gllqvl_(integer *, integer *), glisvl_(integer *, 
	    integer *), gllsvl_(integer *, integer *), glrqvl_(integer *, 
	    integer *), glrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_glpqid;
	case 2: goto L_glpqcp;
	case 3: goto L_glpqcl;
	case 4: goto L_glpqit;
	case 5: goto L_glpqvl;
	case 6: goto L_glpsvl;
	case 7: goto L_glpqin;
	}

/*     / LONG NAME / */
    *ncp = 20;
    return 0;
/* ----------------------------------------------------------------------- */

L_glpqid:
    for (n = 1; n <= 20; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "GLPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_glpqcp:
    if (1 <= *idx && *idx <= 20) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "GLPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glpqcl:
    if (1 <= *idx && *idx <= 20) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "GLPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glpqit:
    if (1 <= *idx && *idx <= 20) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "GLPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glpqvl:
    if (1 <= *idx && *idx <= 20) {
	if (itype[*idx - 1] == 1) {
	    gliqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    gliqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    gllqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    gllqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    glrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    glrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "GLPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glpsvl:
    if (1 <= *idx && *idx <= 20) {
	if (itype[*idx - 1] == 1) {
	    gliqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    glisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    gllqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    gllsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    glrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    glrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "GLPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glpqin:
    for (n = 1; n <= 20; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* glpqnp_ */

/* Subroutine */ int glpqnp_(integer *ncp)
{
    return glpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int glpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return glpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int glpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return glpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int glpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return glpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int glpqit_(integer *idx, integer *itp)
{
    return glpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int glpqvl_(integer *idx, integer *ipara)
{
    return glpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int glpsvl_(integer *idx, integer *ipara)
{
    return glpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int glpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return glpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }

