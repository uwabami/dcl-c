/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__5 = 5;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     PROCESS TABLE HANDLER */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int prcopn_0_(int n__, char *cproc, integer *nlev, ftnlen 
	cproc_len)
{
    /* Initialized data */

    static integer nlevel = 0;
    static char cprocz[32*17] = "MAIN                            ";

    /* System generated locals */
    address a__1[5], a__2[3];
    integer i__1[5], i__2[3];

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_stop(char *, ftnlen);

    /* Local variables */
    static integer n0;
    static char cmsgx[200];
    static integer iunit;
    extern /* Subroutine */ int gliget_(char *, integer *, ftnlen), osabrt_(
	    void);
    static integer lnsize;
    extern /* Subroutine */ int mszdmp_(char *, integer *, integer *, ftnlen);

    switch(n__) {
	case 1: goto L_prccls;
	case 2: goto L_prclvl;
	case 3: goto L_prcnam;
	}

    ++nlevel;
    s_copy(cprocz + (nlevel << 5), cproc, (ftnlen)32, cproc_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_prccls:
    if (s_cmp(cproc, cprocz + (nlevel << 5), cproc_len, (ftnlen)32) == 0) {
	s_copy(cprocz + (nlevel << 5), " ", (ftnlen)32, (ftnlen)1);
	--nlevel;
    } else {
	gliget_("MSGUNIT", &iunit, (ftnlen)7);
	gliget_("NLNSIZE", &lnsize, (ftnlen)7);
	n0 = min(nlevel,1);
/* Writing concatenation */
	i__1[0] = 21, a__1[0] = "*** ERROR (PRCCLS IN ";
	i__1[1] = 32, a__1[1] = cprocz + (n0 << 5);
	i__1[2] = 12, a__1[2] = ") * PROCESS ";
	i__1[3] = 32, a__1[3] = cprocz + (nlevel << 5);
	i__1[4] = 25, a__1[4] = " HAS NOT BEEN CLOSED YET.";
	s_cat(cmsgx, a__1, i__1, &c__5, (ftnlen)200);
	mszdmp_(cmsgx, &iunit, &lnsize, (ftnlen)200);
	osabrt_();
	s_stop("", (ftnlen)0);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_prclvl:
    *nlev = nlevel;
    return 0;
/* ----------------------------------------------------------------------- */

L_prcnam:
    if (*nlev <= nlevel) {
	s_copy(cproc, cprocz + (*nlev << 5), cproc_len, (ftnlen)32);
    } else {
	gliget_("MSGUNIT", &iunit, (ftnlen)7);
	gliget_("NLNSIZE", &lnsize, (ftnlen)7);
	n0 = min(nlevel,1);
/* Writing concatenation */
	i__2[0] = 21, a__2[0] = "*** ERROR (PRCNAM IN ";
	i__2[1] = 32, a__2[1] = cprocz + (n0 << 5);
	i__2[2] = 35, a__2[2] = ") * TOO LARGE PROCESS LEVEL (NLEV).";
	s_cat(cmsgx, a__2, i__2, &c__3, (ftnlen)200);
	mszdmp_(cmsgx, &iunit, &lnsize, (ftnlen)200);
	osabrt_();
	s_stop("", (ftnlen)0);
    }
    return 0;
} /* prcopn_ */

/* Subroutine */ int prcopn_(char *cproc, ftnlen cproc_len)
{
    return prcopn_0_(0, cproc, (integer *)0, cproc_len);
    }

/* Subroutine */ int prccls_(char *cproc, ftnlen cproc_len)
{
    return prcopn_0_(1, cproc, (integer *)0, cproc_len);
    }

/* Subroutine */ int prclvl_(integer *nlev)
{
    return prcopn_0_(2, (char *)0, nlev, (ftnint)0);
    }

/* Subroutine */ int prcnam_(integer *nlev, char *cproc, ftnlen cproc_len)
{
    return prcopn_0_(3, cproc, nlev, cproc_len);
    }

