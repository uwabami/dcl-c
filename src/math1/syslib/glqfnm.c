/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int glqfnm_(char *cpara, char *cfname, ftnlen cpara_len, 
	ftnlen cfname_len)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int glcget_(char *, char *, ftnlen, ftnlen), 
	    cfsrch_(char *, integer *, char *, integer *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static char cflist[1024*1], cplist[1024*3];

    s_copy(cplist, " ", (ftnlen)1024, (ftnlen)1);
    glcget_("DUPATH", cplist + 1024, (ftnlen)6, (ftnlen)1024);
    glcget_("DSPATH", cplist + 2048, (ftnlen)6, (ftnlen)1024);
    glcget_(cpara, cflist, cpara_len, (ftnlen)1024);
    cfsrch_(cplist, &c__3, cflist, &c__1, cfname, (ftnlen)1024, (ftnlen)1024, 
	    cfname_len);
    return 0;
} /* glqfnm_ */

