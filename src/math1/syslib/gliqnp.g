/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;
static integer c__12 = 12;

/* ----------------------------------------------------------------------- */
/*     INTEGER PARAMETER CONTROL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int gliqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*12] = "IMISS   " "IUNDEF  " "INTMAX  " "NBITSPW " 
	    "NCHRSPW " "IIUNIT  " "IOUNIT  " "MSGUNIT " "MAXMSG  " "MSGLEV  " 
	    "NLNSIZE " "SCKPORT ";
    static integer ix[12] = { 999,-999,@INTMAX,32,4,5,6,0,20,0,78,57010 };
    static char cparal[40*12] = "MISSING_INT                             " 
	    "UNDEFINED_INT                           " "MAX_INT             "
	    "                    " "WORD_LENGTH_IN_BIT                      " 
	    "WORD_LENGTH_IN_CHAR                     " "INPUT_UNIT          "
	    "                    " "OUTPUT_UNIT                             " 
	    "MESSAGE_UNIT                            " "MAX_MESSAGE_NUMBER  "
	    "                    " "MESSAGE_LEVEL                           " 
	    "LINE_SIZE                               " "SOCKET_PORT         "
	    "                    ";
    static logical lw[12] = { TRUE_,TRUE_,FALSE_,FALSE_,FALSE_,TRUE_,TRUE_,
	    TRUE_,TRUE_,TRUE_,TRUE_,TRUE_ };
    static logical lfirst = TRUE_;

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), rtiget_(
	    char *, char *, integer *, integer *, ftnlen, ftnlen);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_gliqid;
	case 2: goto L_gliqcp;
	case 3: goto L_gliqcl;
	case 4: goto L_gliqvl;
	case 5: goto L_glisvl;
	case 6: goto L_gliqin;
	}

/*     / LONG NAME / */
    *ncp = 12;
    return 0;
/* ----------------------------------------------------------------------- */

L_gliqid:
    for (n = 1; n <= 12; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "GLIQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_gliqcp:
    if (1 <= *idx && *idx <= 12) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "GLIQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_gliqcl:
    if (1 <= *idx && *idx <= 12) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "GLIQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_gliqvl:
    if (lfirst) {
	rtiget_("GL", cparas, ix, &c__12, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__12, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 12) {
	*ipara = ix[*idx - 1];
    } else {
	msgdmp_("E", "GLIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_glisvl:
    if (lfirst) {
	rtiget_("GL", cparas, ix, &c__12, (ftnlen)2, (ftnlen)8);
	rliget_(cparal, ix, &c__12, (ftnlen)40);
	lfirst = FALSE_;
    }
    if (1 <= *idx && *idx <= 12) {
	if (lw[*idx - 1]) {
	    ix[*idx - 1] = *ipara;
	    return 0;
	} else {
/* Writing concatenation */
	    i__1[0] = 10, a__1[0] = "PARAMETER'";
	    i__1[1] = 8, a__1[1] = cparas + (*idx - 1 << 3);
	    i__1[2] = 16, a__1[2] = "' CANNOT BE SET.";
	    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
	    msgdmp_("E", "GLIQVL", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	}
    } else {
	msgdmp_("E", "GLIQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_gliqin:
    for (n = 1; n <= 12; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* gliqnp_ */

/* Subroutine */ int gliqnp_(integer *ncp)
{
    return gliqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (ftnint)0);
    }

/* Subroutine */ int gliqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return gliqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int gliqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return gliqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int gliqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return gliqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, 
	    cp_len);
    }

/* Subroutine */ int gliqvl_(integer *idx, integer *ipara)
{
    return gliqnp_0_(4, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int glisvl_(integer *idx, integer *ipara)
{
    return gliqnp_0_(5, (integer *)0, (char *)0, idx, ipara, (integer *)0, (
	    ftnint)0);
    }

/* Subroutine */ int gliqin_(char *cp, integer *in, ftnlen cp_len)
{
    return gliqnp_0_(6, (integer *)0, cp, (integer *)0, (integer *)0, in, 
	    cp_len);
    }

