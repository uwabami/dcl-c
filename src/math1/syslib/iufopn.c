/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     IUFOPN */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer iufopn_(void)
{
    /* System generated locals */
    integer ret_val;
    inlist ioin__1;

    /* Builtin functions */
    integer f_inqu(inlist *);

    /* Local variables */
    static integer iu;
    static logical lopen;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static logical lexist;

    for (iu = 1; iu <= 99; ++iu) {
	ioin__1.inerr = 0;
	ioin__1.inunit = iu;
	ioin__1.infile = 0;
	ioin__1.inex = &lexist;
	ioin__1.inopen = &lopen;
	ioin__1.innum = 0;
	ioin__1.innamed = 0;
	ioin__1.inname = 0;
	ioin__1.inacc = 0;
	ioin__1.inseq = 0;
	ioin__1.indir = 0;
	ioin__1.infmt = 0;
	ioin__1.inform = 0;
	ioin__1.inunf = 0;
	ioin__1.inrecl = 0;
	ioin__1.innrec = 0;
	ioin__1.inblank = 0;
	f_inqu(&ioin__1);
/*       / THE FOLLOWING 1 LINE IS JUST FOR CONVENIENCE / */
	lexist = TRUE_;
	if (lexist && ! lopen) {
	    ret_val = iu;
	    return ret_val;
	}
/* L10: */
    }
    msgdmp_("E", "IUFOPN", "THERE IS NO UNIT TO BE OPENED.", (ftnlen)1, (
	    ftnlen)6, (ftnlen)30);
    return ret_val;
} /* iufopn_ */

