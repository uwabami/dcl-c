/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b3 = 1.f;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (LAMBERT CONICAL 1)               93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfcoa_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y, real *ylat0)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal), sqrt(doublereal), atan2(
	    doublereal, doublereal), asin(doublereal), r_sign(real *, real *);

    /* Local variables */
    static real r__, s, bk, ck, dk, rc, pi, th, dlm, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpicoa;
	case 2: goto L_mpscoa;
	}

    th = pi / 2 - s * *ylat;
    r__ = ck * sin(th / 2);
    dlm = xmplon_(xlon) * dk;
    *x = r__ * sin(dlm);
    *y = -s * r__ * cos(dlm);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpicoa:
    r__ = sqrt(*x * *x + *y * *y);
    if (r__ == 0.f) {
	*xlon = 0.f;
    } else {
	*xlon = atan2(*x, -s * *y) / dk;
    }
    if (abs(*xlon) <= pi) {
	rc = r__ / ck;
	if (rc <= 1.f) {
	    *ylat = s * (pi / 2 - asin(rc) * 2);
	    return 0;
	}
    }
    *xlon = rna;
    *ylat = rna;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpscoa:
    pi = rfpi_();
    glrget_("RUNDEF", &rna, (ftnlen)6);
    s = r_sign(&c_b3, ylat0);
    bk = cos((pi / 2 - abs(*ylat0)) / 2);
    ck = 2 / bk;
    dk = bk * bk;
    return 0;
} /* mpfcoa_ */

/* Subroutine */ int mpfcoa_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfcoa_0_(0, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpicoa_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfcoa_0_(1, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpscoa_(real *ylat0)
{
    return mpfcoa_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, ylat0);
    }

