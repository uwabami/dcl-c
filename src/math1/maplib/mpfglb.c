/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (GLOBULAR)                     2007-11-24 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfglb_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double atan(doublereal), sin(doublereal), cos(doublereal), asin(
	    doublereal);

    /* Local variables */
    static real xx, rna;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    static real coslat;
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpiglb;
	}

    xx = xmplon_(xlon);
    *y = sin(*ylat);
    *x = cos(*ylat) * xx / (atan(1.f) * 2.f);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpiglb:
/* Computing 2nd power */
    r__1 = *x;
/* Computing 2nd power */
    r__2 = *y;
    if (r__1 * r__1 + r__2 * r__2 > 1.f) {
	goto L999;
    }
    *ylat = asin(*y);
    coslat = cos(*ylat);
    if (abs(coslat) > 1e-5f) {
	*xlon = *x * (atan(1.f) * 2.f) / coslat;
    } else {
	*xlon = 0.f;
    }
    return 0;
L999:
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfglb_ */

/* Subroutine */ int mpfglb_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfglb_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpiglb_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfglb_0_(1, xlon, ylat, x, y);
    }

