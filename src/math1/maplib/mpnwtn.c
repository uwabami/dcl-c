/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/* INV MAP PROJECTION (NEWTON METHOD ON SPHERE)      2007-11-10 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpnwtn_(real *x, real *y, real *xlon, real *ylat, S_fp 
	func, logical *xcyc)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double atan(doublereal);

    /* Local variables */
    static integer i__;
    static real x0, y0, x1, y1, x2, y2, xb, yb, dx, dy, cx, cy, xl, yl, det, 
	    rna, rdet, dist, ylat2, xlon2, accel;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);

    accel = 1.f;
    i__ = 0;
L100:
    if (*xlon > 0.f) {
	xlon2 = *xlon - 1e-4f;
    } else {
	xlon2 = *xlon + 1e-4f;
    }
    if (*ylat > 0.f) {
	ylat2 = *ylat - 1e-4f;
    } else {
	ylat2 = *ylat + 1e-4f;
    }
    (*func)(xlon, ylat, &x0, &y0);
    (*func)(&xlon2, ylat, &x1, &y1);
    (*func)(xlon, &ylat2, &x2, &y2);
/*         print "('foot:',4f13.6)", degree(xlon), degree(xlon2), */
/*    +     degree(ylat), degree(ylat2) */
    xl = (x1 - x0) / (xlon2 - *xlon);
    xb = (x2 - x0) / (ylat2 - *ylat);
    yl = (y1 - y0) / (xlon2 - *xlon);
    yb = (y2 - y0) / (ylat2 - *ylat);
    dx = *x - x0;
    dy = *y - y0;
    det = xl * yb - xb * yl;
    if (abs(det) < 1e-7f) {
/*           print "('singularity')" */
	goto L110;
    }
    rdet = accel / det;
/*         print "('det=',f15.8)", det */
    cx = rdet * (yb * dx - xb * dy);
    cy = rdet * (-yl * dx + xl * dy);
/*         print "('corr=',2f13.6)", degree(cx), degree(cy) */
    *xlon += cx;
    if (*xlon > atan(1.f) * 4.f + 1e-4f) {
/*           print "(' xlon > 180')" */
	if (*xcyc) {
	    *xlon -= atan(1.f) * 4.f * 2.f;
	} else {
	    *xlon = atan(1.f) * 4.f - .01f;
	}
	accel = .5f;
    } else if (*xlon < -(atan(1.f) * 4.f) - 1e-4f) {
/*           print "(' xlon < -180')" */
	if (*xcyc) {
	    *xlon += atan(1.f) * 4.f * 2.f;
	} else {
	    *xlon = -(atan(1.f) * 4.f) + .01f;
	}
	accel = .5f;
    }
    *ylat += cy;
    if (*ylat > atan(1.f) * 4.f * .5f) {
/*           print "(' ylat > 90')" */
	*ylat = atan(1.f) * 4.f * .5f - .01f;
	accel = .5f;
    } else if (*ylat < atan(1.f) * 4.f * -.5f) {
/*           print "(' ylat < -90')" */
	*ylat = atan(1.f) * 4.f * -.5f + .01f;
	accel = .5f;
    }
/* Computing MAX */
    r__1 = abs(cx), r__2 = abs(cy);
    dist = max(r__1,r__2);
    if (dist < 3e-5f) {
	goto L110;
    }
    ++i__;
    if (i__ > 40) {
	goto L911;
    }
    goto L100;
L110:
    return 0;
L911:
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpnwtn_ */

