/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (POLYCONIC)                    2007-10-29 E. TOYODA */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfplc_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* Initialized data */

    static logical lfirst = TRUE_;

    /* System generated locals */
    real r__1, r__2, r__3, r__4;

    /* Local variables */
    static integer i__, j;
    static real cx[72]	/* was [12][6] */, cy[72]	/* was [12][6] */, xx,
	     clat, clon, dist, cdist;
    extern /* Subroutine */ int mpxplc_(real *, real *, real *, real *);
    extern real xmplon_(real *);
    extern /* Subroutine */ int mpnwtn_(real *, real *, real *, real *, S_fp, 
	    logical *);

    switch(n__) {
	case 1: goto L_mpiplc;
	}

/* ----------------------------------------------------------------------- */
/* ---- ENTRY MPFPLC(XLON, YLAT, X, Y) */
    xx = xmplon_(xlon);
    mpxplc_(&xx, ylat, x, y);
    return 0;
/* ----------------------------------------------------------------------- */

L_mpiplc:
    if (lfirst) {
	for (i__ = 1; i__ <= 12; ++i__) {
	    clon = ((i__ - .5f) / 12 * 360.f - 180.f) * .017453292519943292f;
	    for (j = 1; j <= 6; ++j) {
		clat = ((j - .5f) / 6 * 180.f - 90.f) * .017453292519943292f;
		mpxplc_(&clon, &clat, &cx[i__ + j * 12 - 13], &cy[i__ + j * 
			12 - 13]);
/* L1010: */
	    }
/* L1000: */
	}
	lfirst = FALSE_;
    }
    dist = 1e10f;
    for (i__ = 1; i__ <= 12; ++i__) {
	for (j = 1; j <= 6; ++j) {
/* Computing MAX */
	    r__3 = (r__1 = cx[i__ + j * 12 - 13] - *x, abs(r__1)), r__4 = (
		    r__2 = cy[i__ + j * 12 - 13] - *y, abs(r__2));
	    cdist = max(r__3,r__4);
	    if (cdist < dist) {
		*xlon = ((i__ - .5f) / 12 * 360.f - 180.f) * 
			.017453292519943292f;
		*ylat = ((j - .5f) / 6 * 180.f - 90.f) * .017453292519943292f;
		dist = cdist;
	    }
/* L1110: */
	}
/* L1100: */
    }
    mpnwtn_(x, y, xlon, ylat, (S_fp)mpxplc_, &c_true);
    return 0;
} /* mpfplc_ */

/* Subroutine */ int mpfplc_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfplc_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpiplc_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfplc_0_(1, xlon, ylat, x, y);
    }

/* Subroutine */ int mpxplc_(real *xlon, real *ylat, real *x, real *y)
{
    /* Builtin functions */
    double sin(doublereal), tan(doublereal), cos(doublereal);

    /* Local variables */
    static real th, tanz;

    if (abs(*ylat) < 1e-4f) {
	*x = *xlon;
	*y = *ylat;
    } else {
	th = *xlon * sin(*ylat);
	tanz = 1.f / tan(*ylat);
	*x = sin(th) * tanz;
	*y = *ylat + tanz * (1.f - cos(th));
    }
    return 0;
} /* mpxplc_ */

