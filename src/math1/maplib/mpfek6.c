/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (ECKERT NO.6)                     93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfek6_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y)
{
    /* Builtin functions */
    double cos(doublereal), sin(doublereal), asin(doublereal);

    /* Local variables */
    static real b, pi, phi, rna;
    extern real rfpi_(void);
    static real alpha;
    extern /* Subroutine */ int mpzek6_();
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);
    extern /* Subroutine */ int mpznwt_(U_fp, real *, real *);

    switch(n__) {
	case 1: goto L_mpiek6;
	}

    pi = rfpi_();
    alpha = *ylat;
    mpznwt_((U_fp)mpzek6_, ylat, &alpha);
    *x = xmplon_(xlon) * .8820255f * (cos(alpha) + 1) / 2;
    *y = alpha * .8820255f;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpiek6:
    pi = rfpi_();
    b = (pi + 2) / 2;
    alpha = *y / .8820255f;
    if (abs(alpha) <= pi / 2) {
	phi = asin((alpha + sin(alpha)) / b);
	*xlon = *x * 2 / .8820255f / (cos(alpha) + 1);
	if (abs(*xlon) <= pi) {
	    *ylat = phi;
	    return 0;
	}
    }
    glrget_("RUNDEF", &rna, (ftnlen)6);
    *xlon = rna;
    *ylat = rna;
    return 0;
} /* mpfek6_ */

/* Subroutine */ int mpfek6_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfek6_0_(0, xlon, ylat, x, y);
    }

/* Subroutine */ int mpiek6_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfek6_0_(1, xlon, ylat, x, y);
    }

