/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b3 = 1.f;

/* ----------------------------------------------------------------------- */
/*     MAP PROJECTION (BONNE)                           93/02/20 S.SAKAI */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int mpfbon_0_(int n__, real *xlon, real *ylat, real *x, real 
	*y, real *ylat0)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal), sqrt(doublereal), atan2(
	    doublereal, doublereal), r_sign(real *, real *), tan(doublereal);

    /* Local variables */
    static real r__, s, cc, pi, th, dlm, rna;
    extern real rfpi_(void);
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen);
    extern real xmplon_(real *);

    switch(n__) {
	case 1: goto L_mpibon;
	case 2: goto L_mpsbon;
	}

    th = pi / 2 - s * *ylat;
    r__ = th + cc;
    if (r__ == 0.f) {
	*x = 0.f;
	*y = 0.f;
    } else {
	dlm = xmplon_(xlon) * sin(th) / r__;
	*x = r__ * sin(dlm);
	*y = -s * r__ * cos(dlm);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_mpibon:
    r__ = sqrt(*x * *x + *y * *y);
    th = r__ - cc;
    *ylat = s * (pi / 2 - th);
    if (r__ == 0.f) {
	*xlon = 0.f;
	return 0;
    } else if (abs(*ylat) < pi / 2) {
	*xlon = atan2(*x, -s * *y) / sin(th) * r__;
	if (abs(*xlon) <= pi) {
	    return 0;
	}
    } else if (abs(*ylat) == pi / 2) {
	*xlon = 0.f;
	return 0;
    }
    *xlon = rna;
    *ylat = rna;
    return 0;
/* ----------------------------------------------------------------------- */

L_mpsbon:
    pi = rfpi_();
    glrget_("RUNDEF", &rna, (ftnlen)6);
    s = r_sign(&c_b3, ylat0);
    th = pi / 2 - abs(*ylat0);
    cc = tan(th) - th;
    return 0;
} /* mpfbon_ */

/* Subroutine */ int mpfbon_(real *xlon, real *ylat, real *x, real *y)
{
    return mpfbon_0_(0, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpibon_(real *x, real *y, real *xlon, real *ylat)
{
    return mpfbon_0_(1, xlon, ylat, x, y, (real *)0);
    }

/* Subroutine */ int mpsbon_(real *ylat0)
{
    return mpfbon_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, ylat0);
    }

