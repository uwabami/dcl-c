/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     IBLKLE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
integer iblkle_(real *rx, integer *n, real *rr)
{
    /* System generated locals */
    integer ret_val, i__1;

    /* Local variables */
    static integer i__;
    extern logical lrle_(real *, real *);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    /* Parameter adjustments */
    --rx;

    /* Function Body */
    i__1 = *n - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (! (rx[i__] < rx[i__ + 1])) {
	    msgdmp_("E", "IBLKLE", "ORDER OF RX IS INVALID.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)23);
	}
/* L10: */
    }
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (lrle_(rr, &rx[i__])) {
	    ret_val = i__;
	    return ret_val;
	}
/* L15: */
    }
    ret_val = *n + 1;
    return ret_val;
} /* iblkle_ */

