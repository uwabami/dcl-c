/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     G2FBL2 */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/*     G2FBL2  Bilinear interpolation of 2 parameters: */
/*             Interpolation if 0<=P<=1 and 0<=Q<=1. */
/*             Extraporation otherwise. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int g2fbl2_(real *p, real *q, real *x00, real *x10, real *
	x01, real *x11, real *y00, real *y10, real *y01, real *y11, real *x, 
	real *y)
{
    extern /* Subroutine */ int g2fbli_(real *, real *, real *, real *, real *
	    , real *, real *);

    g2fbli_(p, q, x00, x10, x01, x11, x);
    g2fbli_(p, q, y00, y10, y01, y11, y);
    return 0;
} /* g2fbl2_ */

