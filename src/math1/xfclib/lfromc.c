/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
logical lfromc_(char *cx, ftnlen cx_len)
{
    /* System generated locals */
    logical ret_val;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer lc1, lc2;
    static char cmsg[80];
    extern integer leny_(char *, ftnlen), lenz_(char *, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

    lc1 = leny_(cx, cx_len) + 1;
    lc2 = lenz_(cx, cx_len);
    if (lc2 == 0) {
	s_copy(cmsg, "THERE IS NO VALID CHARACTER.", (ftnlen)80, (ftnlen)28);
	msgdmp_("E", "IFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    if (*(unsigned char *)&cx[lc1 - 1] == '.') {
	++lc1;
    }
    if (lchreq_(cx + (lc1 - 1), "T", (ftnlen)1, (ftnlen)1)) {
	ret_val = TRUE_;
    } else if (lchreq_(cx + (lc1 - 1), "F", (ftnlen)1, (ftnlen)1)) {
	ret_val = FALSE_;
    } else {
	s_copy(cmsg, "THIS IS INVALID LOGICAL EXPRESSION.", (ftnlen)80, (
		ftnlen)35);
	msgdmp_("E", "LFROMC", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    }
    return ret_val;
} /* lfromc_ */

