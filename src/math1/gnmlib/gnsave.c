/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     GNSAVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int gnsave_0_(int n__)
{
    static integer nb;
    static real xb[20];
    extern /* Subroutine */ int gnqblk_(real *, integer *), gnsblk_(real *, 
	    integer *);

    switch(n__) {
	case 1: goto L_gnrset;
	}

    gnqblk_(xb, &nb);
    return 0;
/* ----------------------------------------------------------------------- */
/*     GNRSET */
/* ----------------------------------------------------------------------- */

L_gnrset:
    gnsblk_(xb, &nb);
    return 0;
} /* gnsave_ */

/* Subroutine */ int gnsave_(void)
{
    return gnsave_0_(0);
    }

/* Subroutine */ int gnrset_(void)
{
    return gnsave_0_(1);
    }

