/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__10 = 10;

/* ----------------------------------------------------------------------- */
/*     RGNLT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
real rgnlt_(real *rx)
{
    /* System generated locals */
    real ret_val;

    /* Local variables */
    static integer ip;
    static real bx;
    extern /* Subroutine */ int gnlt_(real *, real *, integer *);
    extern real rexp_(real *, integer *, integer *);

    gnlt_(rx, &bx, &ip);
    ret_val = rexp_(&bx, &c__10, &ip);
    return ret_val;
} /* rgnlt_ */

