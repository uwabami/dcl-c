/*
 *	osgenv
 *
 *    Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved.
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

#ifndef WINDOWS
void osgenv_(char *cename, char *ceval, int Lename, int Leval)
#else
void OSGENV(char *cename, char *ceval, int Lename, int Leval)
#endif
{
    int nc;
    char cpx[80], *buf;

    memcpy (cpx, cename, Lename);
    for (nc = Lename; nc > 1; --nc)
	if (*(cename + nc - 1) != '\0' && *(cename + nc - 1) != ' ')
	    break;
    cpx[nc] = '\0';
    memset (ceval, ' ', Leval);
    if ((buf = getenv (cpx)) != NULL) {
	memcpy (ceval, buf, strlen (buf));
    }
}
