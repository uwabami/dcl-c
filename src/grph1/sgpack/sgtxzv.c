/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtxzv_(real *vx, real *vy, char *chars, real *rsize, 
	integer *irota, integer *icent, integer *index, ftnlen chars_len)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sztxcl_(void), sztxop_(real *, integer *, 
	    integer *, integer *), sztxzv_(real *, real *, char *, ftnlen);

    if (*rsize == 0.f) {
	msgdmp_("M", "SGTXZV", "TEXT HEIGHT IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "SGTXZV", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (! (-1 <= *icent && *icent <= 1)) {
	msgdmp_("E", "SGTXZV", "CENTERING OPTION IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)28);
    }
    if (*index == 0) {
	msgdmp_("M", "SGTXZV", "TEXT INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGTXZV", "TEXT INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    sztxop_(rsize, irota, icent, index);
    sztxzv_(vx, vy, chars, chars_len);
    sztxcl_();
    return 0;
} /* sgtxzv_ */

