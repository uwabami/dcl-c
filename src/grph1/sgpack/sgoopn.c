/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgoopn_0_(int n__, char *cprc, char *com, ftnlen 
	cprc_len, ftnlen com_len)
{
    extern /* Subroutine */ int prccls_(char *, ftnlen), prcopn_(char *, 
	    ftnlen), swocls_(char *, ftnlen), swoopn_(char *, char *, ftnlen, 
	    ftnlen);

    switch(n__) {
	case 1: goto L_sgocls;
	}

    prcopn_(cprc, cprc_len);
    swoopn_(cprc, com, cprc_len, com_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgocls:
    prccls_(cprc, cprc_len);
    swocls_(cprc, cprc_len);
    return 0;
} /* sgoopn_ */

/* Subroutine */ int sgoopn_(char *cprc, char *com, ftnlen cprc_len, ftnlen 
	com_len)
{
    return sgoopn_0_(0, cprc, com, cprc_len, com_len);
    }

/* Subroutine */ int sgocls_(char *cprc, ftnlen cprc_len)
{
    return sgoopn_0_(1, cprc, (char *)0, cprc_len, (ftnint)0);
    }

