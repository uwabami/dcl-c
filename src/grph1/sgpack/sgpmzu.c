/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgpmzu_(integer *n, real *upx, real *upy, integer *itype,
	 integer *index, real *rsize)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szpmcl_(void), szpmop_(integer *, integer *, 
	    real *), szpmzu_(integer *, real *, real *);

    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "SGPMZU", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "SGPMZU", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "SGPMZU", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)35);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGPMZU", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*rsize == 0.f) {
	msgdmp_("M", "SGPMZU", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
	return 0;
    }
    if (*rsize < 0.f) {
	msgdmp_("E", "SGPMZU", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    szpmop_(itype, index, rsize);
    szpmzu_(n, &upx[1], &upy[1]);
    szpmcl_();
    return 0;
} /* sgpmzu_ */

