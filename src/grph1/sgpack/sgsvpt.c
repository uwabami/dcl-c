/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     NORMALIZATION TRANSFORMATION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgsvpt_0_(int n__, real *vxmin, real *vxmax, real *vymin,
	 real *vymax)
{
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_sgqvpt;
	}

    sgrset_("VXMIN", vxmin, (ftnlen)5);
    sgrset_("VXMAX", vxmax, (ftnlen)5);
    sgrset_("VYMIN", vymin, (ftnlen)5);
    sgrset_("VYMAX", vymax, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqvpt:
    sgrget_("VXMIN", vxmin, (ftnlen)5);
    sgrget_("VXMAX", vxmax, (ftnlen)5);
    sgrget_("VYMIN", vymin, (ftnlen)5);
    sgrget_("VYMAX", vymax, (ftnlen)5);
    return 0;
} /* sgsvpt_ */

/* Subroutine */ int sgsvpt_(real *vxmin, real *vxmax, real *vymin, real *
	vymax)
{
    return sgsvpt_0_(0, vxmin, vxmax, vymin, vymax);
    }

/* Subroutine */ int sgqvpt_(real *vxmin, real *vxmax, real *vymin, real *
	vymax)
{
    return sgsvpt_0_(1, vxmin, vxmax, vymin, vymax);
    }

