/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     POLYMARKER PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgpmu_0_(int n__, integer *n, real *upx, real *upy, real 
	*vpx, real *vpy, real *rpx, real *rpy, integer *itype, integer *index,
	 real *rsize)
{
    /* Initialized data */

    static integer itypez = 1;
    static integer indexz = 1;
    static real rsizez = .01f;

    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szpmcl_(void), szpmop_(integer *, integer *, 
	    real *), szpmzr_(integer *, real *, real *), szpmzu_(integer *, 
	    real *, real *), szpmzv_(integer *, real *, real *);

    /* Parameter adjustments */
    if (upx) {
	--upx;
	}
    if (upy) {
	--upy;
	}
    if (vpx) {
	--vpx;
	}
    if (vpy) {
	--vpy;
	}
    if (rpx) {
	--rpx;
	}
    if (rpy) {
	--rpy;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_sgpmv;
	case 2: goto L_sgpmr;
	case 3: goto L_sgspmt;
	case 4: goto L_sgqpmt;
	case 5: goto L_sgspmi;
	case 6: goto L_sgqpmi;
	case 7: goto L_sgspms;
	case 8: goto L_sgqpms;
	}

    if (*n < 1) {
	msgdmp_("E", "SGPMU", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SGPMU", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGPMU", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)5, (ftnlen)35);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGPMU", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (rsizez == 0.f) {
	msgdmp_("M", "SGPMU", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SGPMU", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szpmop_(&itypez, &indexz, &rsizez);
    szpmzu_(n, &upx[1], &upy[1]);
    szpmcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpmv:
    if (*n < 1) {
	msgdmp_("E", "SGPMV", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SGPMV", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGPMV", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)5, (ftnlen)35);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGPMV", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (rsizez == 0.f) {
	msgdmp_("M", "SGPMV", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SGPMV", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szpmop_(&itypez, &indexz, &rsizez);
    szpmzv_(n, &vpx[1], &vpy[1]);
    szpmcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgpmr:
    if (*n < 1) {
	msgdmp_("E", "SGPMR", "NUMBER OF POINTS IS LESS THAN 1.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (itypez == 0) {
	msgdmp_("M", "SGPMR", "MARKER TYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (indexz == 0) {
	msgdmp_("M", "SGPMR", "POLYMARKER INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)5, (ftnlen)35);
	return 0;
    }
    if (indexz < 0) {
	msgdmp_("E", "SGPMR", "POLYMARKER INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)5, (ftnlen)32);
    }
    if (rsizez == 0.f) {
	msgdmp_("M", "SGPMR", "MARKER SIZE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
	return 0;
    }
    if (rsizez < 0.f) {
	msgdmp_("E", "SGPMR", "MARKER SIZE IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)5, (ftnlen)30);
    }
    szpmop_(&itypez, &indexz, &rsizez);
    szpmzr_(n, &rpx[1], &rpy[1]);
    szpmcl_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgspmt:
    itypez = *itype;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqpmt:
    *itype = itypez;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgspmi:
    indexz = *index;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqpmi:
    *index = indexz;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgspms:
    rsizez = *rsize;
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqpms:
    *rsize = rsizez;
    return 0;
} /* sgpmu_ */

/* Subroutine */ int sgpmu_(integer *n, real *upx, real *upy)
{
    return sgpmu_0_(0, n, upx, upy, (real *)0, (real *)0, (real *)0, (real *)
	    0, (integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int sgpmv_(integer *n, real *vpx, real *vpy)
{
    return sgpmu_0_(1, n, (real *)0, (real *)0, vpx, vpy, (real *)0, (real *)
	    0, (integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int sgpmr_(integer *n, real *rpx, real *rpy)
{
    return sgpmu_0_(2, n, (real *)0, (real *)0, (real *)0, (real *)0, rpx, 
	    rpy, (integer *)0, (integer *)0, (real *)0);
    }

/* Subroutine */ int sgspmt_(integer *itype)
{
    return sgpmu_0_(3, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int sgqpmt_(integer *itype)
{
    return sgpmu_0_(4, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, itype, (integer *)0, (real *)0);
    }

/* Subroutine */ int sgspmi_(integer *index)
{
    return sgpmu_0_(5, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int sgqpmi_(integer *index)
{
    return sgpmu_0_(6, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, index, (real *)0);
    }

/* Subroutine */ int sgspms_(real *rsize)
{
    return sgpmu_0_(7, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, (integer *)0, rsize);
    }

/* Subroutine */ int sgqpms_(real *rsize)
{
    return sgpmu_0_(8, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, (real *)0, (real *)0, (integer *)0, (integer *)0, rsize);
    }

