/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgsmpl_0_(int n__, real *plx, real *ply, real *plrot)
{
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_sgqmpl;
	}

    sgrset_("PLX", plx, (ftnlen)3);
    sgrset_("PLY", ply, (ftnlen)3);
    sgrset_("PLROT", plrot, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqmpl:
    sgrget_("PLX", plx, (ftnlen)3);
    sgrget_("PLY", ply, (ftnlen)3);
    sgrget_("PLROT", plrot, (ftnlen)5);
    return 0;
} /* sgsmpl_ */

/* Subroutine */ int sgsmpl_(real *plx, real *ply, real *plrot)
{
    return sgsmpl_0_(0, plx, ply, plrot);
    }

/* Subroutine */ int sgqmpl_(real *plx, real *ply, real *plrot)
{
    return sgsmpl_0_(1, plx, ply, plrot);
    }

