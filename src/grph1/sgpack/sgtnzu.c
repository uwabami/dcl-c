/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgtnzu_(integer *n, real *upx, real *upy, integer *itpat)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sztncl_(void), sztnop_(integer *), sztnzu_(
	    integer *, real *, real *);

    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    if (*n < 3) {
	msgdmp_("E", "SGTNZU", "NUMBER OF POINTS IS LESS THAN 3.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itpat == 0) {
	msgdmp_("M", "SGTNZU", "TONE PAT. INDEX IS 0 / DO NOTHING.", (ftnlen)
		1, (ftnlen)6, (ftnlen)34);
	return 0;
    }
    if (*itpat < 0) {
	msgdmp_("E", "SGTNZU", "TONE PAT. INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)31);
    }
    sztnop_(itpat);
    sztnzu_(n, &upx[1], &upy[1]);
    sztncl_();
    return 0;
} /* sgtnzu_ */

