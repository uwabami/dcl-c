/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int sglnxr_(real *rx1, real *ry1, real *rx2, real *ry2, 
	integer *index, integer *icolor)
{
    static logical lfcatr;
    extern /* Subroutine */ int swqfcc_(logical *), msgdmp_(char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen), swsfcm_(logical *), szlncl_(void)
	    , swslcl_(integer *), szlnop_(integer *), szlnzr_(real *, real *, 
	    real *, real *);

    swqfcc_(&lfcatr);
    if (! lfcatr) {
	msgdmp_("M", "SGLNXR", "FULL COLOR MODE IS NOT SUPPORTED.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
    }
    if (*index == 0) {
	msgdmp_("M", "SGLNXR", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGLNXR", "LINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    swsfcm_(&c_true);
    swslcl_(icolor);
    szlnop_(index);
    szlnzr_(rx1, ry1, rx2, ry2);
    szlncl_();
    swsfcm_(&c_false);
    return 0;
} /* sglnxr_ */

