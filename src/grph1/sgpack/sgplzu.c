/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgplzu_(integer *n, real *upx, real *upy, integer *itype,
	 integer *index)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szplcl_(void), szplop_(integer *, integer *), 
	    szplzu_(integer *, real *, real *);

    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    if (*n < 2) {
	msgdmp_("E", "SGPLZU", "NUMBER OF POINTS IS LESS THAN 2.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
    }
    if (*itype == 0) {
	msgdmp_("M", "SGPLZU", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "SGPLZU", "POLYLINE INDEX IS 0 / DO NOTHING.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGPLZU", "POLYLINE INDEX IS LESS THAN 0.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    szplop_(itype, index);
    szplzu_(n, &upx[1], &upy[1]);
    szplcl_();
    return 0;
} /* sgplzu_ */

