/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/* Subroutine */ int sglaxu_(real *ux1, real *uy1, real *ux2, real *uy2, 
	integer *itype, integer *index, integer *icolor)
{
    static logical lfcatr;
    extern /* Subroutine */ int swqfcc_(logical *), msgdmp_(char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen), szlacl_(void), swsfcm_(logical *)
	    , swslcl_(integer *), szlaop_(integer *, integer *), szlazu_(real 
	    *, real *, real *, real *);

    swqfcc_(&lfcatr);
    if (! lfcatr) {
	msgdmp_("M", "SGLAXU", "FULL COLOR MODE IS NOT SUPPORTED.", (ftnlen)1,
		 (ftnlen)6, (ftnlen)33);
    }
    if (*itype == 0) {
	msgdmp_("M", "SGLAXU", "LINETYPE IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)27);
	return 0;
    }
    if (*index == 0) {
	msgdmp_("M", "SGLAXU", "LINE INDEX IS 0 / DO NOTHING.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
	return 0;
    }
    if (*index < 0) {
	msgdmp_("E", "SGLAXU", "LINE INDEX IS INVALID.", (ftnlen)1, (ftnlen)6,
		 (ftnlen)22);
    }
    swsfcm_(&c_true);
    swslcl_(icolor);
    szlaop_(itype, index);
    szlazu_(ux1, uy1, ux2, uy2);
    szlacl_();
    swsfcm_(&c_false);
    return 0;
} /* sglaxu_ */

