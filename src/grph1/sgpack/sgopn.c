/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b9 = 0.f;
static real c_b16 = 1.f;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     CONTROL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgopn_0_(int n__, integer *iws)
{
    /* System generated locals */
    real r__1, r__2;

    /* Local variables */
    static integer nw;
    static real wx0, wy0;
    static integer ipg, ifr, jfr, lev, itr, jtr, jws;
    static real rxm, rym, wxl, wyl, fact;
    static integer iwsx, ipage;
    static logical lpage;
    static real xamin, yamin;
    static logical lfull;
    static real xamax, yamax, rxmin, rymin, rxmax, rymax, wxmin, wymin, wxmax,
	     wymax, wsxmn, wsymn, wsxmx, wsymx;
    extern /* Subroutine */ int slpage_(integer *, integer *, integer *), 
	    sgiget_(char *, integer *, ftnlen), sglget_(char *, logical *, 
	    ftnlen), msgdmp_(char *, char *, char *, ftnlen, ftnlen, ftnlen), 
	    sgiset_(char *, integer *, ftnlen), swdcls_(void), slpcnr_(void), 
	    swiget_(char *, integer *, ftnlen);
    static logical lfirst;
    extern /* Subroutine */ int slinit_(real *, real *, real *), sgswnd_(real 
	    *, real *, real *, real *), sgstrf_(void), slqrct_(integer *, 
	    integer *, real *, real *, real *, real *), swdopn_(void), 
	    swpcls_(void), swiset_(char *, integer *, ftnlen), sgstrn_(
	    integer *), slpttl_(void), swqrct_(real *, real *, real *, real *,
	     real *), stswrc_(real *, real *, real *, real *), sgsvpt_(real *,
	     real *, real *, real *), swpopn_(void), swsrot_(integer *), 
	    stqwtr_(real *, real *, real *, real *, real *, real *, real *, 
	    real *, integer *), stswtr_(real *, real *, real *, real *, real *
	    , real *, real *, real *, integer *);

/*     / SET WORKSTATION NUMBER / */
    switch(n__) {
	case 1: goto L_sginit;
	case 2: goto L_sgfrm;
	case 3: goto L_sgcls;
	}

    sgiset_("IWS", iws, (ftnlen)3);
/* ----------------------------------------------------------------------- */

L_sginit:
/*     / SET INITIALIZATION FLAG / */
    lfirst = TRUE_;
/*     / CHECK WORKSTATION NUMBER / */
    sgiget_("IWS", &iwsx, (ftnlen)3);
    swiget_("MAXWNU", &nw, (ftnlen)6);
    jws = abs(iwsx);
    if (! (1 <= jws && jws <= nw)) {
	msgdmp_("E", "SGINIT", "WORKSTATION NUMBER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
/*     / OPEN WORKSTATION / */
    swiset_("IWS", &jws, (ftnlen)3);
    swdopn_();
/*     / GET RANGE OF WORKSTATION RECTANGLE AND SCALING FACTOR / */
    swqrct_(&wsxmn, &wsxmx, &wsymn, &wsymx, &fact);
/*     / SET WORKSTATION RECTANGLE / */
    if (iwsx >= 0) {
	wx0 = wsxmx - wsxmn;
	wy0 = wsymx - wsymn;
	itr = 1;
    } else {
	wx0 = wsymx - wsymn;
	wy0 = wsxmx - wsxmn;
	itr = 2;
    }
    stswrc_(&wsxmn, &wsxmx, &wsymn, &wsymx);
    swsrot_(&itr);
/*     / INITIALIZATION FOR LAYOUT / */
    slinit_(&wx0, &wy0, &fact);
/*     / SET DUMMY TRANSFORMATION / */
/* Computing MIN */
    r__1 = 1.f, r__2 = wx0 / wy0;
    rxm = min(r__1,r__2);
/* Computing MIN */
    r__1 = wy0 / wx0;
    rym = min(r__1,1.f);
    stswtr_(&c_b9, &rxm, &c_b9, &rym, &c_b9, &wx0, &c_b9, &wy0, &itr);
    sgsvpt_(&c_b9, &rxm, &c_b9, &rym);
    sgswnd_(&c_b9, &c_b16, &c_b9, &c_b16);
    sgstrn_(&c__1);
    sgstrf_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgfrm:
/*     / INQUIRE LAYOUT STATUS  / */
    sgiget_("NLEVEL", &lev, (ftnlen)6);
    sgiget_("NFRAME", &ifr, (ftnlen)6);
    sgiget_("NPAGE", &ipg, (ftnlen)5);
/*     / SET NEW FRAME / */
    ++ifr;
    jfr = ifr;
    sgiset_("NFRAME", &ifr, (ftnlen)6);
/*     / INQUIRE PAGE NO. / */
    slpage_(&lev, &jfr, &ipage);
    lpage = ipage != ipg;
    if (lpage) {
/*       / CLOSE PAGE / */
	if (lfirst) {
	    lfirst = FALSE_;
	} else {
	    swpcls_();
	}
/*       / NEXT PAGE / */
	sgiset_("NPAGE", &ipage, (ftnlen)5);
	swpopn_();
/*       / PLOT CORNER MARKS & TITLES / */
	slqrct_(&c__1, &c__1, &xamin, &xamax, &yamin, &yamax);
	wxl = xamax - xamin;
	wyl = yamax - yamin;
/* Computing MIN */
	r__1 = 1.f, r__2 = wxl / wyl;
	rxm = min(r__1,r__2);
/* Computing MIN */
	r__1 = wyl / wxl;
	rym = min(r__1,1.f);
	stswtr_(&c_b9, &rxm, &c_b9, &rym, &xamin, &xamax, &yamin, &yamax, &
		itr);
	slpcnr_();
	slpttl_();
    }
/*     / GET WORKSTATION RECTANGLE / */
    slqrct_(&lev, &ifr, &xamin, &xamax, &yamin, &yamax);
    wxl = xamax - xamin;
    wyl = yamax - yamin;
/*     / SET WORKSTATION TRANSFORMATION / */
    sglget_("LFULL", &lfull, (ftnlen)5);
    if (lfull) {
/* Computing MIN */
	r__1 = 1.f, r__2 = wxl / wyl;
	rxm = min(r__1,r__2);
/* Computing MIN */
	r__1 = wyl / wxl;
	rym = min(r__1,1.f);
	stswtr_(&c_b9, &rxm, &c_b9, &rym, &xamin, &xamax, &yamin, &yamax, &
		itr);
    } else {
	stswtr_(&c_b9, &c_b16, &c_b9, &c_b16, &xamin, &xamax, &yamin, &yamax, 
		&itr);
    }
/*     / SET NORMALIZATION TRANSFORMATION / */
    stqwtr_(&rxmin, &rxmax, &rymin, &rymax, &wxmin, &wxmax, &wymin, &wymax, &
	    jtr);
    sgsvpt_(&rxmin, &rxmax, &rymin, &rymax);
    sgswnd_(&rxmin, &rxmax, &rymin, &rymax);
    sgstrn_(&c__1);
    sgstrf_();
    return 0;
/* ----------------------------------------------------------------------- */

L_sgcls:
/*     / CLOSE SGKS / */
    swpcls_();
    swdcls_();
    return 0;
} /* sgopn_ */

/* Subroutine */ int sgopn_(integer *iws)
{
    return sgopn_0_(0, iws);
    }

/* Subroutine */ int sginit_(void)
{
    return sgopn_0_(1, (integer *)0);
    }

/* Subroutine */ int sgfrm_(void)
{
    return sgopn_0_(2, (integer *)0);
    }

/* Subroutine */ int sgcls_(void)
{
    return sgopn_0_(3, (integer *)0);
    }

