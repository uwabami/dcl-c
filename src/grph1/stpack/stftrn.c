/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 1.f;
static doublereal c_b19 = 10.;

/* ----------------------------------------------------------------------- */
/*     NORMALIZATION TRANSFORMATION (INCLUDING MAP PROJECTION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int stftrn_0_(int n__, real *ux, real *uy, real *vx, real *
	vy, integer *itr, real *cxa, real *cya, real *vxoff, real *vyoff)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double r_sign(real *, real *), r_lg10(real *), pow_dd(doublereal *, 
	    doublereal *);

    /* Local variables */
    static real cx, cy, xx, yy, vx0, vy0, rna, uxs, uys;
    static integer itrz;
    extern /* Subroutine */ int ct2bc_(real *, real *, real *, real *), 
	    ct2pc_(real *, real *, real *, real *), ct2cp_(real *, real *, 
	    real *, real *);
    static real uxmin, uymin, uxmax, uymax;
    extern /* Subroutine */ int g2fctr_(real *, real *, real *, real *), 
	    mpfek6_(real *, real *, real *, real *), g2ictr_(real *, real *, 
	    real *, real *), mpiek6_(real *, real *, real *, real *), mpfcoa_(
	    real *, real *, real *, real *), mpfcoc_(real *, real *, real *, 
	    real *), mpicoa_(real *, real *, real *, real *), mpicoc_(real *, 
	    real *, real *, real *), mpfaza_(real *, real *, real *, real *), 
	    mpfbon_(real *, real *, real *, real *), mpfcon_(real *, real *, 
	    real *, real *), mpfvdg_(real *, real *, real *, real *), mpfmil_(
	    real *, real *, real *, real *), mpfktd_(real *, real *, real *, 
	    real *), mpfmer_(real *, real *, real *, real *), msgdmp_(char *, 
	    char *, char *, ftnlen, ftnlen, ftnlen), mpfplc_(real *, real *, 
	    real *, real *), mpfhmr_(real *, real *, real *, real *), mpfcyl_(
	    real *, real *, real *, real *), mpfmwd_(real *, real *, real *, 
	    real *), mpfrbs_(real *, real *, real *, real *), mpfsin_(real *, 
	    real *, real *, real *), mpfotg_(real *, real *, real *, real *), 
	    mpfazm_(real *, real *, real *, real *), mpfgno_(real *, real *, 
	    real *, real *), mpicyl_(real *, real *, real *, real *), mpimer_(
	    real *, real *, real *, real *), sgqwnd_(real *, real *, real *, 
	    real *), mpimwd_(real *, real *, real *, real *), mpihmr_(real *, 
	    real *, real *, real *), mpiktd_(real *, real *, real *, real *), 
	    mpimil_(real *, real *, real *, real *), mpirbs_(real *, real *, 
	    real *, real *), mpfpst_(real *, real *, real *, real *), mpisin_(
	    real *, real *, real *, real *), mpivdg_(real *, real *, real *, 
	    real *), mpicon_(real *, real *, real *, real *), mpibon_(real *, 
	    real *, real *, real *), mpiplc_(real *, real *, real *, real *), 
	    mpiotg_(real *, real *, real *, real *), mpipst_(real *, real *, 
	    real *, real *), mpiazm_(real *, real *, real *, real *), mpiaza_(
	    real *, real *, real *, real *), mpigno_(real *, real *, real *, 
	    real *), stfusr_(real *, real *, real *, real *), glrget_(char *, 
	    real *, ftnlen), stiusr_(real *, real *, real *, real *);

    switch(n__) {
	case 1: goto L_stitrn;
	case 2: goto L_ststri;
	case 3: goto L_ststrp;
	}

    if (itrz == 1) {
	xx = *ux;
	yy = *uy;
    } else if (itrz == 2) {
	sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
	xx = *ux;
	uys = r_sign(&c_b2, &uymin) * *uy;
	if (uys <= 0.f) {
	    msgdmp_("E", "STFTRN", "UY HAS INVALID SIGN FOR LOG TRANSFORMATI"
		    "ON OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	}
	yy = r_lg10(&uys);
    } else if (itrz == 3) {
	sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
	uxs = r_sign(&c_b2, &uxmin) * *ux;
	if (uxs <= 0.f) {
	    msgdmp_("E", "STFTRN", "UX HAS INVALID SIGN FOR LOG TRANSFORMATI"
		    "ON OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	}
	xx = r_lg10(&uxs);
	yy = *uy;
    } else if (itrz == 4) {
	sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
	uxs = r_sign(&c_b2, &uxmin) * *ux;
	uys = r_sign(&c_b2, &uymin) * *uy;
	if (uxs <= 0.f) {
	    msgdmp_("E", "STFTRN", "UX HAS INVALID SIGN FOR LOG TRANSFORMATI"
		    "ON OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	}
	if (uys <= 0.f) {
	    msgdmp_("E", "STFTRN", "UY HAS INVALID SIGN FOR LOG TRANSFORMATI"
		    "ON OR IS 0", (ftnlen)1, (ftnlen)6, (ftnlen)50);
	}
	xx = r_lg10(&uxs);
	yy = r_lg10(&uys);
    } else if (itrz == 5) {
	ct2pc_(ux, uy, &xx, &yy);
    } else if (itrz == 6) {
	ct2bc_(ux, uy, &xx, &yy);
    } else if (itrz == 10) {
	mpfcyl_(ux, uy, &xx, &yy);
    } else if (itrz == 11) {
	mpfmer_(ux, uy, &xx, &yy);
    } else if (itrz == 12) {
	mpfmwd_(ux, uy, &xx, &yy);
    } else if (itrz == 13) {
	mpfhmr_(ux, uy, &xx, &yy);
    } else if (itrz == 14) {
	mpfek6_(ux, uy, &xx, &yy);
    } else if (itrz == 15) {
	mpfktd_(ux, uy, &xx, &yy);
    } else if (itrz == 16) {
	mpfmil_(ux, uy, &xx, &yy);
    } else if (itrz == 17) {
	mpfrbs_(ux, uy, &xx, &yy);
    } else if (itrz == 18) {
	mpfsin_(ux, uy, &xx, &yy);
    } else if (itrz == 19) {
	mpfvdg_(ux, uy, &xx, &yy);
    } else if (itrz == 20) {
	mpfcon_(ux, uy, &xx, &yy);
    } else if (itrz == 21) {
	mpfcoa_(ux, uy, &xx, &yy);
    } else if (itrz == 22) {
	mpfcoc_(ux, uy, &xx, &yy);
    } else if (itrz == 23) {
	mpfbon_(ux, uy, &xx, &yy);
    } else if (itrz == 24) {
	mpfplc_(ux, uy, &xx, &yy);
    } else if (itrz == 30) {
	mpfotg_(ux, uy, &xx, &yy);
    } else if (itrz == 31) {
	mpfpst_(ux, uy, &xx, &yy);
    } else if (itrz == 32) {
	mpfazm_(ux, uy, &xx, &yy);
    } else if (itrz == 33) {
	mpfaza_(ux, uy, &xx, &yy);
    } else if (itrz == 34) {
	mpfgno_(ux, uy, &xx, &yy);
    } else if (itrz == 40) {
	mpfvdg_(ux, uy, &xx, &yy);
    } else if (itrz == 51) {
	g2fctr_(ux, uy, &xx, &yy);
    } else if (itrz == 99) {
	stfusr_(ux, uy, &xx, &yy);
    }
    if (xx == rna || yy == rna) {
	*vx = rna;
	*vy = rna;
    } else {
	*vx = cx * xx + vx0;
	*vy = cy * yy + vy0;
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_stitrn:
    xx = (*vx - vx0) / cx;
    yy = (*vy - vy0) / cy;
    if (itrz == 1) {
	*ux = xx;
	*uy = yy;
    } else if (itrz == 2) {
	sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
	*ux = xx;
	d__1 = (doublereal) yy;
	*uy = r_sign(&c_b2, &uymin) * pow_dd(&c_b19, &d__1);
    } else if (itrz == 3) {
	sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
	d__1 = (doublereal) xx;
	*ux = r_sign(&c_b2, &uxmin) * pow_dd(&c_b19, &d__1);
	*uy = *vy;
    } else if (itrz == 4) {
	sgqwnd_(&uxmin, &uxmax, &uymin, &uymax);
	d__1 = (doublereal) xx;
	*ux = r_sign(&c_b2, &uxmin) * pow_dd(&c_b19, &d__1);
	d__1 = (doublereal) yy;
	*uy = r_sign(&c_b2, &uymin) * pow_dd(&c_b19, &d__1);
    } else if (itrz == 5) {
	ct2cp_(&xx, &yy, ux, uy);
    } else if (itrz == 6) {
	msgdmp_("E", "STITRN", "INVERSE TRANSFORMATION IS NOT DEFINED FOR IT"
		"R=6.", (ftnlen)1, (ftnlen)6, (ftnlen)48);
    } else if (itrz == 10) {
	mpicyl_(&xx, &yy, ux, uy);
    } else if (itrz == 11) {
	mpimer_(&xx, &yy, ux, uy);
    } else if (itrz == 12) {
	mpimwd_(&xx, &yy, ux, uy);
    } else if (itrz == 13) {
	mpihmr_(&xx, &yy, ux, uy);
    } else if (itrz == 14) {
	mpiek6_(&xx, &yy, ux, uy);
    } else if (itrz == 15) {
	mpiktd_(&xx, &yy, ux, uy);
    } else if (itrz == 16) {
	mpimil_(&xx, &yy, ux, uy);
    } else if (itrz == 17) {
	mpirbs_(&xx, &yy, ux, uy);
    } else if (itrz == 18) {
	mpisin_(&xx, &yy, ux, uy);
    } else if (itrz == 19) {
	mpivdg_(&xx, &yy, ux, uy);
    } else if (itrz == 20) {
	mpicon_(&xx, &yy, ux, uy);
    } else if (itrz == 21) {
	mpicoa_(&xx, &yy, ux, uy);
    } else if (itrz == 22) {
	mpicoc_(&xx, &yy, ux, uy);
    } else if (itrz == 23) {
	mpibon_(&xx, &yy, ux, uy);
    } else if (itrz == 24) {
	mpiplc_(&xx, &yy, ux, uy);
    } else if (itrz == 30) {
	mpiotg_(&xx, &yy, ux, uy);
    } else if (itrz == 31) {
	mpipst_(&xx, &yy, ux, uy);
    } else if (itrz == 32) {
	mpiazm_(&xx, &yy, ux, uy);
    } else if (itrz == 33) {
	mpiaza_(&xx, &yy, ux, uy);
    } else if (itrz == 34) {
	mpigno_(&xx, &yy, ux, uy);
    } else if (itrz == 51) {
	g2ictr_(&xx, &yy, ux, uy);
    } else if (itrz == 99) {
	stiusr_(&xx, &yy, ux, uy);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_ststri:
    itrz = *itr;
    glrget_("RUNDEF", &rna, (ftnlen)6);
    return 0;
/* ----------------------------------------------------------------------- */

L_ststrp:
    cx = *cxa;
    cy = *cya;
    vx0 = *vxoff;
    vy0 = *vyoff;
    return 0;
} /* stftrn_ */

/* Subroutine */ int stftrn_(real *ux, real *uy, real *vx, real *vy)
{
    return stftrn_0_(0, ux, uy, vx, vy, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0);
    }

/* Subroutine */ int stitrn_(real *vx, real *vy, real *ux, real *uy)
{
    return stftrn_0_(1, ux, uy, vx, vy, (integer *)0, (real *)0, (real *)0, (
	    real *)0, (real *)0);
    }

/* Subroutine */ int ststri_(integer *itr)
{
    return stftrn_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, itr, (
	    real *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int ststrp_(real *cxa, real *cya, real *vxoff, real *vyoff)
{
    return stftrn_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (integer *
	    )0, cxa, cya, vxoff, vyoff);
    }

