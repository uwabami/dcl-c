/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgsmap_0_(int n__, real *simfac, real *vxoff, real *
	vyoff, real *plx, real *ply, real *plrot)
{
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), sgqsim_(real *, real *, real *), sgqmpl_(real *, 
	    real *, real *), sgssim_(real *, real *, real *), sgsmpl_(real *, 
	    real *, real *);

    switch(n__) {
	case 1: goto L_sgqmap;
	}

    msgdmp_("M", "SGSMAP", "THIS IS OLD INTERFACE - USE SGSSIM/SGSMPL !", (
	    ftnlen)1, (ftnlen)6, (ftnlen)43);
    sgssim_(simfac, vxoff, vyoff);
    sgsmpl_(plx, ply, plrot);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqmap:
    msgdmp_("M", "SGQMAP", "THIS IS OLD INTERFACE - USE SGQSIM/SGQMPL !", (
	    ftnlen)1, (ftnlen)6, (ftnlen)43);
    sgqsim_(simfac, vxoff, vyoff);
    sgqmpl_(plx, ply, plrot);
    return 0;
} /* sgsmap_ */

/* Subroutine */ int sgsmap_(real *simfac, real *vxoff, real *vyoff, real *
	plx, real *ply, real *plrot)
{
    return sgsmap_0_(0, simfac, vxoff, vyoff, plx, ply, plrot);
    }

/* Subroutine */ int sgqmap_(real *simfac, real *vxoff, real *vyoff, real *
	plx, real *ply, real *plrot)
{
    return sgsmap_0_(1, simfac, vxoff, vyoff, plx, ply, plrot);
    }

