/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sgstrc_0_(int n__, char *ctr, ftnlen ctr_len)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer itr;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen);
    extern integer isgtrc_(char *, ftnlen);
    extern /* Subroutine */ int sgtrnl_(integer *, char *, ftnlen), sgstrn_(
	    integer *), sgtrns_(integer *, char *, ftnlen);

    switch(n__) {
	case 1: goto L_sgqtrc;
	case 2: goto L_sgqtra;
	}

    msgdmp_("M", "SGSTRC", "THIS IS OLD INTERFACE - USE SGSTRN(ISGTRC) !", (
	    ftnlen)1, (ftnlen)6, (ftnlen)44);
    i__1 = isgtrc_(ctr, ctr_len);
    sgstrn_(&i__1);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqtrc:
    msgdmp_("M", "SGQTRC", "THIS IS OLD INTERFACE - USE SGTRNL !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgiget_("ITR", &itr, (ftnlen)3);
    sgtrnl_(&itr, ctr, ctr_len);
    return 0;
/* ----------------------------------------------------------------------- */

L_sgqtra:
    msgdmp_("M", "SGQTRA", "THIS IS OLD INTERFACE - USE SGTRNS !", (ftnlen)1, 
	    (ftnlen)6, (ftnlen)36);
    sgiget_("ITR", &itr, (ftnlen)3);
    sgtrns_(&itr, ctr, ctr_len);
    return 0;
} /* sgstrc_ */

/* Subroutine */ int sgstrc_(char *ctr, ftnlen ctr_len)
{
    return sgstrc_0_(0, ctr, ctr_len);
    }

/* Subroutine */ int sgqtrc_(char *ctr, ftnlen ctr_len)
{
    return sgstrc_0_(1, ctr, ctr_len);
    }

/* Subroutine */ int sgqtra_(char *ctr, ftnlen ctr_len)
{
    return sgstrc_0_(2, ctr, ctr_len);
    }

