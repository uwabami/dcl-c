/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__2 = 2;

/* ----------------------------------------------------------------------- */
/*     INTERPOLATION FOR ARROW PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpipz_0_(int n__, real *uxp0, real *uyp0, real *uxp1, 
	real *uyp1, integer *nn, real *uxg0, real *uyg0, real *uxg1, real *
	uyg1, integer *n0)
{
    /* Initialized data */

    static logical lfrst = TRUE_;

    static integer n1, nc;
    static real pi, xz[100], yz[100], ux0, uy0, ux1, uy1, xx1, xx0, yy1, yy0, 
	    dxx;
    static logical lmap;
    extern real rfpi_(void);
    extern /* Subroutine */ int stfrad_(real *, real *, real *, real *), 
	    stirad_(real *, real *, real *, real *), msgdmp_(char *, char *, 
	    char *, ftnlen, ftnlen, ftnlen);
    static logical lcontt, lcontu;
    extern /* Subroutine */ int szgipl_(real *, real *, logical *), szgipt_(
	    real *, real *, logical *), szpipl_(real *, real *, real *, real *
	    , integer *), stqtrf_(logical *), szpipt_(real *, real *, real *, 
	    real *, integer *);

    switch(n__) {
	case 1: goto L_szgipz;
	}

    pi = rfpi_();
    stqtrf_(&lmap);
    stfrad_(uxp0, uyp0, &ux0, &uy0);
    stfrad_(uxp1, uyp1, &ux1, &uy1);
    dxx = ux1 - ux0;
    if (lmap) {
	if (dxx > pi) {
	    ux1 -= pi * 2;
	} else if (dxx < -pi) {
	    ux1 += pi * 2;
	}
    }
    xx1 = ux0;
    yy1 = uy0;
    nc = 1;
    xz[0] = ux0;
    yz[0] = uy0;
    szpipl_(&ux0, &uy0, &ux1, &uy1, &c__2);
L10:
    xx0 = xx1;
    yy0 = yy1;
    szgipl_(&xx1, &yy1, &lcontu);
    szpipt_(&xx0, &yy0, &xx1, &yy1, &c__2);
L20:
    ++nc;
    if (nc > 100) {
	if (lfrst) {
	    msgdmp_("M", "SGSIPU", "WORKING AREA OVERFLOW.", (ftnlen)1, (
		    ftnlen)6, (ftnlen)22);
	    lfrst = FALSE_;
	}
	xz[98] = xz[99];
	yz[98] = yz[99];
	nc = 100;
    }
    szgipt_(&xz[nc - 1], &yz[nc - 1], &lcontt);
    if (lcontt) {
	goto L20;
    }
    if (lcontu) {
	goto L10;
    }
    *nn = nc - 1;
    return 0;
/* ----------------------------------------------------------------------- */

L_szgipz:
    n1 = *n0 + 1;
    stirad_(&xz[*n0 - 1], &yz[*n0 - 1], uxg0, uyg0);
    stirad_(&xz[n1 - 1], &yz[n1 - 1], uxg1, uyg1);
    return 0;
} /* szpipz_ */

/* Subroutine */ int szpipz_(real *uxp0, real *uyp0, real *uxp1, real *uyp1, 
	integer *nn)
{
    return szpipz_0_(0, uxp0, uyp0, uxp1, uyp1, nn, (real *)0, (real *)0, (
	    real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int szgipz_(real *uxg0, real *uyg0, real *uxg1, real *uyg1, 
	integer *n0)
{
    return szpipz_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (integer *
	    )0, uxg0, uyg0, uxg1, uyg1, n0);
    }

