/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

/* Table of constant values */

static integer c__0 = 0;
static real c_b4 = 1e-5f;
static integer c_n1 = -1;
static real c_b26 = 4.f;
static integer c__4 = 4;

/* ----------------------------------------------------------------------- */
/*     TONE ROUTINE ON RC (HARD FILL - CLIPPING) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoptr_0_(int n__, real *rx, real *ry)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1;

    /* Builtin functions */
    integer i_nint(real *), pow_ii(integer *, integer *);

    /* Local variables */
    static integer i__, j, ii, jj;
    static real xb[2], yb[2];
    static integer nn, nx, ns0, ns1;
    static real rx0, ry0, rx1, ry1, dif;
    static integer nbg;
    static real bdx[4], bdy[4], pos[100];
    static integer nst;
    static real rxx[16384], dif0, ryy[16384], pos0, pos1;
    static integer nbgn[100], nend;
    extern integer imod_(integer *, integer *);
    static integer nlen[100];
    extern real rmod_(real *, real *);
    static integer iline;
    extern logical lreqa_(real *, real *, real *);
    static integer nline;
    static logical lcont, lmove;
    static integer nstat[100];
    static logical lvalid;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szgcll_(real *, real *, logical *, logical *, 
	    integer *);
    static logical lfirst;
    static real xwidth, ywidth;
    extern /* Subroutine */ int szqcll_(real *, real *, real *, real *, 
	    integer *), szpcll_(real *, real *, real *, real *, logical *, 
	    integer *), szcltz_(void), szoptz_(void), szsttz_(real *, real *);

/*     POSBD RAGES FROM 0 TO 4. (AT UPPER LEFT CORNER) */
/*     ANTI-CLOCK WISE (IMODE=0), CLOCK WISE (IMODE=1) */
    switch(n__) {
	case 1: goto L_szsttr;
	case 2: goto L_szcltr;
	}

    szqcll_(xb, &xb[1], yb, &yb[1], &c__0);
    xwidth = xb[1] - xb[0];
    ywidth = yb[1] - yb[0];
    for (i__ = 1; i__ <= 4; ++i__) {
	bdx[i__ - 1] = xb[(i__ + 3 + szbtn2_1.irmodr) / 2 % 2];
	bdy[i__ - 1] = yb[(i__ + 2 - szbtn2_1.irmodr) / 2 % 2];
/* L5: */
    }
    nn = 0;
    nline = 0;
    lfirst = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_szsttr:
    if (lfirst) {
	rx0 = *rx;
	ry0 = *ry;
	lfirst = FALSE_;
    } else {
	if (lreqa_(&rx0, &rx1, &c_b4) && lreqa_(&ry0, &ry1, &c_b4)) {
	    return 0;
	}
    }
    szpcll_(&rx0, &ry0, rx, ry, &lvalid, &c__0);
    if (lvalid) {
L10:
	++nn;
	if (nn > 16384) {
	    msgdmp_("E", "SZSTTR", "WORKING AREA OVER FLOW (TOO MANY POINTS)",
		     (ftnlen)1, (ftnlen)6, (ftnlen)40);
	}
	szgcll_(&rxx[nn - 1], &ryy[nn - 1], &lcont, &lmove, &c__0);
	if (lmove) {
	    ++nline;
	    if (nline > 100) {
		msgdmp_("E", "SZSTTR", "WORKING AREA OVER FLOW (TOO MANY LIN"
			"ES)", (ftnlen)1, (ftnlen)6, (ftnlen)39);
	    }
	    nbgn[nline - 1] = nn;
	}
	if (lcont) {
	    goto L10;
	}
    }
    rx0 = *rx;
    ry0 = *ry;
    return 0;
/* ----------------------------------------------------------------------- */

L_szcltr:
    if (lfirst) {
	return 0;
    }
    if (nline == 0) {
	if (nn >= 3) {
	    szoptz_();
	    i__1 = nn;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		szsttz_(&rxx[i__ - 1], &ryy[i__ - 1]);
/* L200: */
	    }
	    szcltz_();
	}
	return 0;
    } else {
/*       / PREPEARIG / */
	i__1 = nline - 1;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    nlen[i__ - 1] = nbgn[i__] - nbgn[i__ - 1];
/* L300: */
	}
	nlen[nline - 1] = nn - nbgn[nline - 1] + nbgn[0];
	i__1 = nline;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    nstat[i__ - 1] = 0;
	    if (nlen[i__ - 1] <= 2) {
		nstat[i__ - 1] = 2;
	    }
	    nbg = nbgn[i__ - 1];
	    r__1 = ((xb[1] - rxx[nbg - 1]) / xwidth + (yb[1] - ryy[nbg - 1]) /
		     ywidth) / 2;
	    i__2 = i_nint(&r__1) + szbtn2_1.irmodr;
	    pos[i__ - 1] = pow_ii(&c_n1, &i__2) * ((xb[1] - rxx[nbg - 1]) / 
		    xwidth + (ryy[nbg - 1] - yb[0]) / ywidth) + 2;
/* L310: */
	}
/*       / SCANNING STARTING POINT / */
L400:
	nx = 0;
	i__1 = nline;
	for (iline = 1; iline <= i__1; ++iline) {
	    if (nstat[iline - 1] == 0) {
		goto L420;
	    }
/* L410: */
	}
	return 0;
L420:
	nstat[iline - 1] = 1;
	szoptz_();
L430:
	nend = nbgn[iline - 1] + nlen[iline - 1] - 1;
	i__1 = nend;
	for (j = nbgn[iline - 1]; j <= i__1; ++j) {
	    ++nx;
	    jj = (j - 1) % nn + 1;
	    szsttz_(&rxx[jj - 1], &ryy[jj - 1]);
/* L440: */
	}
	nend = (nend - 1) % nn + 1;
	r__1 = ((xb[1] - rxx[nend - 1]) / xwidth + (yb[1] - ryy[nend - 1]) / 
		ywidth) / 2;
	i__1 = i_nint(&r__1) + szbtn2_1.irmodr;
	pos0 = pow_ii(&c_n1, &i__1) * ((xb[1] - rxx[nend - 1]) / xwidth + (
		ryy[nend - 1] - yb[0]) / ywidth) + 2;
/*         / SEARCH NEXT LINE / */
	dif0 = 4.f;
	i__1 = nline;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (nstat[i__ - 1] == 2) {
		goto L450;
	    }
	    r__1 = pos[i__ - 1] - pos0 + 1e-5f;
	    dif = rmod_(&r__1, &c_b26);
	    if (dif >= dif0) {
		goto L450;
	    }
	    if (nlen[i__ - 1] <= 2) {
		goto L450;
	    }
	    iline = i__;
	    dif0 = dif;
L450:
	    ;
	}
/*         / BOUNDARY / */
	pos1 = pos[iline - 1];
	ns0 = pos0 + 1;
	ns1 = pos1 + 1;
	nst = ns0 + 1;
	i__1 = ns1 - ns0;
	nend = ns0 + imod_(&i__1, &c__4);
	if (ns0 == ns1 && pos0 > pos1 + 1e-5f) {
	    nend += 4;
	}
	i__1 = nend;
	for (i__ = nst; i__ <= i__1; ++i__) {
	    i__2 = i__ - 1;
	    ii = imod_(&i__2, &c__4) + 1;
	    szsttz_(&bdx[ii - 1], &bdy[ii - 1]);
/* L600: */
	}
	if (nstat[iline - 1] == 0) {
	    nstat[iline - 1] = 2;
	    goto L430;
	}
	nstat[iline - 1] = 2;
	szcltz_();
	goto L400;
    }
    return 0;
} /* szoptr_ */

/* Subroutine */ int szoptr_(void)
{
    return szoptr_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szsttr_(real *rx, real *ry)
{
    return szoptr_0_(1, rx, ry);
    }

/* Subroutine */ int szcltr_(void)
{
    return szoptr_0_(2, (real *)0, (real *)0);
    }

