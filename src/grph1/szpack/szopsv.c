/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lclip;
} szbtx3_;

#define szbtx3_1 szbtx3_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON VC (CLIPPING - SOLID LINE FOR TEXT) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szopsv_0_(int n__, real *vx, real *vy)
{
    static real xx, yy, vx0, vy0;
    static logical lcont, lmove, lvalid;
    extern /* Subroutine */ int szgcll_(real *, real *, logical *, logical *, 
	    integer *), szpcll_(real *, real *, real *, real *, logical *, 
	    integer *), szcllp_(void), szpllp_(real *, real *), szoplp_(void),
	     szmvlp_(real *, real *);

    switch(n__) {
	case 1: goto L_szmvsv;
	case 2: goto L_szplsv;
	case 3: goto L_szclsv;
	}

    szoplp_();
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvsv:
    if (szbtx3_1.lclip) {
	szpcll_(vx, vy, vx, vy, &lvalid, &c__1);
	if (lvalid) {
	    szmvlp_(vx, vy);
	}
	vx0 = *vx;
	vy0 = *vy;
    } else {
	szmvlp_(vx, vy);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szplsv:
    if (szbtx3_1.lclip) {
	szpcll_(&vx0, &vy0, vx, vy, &lvalid, &c__1);
	if (lvalid) {
L10:
	    szgcll_(&xx, &yy, &lcont, &lmove, &c__1);
	    if (lmove) {
		szmvlp_(&xx, &yy);
	    } else {
		szpllp_(&xx, &yy);
	    }
	    if (lcont) {
		goto L10;
	    }
	}
	vx0 = *vx;
	vy0 = *vy;
    } else {
	szpllp_(vx, vy);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szclsv:
    szcllp_();
    return 0;
} /* szopsv_ */

/* Subroutine */ int szopsv_(void)
{
    return szopsv_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvsv_(real *vx, real *vy)
{
    return szopsv_0_(1, vx, vy);
    }

/* Subroutine */ int szplsv_(real *vx, real *vy)
{
    return szopsv_0_(2, vx, vy);
    }

/* Subroutine */ int szclsv_(void)
{
    return szopsv_0_(3, (real *)0, (real *)0);
    }

