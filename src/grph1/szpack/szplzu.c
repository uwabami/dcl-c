/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
} szbpl1_;

#define szbpl1_1 szbpl1_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szplzu_(integer *n, real *upx, real *upy)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    static logical lflag;
    extern /* Subroutine */ int szcllu_(void), szpllu_(real *, real *), 
	    szoplu_(void), szmvlu_(real *, real *);

    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    szoplu_();
    if (! szbpl1_1.lmiss) {
	szmvlu_(&upx[1], &upy[1]);
	i__1 = *n;
	for (i__ = 2; i__ <= i__1; ++i__) {
	    szpllu_(&upx[i__], &upy[i__]);
/* L10: */
	}
    } else {
	lflag = FALSE_;
	i__1 = *n;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (upx[i__] == szbpl1_1.rmiss || upy[i__] == szbpl1_1.rmiss) {
		lflag = FALSE_;
	    } else if (lflag) {
		szpllu_(&upx[i__], &upy[i__]);
	    } else {
		szmvlu_(&upx[i__], &upy[i__]);
		lflag = TRUE_;
	    }
/* L20: */
	}
    }
    szcllu_();
    return 0;
} /* szplzu_ */

