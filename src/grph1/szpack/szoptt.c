/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

/* Table of constant values */

static real c_b3 = 1e-5f;
static integer c__1 = 1;
static logical c_false = FALSE_;
static integer c_n1 = -1;
static real c_b31 = 4.f;
static integer c__4 = 4;
static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     TONE ROUTINE ON TC */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoptt_0_(int n__, real *tx, real *ty)
{
    /* System generated locals */
    integer i__1, i__2;
    real r__1, r__2, r__3;

    /* Builtin functions */
    integer i_nint(real *), pow_ii(integer *, integer *);

    /* Local variables */
    static integer i__, j, i1, i2;
    static real x1, y1, x2, y2;
    static integer jj;
    static real pi, xb[2], yb[2];
    static integer nn, nx;
    static real vx, vy, xx, yy, bx0, by0, cx0, cy0, bx1, by1, cx1, cy1;
    static integer ns0, ns1;
    static real tx0, tx1, ty1, ty0, dif;
    static integer nbg;
    static real bdx[4], bdy[4], pos[100], dif0, txx[8192], tyy[8192], pos0, 
	    pos1;
    static integer nbgn[100], nend;
    extern integer imod_(integer *, integer *);
    static integer nlen[100];
    extern real rfpi_(void), rmod_(real *, real *);
    static integer next, nlst, iline;
    extern logical lreqa_(real *, real *, real *);
    static integer nline;
    static logical lmove;
    static integer nstat[100];
    static logical lvldx, lvldy, lcont1, lcont2, lcont3;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);
    static logical lfirst;
    static real xwidth, ywidth;
    extern /* Subroutine */ int szgipl_(real *, real *, logical *), szgclx_(
	    real *, real *, logical *, logical *), szgcly_(real *, real *, 
	    logical *);
    extern real xmplon_(real *);
    extern /* Subroutine */ int szgipt_(real *, real *, logical *), stftrn_(
	    real *, real *, real *, real *), szpipl_(real *, real *, real *, 
	    real *, integer *), szpclx_(real *, real *, real *, real *, 
	    logical *, logical *), szqclx_(real *, real *), szqcly_(real *, 
	    real *), szpcly_(real *, real *, real *, real *, logical *, 
	    logical *), szcltv_(void), szpipt_(real *, real *, real *, real *,
	     integer *), szoptv_(void), szsttv_(real *, real *);

/*     POSBD RAGES FROM 0 TO 4. (AT UPPER LEFT CORNER) */
/*     ANTI-CLOCK WISE (IMODE=0), CLOCK WISE (IMODE=1) */
    switch(n__) {
	case 1: goto L_szsttt;
	case 2: goto L_szcltt;
	}

    pi = rfpi_();
    szqclx_(xb, &xb[1]);
    szqcly_(yb, &yb[1]);
    xwidth = xb[1] - xb[0];
    ywidth = yb[1] - yb[0];
    for (i__ = 1; i__ <= 4; ++i__) {
	bdx[i__ - 1] = xb[(i__ + 3 + szbtn2_1.irmode) / 2 % 2];
	bdy[i__ - 1] = yb[(i__ + 2 - szbtn2_1.irmode) / 2 % 2];
/* L5: */
    }
    nn = 0;
    nline = 0;
    lfirst = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_szsttt:
    tx1 = xmplon_(tx);
    ty1 = *ty;
    if (lfirst) {
	lfirst = FALSE_;
	tx0 = tx1;
	ty0 = ty1;
	bx0 = tx1;
	by0 = ty1;
	cx0 = tx1;
	cy0 = ty1;
    } else {
	if (lreqa_(&tx1, &tx0, &c_b3) && lreqa_(&ty1, &ty0, &c_b3)) {
	    return 0;
	}
    }
    szpipt_(&tx0, &ty0, &tx1, &ty1, &c__1);
L10:
    szgipt_(&bx1, &by1, &lcont1);
    szpcly_(&bx0, &by0, &bx1, &by1, &lvldy, &c_false);
    if (lvldy) {
L20:
	szgcly_(&cx1, &cy1, &lcont2);
	cx1 = xmplon_(&cx1);
	szpclx_(&cx0, &cy0, &cx1, &cy1, &lvldx, &c_false);
	if (lvldx) {
L30:
	    ++nn;
	    if (nn > 8192) {
		msgdmp_("E", "SZSTTT", "WORKING AREA OVER FLOW. (TOO MANY PO"
			"INTS)", (ftnlen)1, (ftnlen)6, (ftnlen)41);
	    }
	    szgclx_(&txx[nn - 1], &tyy[nn - 1], &lcont3, &lmove);
	    r__2 = (r__1 = tyy[nn - 1], abs(r__1));
	    r__3 = pi / 2;
	    if (! lreqa_(&r__2, &r__3, &c_b3)) {
		if (lreqa_(&txx[nn - 1], xb, &c_b3)) {
		    txx[nn - 1] = xb[0] - 1e-5f;
		} else if (lreqa_(&txx[nn - 1], &xb[1], &c_b3)) {
		    txx[nn - 1] = xb[1] + 1e-5f;
		}
	    }
	    if (lmove || lcont2) {
		++nline;
		if (nline > 100) {
		    msgdmp_("E", "SZSTTT", "WORKING AREA OVER FLOW. (TOO MAN"
			    "Y CROSSINGS)", (ftnlen)1, (ftnlen)6, (ftnlen)44);
		}
		nbgn[nline - 1] = nn;
	    }
	    if (lcont3) {
		goto L30;
	    }
	}
	cx0 = cx1;
	cy0 = cy1;
	if (lcont2) {
	    goto L20;
	}
    }
    bx0 = bx1;
    by0 = by1;
    if (lcont1) {
	goto L10;
    }
    tx0 = tx1;
    ty0 = ty1;
    return 0;
/* ----------------------------------------------------------------------- */

L_szcltt:
    if (lfirst) {
	return 0;
    }
    if (nline == 0) {
	if (nn >= 3) {
	    szoptv_();
	    i__1 = nn;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		stftrn_(&txx[i__ - 1], &tyy[i__ - 1], &vx, &vy);
		szsttv_(&vx, &vy);
/* L200: */
	    }
	    szcltv_();
	}
	return 0;
    } else {
/*       / PREPEARIG / */
	i__1 = nline - 1;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    nlen[i__ - 1] = nbgn[i__] - nbgn[i__ - 1];
/* L300: */
	}
	nlen[nline - 1] = nn - nbgn[nline - 1] + nbgn[0];
	i__1 = nline;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    nstat[i__ - 1] = 0;
	    if (nlen[i__ - 1] <= 2) {
		nstat[i__ - 1] = 2;
	    }
	    nbg = nbgn[i__ - 1];
	    r__1 = ((xb[1] - txx[nbg - 1]) / xwidth + (yb[1] - tyy[nbg - 1]) /
		     ywidth) / 2;
	    i__2 = i_nint(&r__1) + szbtn2_1.irmode;
	    pos[i__ - 1] = pow_ii(&c_n1, &i__2) * ((xb[1] - txx[nbg - 1]) / 
		    xwidth + (tyy[nbg - 1] - yb[0]) / ywidth) + 2;
/* L310: */
	}
/*       / SCANNING STARTING POINT / */
L400:
	nx = 0;
	i__1 = nline;
	for (iline = 1; iline <= i__1; ++iline) {
	    if (nstat[iline - 1] == 0) {
		goto L420;
	    }
/* L410: */
	}
	return 0;
L420:
	nstat[iline - 1] = 1;
	szoptv_();
L430:
	nend = nbgn[iline - 1] + nlen[iline - 1] - 1;
	i__1 = nend;
	for (j = nbgn[iline - 1]; j <= i__1; ++j) {
	    ++nx;
	    jj = (j - 1) % nn + 1;
	    stftrn_(&txx[jj - 1], &tyy[jj - 1], &vx, &vy);
	    szsttv_(&vx, &vy);
/* L440: */
	}
	nend = (nend - 1) % nn + 1;
	r__1 = ((xb[1] - txx[nend - 1]) / xwidth + (yb[1] - tyy[nend - 1]) / 
		ywidth) / 2;
	i__1 = i_nint(&r__1) + szbtn2_1.irmode;
	pos0 = pow_ii(&c_n1, &i__1) * ((xb[1] - txx[nend - 1]) / xwidth + (
		tyy[nend - 1] - yb[0]) / ywidth) + 2;
/*         / SEARCH NEXT LINE / */
	dif0 = 4.f;
	i__1 = nline;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (nstat[i__ - 1] == 2) {
		goto L450;
	    }
	    r__1 = pos[i__ - 1] - pos0 + .001f;
	    dif = rmod_(&r__1, &c_b31);
	    if (dif >= dif0) {
		goto L450;
	    }
	    if (nlen[i__ - 1] <= 2) {
		goto L450;
	    }
	    iline = i__;
	    dif0 = dif;
L450:
	    ;
	}
/*         / BOUNDARY / */
	pos1 = pos[iline - 1];
	ns0 = pos0 + 1;
	ns1 = pos1 + 1;
	i__1 = ns1 - ns0;
	nlst = ns0 + imod_(&i__1, &c__4);
	if (ns0 == ns1 && pos0 > pos1 + .001f) {
	    nlst += 4;
	}
	next = nbgn[iline - 1];
	i__1 = nlst;
	for (i__ = ns0; i__ <= i__1; ++i__) {
	    i__2 = i__ - 1;
	    i1 = imod_(&i__2, &c__4) + 1;
	    i2 = imod_(&i__, &c__4) + 1;
	    if (i__ == ns0) {
		x1 = txx[nend - 1];
		y1 = tyy[nend - 1];
	    } else {
		x1 = bdx[i1 - 1];
		y1 = bdy[i1 - 1];
	    }
	    if (i__ == nlst) {
		x2 = txx[next - 1];
		y2 = tyy[next - 1];
	    } else {
		x2 = bdx[i2 - 1];
		y2 = bdy[i2 - 1];
	    }
	    szpipl_(&x1, &y1, &x2, &y2, &c__3);
L520:
	    szgipl_(&xx, &yy, &lcont1);
	    stftrn_(&xx, &yy, &vx, &vy);
	    szsttv_(&vx, &vy);
	    if (lcont1) {
		goto L520;
	    }
/* L600: */
	}
	if (nstat[iline - 1] == 0) {
	    nstat[iline - 1] = 2;
	    goto L430;
	}
	nstat[iline - 1] = 2;
	szcltv_();
	goto L400;
    }
    return 0;
} /* szoptt_ */

/* Subroutine */ int szoptt_(void)
{
    return szoptt_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szsttt_(real *tx, real *ty)
{
    return szoptt_0_(1, tx, ty);
    }

/* Subroutine */ int szcltt_(void)
{
    return szoptt_0_(2, (real *)0, (real *)0);
    }

