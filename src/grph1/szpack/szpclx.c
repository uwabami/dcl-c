/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static real c_b2 = 1e-5f;
static real c_b22 = 0.f;
static real c_b23 = 1e-4f;

/* ----------------------------------------------------------------------- */
/*     CLIPPING ON TC - X */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpclx_0_(int n__, real *tx0, real *ty0, real *tx1, real 
	*ty1, logical *lvalid, logical *lbound, real *tx, real *ty, logical *
	lcont, logical *lmove, real *xbnd1, real *xbnd2)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double r_sign(real *, real *);

    /* Local variables */
    static real pi, xb[2], pi2, px0, py0, px1, py1, cxx, dxx, pxx;
    static integer nend, nmod;
    extern real rfpi_(void);
    extern logical lrnea_(real *, real *, real *), lreqa_(real *, real *, 
	    real *);
    extern /* Subroutine */ int szsgcl_(real *, real *, real *, real *), 
	    szqgcy_(real *, real *);
    extern real szxmod_(real *);

    switch(n__) {
	case 1: goto L_szgclx;
	case 2: goto L_szsclx;
	case 3: goto L_szqclx;
	}

    pi = rfpi_();
    pi2 = pi / 2;
    px0 = *tx0;
    py0 = *ty0;
    px1 = *tx1;
    py1 = *ty1;
    r__1 = px1 - px0;
    dxx = szxmod_(&r__1);
    pxx = px0 + dxx;
    if ((lreqa_(xb, &px0, &c_b2) || lreqa_(&px0, &xb[1], &c_b2)) && *lbound) {
	if (lreqa_(xb, &px1, &c_b2) || lreqa_(&px1, &xb[1], &c_b2)) {
	    if (lrnea_(&pxx, &px1, &c_b2)) {
		nmod = 2;
		nend = 2;
	    } else {
		nmod = 3;
		nend = 3;
	    }
	} else if (xb[0] < px1 && px1 < xb[1]) {
	    nmod = 3;
	    nend = 3;
	    if (lrnea_(&pxx, &px1, &c_b2)) {
		nmod = 2;
	    }
	} else {
	    *lvalid = FALSE_;
	    return 0;
	}
    } else if (xb[0] < px0 && px0 < xb[1] && ! (lreqa_(xb, &px0, &c_b2) || 
	    lreqa_(&px0, &xb[1], &c_b2))) {
	if ((lreqa_(xb, &px1, &c_b2) || lreqa_(&px1, &xb[1], &c_b2)) && *
		lbound) {
	    nmod = 1;
	    nend = 1;
	    if (lrnea_(&pxx, &px1, &c_b2)) {
		nend = 2;
	    }
	} else if (xb[0] < px1 && px1 < xb[1] && ! (lreqa_(xb, &px1, &c_b2) ||
		 lreqa_(&px1, &xb[1], &c_b2))) {
	    nmod = 3;
	    nend = 3;
	    if (lrnea_(&pxx, &px1, &c_b2)) {
		nmod = 1;
	    }
	} else {
	    nmod = 1;
	    nend = 1;
	}
    } else {
	if ((lreqa_(xb, &px1, &c_b2) || lreqa_(&px1, &xb[1], &c_b2)) && *
		lbound) {
	    nmod = 2;
	    nend = 2;
	} else if (xb[0] < px1 && px1 < xb[1]) {
	    nmod = 2;
	    nend = 3;
	} else {
	    *lvalid = FALSE_;
	    return 0;
	}
    }
    *lvalid = TRUE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_szgclx:
/*     NMOD 1: PLOT TO BOUNDARY */
/*          2: MOVE TO BOUNDARY */
/*          3: PLOT TO (TX1, TY1) */
    *lmove = nmod == 2;
    if (nmod == 3) {
	*tx = px1;
	*ty = py1;
    } else if (nmod == 1 && (lreqa_(xb, &px1, &c_b2) || lreqa_(&px1, &xb[1], &
	    c_b2))) {
	*tx = pxx;
	*ty = py1;
    } else {
	if (nmod == 1) {
	    cxx = px0 + dxx / 2;
	} else if (nmod == 2) {
	    cxx = px1 - dxx / 2;
	}
	if ((r__1 = cxx - xb[0], abs(r__1)) < (r__2 = cxx - xb[1], abs(r__2)))
		 {
	    *tx = xb[0];
	} else {
	    *tx = xb[1];
	}
	r__1 = abs(py1);
	if (lreqa_(&r__1, &pi2, &c_b2)) {
	    *ty = py1;
	} else /* if(complicated condition) */ {
	    r__1 = abs(py0);
	    if (lreqa_(&r__1, &pi2, &c_b2)) {
		*ty = py0;
	    } else if (lreqa_(&dxx, &c_b22, &c_b23)) {
		*ty = py1;
	    } else /* if(complicated condition) */ {
		r__1 = abs(dxx);
		if (lreqa_(&r__1, &pi, &c_b23)) {
		    r__1 = py0 + py1;
		    *ty = r_sign(&pi2, &r__1);
		} else {
		    szsgcl_(&px0, &py0, &px1, &py1);
		    szqgcy_(tx, ty);
		}
	    }
	}
    }
    ++nmod;
    *lcont = nmod <= nend;
    return 0;
/* ----------------------------------------------------------------------- */

L_szsclx:
    xb[0] = *xbnd1;
    xb[1] = *xbnd2;
    return 0;
/* ----------------------------------------------------------------------- */

L_szqclx:
    *xbnd1 = xb[0];
    *xbnd2 = xb[1];
    return 0;
} /* szpclx_ */

/* Subroutine */ int szpclx_(real *tx0, real *ty0, real *tx1, real *ty1, 
	logical *lvalid, logical *lbound)
{
    return szpclx_0_(0, tx0, ty0, tx1, ty1, lvalid, lbound, (real *)0, (real *
	    )0, (logical *)0, (logical *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int szgclx_(real *tx, real *ty, logical *lcont, logical *
	lmove)
{
    return szpclx_0_(1, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, (logical *)0, tx, ty, lcont, lmove, (real *)0, (real *)0);
    }

/* Subroutine */ int szsclx_(real *xbnd1, real *xbnd2)
{
    return szpclx_0_(2, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, (logical *)0, (real *)0, (real *)0, (logical *)0, (logical *)
	    0, xbnd1, xbnd2);
    }

/* Subroutine */ int szqclx_(real *xbnd1, real *xbnd2)
{
    return szpclx_0_(3, (real *)0, (real *)0, (real *)0, (real *)0, (logical *
	    )0, (logical *)0, (real *)0, (real *)0, (logical *)0, (logical *)
	    0, xbnd1, xbnd2);
    }

