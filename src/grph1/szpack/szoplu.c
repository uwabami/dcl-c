/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     PLOT ROUTINE ON UC (LINEAR INTERPOLATION) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szoplu_0_(int n__, real *ux, real *uy)
{
    static real tx, ty, vx, vy, xx, yy, ux0, ux1, uy1, uy0;
    static logical lmap, lcont;
    extern /* Subroutine */ int stfrad_(real *, real *, real *, real *), 
	    szgipl_(real *, real *, logical *), szcllt_(void), szcllv_(void), 
	    stftrn_(real *, real *, real *, real *), stfrot_(real *, real *, 
	    real *, real *), szpipl_(real *, real *, real *, real *, integer *
	    ), stqtrf_(logical *), szpllt_(real *, real *), szpllv_(real *, 
	    real *), szoplt_(void), szoplv_(void), szmvlt_(real *, real *), 
	    szmvlv_(real *, real *);

    switch(n__) {
	case 1: goto L_szmvlu;
	case 2: goto L_szpllu;
	case 3: goto L_szcllu;
	}

    stqtrf_(&lmap);
    if (lmap) {
	szoplt_();
    } else {
	szoplv_();
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szmvlu:
    stfrad_(ux, uy, &ux1, &uy1);
    if (lmap) {
	stfrot_(&ux1, &uy1, &tx, &ty);
	szmvlt_(&tx, &ty);
    } else {
	stftrn_(&ux1, &uy1, &vx, &vy);
	szmvlv_(&vx, &vy);
    }
    ux0 = ux1;
    uy0 = uy1;
    return 0;
/* ----------------------------------------------------------------------- */

L_szpllu:
    stfrad_(ux, uy, &ux1, &uy1);
    szpipl_(&ux0, &uy0, &ux1, &uy1, &c__0);
L10:
    szgipl_(&xx, &yy, &lcont);
    if (lmap) {
	stfrot_(&xx, &yy, &tx, &ty);
	szpllt_(&tx, &ty);
    } else {
	stftrn_(&xx, &yy, &vx, &vy);
	szpllv_(&vx, &vy);
    }
    if (lcont) {
	goto L10;
    }
    ux0 = ux1;
    uy0 = uy1;
    return 0;
/* ----------------------------------------------------------------------- */

L_szcllu:
    if (lmap) {
	szcllt_();
    } else {
	szcllv_();
    }
    return 0;
} /* szoplu_ */

/* Subroutine */ int szoplu_(void)
{
    return szoplu_0_(0, (real *)0, (real *)0);
    }

/* Subroutine */ int szmvlu_(real *ux, real *uy)
{
    return szoplu_0_(1, ux, uy);
    }

/* Subroutine */ int szpllu_(real *ux, real *uy)
{
    return szoplu_0_(2, ux, uy);
    }

/* Subroutine */ int szcllu_(void)
{
    return szoplu_0_(3, (real *)0, (real *)0);
    }

