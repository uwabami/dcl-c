/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical lmiss;
    real rmiss;
    integer npm;
} szbpm1_;

#define szbpm1_1 szbpm1_

struct {
    char cmark[1];
} szbpm2_;

#define szbpm2_1 szbpm2_

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szpmzu_(integer *n, real *upx, real *upy)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer i__;
    static logical lflag;
    extern /* Subroutine */ int sztxzu_(real *, real *, char *, ftnlen);

    /* Parameter adjustments */
    --upy;
    --upx;

    /* Function Body */
    i__1 = *n;
    i__2 = szbpm1_1.npm;
    for (i__ = 1; i__2 < 0 ? i__ >= i__1 : i__ <= i__1; i__ += i__2) {
	lflag = szbpm1_1.lmiss && (upx[i__] == szbpm1_1.rmiss || upy[i__] == 
		szbpm1_1.rmiss);
	if (! lflag) {
	    sztxzu_(&upx[i__], &upy[i__], szbpm2_1.cmark, (ftnlen)1);
	}
/* L10: */
    }
    return 0;
} /* szpmzu_ */

