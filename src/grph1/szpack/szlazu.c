/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical larrwz, lpropz;
    real afactz, constz, anglez;
    logical latonz, luarwz;
    real consmz, rdunit;
} szbla1_;

#define szbla1_1 szbla1_

/* Table of constant values */

static real c_b2 = 0.f;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szlazu_(real *ux1, real *uy1, real *ux2, real *uy2)
{
    /* System generated locals */
    integer i__1;
    real r__1, r__2, r__3;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static integer n;
    static real ar, pi;
    static integer nn;
    static real xe, ye, rv, ph1, ph2, th1, th2, rx1, ry1, rx2, ry2, vx1, vy1, 
	    vx2, vy2, xt1, yt1, xt2, yt2, uxa, uya, phx, uxb, uyb, vxa, thx, 
	    vya, vxb, vyb, rvx;
    extern /* Subroutine */ int cr2c_(real *, real *, real *, real *, real *);
    static real phf1, phf2, thf1, thf2;
    extern /* Subroutine */ int cr3s_(real *, real *, real *, real *, real *, 
	    real *, real *);
    static logical lmap;
    extern real rfpi_(void);
    extern /* Subroutine */ int stfrad_(real *, real *, real *, real *), 
	    stirad_(real *, real *, real *, real *), stftrf_(real *, real *, 
	    real *, real *), szcllu_(void), szcllv_(void), stqtrf_(logical *),
	     szcltu_(void), szgipz_(real *, real *, real *, real *, integer *)
	    , szpllu_(real *, real *), szcltv_(void), szpllv_(real *, real *),
	     szoplu_(void), szoplv_(void), szpipz_(real *, real *, real *, 
	    real *, integer *), szmvlu_(real *, real *), szmvlv_(real *, real 
	    *), szoptu_(void), szoptv_(void), szsttu_(real *, real *), 
	    szsttv_(real *, real *);

    if (*ux1 == *ux2 && *uy1 == *uy2) {
	return 0;
    }
    szoplu_();
    szmvlu_(ux1, uy1);
    szpllu_(ux2, uy2);
    szcllu_();
    if (! szbla1_1.larrwz) {
	return 0;
    }
    pi = rfpi_();
    stqtrf_(&lmap);
    szpipz_(ux1, uy1, ux2, uy2, &nn);
    if (lmap) {
	r__1 = pi / szbla1_1.rdunit / 2 - *uy1;
	stfrad_(ux1, &r__1, &ph1, &th1);
	r__1 = pi / szbla1_1.rdunit / 2 - *uy2;
	stfrad_(ux2, &r__1, &ph2, &th2);
	cr3s_(&th2, &ph2, &c_b2, &th1, &ph1, &thx, &phx);
	rv = abs(thx);
	szgipz_(&rx1, &ry1, &rx2, &ry2, &nn);
	stftrf_(&rx1, &ry1, &vx1, &vy1);
	stftrf_(&rx2, &ry2, &vx2, &vy2);
    } else {
	rv = 0.f;
	i__1 = nn;
	for (n = 1; n <= i__1; ++n) {
	    szgipz_(&rx1, &ry1, &rx2, &ry2, &n);
	    stftrf_(&rx1, &ry1, &vx1, &vy1);
	    stftrf_(&rx2, &ry2, &vx2, &vy2);
/* Computing 2nd power */
	    r__1 = vx2 - vx1;
/* Computing 2nd power */
	    r__2 = vy2 - vy1;
	    rv += sqrt(r__1 * r__1 + r__2 * r__2);
/* L10: */
	}
    }
    if (szbla1_1.lpropz && szbla1_1.luarwz) {
	ar = rv * szbla1_1.afactz;
    } else {
	if (lmap && szbla1_1.luarwz) {
	    ar = szbla1_1.consmz * szbla1_1.rdunit;
	} else {
	    ar = szbla1_1.constz;
	}
    }
    if (lmap && szbla1_1.luarwz) {
	r__1 = pi / szbla1_1.rdunit / 2 - ry1;
	stfrad_(&rx1, &r__1, &ph1, &th1);
	r__1 = pi / szbla1_1.rdunit / 2 - ry2;
	stfrad_(&rx2, &r__1, &ph2, &th2);
	cr3s_(&th2, &ph2, &c_b2, &th1, &ph1, &thx, &phx);
	r__1 = -th2;
	r__2 = -ph2;
	r__3 = phx + szbla1_1.anglez * szbla1_1.rdunit;
	cr3s_(&r__1, &c_b2, &r__2, &ar, &r__3, &thf1, &phf1);
	r__1 = -th2;
	r__2 = -ph2;
	r__3 = phx - szbla1_1.anglez * szbla1_1.rdunit;
	cr3s_(&r__1, &c_b2, &r__2, &ar, &r__3, &thf2, &phf2);
	stirad_(&phf1, &thf1, &uxa, &uya);
	stirad_(&phf2, &thf2, &uxb, &uyb);
	uya = pi / szbla1_1.rdunit / 2 - uya;
	uyb = pi / szbla1_1.rdunit / 2 - uyb;
	if (szbla1_1.latonz) {
	    szoptu_();
	    szsttu_(&uxa, &uya);
	    szsttu_(ux2, uy2);
	    szsttu_(&uxb, &uyb);
	    szcltu_();
	} else {
	    szoplu_();
	    szmvlu_(&uxa, &uya);
	    szpllu_(ux2, uy2);
	    szpllu_(&uxb, &uyb);
	    szcllu_();
	}
    } else {
/* Computing 2nd power */
	r__1 = vx2 - vx1;
/* Computing 2nd power */
	r__2 = vy2 - vy1;
	rvx = sqrt(r__1 * r__1 + r__2 * r__2);
	xe = (vx2 - vx1) / rvx * ar;
	ye = (vy2 - vy1) / rvx * ar;
	r__1 = -(pi - szbla1_1.anglez * szbla1_1.rdunit);
	cr2c_(&r__1, &xe, &ye, &xt1, &yt1);
	r__1 = -(pi + szbla1_1.anglez * szbla1_1.rdunit);
	cr2c_(&r__1, &xe, &ye, &xt2, &yt2);
	vxa = vx2 + xt1;
	vya = vy2 + yt1;
	vxb = vx2 + xt2;
	vyb = vy2 + yt2;
	if (szbla1_1.latonz) {
	    szoptv_();
	    szsttv_(&vxa, &vya);
	    szsttv_(&vx2, &vy2);
	    szsttv_(&vxb, &vyb);
	    szcltv_();
	} else {
	    szoplv_();
	    szmvlv_(&vxa, &vya);
	    szpllv_(&vx2, &vy2);
	    szpllv_(&vxb, &vyb);
	    szcllv_();
	}
    }
    return 0;
} /* szlazu_ */

