/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical llnint, lgcint;
    real rdxr, rdyr;
} szbls1_;

#define szbls1_1 szbls1_

struct {
    integer irmode, irmodr;
} szbtn2_;

#define szbtn2_1 szbtn2_

struct {
    logical lclip;
} szbtn3_;

#define szbtn3_1 szbtn3_

/* Table of constant values */

static real c_b8 = 0.f;
static real c_b11 = 1.f;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     TONE PRIMITIVE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int sztnop_0_(int n__, integer *itpat)
{
    /* Builtin functions */
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;

    /* Local variables */
    static real rx0, ry0, rx1, ry1, rx2, ry2, rdx, rdy, rot;
    static char cobj[80];
    extern /* Subroutine */ int cdblk_(char *, ftnlen), stfpr2_(real *, real *
	    , real *, real *), sgiget_(char *, integer *, ftnlen), stfrad_(
	    real *, real *, real *, real *), sglget_(char *, logical *, 
	    ftnlen), sgrget_(char *, real *, ftnlen), swocls_(char *, ftnlen),
	     swoopn_(char *, char *, ftnlen, ftnlen), szstni_(integer *);

    /* Fortran I/O blocks */
    static icilist io___11 = { 0, cobj, 0, "(I8)", 80, 1 };


    switch(n__) {
	case 1: goto L_sztncl;
	}

    sglget_("LLNINT", &szbls1_1.llnint, (ftnlen)6);
    sglget_("LGCINT", &szbls1_1.lgcint, (ftnlen)6);
    sgrget_("RDX", &rdx, (ftnlen)3);
    sgrget_("RDY", &rdy, (ftnlen)3);
    sgiget_("IRMODE", &szbtn2_1.irmode, (ftnlen)6);
    sglget_("LCLIP", &szbtn3_1.lclip, (ftnlen)5);
    stfpr2_(&c_b8, &c_b8, &rx0, &ry0);
    stfpr2_(&c_b8, &c_b11, &rx1, &ry1);
    stfpr2_(&c_b11, &c_b8, &rx2, &ry2);
    rot = (rx2 - rx0) * (ry1 - ry0) - (ry2 - ry0) * (rx1 - rx0);
    szbtn2_1.irmode %= 2;
    if (rot > 0.f) {
	szbtn2_1.irmodr = szbtn2_1.irmode;
    } else {
	szbtn2_1.irmodr = (szbtn2_1.irmode + 1) % 2;
    }
    stfrad_(&rdx, &rdy, &szbls1_1.rdxr, &szbls1_1.rdyr);
    s_wsfi(&io___11);
    do_fio(&c__1, (char *)&(*itpat), (ftnlen)sizeof(integer));
    e_wsfi();
    cdblk_(cobj, (ftnlen)80);
    swoopn_("SZTN", cobj, (ftnlen)4, (ftnlen)80);
    szstni_(itpat);
    return 0;
/* ----------------------------------------------------------------------- */

L_sztncl:
    swocls_("SZTN", (ftnlen)4);
    return 0;
} /* sztnop_ */

/* Subroutine */ int sztnop_(integer *itpat)
{
    return sztnop_0_(0, itpat);
    }

/* Subroutine */ int sztncl_(void)
{
    return sztnop_0_(1, (integer *)0);
    }

