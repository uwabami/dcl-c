/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     LINE TYPE ATTRIBUTE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szstyp_0_(int n__, integer *itype)
{
    /* Initialized data */

    static char cpat[32*4] = "11111111111111111111111111111111" "11111111111"
	    "100001111111111110000" "11001100110011001100110011001100" "11111"
	    "111100010001111111110001000";
    static integer idashz = 0;
    static logical lfrst = TRUE_;

    static integer n, ipat[4];
    extern /* Subroutine */ int bitpci_(char *, integer *, ftnlen);
    static integer itypez;
    extern /* Subroutine */ int szstyz_(integer *);

    switch(n__) {
	case 1: goto L_szqtyp;
	}

    if (lfrst) {
	for (n = 1; n <= 4; ++n) {
	    bitpci_(cpat + (n - 1 << 5), &ipat[n - 1], (ftnlen)32);
/* L10: */
	}
	lfrst = FALSE_;
    }
    itypez = *itype;
    if (1 <= itypez && itypez <= 4) {
	idashz = ipat[itypez - 1];
    } else {
	idashz = itypez;
    }
    szstyz_(&idashz);
    return 0;
/* ----------------------------------------------------------------------- */

L_szqtyp:
    *itype = itypez;
    return 0;
} /* szstyp_ */

/* Subroutine */ int szstyp_(integer *itype)
{
    return szstyp_0_(0, itype);
    }

/* Subroutine */ int szqtyp_(integer *itype)
{
    return szstyp_0_(1, itype);
    }

