/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__16 = 16;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     TONE ROUTINE ON VC (SOFT FILL - BUFFERING) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szopts_0_(int n__, real *vx, real *vy, integer *itpat)
{
    /* Initialized data */

    static char cpat[16*5] = "1000000000000000" "1000000010000000" "10001000"
	    "10001000" "1010101010101010" "1111111111111111";
    static logical lfrst = TRUE_;

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsfi(icilist *), do_fio(integer *, char *, ftnlen), e_wsfi(void)
	    ;
    double sqrt(doublereal);

    /* Local variables */
    static integer i__, l1, l2, l3, lc;
    static real dw;
    static integer nn, lp, il1;
    static real rl1, vpx[8192], vpy[8192];
    static char cmsg[80];
    static integer ipat[5];
    static logical ldble;
    static integer index;
    static logical lundf;
    static real rspce;
    static logical lbits;
    static integer irota, itype;
    static logical lrtrn;
    extern /* Subroutine */ int bitpci_(char *, integer *, ftnlen);
    static real bitlen;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), sgrget_(char *, 
	    real *, ftnlen), sgiset_(char *, integer *, ftnlen);
    static real bitlnz;
    extern /* Subroutine */ int sgrset_(char *, real *, ftnlen);
    static integer nbitsz;
    extern /* Subroutine */ int sztnsv_(integer *, real *, real *, integer *, 
	    real *, integer *, integer *);

    /* Fortran I/O blocks */
    static icilist io___23 = { 0, cmsg+15, 0, "(I3)", 3, 1 };


    switch(n__) {
	case 1: goto L_szstts;
	case 2: goto L_szclts;
	case 3: goto L_sztits;
	}

    if (lrtrn) {
	return 0;
    }
    nn = 0;
    if (! lbits) {
	sgiset_("NBITS", &c__16, (ftnlen)5);
    }
    if (l3 == 0) {
	sgrset_("BITLEN", &rl1, (ftnlen)6);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szstts:
    if (lrtrn) {
	return 0;
    }
    ++nn;
    if (nn > 8192) {
	msgdmp_("E", "SZSTTS", "WORKING AREA OVER FLOW", (ftnlen)1, (ftnlen)6,
		 (ftnlen)22);
    }
    vpx[nn - 1] = *vx;
    vpy[nn - 1] = *vy;
    return 0;
/* ----------------------------------------------------------------------- */

L_szclts:
    if (lrtrn) {
	return 0;
    }
    sztnsv_(&nn, vpx, vpy, &irota, &rspce, &itype, &index);
    if (ldble) {
	i__1 = irota + 90;
	sztnsv_(&nn, vpx, vpy, &i__1, &rspce, &itype, &index);
    }
    if (! lbits) {
	sgiset_("NBITS", &nbitsz, (ftnlen)5);
    }
    if (l3 == 0) {
	sgrset_("BITLEN", &bitlnz, (ftnlen)6);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_sztits:
    l1 = *itpat % 10;
    l2 = *itpat / 10 % 10;
    l3 = *itpat / 100 % 10;
    lp = *itpat % 1000;
    lc = *itpat / 1000;
    lundf = FALSE_;
    lrtrn = FALSE_;
    if (0 <= l3 && l3 <= 6) {
	if (l1 == 0) {
	    lrtrn = TRUE_;
	} else if (l1 > 5) {
	    lundf = TRUE_;
	}
	if (l2 == 0) {
	    l2 = 1;
	} else if (l2 > 5) {
	    lundf = TRUE_;
	}
    }
    if (7 <= l3 && l3 <= 8) {
	lundf = TRUE_;
    }
    if (l3 == 9) {
	if (lp != 999) {
	    lundf = TRUE_;
	}
    }
    if (lundf) {
	s_copy(cmsg, "PATTERN NUMBER ### IS NOT DEFINED.", (ftnlen)80, (
		ftnlen)34);
	s_wsfi(&io___23);
	do_fio(&c__1, (char *)&lp, (ftnlen)sizeof(integer));
	e_wsfi();
	msgdmp_("M", "SZTNSR", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
	lrtrn = TRUE_;
    }
    if (lrtrn) {
	return 0;
    }
    if (lfrst) {
	sgiget_("NBITS", &nbitsz, (ftnlen)5);
	sgrget_("BITLEN", &bitlnz, (ftnlen)6);
	sgrget_("TNBLEN", &bitlen, (ftnlen)6);
	lbits = nbitsz == 16;
	for (i__ = 1; i__ <= 5; ++i__) {
	    bitpci_(cpat + (i__ - 1 << 4), &ipat[i__ - 1], (ftnlen)16);
/* L10: */
	}
	lfrst = FALSE_;
    }
    dw = bitlen * 16;
    ldble = FALSE_;
    if (l3 == 0) {
	il1 = (l1 + 1) / 2;
	if (l1 % 2 == 0) {
	    rl1 = bitlen / sqrt(2.f);
	} else {
	    rl1 = bitlen;
	}
	irota = (l1 + 1) % 2 * 45;
	rspce = rl1 * 16 / il1;
	itype = ipat[il1 - 1];
	index = lc * 10 + l2;
    } else if (1 <= l3 && l3 <= 4) {
	if (l3 % 2 == 0) {
	    dw /= sqrt(2.f);
	}
	irota = (l3 - 1) * 45;
	rspce = dw / l1;
	itype = 1;
	index = lc * 10 + l2;
    } else if (5 <= l3 && l3 <= 6) {
	ldble = TRUE_;
	if (l3 == 5) {
	    l3 = 1;
	} else {
	    l3 = 2;
	}
	if (l3 % 2 == 0) {
	    dw /= sqrt(2.f);
	}
	irota = (l3 - 1) * 45;
	rspce = dw / l1;
	itype = 1;
	index = lc * 10 + l2;
    } else if (l3 == 9) {
	irota = 0;
	rspce = dw / 5;
	itype = 1;
	index = lc * 10 + 5;
    }
    return 0;
} /* szopts_ */

/* Subroutine */ int szopts_(void)
{
    return szopts_0_(0, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int szstts_(real *vx, real *vy)
{
    return szopts_0_(1, vx, vy, (integer *)0);
    }

/* Subroutine */ int szclts_(void)
{
    return szopts_0_(2, (real *)0, (real *)0, (integer *)0);
    }

/* Subroutine */ int sztits_(integer *itpat)
{
    return szopts_0_(3, (real *)0, (real *)0, itpat);
    }

