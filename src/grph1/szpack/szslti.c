/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    logical llnint, lgcint;
    real rdxr, rdyr;
} szbls1_;

#define szbls1_1 szbls1_

struct {
    logical lclip;
} szbls2_;

#define szbls2_1 szbls2_

struct {
    logical lchaz;
} szbls3_;

#define szbls3_1 szbls3_

/* ----------------------------------------------------------------------- */
/*     COMMON SETTING FOR LINE SEGMENT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szslti_(integer *itype, integer *index)
{
    static real rdx, rdy;
    extern /* Subroutine */ int stfrad_(real *, real *, real *, real *), 
	    sglget_(char *, logical *, ftnlen), sgrget_(char *, real *, 
	    ftnlen), szsidx_(integer *), szstyp_(integer *);

    sglget_("LLNINT", &szbls1_1.llnint, (ftnlen)6);
    sglget_("LGCINT", &szbls1_1.lgcint, (ftnlen)6);
    sgrget_("RDX", &rdx, (ftnlen)3);
    sgrget_("RDY", &rdy, (ftnlen)3);
    sglget_("LCLIP", &szbls2_1.lclip, (ftnlen)5);
    sglget_("LCHAR", &szbls3_1.lchaz, (ftnlen)5);
    stfrad_(&rdx, &rdy, &szbls1_1.rdxr, &szbls1_1.rdyr);
    szsidx_(index);
    szstyp_(itype);
    return 0;
} /* szslti_ */

