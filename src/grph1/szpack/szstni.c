/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__100 = 100;

/* ----------------------------------------------------------------------- */
/*     TONE PATTERN ATTRIBUTE */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int szstni_0_(int n__, integer *itpat)
{
    /* Initialized data */

    static logical lfrst = TRUE_;
    static logical lmsg = TRUE_;

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer lc, lp;
    static char cmsg[80];
    extern /* Subroutine */ int chngi_(char *, char *, integer *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static logical lhard;
    static integer icpat[100];
    static logical lcl2tn;
    static integer ibgcli, ifcidx;
    static logical lclatr;
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), sglget_(
	    char *, logical *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static logical lmonof;
    extern /* Subroutine */ int swqclc_(logical *);
    static logical lsoftf;
    static integer itcliy;
    static logical ltnatr;
    static integer itpaty, itpatz;
    extern /* Subroutine */ int szcltn_(integer *, integer *), swqtnc_(
	    logical *);
    static integer itpnmy;
    extern /* Subroutine */ int szstmd_(logical *), sztits_(integer *), 
	    sztitz_(integer *);

    switch(n__) {
	case 1: goto L_szqtni;
	}

/*     / INITIALIZE / */
    itpatz = *itpat;
    sgiget_("IBGCLI", &ibgcli, (ftnlen)6);
    sgiget_("IFCIDX", &ifcidx, (ftnlen)6);
    if (lfrst) {
	swqtnc_(&ltnatr);
	swqclc_(&lclatr);
	sglget_("LCL2TN", &lcl2tn, (ftnlen)6);
	if (lcl2tn) {
	    lmonof = TRUE_;
	} else {
	    if (lclatr) {
		lmonof = FALSE_;
	    } else {
		lmonof = TRUE_;
	    }
	}
	if (lmonof) {
	    szcltn_(icpat, &c__100);
	    msgdmp_("M", "SZSTNI", "COLOR TO TONE CONVERSION TABLE IS USED.", 
		    (ftnlen)1, (ftnlen)6, (ftnlen)39);
	}
	lfrst = FALSE_;
    }
/*     / SOFT FILL OR HARD FILL (FORCE TO SOFT FILL) / */
    sglget_("LSOFTF", &lsoftf, (ftnlen)6);
    lhard = FALSE_;
    if (lsoftf) {
    } else {
	lsoftf = TRUE_;
	lhard = FALSE_;
	if (lmsg) {
	    msgdmp_("W", "SZSTNI", "HARD FILL IS OBSOLETE.SOFT FILLED", (
		    ftnlen)1, (ftnlen)6, (ftnlen)33);
	    lmsg = FALSE_;
	}
    }
/*     / COLOR TO TONE CONVERSION / */
    if (lmonof) {
	lp = itpatz % 1000;
	lc = itpatz / 1000;
	if (lp == 999 && lc != ibgcli) {
	    if (1 <= lc && lc <= 100) {
		if (icpat[lc - 1] >= 0) {
		    itpatz = icpat[lc - 1];
		} else {
		    s_copy(cmsg, "COLOR NUMBER ## IS NOT DEFINED IN CL2TNMAP."
			    , (ftnlen)80, (ftnlen)43);
		    chngi_(cmsg, "##", &lc, "(I2)", (ftnlen)80, (ftnlen)2, (
			    ftnlen)4);
		    msgdmp_("W", "SWGTON", cmsg, (ftnlen)1, (ftnlen)6, (
			    ftnlen)80);
		}
	    } else if (lc == 999) {
	    } else {
		s_copy(cmsg, "COLOR NUMBER ## IS NOT DEFINED IN CL2TNMAP.", (
			ftnlen)80, (ftnlen)43);
		chngi_(cmsg, "##", &lc, "(I2)", (ftnlen)80, (ftnlen)2, (
			ftnlen)4);
		msgdmp_("W", "SWGTON", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80)
			;
	    }
	}
    }
    itcliy = itpatz / 1000;
    itpnmy = itpatz % 1000;
    if (itcliy == ibgcli) {
	itpaty = itpnmy;
    } else {
	if (itcliy == 0) {
	    itcliy = 1;
	}
	itpaty = itpnmy + itcliy * 1000;
    }
    if (itpnmy == ifcidx || itpnmy == 999) {
	lhard = TRUE_;
    }
    szstmd_(&lhard);
    if (lhard) {
	sztitz_(&itpatz);
    } else {
	sztits_(&itpaty);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_szqtni:
    *itpat = itpaty;
    return 0;
} /* szstni_ */

/* Subroutine */ int szstni_(integer *itpat)
{
    return szstni_0_(0, itpat);
    }

/* Subroutine */ int szqtni_(integer *itpat)
{
    return szstni_0_(1, itpat);
    }

