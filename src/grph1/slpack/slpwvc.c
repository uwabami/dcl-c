/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;
static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SLPWWC : PLOT MAXIMUM-DRAWING-REGION CORNER MARKS */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slpwvc_(integer *index, real *rc)
{
    /* System generated locals */
    real r__1;

    /* Local variables */
    static integer itr;
    static real rxmn, rymn, wxmn, wymn, rxmx, rymx, wxmx, wymx, cxmin, cymin, 
	    cxmax, cymax, rxmin, rymin, rxmax, rymax, wxmin, wymin, wxmax, 
	    wymax;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen), szqcll_(real *, real *, real *, real *, integer *
	    ), szcllr_(void), szscll_(real *, real *, real *, real *, integer 
	    *), stqwrc_(real *, real *, real *, real *), szpllr_(real *, real 
	    *), szslti_(integer *, integer *), szoplr_(void), stiwtr_(real *, 
	    real *, real *, real *), szmvlr_(real *, real *), stqwtr_(real *, 
	    real *, real *, real *, real *, real *, real *, real *, integer *)
	    ;

    if (*index <= 0) {
	if (*index == 0) {
	    msgdmp_("M", "SLPWVC", "LINE INDEX IS ZERO / DO NOTHING.", (
		    ftnlen)1, (ftnlen)6, (ftnlen)32);
	    return 0;
	} else {
	    msgdmp_("E", "SLPWVC", "LINE INDEX IS LESS THAN ZERO.", (ftnlen)1,
		     (ftnlen)6, (ftnlen)29);
	}
    }
    if (*rc <= 0.f) {
	if (*rc == 0.f) {
	    msgdmp_("E", "SLPWVC", "LENGTH OF CORNER MARKS IS ZERO / DO NOTH"
		    "ING.", (ftnlen)1, (ftnlen)6, (ftnlen)44);
	    return 0;
	} else {
	    msgdmp_("E", "SLPWVC", "LENGTH OF CORNER MARKS IS LESS THAN ZERO."
		    , (ftnlen)1, (ftnlen)6, (ftnlen)41);
	}
    }
    stqwtr_(&rxmn, &rxmx, &rymn, &rymx, &wxmn, &wxmx, &wymn, &wymx, &itr);
    if (itr == 1) {
	stqwrc_(&wxmin, &wxmax, &wymin, &wymax);
    } else {
	stqwrc_(&wxmin, &wxmax, &wymax, &wymin);
    }
    szqcll_(&cxmin, &cxmax, &cymin, &cymax, &c__0);
    stiwtr_(&wxmin, &wymin, &rxmin, &rymin);
    stiwtr_(&wxmax, &wymax, &rxmax, &rymax);
    szscll_(&rxmin, &rxmax, &rymin, &rymax, &c__0);
    szslti_(&c__1, index);
    szoplr_();
    r__1 = rymin + *rc;
    szmvlr_(&rxmin, &r__1);
    szpllr_(&rxmin, &rymin);
    r__1 = rxmin + *rc;
    szpllr_(&r__1, &rymin);
    szcllr_();
    szoplr_();
    r__1 = rxmax - *rc;
    szmvlr_(&r__1, &rymin);
    szpllr_(&rxmax, &rymin);
    r__1 = rymin + *rc;
    szpllr_(&rxmax, &r__1);
    szcllr_();
    szoplr_();
    r__1 = rymax - *rc;
    szmvlr_(&rxmax, &r__1);
    szpllr_(&rxmax, &rymax);
    r__1 = rxmax - *rc;
    szpllr_(&r__1, &rymax);
    szcllr_();
    szoplr_();
    r__1 = rxmin + *rc;
    szmvlr_(&r__1, &rymax);
    szpllr_(&rxmin, &rymax);
    r__1 = rymax - *rc;
    szpllr_(&rxmin, &r__1);
    szcllr_();
    szscll_(&cxmin, &cxmax, &cymin, &cymax, &c__0);
    return 0;
} /* slpwvc_ */

