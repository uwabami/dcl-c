/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slsttl_0_(int n__, char *cttl, char *cside, real *px, 
	real *py, real *ht, integer *nt, ftnlen cttl_len, ftnlen cside_len)
{
    /* Initialized data */

    static logical lset[5] = { FALSE_,FALSE_,FALSE_,FALSE_,FALSE_ };

    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__, lc;
    static char cs[1];
    static real htz[5], pxz[5], pyz[5];
    extern integer lenc_(char *, ftnlen);
    static integer index;
    static char cttlw[2048], cttlz[2048*5];
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    static char csidez[1*5];
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), sglget_(
	    char *, logical *, ftnlen), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen);
    static logical ltitle;
    extern /* Subroutine */ int sltlcv_(char *, char *, integer *, ftnlen, 
	    ftnlen), slzttl_(char *, char *, real *, real *, real *, integer *
	    , ftnlen, ftnlen);

    switch(n__) {
	case 1: goto L_sldttl;
	case 2: goto L_slpttl;
	}

    *(unsigned char *)cs = *(unsigned char *)cside;
    if (! (lchreq_(cs, "T", (ftnlen)1, (ftnlen)1) || lchreq_(cs, "B", (ftnlen)
	    1, (ftnlen)1))) {
	msgdmp_("E", "SLSTTL", "SIDE PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)26);
    }
    if (! (-1.f <= *px && *px <= 1.f && -1.f <= *py && *py <= 1.f)) {
	msgdmp_("E", "SLSTTL", "POSITION PARAMETER IS INVALID.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (! (*ht >= 0.f)) {
	msgdmp_("E", "SLSTTL", "TEXT HEIGHT IS LESS THAN ZERO.", (ftnlen)1, (
		ftnlen)6, (ftnlen)30);
    }
    if (! (1 <= *nt && *nt <= 5)) {
	msgdmp_("E", "SLSTTL", "TITLE NUMBER IS OUT OF RANGE.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
    }
    lset[*nt - 1] = TRUE_;
    if (lenc_(cttl, cttl_len) > 1024) {
	msgdmp_("W", "SLSTTL", "STRING LENGTH TOO LONG.SHORTEND.", (ftnlen)1, 
		(ftnlen)6, (ftnlen)32);
	s_copy(cttlz + (*nt - 1 << 11), cttl, (ftnlen)2048, (ftnlen)1024);
    } else {
	s_copy(cttlz + (*nt - 1 << 11), cttl, (ftnlen)2048, lenc_(cttl, 
		cttl_len));
    }
    *(unsigned char *)&csidez[*nt - 1] = *(unsigned char *)cs;
    pxz[*nt - 1] = *px;
    pyz[*nt - 1] = *py;
    htz[*nt - 1] = *ht;
    return 0;
/* ----------------------------------------------------------------------- */

L_sldttl:
    if (! (1 <= *nt && *nt <= 5)) {
	msgdmp_("E", "SLDTTL", "TITLE NUMBER IS OUT OF RANGE.", (ftnlen)1, (
		ftnlen)6, (ftnlen)29);
    }
    lset[*nt - 1] = FALSE_;
    return 0;
/* ----------------------------------------------------------------------- */

L_slpttl:
    sgiget_("INDEX", &index, (ftnlen)5);
    sglget_("LTITLE", &ltitle, (ftnlen)6);
    if (! ltitle) {
	return 0;
    }
    for (i__ = 1; i__ <= 5; ++i__) {
	if (lset[i__ - 1]) {
	    sltlcv_(cttlz + (i__ - 1 << 11), cttlw, &lc, (ftnlen)2048, (
		    ftnlen)2048);
	    slzttl_(csidez + (i__ - 1), cttlw, &pxz[i__ - 1], &pyz[i__ - 1], &
		    htz[i__ - 1], &index, (ftnlen)1, lc);
	}
/* L10: */
    }
    return 0;
} /* slsttl_ */

/* Subroutine */ int slsttl_(char *cttl, char *cside, real *px, real *py, 
	real *ht, integer *nt, ftnlen cttl_len, ftnlen cside_len)
{
    return slsttl_0_(0, cttl, cside, px, py, ht, nt, cttl_len, cside_len);
    }

/* Subroutine */ int sldttl_(integer *nt)
{
    return slsttl_0_(1, (char *)0, (char *)0, (real *)0, (real *)0, (real *)0,
	     nt, (ftnint)0, (ftnint)0);
    }

/* Subroutine */ int slpttl_(void)
{
    return slsttl_0_(2, (char *)0, (char *)0, (real *)0, (real *)0, (real *)0,
	     (integer *)0, (ftnint)0, (ftnint)0);
    }

