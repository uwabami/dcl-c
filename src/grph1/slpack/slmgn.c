/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Common Block Declarations */

struct {
    real xmin[1000], xmax[1000], ymin[1000], ymax[1000];
    integer nn[4];
} slblk1_;

#define slblk1_1 slblk1_

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int slmgn_(real *xl, real *xr, real *yb, real *yt)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer l1, l2, li, lev;
    extern integer isum0_(integer *, integer *, integer *);
    extern /* Subroutine */ int sgiget_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), slmgnz_(real *, 
	    real *, real *, real *, real *, real *, real *, real *);

    if (*xl < 0.f || *xr < 0.f || *yb < 0.f || *yt < 0.f) {
	msgdmp_("E", "SLMGN ", "MARGIN PARAMETER IS LESS THAN ZERO.", (ftnlen)
		1, (ftnlen)6, (ftnlen)35);
    }
    if (*xl + *xr >= 1.f || *yb + *yt >= 1.f) {
	msgdmp_("E", "SLMGN ", "SUM OF MARGIN PARAMETERS IS LARGER THAN 1.0.",
		 (ftnlen)1, (ftnlen)6, (ftnlen)44);
    }
    sgiget_("NLEVEL", &lev, (ftnlen)6);
    l1 = isum0_(slblk1_1.nn, &lev, &c__1) + 1;
    i__1 = lev + 1;
    l2 = isum0_(slblk1_1.nn, &i__1, &c__1);
    i__1 = l2;
    for (li = l1; li <= i__1; ++li) {
	slmgnz_(&slblk1_1.xmin[li - 1], &slblk1_1.xmax[li - 1], &
		slblk1_1.ymin[li - 1], &slblk1_1.ymax[li - 1], xl, xr, yb, yt)
		;
/* L10: */
    }
    return 0;
} /* slmgn_ */

