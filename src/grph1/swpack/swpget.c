/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;

/* ----------------------------------------------------------------------- */
/*     SWPGET / SWPSET / SWPSTX */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int swpget_0_(int n__, char *cp, integer *ipara, ftnlen 
	cp_len)
{
    static char cl[40];
    static integer ip;
    static char cx[8];
    static integer it, idx;
    extern /* Subroutine */ int rliget_(char *, integer *, integer *, ftnlen),
	     rllget_(char *, integer *, integer *, ftnlen), rtiget_(char *, 
	    char *, integer *, integer *, ftnlen, ftnlen), rlrget_(char *, 
	    integer *, integer *, ftnlen), swiqid_(char *, integer *, ftnlen),
	     rtlget_(char *, char *, integer *, integer *, ftnlen, ftnlen), 
	    swlqid_(char *, integer *, ftnlen), swpqid_(char *, integer *, 
	    ftnlen), rtrget_(char *, char *, integer *, integer *, ftnlen, 
	    ftnlen), swpqcl_(integer *, char *, ftnlen), swrqid_(char *, 
	    integer *, ftnlen), swpqcp_(integer *, char *, ftnlen), swpqit_(
	    integer *, integer *), swisvl_(integer *, integer *), swlsvl_(
	    integer *, integer *), swpqvl_(integer *, integer *), swpsvl_(
	    integer *, integer *), swrsvl_(integer *, integer *);

    switch(n__) {
	case 1: goto L_swpset;
	case 2: goto L_swpstx;
	}

    swpqid_(cp, &idx, cp_len);
    swpqvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_swpset:
    swpqid_(cp, &idx, cp_len);
    swpsvl_(&idx, ipara);
    return 0;
/* ----------------------------------------------------------------------- */

L_swpstx:
    ip = *ipara;
    swpqid_(cp, &idx, cp_len);
    swpqit_(&idx, &it);
    swpqcp_(&idx, cx, (ftnlen)8);
    swpqcl_(&idx, cl, (ftnlen)40);
    if (it == 1) {
	rtiget_("SW", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rliget_(cl, &ip, &c__1, (ftnlen)40);
	swiqid_(cp, &idx, cp_len);
	swisvl_(&idx, &ip);
    } else if (it == 2) {
	rtlget_("SW", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rllget_(cl, &ip, &c__1, (ftnlen)40);
	swlqid_(cp, &idx, cp_len);
	swlsvl_(&idx, &ip);
    } else if (it == 3) {
	rtrget_("SW", cx, &ip, &c__1, (ftnlen)2, (ftnlen)8);
	rlrget_(cl, &ip, &c__1, (ftnlen)40);
	swrqid_(cp, &idx, cp_len);
	swrsvl_(&idx, &ip);
    }
    return 0;
} /* swpget_ */

/* Subroutine */ int swpget_(char *cp, integer *ipara, ftnlen cp_len)
{
    return swpget_0_(0, cp, ipara, cp_len);
    }

/* Subroutine */ int swpset_(char *cp, integer *ipara, ftnlen cp_len)
{
    return swpget_0_(1, cp, ipara, cp_len);
    }

/* Subroutine */ int swpstx_(char *cp, integer *ipara, ftnlen cp_len)
{
    return swpget_0_(2, cp, ipara, cp_len);
    }

