/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     3-D TRANSFORMATION */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scsobj_0_(int n__, real *xobj3, real *yobj3, real *zobj3)
{
    extern /* Subroutine */ int sgrget_(char *, real *, ftnlen), sgrset_(char 
	    *, real *, ftnlen);

    switch(n__) {
	case 1: goto L_scqobj;
	}

    sgrset_("XOBJ3", xobj3, (ftnlen)5);
    sgrset_("YOBJ3", yobj3, (ftnlen)5);
    sgrset_("ZOBJ3", zobj3, (ftnlen)5);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqobj:
    sgrget_("XOBJ3", xobj3, (ftnlen)5);
    sgrget_("YOBJ3", yobj3, (ftnlen)5);
    sgrget_("ZOBJ3", zobj3, (ftnlen)5);
    return 0;
} /* scsobj_ */

/* Subroutine */ int scsobj_(real *xobj3, real *yobj3, real *zobj3)
{
    return scsobj_0_(0, xobj3, yobj3, zobj3);
    }

/* Subroutine */ int scqobj_(real *xobj3, real *yobj3, real *zobj3)
{
    return scsobj_0_(1, xobj3, yobj3, zobj3);
    }

