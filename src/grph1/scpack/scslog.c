/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int scslog_0_(int n__, logical *lxlog3, logical *lylog3, 
	logical *lzlog3)
{
    extern /* Subroutine */ int sglget_(char *, logical *, ftnlen), sglset_(
	    char *, logical *, ftnlen);

    switch(n__) {
	case 1: goto L_scqlog;
	}

    sglset_("LXLOG3", lxlog3, (ftnlen)6);
    sglset_("LYLOG3", lylog3, (ftnlen)6);
    sglset_("LZLOG3", lzlog3, (ftnlen)6);
    return 0;
/* ----------------------------------------------------------------------- */

L_scqlog:
    sglget_("LXLOG3", lxlog3, (ftnlen)6);
    sglget_("LYLOG3", lylog3, (ftnlen)6);
    sglget_("LZLOG3", lzlog3, (ftnlen)6);
    return 0;
} /* scslog_ */

/* Subroutine */ int scslog_(logical *lxlog3, logical *lylog3, logical *
	lzlog3)
{
    return scslog_0_(0, lxlog3, lylog3, lzlog3);
    }

/* Subroutine */ int scqlog_(logical *lxlog3, logical *lylog3, logical *
	lzlog3)
{
    return scslog_0_(1, lxlog3, lylog3, lzlog3);
    }

