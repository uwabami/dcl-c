/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__3 = 3;

/* ----------------------------------------------------------------------- */
/*     ODPQNP / ODPQID / ODPQCP / ODPQVL / ODPSVL */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odpqnp_0_(int n__, integer *ncp, char *cp, integer *idx, 
	integer *itp, integer *ipara, integer *in, ftnlen cp_len)
{
    /* Initialized data */

    static char cparas[8*16] = "LBAR    " "EPSILON " "MAXSTEP " "NSTEP   ";
    static integer itype[16] = { 2,3,1,1 };
    static char cparal[40*16] = "LBAR                                    " 
	    "EPSILON                                 " "MAXSTEP             "
	    "                    " "NSTEP                                   ";

    /* System generated locals */
    address a__1[3];
    integer i__1[3];

    /* Builtin functions */
    /* Subroutine */ int s_cat(char *, char **, integer *, integer *, ftnlen),
	     s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer n, id;
    extern integer lenc_(char *, ftnlen);
    static char cmsg[80];
    extern /* Subroutine */ int odiqid_(char *, integer *, ftnlen), odlqid_(
	    char *, integer *, ftnlen);
    extern logical lchreq_(char *, char *, ftnlen, ftnlen);
    extern /* Subroutine */ int odrqid_(char *, integer *, ftnlen), msgdmp_(
	    char *, char *, char *, ftnlen, ftnlen, ftnlen), odiqvl_(integer *
	    , integer *), odlqvl_(integer *, integer *), odisvl_(integer *, 
	    integer *), odlsvl_(integer *, integer *), odrqvl_(integer *, 
	    integer *), odrsvl_(integer *, integer *);

/*     / SHORT NAME / */
    switch(n__) {
	case 1: goto L_odpqid;
	case 2: goto L_odpqcp;
	case 3: goto L_odpqcl;
	case 4: goto L_odpqit;
	case 5: goto L_odpqvl;
	case 6: goto L_odpsvl;
	case 7: goto L_odpqin;
	}

/*     / LONG NAME / */
    *ncp = 16;
    return 0;
/* ----------------------------------------------------------------------- */

L_odpqid:
    for (n = 1; n <= 16; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *idx = n;
	    return 0;
	}
/* L10: */
    }
/* Writing concatenation */
    i__1[0] = 11, a__1[0] = "PARAMETER '";
    i__1[1] = lenc_(cp, cp_len), a__1[1] = cp;
    i__1[2] = 17, a__1[2] = "' IS NOT DEFINED.";
    s_cat(cmsg, a__1, i__1, &c__3, (ftnlen)80);
    msgdmp_("E", "ODPQID", cmsg, (ftnlen)1, (ftnlen)6, (ftnlen)80);
    return 0;
/* ----------------------------------------------------------------------- */

L_odpqcp:
    if (1 <= *idx && *idx <= 16) {
	s_copy(cp, cparas + (*idx - 1 << 3), cp_len, (ftnlen)8);
    } else {
	msgdmp_("E", "ODPQCP", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odpqcl:
    if (1 <= *idx && *idx <= 16) {
	s_copy(cp, cparal + (*idx - 1) * 40, cp_len, (ftnlen)40);
    } else {
	msgdmp_("E", "ODPQCL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odpqit:
    if (1 <= *idx && *idx <= 16) {
	*itp = itype[*idx - 1];
    } else {
	msgdmp_("E", "ODPQIT", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odpqvl:
    if (1 <= *idx && *idx <= 16) {
	if (itype[*idx - 1] == 1) {
	    odiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    odiqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    odlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    odlqvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    odrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    odrqvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "ODPQVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odpsvl:
    if (1 <= *idx && *idx <= 16) {
	if (itype[*idx - 1] == 1) {
	    odiqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    odisvl_(&id, ipara);
	} else if (itype[*idx - 1] == 2) {
	    odlqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    odlsvl_(&id, ipara);
	} else if (itype[*idx - 1] == 3) {
	    odrqid_(cparas + (*idx - 1 << 3), &id, (ftnlen)8);
	    odrsvl_(&id, ipara);
	}
    } else {
	msgdmp_("E", "ODPSVL", "IDX IS OUT OF RANGE.", (ftnlen)1, (ftnlen)6, (
		ftnlen)20);
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_odpqin:
    for (n = 1; n <= 16; ++n) {
	if (lchreq_(cp, cparas + (n - 1 << 3), cp_len, (ftnlen)8) || lchreq_(
		cp, cparal + (n - 1) * 40, cp_len, (ftnlen)40)) {
	    *in = n;
	    return 0;
	}
/* L20: */
    }
    *in = 0;
    return 0;
} /* odpqnp_ */

/* Subroutine */ int odpqnp_(integer *ncp)
{
    return odpqnp_0_(0, ncp, (char *)0, (integer *)0, (integer *)0, (integer *
	    )0, (integer *)0, (ftnint)0);
    }

/* Subroutine */ int odpqid_(char *cp, integer *idx, ftnlen cp_len)
{
    return odpqnp_0_(1, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int odpqcp_(integer *idx, char *cp, ftnlen cp_len)
{
    return odpqnp_0_(2, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int odpqcl_(integer *idx, char *cp, ftnlen cp_len)
{
    return odpqnp_0_(3, (integer *)0, cp, idx, (integer *)0, (integer *)0, (
	    integer *)0, cp_len);
    }

/* Subroutine */ int odpqit_(integer *idx, integer *itp)
{
    return odpqnp_0_(4, (integer *)0, (char *)0, idx, itp, (integer *)0, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int odpqvl_(integer *idx, integer *ipara)
{
    return odpqnp_0_(5, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int odpsvl_(integer *idx, integer *ipara)
{
    return odpqnp_0_(6, (integer *)0, (char *)0, idx, (integer *)0, ipara, (
	    integer *)0, (ftnint)0);
    }

/* Subroutine */ int odpqin_(char *cp, integer *in, ftnlen cp_len)
{
    return odpqnp_0_(7, (integer *)0, cp, (integer *)0, (integer *)0, (
	    integer *)0, in, cp_len);
    }

