/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     Runge-Kutta-Gill algorithm routine. */
/*                                                 Oct. 5, 1990  S.Sakai */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrkg_(integer *n, S_fp fcn, real *t, real *dt, real *x, 
	real *dx, real *xout, real *work)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static integer i__;
    static real c1, c2, c3, c4, c5, c6, tt, dt2;

    /* Parameter adjustments */
    --work;
    --xout;
    --dx;
    --x;

    /* Function Body */
    dt2 = *dt / 2.f;
    c1 = (1.f - 1.f / sqrt(2.f)) * *dt;
    c2 = 3.f / sqrt(2.f) - 2.f;
    c3 = 2.f - sqrt(2.f);
    c4 = (1.f / sqrt(2.f) + 1.f) * *dt;
    c5 = (3.f / sqrt(2.f) + 2.f) / 3.f;
    c6 = -(sqrt(2.f) + 2.f) / 3.f;
/* --------------------------- 1st step ---------------------------------- */
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xout[i__] = x[i__] + dt2 * dx[i__];
/* L10: */
    }
/* --------------------------- 2nd step ---------------------------------- */
    tt = *t + dt2;
    (*fcn)(n, &tt, &xout[1], &work[1]);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xout[i__] += c1 * (work[i__] - dx[i__]);
	dx[i__] = c2 * dx[i__] + c3 * work[i__];
/* L20: */
    }
/* --------------------------- 3rd step ---------------------------------- */
    (*fcn)(n, &tt, &xout[1], &work[1]);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xout[i__] += c4 * (work[i__] - dx[i__]);
	dx[i__] = c5 * dx[i__] + c6 * work[i__];
/* L30: */
    }
/* --------------------------- 4th step ---------------------------------- */
    tt = *t + *dt;
    (*fcn)(n, &tt, &xout[1], &work[1]);
    i__1 = *n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	xout[i__] += *dt * (work[i__] / 6.f + dx[i__]);
/* L40: */
    }
    return 0;
} /* odrkg_ */

