/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*    Runge-Kutta driver (Uniform step) */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int odrkdu_(integer *n, S_fp algr, S_fp fcn, real *t, real *
	tend, integer *istep, real *x, real *work)
{
    /* System generated locals */
    integer work_dim1, work_offset, i__1;

    /* Local variables */
    static integer j;
    static real t0, dt;
    extern /* Subroutine */ int msgdmp_(char *, char *, char *, ftnlen, 
	    ftnlen, ftnlen);

/*                      WORK(N,2) FOR RKG, ODRK2, ODRK1 */
/*                      WORK(N,4) FOR ODRK4 */
    /* Parameter adjustments */
    work_dim1 = *n;
    work_offset = 1 + work_dim1;
    work -= work_offset;
    --x;

    /* Function Body */
    if (*n < 1) {
	msgdmp_("E", "ODRKDU", "INVALID N.", (ftnlen)1, (ftnlen)6, (ftnlen)10)
		;
    }
    t0 = *t;
    dt = (*tend - t0) / *istep;
    i__1 = *istep;
    for (j = 1; j <= i__1; ++j) {
	(*fcn)(n, t, &x[1], &work[work_dim1 + 1]);
	(*algr)(n, (S_fp)fcn, t, &dt, &x[1], &work[work_dim1 + 1], &x[1], &
		work[(work_dim1 << 1) + 1]);
	*t = t0 + dt * j;
/* L30: */
    }
    return 0;
} /* odrkdu_ */

