/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE COSTI(N,WSAVE) */

/*     SUBROUTINE COSTI INITIALIZES THE ARRAY WSAVE WHICH IS USED IN */
/*     SUBROUTINE COST. THE PRIME FACTORIZATION OF N TOGETHER WITH */
/*     A TABULATION OF THE TRIGONOMETRIC FUNCTIONS ARE COMPUTED AND */
/*     STORED IN WSAVE. */

/*     INPUT PARAMETER */

/*     N       THE LENGTH OF THE SEQUENCE TO BE TRANSFORMED.  THE METHOD */
/*             IS MOST EFFICIENT WHEN N-1 IS A PRODUCT OF SMALL PRIMES. */

/*     OUTPUT PARAMETER */

/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 3*N+15. */
/*             DIFFERENT WSAVE ARRAYS ARE REQUIRED FOR DIFFERENT VALUES */
/*             OF N. THE CONTENTS OF WSAVE MUST NOT BE CHANGED BETWEEN */
/*             CALLS OF COST. */

/* Subroutine */ int costi_(integer *n, real *wsave)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    double sin(doublereal), cos(doublereal);

    /* Local variables */
    static integer k, kc;
    static real fk, dt, pi;
    static integer nm1, np1, ns2;
    extern real rfpi_(void);
    extern /* Subroutine */ int rffti_(integer *, real *);


    /* Parameter adjustments */
    --wsave;

    /* Function Body */
    pi = rfpi_();
    if (*n <= 3) {
	return 0;
    }
    nm1 = *n - 1;
    np1 = *n + 1;
    ns2 = *n / 2;
    dt = pi / (real) nm1;
    fk = 0.f;
    i__1 = ns2;
    for (k = 2; k <= i__1; ++k) {
	kc = np1 - k;
	fk += 1.f;
	wsave[k] = sin(fk * dt) * 2.f;
	wsave[kc] = cos(fk * dt) * 2.f;
/* L101: */
    }
    rffti_(&nm1, &wsave[*n + 1]);
    return 0;
} /* costi_ */

