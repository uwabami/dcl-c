/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE COST(N,X,WSAVE) */

/*     SUBROUTINE COST COMPUTES THE DISCRETE FOURIER COSINE TRANSFORM */
/*     OF AN EVEN SEQUENCE X(I). THE TRANSFORM IS DEFINED BELOW AT OUTPUT */
/*     PARAMETER X. */

/*     COST IS THE UNNORMALIZED INVERSE OF ITSELF SINCE A CALL OF COST */
/*     FOLLOWED BY ANOTHER CALL OF COST WILL MULTIPLY THE INPUT SEQUENCE */
/*     X BY 2*(N-1). THE TRANSFORM IS DEFINED BELOW AT OUTPUT PARAMETER X */

/*     THE ARRAY WSAVE WHICH IS USED BY SUBROUTINE COST MUST BE */
/*     INITIALIZED BY CALLING SUBROUTINE COSTI(N,WSAVE). */

/*     INPUT PARAMETERS */

/*     N       THE LENGTH OF THE SEQUENCE X. N MUST BE GREATER THAN 1. */
/*             THE METHOD IS MOST EFFICIENT WHEN N-1 IS A PRODUCT OF */
/*             SMALL PRIMES. */

/*     X       AN ARRAY WHICH CONTAINS THE SEQUENCE TO BE TRANSFORMED */

/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 3*N+15 */
/*             IN THE PROGRAM THAT CALLS COST. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE COSTI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */

/*     OUTPUT PARAMETERS */

/*     X       FOR I=1,...,N */

/*                 X(I) = X(1)+(-1)**(I-1)*X(N) */

/*                  + THE SUM FROM K=2 TO K=N-1 */

/*                      2*X(K)*COS((K-1)*(I-1)*PI/(N-1)) */

/*                  A CALL OF COST FOLLOWED BY ANOTHER CALL OF */
/*                  COST WILL MULTIPLY THE SEQUENCE X BY 2*(N-1) */
/*                  HENCE COST IS THE UNNORMALIZED INVERSE */
/*                  OF ITSELF. */

/*     WSAVE   CONTAINS INITIALIZATION CALCULATIONS WHICH MUST NOT BE */
/*             DESTROYED BETWEEN CALLS OF COST. */

/* Subroutine */ int cost_(integer *n, real *x, real *wsave)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__, k;
    static real c1, t1, t2;
    static integer kc;
    static real xi;
    static integer nm1, np1;
    static real x1h;
    static integer ns2;
    static real tx2, x1p3, xim2;
    static integer modn;
    extern /* Subroutine */ int rfftf_(integer *, real *, real *);


    /* Parameter adjustments */
    --wsave;
    --x;

    /* Function Body */
    nm1 = *n - 1;
    np1 = *n + 1;
    ns2 = *n / 2;
    if ((i__1 = *n - 2) < 0) {
	goto L106;
    } else if (i__1 == 0) {
	goto L101;
    } else {
	goto L102;
    }
L101:
    x1h = x[1] + x[2];
    x[2] = x[1] - x[2];
    x[1] = x1h;
    return 0;
L102:
    if (*n > 3) {
	goto L103;
    }
    x1p3 = x[1] + x[3];
    tx2 = x[2] + x[2];
    x[2] = x[1] - x[3];
    x[1] = x1p3 + tx2;
    x[3] = x1p3 - tx2;
    return 0;
L103:
    c1 = x[1] - x[*n];
    x[1] += x[*n];
    i__1 = ns2;
    for (k = 2; k <= i__1; ++k) {
	kc = np1 - k;
	t1 = x[k] + x[kc];
	t2 = x[k] - x[kc];
	c1 += wsave[kc] * t2;
	t2 = wsave[k] * t2;
	x[k] = t1 - t2;
	x[kc] = t1 + t2;
/* L104: */
    }
    modn = *n % 2;
    if (modn != 0) {
	x[ns2 + 1] += x[ns2 + 1];
    }
    rfftf_(&nm1, &x[1], &wsave[*n + 1]);
    xim2 = x[2];
    x[2] = c1;
    i__1 = *n;
    for (i__ = 4; i__ <= i__1; i__ += 2) {
	xi = x[i__];
	x[i__] = x[i__ - 2] - x[i__ - 1];
	x[i__ - 1] = xim2;
	xim2 = xi;
/* L105: */
    }
    if (modn != 0) {
	x[*n] = xim2;
    }
L106:
    return 0;
} /* cost_ */

