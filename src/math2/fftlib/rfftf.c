/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ----------------------------------------------------------------------- */
/*     SUBROUTINE RFFTF(N,R,WSAVE) */

/*     SUBROUTINE RFFTF COMPUTES THE FOURIER COEFFICIENTS OF A REAL */
/*     PERODIC SEQUENCE (FOURIER ANALYSIS). THE TRANSFORM IS DEFINED */
/*     BELOW AT OUTPUT PARAMETER R. */

/*     INPUT PARAMETERS */

/*     N       THE LENGTH OF THE ARRAY R TO BE TRANSFORMED.  THE METHOD */
/*             IS MOST EFFICIENT WHEN N IS A PRODUCT OF SMALL PRIMES. */
/*             N MAY CHANGE SO LONG AS DIFFERENT WORK ARRAYS ARE PROVIDED */

/*     R       A REAL ARRAY OF LENGTH N WHICH CONTAINS THE SEQUENCE */
/*             TO BE TRANSFORMED */

/*     WSAVE   A WORK ARRAY WHICH MUST BE DIMENSIONED AT LEAST 2*N+15. */
/*             IN THE PROGRAM THAT CALLS RFFTF. THE WSAVE ARRAY MUST BE */
/*             INITIALIZED BY CALLING SUBROUTINE RFFTI(N,WSAVE) AND A */
/*             DIFFERENT WSAVE ARRAY MUST BE USED FOR EACH DIFFERENT */
/*             VALUE OF N. THIS INITIALIZATION DOES NOT HAVE TO BE */
/*             REPEATED SO LONG AS N REMAINS UNCHANGED THUS SUBSEQUENT */
/*             TRANSFORMS CAN BE OBTAINED FASTER THAN THE FIRST. */
/*             THE SAME WSAVE ARRAY CAN BE USED BY RFFTF AND RFFTB. */


/*     OUTPUT PARAMETERS */

/*     R       R(1) = THE SUM FROM I=1 TO I=N OF R(I) */

/*             IF N IS EVEN SET L =N/2   , IF N IS ODD SET L = (N+1)/2 */

/*               THEN FOR K = 2,...,L */

/*                  R(2*K-2) = THE SUM FROM I = 1 TO I = N OF */

/*                       R(I)*COS((K-1)*(I-1)*2*PI/N) */

/*                  R(2*K-1) = THE SUM FROM I = 1 TO I = N OF */

/*                      -R(I)*SIN((K-1)*(I-1)*2*PI/N) */

/*             IF N IS EVEN */

/*                  R(N) = THE SUM FROM I = 1 TO I = N OF */

/*                       (-1)**(I-1)*R(I) */

/*      *****  NOTE */
/*                  THIS TRANSFORM IS UNNORMALIZED SINCE A CALL OF RFFTF */
/*                  FOLLOWED BY A CALL OF RFFTB WILL MULTIPLY THE INPUT */
/*                  SEQUENCE BY N. */

/*     WSAVE   CONTAINS RESULTS WHICH MUST NOT BE DESTROYED BETWEEN */
/*             CALLS OF RFFTF OR RFFTB. */

/* Subroutine */ int rfftf_(integer *n, real *r__, real *wsave)
{
    extern /* Subroutine */ int rfftf1_(integer *, real *, real *, real *, 
	    real *);


    /* Parameter adjustments */
    --wsave;
    --r__;

    /* Function Body */
    if (*n == 1) {
	return 0;
    }
    rfftf1_(n, &r__[1], &wsave[1], &wsave[*n + 1], &wsave[(*n << 1) + 1]);
    return 0;
} /* rfftf_ */

