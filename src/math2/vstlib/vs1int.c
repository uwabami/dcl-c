/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;

/* ----------------------------------------------------------------------- */
/*     VS1INT / VS1DIN / VS1OUT */
/* ----------------------------------------------------------------------- */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int vs1int_0_(int n__, real *wz, integer *nw, integer *ix, 
	real *x)
{
    /* System generated locals */
    integer wz_dim1, wz_offset, i__1;

    /* Local variables */
    static integer i__;
    static real xave, xvar;
    extern /* Subroutine */ int iset0_(integer *, integer *, integer *, 
	    integer *), rset0_(real *, integer *, integer *, integer *);
    static logical lmiss;
    static real rmiss;
    extern /* Subroutine */ int gllget_(char *, logical *, ftnlen), glrget_(
	    char *, real *, ftnlen);

    /* Parameter adjustments */
    wz_dim1 = *ix;
    wz_offset = 1 + wz_dim1;
    wz -= wz_offset;
    --nw;
    if (x) {
	--x;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_vs1din;
	case 2: goto L_vs1out;
	}

    gllget_("LMISS", &lmiss, (ftnlen)5);
    glrget_("RMISS", &rmiss, (ftnlen)5);
    i__1 = *ix << 1;
    rset0_(&wz[wz_offset], &i__1, &c__1, &c__0);
    iset0_(&nw[1], ix, &c__1, &c__0);
    return 0;
/* ----------------------------------------------------------------------- */

L_vs1din:
    i__1 = *ix;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (! (lmiss && x[i__] == rmiss)) {
	    ++nw[i__];
	    wz[i__ + wz_dim1] += x[i__];
	    wz[i__ + (wz_dim1 << 1)] += x[i__] * x[i__];
	}
/* L10: */
    }
    return 0;
/* ----------------------------------------------------------------------- */

L_vs1out:
    i__1 = *ix;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (nw[i__] != 0) {
	    xave = wz[i__ + wz_dim1] / nw[i__];
	    xvar = wz[i__ + (wz_dim1 << 1)] / nw[i__] - xave * xave;
	    wz[i__ + wz_dim1] = xave;
	    wz[i__ + (wz_dim1 << 1)] = xvar;
	} else {
	    wz[i__ + wz_dim1] = rmiss;
	    wz[i__ + (wz_dim1 << 1)] = rmiss;
	}
/* L15: */
    }
    return 0;
} /* vs1int_ */

/* Subroutine */ int vs1int_(real *wz, integer *nw, integer *ix)
{
    return vs1int_0_(0, wz, nw, ix, (real *)0);
    }

/* Subroutine */ int vs1din_(real *wz, integer *nw, integer *ix, real *x)
{
    return vs1int_0_(1, wz, nw, ix, x);
    }

/* Subroutine */ int vs1out_(real *wz, integer *nw, integer *ix)
{
    return vs1int_0_(2, wz, nw, ix, (real *)0);
    }

