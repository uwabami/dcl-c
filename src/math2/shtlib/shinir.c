/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     INITIALIZE MATRICES FOR CALCULATION OF LEGENDRE FUNCTIONS */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shinir_(integer *mm, real *r__)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static integer l, m, n;

    /* Parameter adjustments */
    --r__;

    /* Function Body */
    l = 0;
    i__1 = *mm;
    for (m = 0; m <= i__1; ++m) {
	i__2 = *mm + 1;
	for (n = m + 1; n <= i__2; ++n) {
	    ++l;
	    r__[l] = sqrt((n * n - m * m) * 1.f / ((n << 2) * n - 1));
/* L10: */
	}
	i__2 = *mm + 1;
	for (n = m + 2; n <= i__2; ++n) {
	    ++l;
	    r__[l] = sqrt(((n << 2) * n - 1) * 1.f / (n * n - m * m));
/* L20: */
	}
/* L30: */
    }
    return 0;
} /* shinir_ */

