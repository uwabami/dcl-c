/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__0 = 0;

/* ********************************************************************** */
/*     TRANSFORM WAVE TO GRID */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shfw2g_(integer *mm, integer *jm, integer *im, real *w, 
	real *g, real *h__, real *wfft)
{
    /* System generated locals */
    integer w_dim1, w_offset, g_dim1, g_offset;

    /* Local variables */
    extern /* Subroutine */ int shfwga_(integer *, integer *, integer *, 
	    integer *, integer *, real *, real *, real *, real *);

    /* Parameter adjustments */
    w_dim1 = *jm - (-(*jm)) + 1;
    w_offset = -(*jm) + w_dim1 * (-(*mm));
    w -= w_offset;
    g_dim1 = *im - (-(*im)) + 1;
    g_offset = -(*im) + g_dim1 * (-(*jm));
    g -= g_offset;
    --wfft;

    /* Function Body */
    shfwga_(mm, jm, im, &c__0, mm, &w[w_offset], &g[g_offset], h__, &wfft[1]);
    return 0;
} /* shfw2g_ */

