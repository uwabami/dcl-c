/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     BACKWARD LEGENDRE TRANSFORMATION (MIDDLE LEVEL) */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shlbwm_(integer *mm, integer *jm, integer *m, integer *
	isw, real *sm, real *wm, real *sd, real *pm, real *yc, real *r__)
{
    /* System generated locals */
    integer sm_offset, wm_offset, pm_dim1, pm_offset, i__1;

    /* Local variables */
    static integer j;
    static real wa, ws;
    extern /* Subroutine */ int shlbwl_(integer *, integer *, real *, real *, 
	    real *, real *), shlssd_(integer *, integer *, integer *, real *, 
	    real *, real *);

/*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL */
    /* Parameter adjustments */
    --r__;
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    wm_offset = -(*jm);
    wm -= wm_offset;
    sm_offset = *m;
    sm -= sm_offset;

    /* Function Body */
    if (*isw == -1 && *m == 0) {
	i__1 = *jm;
	for (j = -(*jm); j <= i__1; ++j) {
	    wm[j] = 0.f;
/* L10: */
	}
    } else {
	shlssd_(mm, m, isw, &sm[sm_offset], sd, &r__[1]);
	i__1 = *jm;
	for (j = 0; j <= i__1; ++j) {
	    shlbwl_(mm, m, sd, &ws, &wa, &pm[j * pm_dim1]);
	    wm[j] = ws + wa;
	    wm[-j] = ws - wa;
/* L20: */
	}
	if (*isw == 0 && *m != 0) {
	    i__1 = *jm;
	    for (j = 1; j <= i__1; ++j) {
		wm[j] *= yc[j];
		wm[-j] *= yc[j];
/* L30: */
	    }
	} else if (*isw == 1 && *m == 0) {
	    i__1 = *jm - 1;
	    for (j = 1; j <= i__1; ++j) {
		wm[j] /= yc[j];
		wm[-j] /= yc[j];
/* L40: */
	    }
	    wm[*jm] = 0.f;
	    wm[-(*jm)] = 0.f;
	}
    }
    return 0;
} /* shlbwm_ */

