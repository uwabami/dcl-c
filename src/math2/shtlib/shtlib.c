/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     SPHERICAL HARMONICS TRANSFORMATION LIBRARY */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shtlib_0_(int n__, integer *mm, integer *jm, integer *im,
	 real *q, integer *ind, real *a, real *b, integer *n, integer *m, 
	integer *lr, integer *li, real *fun, integer *isw, real *wm, real *sm,
	 real *s, real *w, integer *m1, integer *m2, real *wz, real *wr, real 
	*wi, integer *j, real *wj, real *g, real *gj)
{
    static integer ih, nh, ir, nr, iy, nx, ny, nz, ix, iz, isd, ixc, nsd, iyc,
	     ipm, nyc, nxc, npm, ipx, ipy, ixs, iys, npx, npy, ixw, nxs, nys, 
	    iyw, iwy, nxw, nyw, nwy, nwx, iwx, ipmj, npmj, iwfft, nwfft;
    extern /* Subroutine */ int shfw2g_(integer *, integer *, integer *, real 
	    *, real *, real *, real *), shfg2w_(integer *, integer *, integer 
	    *, real *, real *, real *, real *), shnm2l_(integer *, integer *, 
	    integer *, integer *, integer *), shms2w_(integer *, integer *, 
	    integer *, real *, real *, real *, real *, real *, real *, real *,
	     real *), shmw2s_(integer *, integer *, integer *, real *, real *,
	     real *, real *, real *, real *, real *, real *, real *, real *, 
	    real *), shfwga_(integer *, integer *, integer *, integer *, 
	    integer *, real *, real *, real *, real *), sholap_(integer *, 
	    integer *, real *, real *), msgdmp_(char *, char *, char *, 
	    ftnlen, ftnlen, ftnlen), shfwgj_(integer *, integer *, integer *, 
	    integer *, real *, real *, real *, real *), shfwgm_(integer *, 
	    integer *, integer *, real *, real *, real *, real *, real *), 
	    shinit_(integer *, integer *, integer *, real *, real *, real *, 
	    real *, real *, real *, real *, real *, real *, real *, real *, 
	    real *), shmswa_(integer *, integer *, integer *, integer *, 
	    integer *, real *, real *, real *, real *, real *, real *, real *,
	     real *), shpfun_(integer *, integer *, integer *, real *, real *,
	     real *, real *, real *, real *), shlbwu_(integer *, integer *, 
	    integer *, integer *, real *, real *, real *, real *, real *, 
	    real *, real *, real *), shlfwu_(integer *, integer *, integer *, 
	    integer *, real *, real *, real *, real *, real *, real *, real *,
	     real *, real *, real *, real *), shmswj_(integer *, integer *, 
	    integer *, integer *, integer *, integer *, real *, real *, real *
	    , real *, real *, real *, real *, real *), shmswm_(integer *, 
	    integer *, integer *, integer *, real *, real *, real *, real *, 
	    real *, real *, real *, real *, real *), shfwgz_(integer *, 
	    integer *, real *, real *), shmswz_(integer *, integer *, integer 
	    *, real *, real *, real *, real *, real *, real *, real *, real *)
	    ;

/*      REAL Q((JM+1)*(4*JM+5*MM+14)+(MM+1)*(MM+1)+MM+2+6*IM+15) */
/*      REAL FUN(-JM:JM,M:MM) */
/*      REAL S((MM+1)*(MM+1)),W(-JM:JM,-MM:MM),G(-IM:IM,-JM:JM) */
/*      REAL SM(M:MM),WM(-JM:JM),WZ(-JM:JM),WR(-JM:JM),WI(-JM:JM) */
/*      REAL WJ(-MM:MM),GJ(-IM:IM) */
/*      REAL A((MM+1)*(MM+1)),B((MM+1)*(MM+1)) */
    /* Parameter adjustments */
    if (q) {
	--q;
	}
    if (a) {
	--a;
	}
    if (b) {
	--b;
	}
    if (fun) {
	--fun;
	}
    if (wm) {
	--wm;
	}
    if (sm) {
	--sm;
	}
    if (s) {
	--s;
	}
    if (w) {
	--w;
	}
    if (wz) {
	--wz;
	}
    if (wr) {
	--wr;
	}
    if (wi) {
	--wi;
	}
    if (wj) {
	--wj;
	}
    if (g) {
	--g;
	}
    if (gj) {
	--gj;
	}

    /* Function Body */
    switch(n__) {
	case 1: goto L_shtint;
	case 2: goto L_shtlap;
	case 3: goto L_shtnml;
	case 4: goto L_shtfun;
	case 5: goto L_shtlfw;
	case 6: goto L_shtlbw;
	case 7: goto L_shts2w;
	case 8: goto L_shtswa;
	case 9: goto L_shtswz;
	case 10: goto L_shtswm;
	case 11: goto L_shtswj;
	case 12: goto L_shtw2s;
	case 13: goto L_shtw2g;
	case 14: goto L_shtwga;
	case 15: goto L_shtwgm;
	case 16: goto L_shtwgz;
	case 17: goto L_shtwgj;
	case 18: goto L_shtg2w;
	case 19: goto L_shts2g;
	case 20: goto L_shtsga;
	case 21: goto L_shtsgz;
	case 22: goto L_shtsgm;
	case 23: goto L_shtsgj;
	case 24: goto L_shtg2s;
	}

    return 0;
/* ---------------------------------------------------------------------- */
/*     INITIALIZATION */
/* ---------------------------------------------------------------------- */

L_shtint:
    if (*im < *mm + 1) {
	msgdmp_("E", "SHTLIB", "IM MUST BE IM >= MM+1", (ftnlen)1, (ftnlen)6, 
		(ftnlen)21);
    }
    if (*jm < (*mm + 1) / 2) {
	msgdmp_("E", "SHTLIB", "JM MUST BE JM >= (MM+1)/2", (ftnlen)1, (
		ftnlen)6, (ftnlen)25);
    }
    ny = *jm + 1;
    nyw = *jm + 1;
    nys = *jm + 1;
    nyc = *jm + 1;
    nx = *jm + 1;
    nxw = *jm + 1;
    nxs = *jm + 1;
    nxc = *jm + 1;
    nz = *jm * (*jm + 1) << 2;
    nr = (*mm + 1) * (*mm + 1);
    npy = (*jm + 1 << 1) * (*mm + 1);
    npx = (*jm + 1 << 1) * (*mm + 1);
    npm = (*mm + 2) * (*jm + 1);
    nsd = *mm + 2;
    nh = *im << 1;
    nwfft = (*im << 2) + 15;
    nwy = *jm + 1 << 1;
    nwx = *jm + 1 << 1;
    npmj = *mm + 2;
    iy = 1;
    iyw = iy + ny;
    iys = iyw + nyw;
    iyc = iys + nys;
    ix = iyc + nyc;
    ixw = ix + nx;
    ixs = ixw + nxw;
    ixc = ixs + nxs;
    iz = ixc + nxc;
    ir = iz + nz;
    ipy = ir + nr;
    ipx = ipy + npy;
    ipm = ipx + npx;
    isd = ipm + npm;
    ih = isd + nsd;
    iwfft = ih + nh;
    iwy = iy;
    iwx = ix;
    ipmj = ipm;
    shinit_(mm, jm, im, &q[iy], &q[iys], &q[iyc], &q[ix], &q[ixs], &q[ixc], &
	    q[ixw], &q[iz], &q[ipy], &q[ipx], &q[ir], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     OPERATE LAPLACIAN */
/* ---------------------------------------------------------------------- */

L_shtlap:
    sholap_(mm, ind, &a[1], &b[1]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     CALCULATE THE POSITION OF A SPECTRUM COEFFICIENT OF P_N^M */
/* ---------------------------------------------------------------------- */

L_shtnml:
    shnm2l_(mm, n, m, lr, li);
    return 0;
/* ---------------------------------------------------------------------- */
/*     CALCULATE LEGENDRE FUNCTIONS */
/* ---------------------------------------------------------------------- */

L_shtfun:
    shpfun_(mm, jm, m, &fun[1], &q[ipm], &q[iys], &q[iyc], &q[ipy], &q[ir]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     FORWARD LEGENDRE TRANSFORMATION */
/* ---------------------------------------------------------------------- */

L_shtlfw:
    shlfwu_(mm, jm, m, isw, &wm[1], &sm[1], &q[isd], &q[ixs], &q[ixc], &q[ipm]
	    , &q[iwy], &q[iwx], &q[ipx], &q[ir], &q[iz]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     BACKWARD LEGENDRE TRANSFORMATION */
/* ---------------------------------------------------------------------- */

L_shtlbw:
    shlbwu_(mm, jm, m, isw, &sm[1], &wm[1], &q[isd], &q[ipm], &q[iys], &q[iyc]
	    , &q[ipy], &q[ir]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO WAVE */
/* ---------------------------------------------------------------------- */

L_shts2w:
    shms2w_(mm, jm, isw, &s[1], &w[1], &q[isd], &q[ipm], &q[iys], &q[iyc], &q[
	    ipy], &q[ir]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO WAVE (FROM M=M1 TO M=M2) */
/* ---------------------------------------------------------------------- */

L_shtswa:
    shmswa_(mm, jm, isw, m1, m2, &s[1], &w[1], &q[isd], &q[ipm], &q[iys], &q[
	    iyc], &q[ipy], &q[ir]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO WAVE (FOR M=0) */
/* ---------------------------------------------------------------------- */

L_shtswz:
    shmswz_(mm, jm, isw, &s[1], &wz[1], &q[isd], &q[ipm], &q[iys], &q[iyc], &
	    q[ipy], &q[ir]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO WAVE (FOR M>0) */
/* ---------------------------------------------------------------------- */

L_shtswm:
    shmswm_(mm, jm, m, isw, &s[1], &wr[1], &wi[1], &q[isd], &q[ipm], &q[iys], 
	    &q[iyc], &q[ipy], &q[ir]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO WAVE FROM M=M1 TO M=M2 AT A LATITUDE */
/* ---------------------------------------------------------------------- */

L_shtswj:
    shmswj_(mm, jm, isw, j, m1, m2, &s[1], &wj[1], &q[isd], &q[ipmj], &q[iys],
	     &q[iyc], &q[ipy], &q[ir]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM WAVE INTO SPECTRA */
/* ---------------------------------------------------------------------- */

L_shtw2s:
    shmw2s_(mm, jm, isw, &s[1], &w[1], &q[isd], &q[ipm], &q[ixs], &q[ixc], &q[
	    ipx], &q[iwy], &q[iwx], &q[ir], &q[iz]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM WAVE TO GRID */
/* ---------------------------------------------------------------------- */

L_shtw2g:
    shfw2g_(mm, jm, im, &w[1], &g[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM WAVE TO GRID FROM M=M1 TO M=M2 */
/* ---------------------------------------------------------------------- */

L_shtwga:
    shfwga_(mm, jm, im, m1, m2, &w[1], &g[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM WAVE TO GRID FOR M>0 */
/* ---------------------------------------------------------------------- */

L_shtwgm:
    shfwgm_(jm, im, m, &wr[1], &wi[1], &g[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM WAVE TO GRID FOR M=0 */
/* ---------------------------------------------------------------------- */

L_shtwgz:
    shfwgz_(jm, im, &wz[1], &g[1]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM WAVE TO GRID FROM M=M1 TO M=M2 AT A LATITUDE */
/* ---------------------------------------------------------------------- */

L_shtwgj:
    shfwgj_(mm, im, m1, m2, &wj[1], &gj[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM GRID INTO WAVE */
/* ---------------------------------------------------------------------- */

L_shtg2w:
    shfg2w_(mm, jm, im, &g[1], &w[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO GRID */
/* ---------------------------------------------------------------------- */

L_shts2g:
    shms2w_(mm, jm, isw, &s[1], &w[1], &q[isd], &q[ipm], &q[iys], &q[iyc], &q[
	    ipy], &q[ir]);
    shfw2g_(mm, jm, im, &w[1], &g[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO GRID (FROM M=M1 TO M=M2) */
/* ---------------------------------------------------------------------- */

L_shtsga:
    shmswa_(mm, jm, isw, m1, m2, &s[1], &w[1], &q[isd], &q[ipm], &q[iys], &q[
	    iyc], &q[ipy], &q[ir]);
    shfwga_(mm, jm, im, m1, m2, &w[1], &g[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO GRID (FOR M=0) */
/* ---------------------------------------------------------------------- */

L_shtsgz:
    shmswz_(mm, jm, isw, &s[1], &wz[1], &q[isd], &q[ipm], &q[iys], &q[iyc], &
	    q[ipy], &q[ir]);
    shfwgz_(jm, im, &wz[1], &g[1]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO GRID (FOR M>0) */
/* ---------------------------------------------------------------------- */

L_shtsgm:
    shmswm_(mm, jm, m, isw, &s[1], &wr[1], &wi[1], &q[isd], &q[ipm], &q[iys], 
	    &q[iyc], &q[ipy], &q[ir]);
    shfwgm_(jm, im, m, &wr[1], &wi[1], &g[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM SPECTRA INTO GRID FROM M=M1 TO M=M2 AT A LATITUDE */
/* ---------------------------------------------------------------------- */

L_shtsgj:
    shmswj_(mm, jm, isw, j, m1, m2, &s[1], &wj[1], &q[isd], &q[ipmj], &q[iys],
	     &q[iyc], &q[ipy], &q[ir]);
    shfwgj_(mm, im, m1, m2, &wj[1], &gj[1], &q[ih], &q[iwfft]);
    return 0;
/* ---------------------------------------------------------------------- */
/*     TRANSFORM GRID INTO SPECTRA */
/* ---------------------------------------------------------------------- */

L_shtg2s:
    shfg2w_(mm, jm, im, &g[1], &w[1], &q[ih], &q[iwfft]);
    shmw2s_(mm, jm, isw, &w[1], &s[1], &q[isd], &q[ipm], &q[ixs], &q[ixc], &q[
	    ipx], &q[iwy], &q[iwx], &q[ir], &q[iz]);
    return 0;
} /* shtlib_ */

/* Subroutine */ int shtlib_(void)
{
    return shtlib_0_(0, (integer *)0, (integer *)0, (integer *)0, (real *)0, (
	    integer *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    integer *)0, (integer *)0, (real *)0, (integer *)0, (real *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (integer *)0, (real 
	    *)0, (real *)0, (real *)0, (integer *)0, (real *)0, (real *)0, (
	    real *)0);
    }

/* Subroutine */ int shtint_(integer *mm, integer *jm, integer *im, real *q)
{
    return shtlib_0_(1, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (integer *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int shtlap_(integer *mm, integer *ind, real *a, real *b)
{
    return shtlib_0_(2, mm, (integer *)0, (integer *)0, (real *)0, ind, a, b, 
	    (integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0,
	     (integer *)0, (real *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (
	    integer *)0, (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int shtnml_(integer *mm, integer *n, integer *m, integer *lr,
	 integer *li)
{
    return shtlib_0_(3, mm, (integer *)0, (integer *)0, (real *)0, (integer *)
	    0, (real *)0, (real *)0, n, m, lr, li, (real *)0, (integer *)0, (
	    real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (integer 
	    *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (real *)0, (
	    real *)0, (real *)0);
    }

/* Subroutine */ int shtfun_(integer *mm, integer *jm, integer *m, real *fun, 
	real *q)
{
    return shtlib_0_(4, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, m, (integer *)0, (integer *)0, fun, (
	    integer *)0, (real *)0, (real *)0, (real *)0, (real *)0, (integer 
	    *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (integer *)0, 
	    (real *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int shtlfw_(integer *mm, integer *jm, integer *m, integer *
	isw, real *wm, real *sm, real *q)
{
    return shtlib_0_(5, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, m, (integer *)0, (integer *)0, (real *)0, 
	    isw, wm, sm, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (real *)0, (real *)
	    0, (real *)0);
    }

/* Subroutine */ int shtlbw_(integer *mm, integer *jm, integer *m, integer *
	isw, real *sm, real *wm, real *q)
{
    return shtlib_0_(6, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, m, (integer *)0, (integer *)0, (real *)0, 
	    isw, wm, sm, (real *)0, (real *)0, (integer *)0, (integer *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (real *)0, (real *)
	    0, (real *)0);
    }

/* Subroutine */ int shts2w_(integer *mm, integer *jm, integer *isw, real *s, 
	real *w, real *q)
{
    return shtlib_0_(7, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (real *)0, isw, (real *)0, (real *)0, s, w, (integer *)0, (
	    integer *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (real 
	    *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int shtswa_(integer *mm, integer *jm, integer *isw, integer *
	m1, integer *m2, real *s, real *w, real *q)
{
    return shtlib_0_(8, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (real *)0, isw, (real *)0, (real *)0, s, w, m1, m2, (real *)0, (
	    real *)0, (real *)0, (integer *)0, (real *)0, (real *)0, (real *)
	    0);
    }

/* Subroutine */ int shtswz_(integer *mm, integer *jm, integer *isw, real *s, 
	real *wz, real *q)
{
    return shtlib_0_(9, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (real *)0, isw, (real *)0, (real *)0, s, (real *)0, (integer *)0, 
	    (integer *)0, wz, (real *)0, (real *)0, (integer *)0, (real *)0, (
	    real *)0, (real *)0);
    }

/* Subroutine */ int shtswm_(integer *mm, integer *jm, integer *m, integer *
	isw, real *s, real *wr, real *wi, real *q)
{
    return shtlib_0_(10, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, m, (integer *)0, (integer *)0, (real *)0, 
	    isw, (real *)0, (real *)0, s, (real *)0, (integer *)0, (integer *)
	    0, (real *)0, wr, wi, (integer *)0, (real *)0, (real *)0, (real *)
	    0);
    }

/* Subroutine */ int shtswj_(integer *mm, integer *jm, integer *isw, integer *
	j, integer *m1, integer *m2, real *s, real *wj, real *q)
{
    return shtlib_0_(11, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (real *)0, isw, (real *)0, (real *)0, s, (real *)0, m1, m2, (real 
	    *)0, (real *)0, (real *)0, j, wj, (real *)0, (real *)0);
    }

/* Subroutine */ int shtw2s_(integer *mm, integer *jm, integer *isw, real *s, 
	real *w, real *q)
{
    return shtlib_0_(12, mm, jm, (integer *)0, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (real *)0, isw, (real *)0, (real *)0, s, w, (integer *)0, (
	    integer *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (real 
	    *)0, (real *)0, (real *)0);
    }

/* Subroutine */ int shtw2g_(integer *mm, integer *jm, integer *im, real *w, 
	real *g, real *q)
{
    return shtlib_0_(13, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (integer *)0, (real *)0, (real *)0, (real *)0, w, (integer *)0, (
	    integer *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (real 
	    *)0, g, (real *)0);
    }

/* Subroutine */ int shtwga_(integer *mm, integer *jm, integer *im, integer *
	m1, integer *m2, real *w, real *g, real *q)
{
    return shtlib_0_(14, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (integer *)0, (real *)0, (real *)0, (real *)0, w, m1, m2, (real *)
	    0, (real *)0, (real *)0, (integer *)0, (real *)0, g, (real *)0);
    }

/* Subroutine */ int shtwgm_(integer *mm, integer *jm, integer *im, integer *
	m, real *wr, real *wi, real *g, real *q)
{
    return shtlib_0_(15, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, m, (integer *)0, (integer *)0, (real *)0, (integer *)
	    0, (real *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (
	    integer *)0, (real *)0, wr, wi, (integer *)0, (real *)0, g, (real 
	    *)0);
    }

/* Subroutine */ int shtwgz_(integer *jm, integer *im, real *wz, real *g)
{
    return shtlib_0_(16, (integer *)0, jm, im, (real *)0, (integer *)0, (real 
	    *)0, (real *)0, (integer *)0, (integer *)0, (integer *)0, (
	    integer *)0, (real *)0, (integer *)0, (real *)0, (real *)0, (real 
	    *)0, (real *)0, (integer *)0, (integer *)0, wz, (real *)0, (real *
	    )0, (integer *)0, (real *)0, g, (real *)0);
    }

/* Subroutine */ int shtwgj_(integer *mm, integer *im, integer *m1, integer *
	m2, real *wj, real *gj, real *q)
{
    return shtlib_0_(17, mm, (integer *)0, im, q, (integer *)0, (real *)0, (
	    real *)0, (integer *)0, (integer *)0, (integer *)0, (integer *)0, 
	    (real *)0, (integer *)0, (real *)0, (real *)0, (real *)0, (real *)
	    0, m1, m2, (real *)0, (real *)0, (real *)0, (integer *)0, wj, (
	    real *)0, gj);
    }

/* Subroutine */ int shtg2w_(integer *mm, integer *jm, integer *im, real *g, 
	real *w, real *q)
{
    return shtlib_0_(18, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    (integer *)0, (real *)0, (real *)0, (real *)0, w, (integer *)0, (
	    integer *)0, (real *)0, (real *)0, (real *)0, (integer *)0, (real 
	    *)0, g, (real *)0);
    }

/* Subroutine */ int shts2g_(integer *mm, integer *jm, integer *im, integer *
	isw, real *s, real *w, real *g, real *q)
{
    return shtlib_0_(19, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    isw, (real *)0, (real *)0, s, w, (integer *)0, (integer *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (real *)0, g, (real 
	    *)0);
    }

/* Subroutine */ int shtsga_(integer *mm, integer *jm, integer *im, integer *
	isw, integer *m1, integer *m2, real *s, real *w, real *g, real *q)
{
    return shtlib_0_(20, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    isw, (real *)0, (real *)0, s, w, m1, m2, (real *)0, (real *)0, (
	    real *)0, (integer *)0, (real *)0, g, (real *)0);
    }

/* Subroutine */ int shtsgz_(integer *mm, integer *jm, integer *im, integer *
	isw, real *s, real *wz, real *g, real *q)
{
    return shtlib_0_(21, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    isw, (real *)0, (real *)0, s, (real *)0, (integer *)0, (integer *)
	    0, wz, (real *)0, (real *)0, (integer *)0, (real *)0, g, (real *)
	    0);
    }

/* Subroutine */ int shtsgm_(integer *mm, integer *jm, integer *im, integer *
	m, integer *isw, real *s, real *wr, real *wi, real *g, real *q)
{
    return shtlib_0_(22, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, m, (integer *)0, (integer *)0, (real *)0, isw, (real 
	    *)0, (real *)0, s, (real *)0, (integer *)0, (integer *)0, (real *)
	    0, wr, wi, (integer *)0, (real *)0, g, (real *)0);
    }

/* Subroutine */ int shtsgj_(integer *mm, integer *jm, integer *im, integer *
	isw, integer *j, integer *m1, integer *m2, real *s, real *wj, real *
	gj, real *q)
{
    return shtlib_0_(23, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    isw, (real *)0, (real *)0, s, (real *)0, m1, m2, (real *)0, (real 
	    *)0, (real *)0, j, wj, (real *)0, gj);
    }

/* Subroutine */ int shtg2s_(integer *mm, integer *jm, integer *im, integer *
	isw, real *g, real *w, real *s, real *q)
{
    return shtlib_0_(24, mm, jm, im, q, (integer *)0, (real *)0, (real *)0, (
	    integer *)0, (integer *)0, (integer *)0, (integer *)0, (real *)0, 
	    isw, (real *)0, (real *)0, s, w, (integer *)0, (integer *)0, (
	    real *)0, (real *)0, (real *)0, (integer *)0, (real *)0, g, (real 
	    *)0);
    }

