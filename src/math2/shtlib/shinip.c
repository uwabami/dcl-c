/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     INITIALIZE BASE LEGENDRE FUNCTIONS */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shinip_(integer *mm, integer *jm, real *ys, real *yc, 
	real *py)
{
    /* System generated locals */
    integer py_dim2, py_offset, i__1, i__2;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static real a, b;
    static integer j, m;

/*     M=0: P^0_0 and P^0_1, */
/*     M>0: P^M_M/\cos\phi and P^M_{M+1}/\cos\phi */
    /* Parameter adjustments */
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;

    /* Function Body */
    i__1 = *jm;
    for (j = 0; j <= i__1; ++j) {
	py[(j << 1) + 1] = 1.f;
	py[(j << 1) + 2] = sqrt(3.f) * ys[j];
/* L10: */
    }
    m = 1;
    a = sqrt(((m << 1) + 1) * 1.f / (m << 1));
    b = sqrt((m << 1) + 3.f);
    i__1 = *jm;
    for (j = 0; j <= i__1; ++j) {
	py[(j + m * py_dim2 << 1) + 1] = a * py[(j + (m - 1) * py_dim2 << 1) 
		+ 1];
	py[(j + m * py_dim2 << 1) + 2] = b * ys[j] * py[(j + m * py_dim2 << 1)
		 + 1];
/* L20: */
    }
    i__1 = *mm;
    for (m = 2; m <= i__1; ++m) {
	a = sqrt(((m << 1) + 1) * 1.f / (m << 1));
	b = sqrt((m << 1) + 3.f);
	i__2 = *jm;
	for (j = 0; j <= i__2; ++j) {
	    py[(j + m * py_dim2 << 1) + 1] = a * yc[j] * py[(j + (m - 1) * 
		    py_dim2 << 1) + 1];
	    py[(j + m * py_dim2 << 1) + 2] = b * ys[j] * py[(j + m * py_dim2 
		    << 1) + 1];
/* L30: */
	}
/* L40: */
    }
    return 0;
} /* shinip_ */

