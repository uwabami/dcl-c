/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     X-DIFFERENTIAL TRANSFORMATION FOR SHLBWM */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shmdxm_(integer *jm, real *wr, real *wi)
{
    /* System generated locals */
    integer wr_offset, wi_offset, i__1;

    /* Local variables */
    static integer j;
    static real wid, wrd;

    /* Parameter adjustments */
    wi_offset = -(*jm);
    wi -= wi_offset;
    wr_offset = -(*jm);
    wr -= wr_offset;

    /* Function Body */
    i__1 = *jm;
    for (j = -(*jm); j <= i__1; ++j) {
	wrd = wr[j];
	wid = wi[j];
	wr[j] = -wid;
	wi[j] = wrd;
/* L10: */
    }
    return 0;
} /* shmdxm_ */

