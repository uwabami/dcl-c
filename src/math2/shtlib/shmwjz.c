/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM SPECTRA INTO WAVE FOR M=0 AT A LATITUDE */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shmwjz_(integer *mm, integer *jm, integer *isw, integer *
	j, real *s, real *wjd, real *sd, real *pmj, real *ys, real *yc, real *
	py, real *r__)
{
    /* System generated locals */
    integer py_dim2, py_offset;

    /* Local variables */
    static integer m, jd;
    extern /* Subroutine */ int shlbwj_(integer *, integer *, integer *, 
	    integer *, integer *, real *, real *, real *, real *, real *, 
	    real *), shppmj_(integer *, integer *, integer *, integer *, real 
	    *, real *, real *, real *);

    /* Parameter adjustments */
    --r__;
    --s;
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;

    /* Function Body */
    jd = abs(*j);
    m = 0;
    if (*isw == -1) {
	*wjd = 0.f;
    } else {
	shppmj_(mm, jm, &m, &jd, pmj, ys, &py[py_offset], &r__[1]);
	shlbwj_(mm, jm, &m, j, isw, &s[1], wjd, sd, pmj, yc, &r__[1]);
    }
    return 0;
} /* shmwjz_ */

