/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* *********************************************************************** */
/*     INITIALIZE MATRICES FOR INTERPOLATION */
/* *********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shiniz_(integer *jm, real *x, real *y, real *z__)
{
    /* System generated locals */
    integer z_dim1, z_dim2, z_offset, i__1, i__2;

    /* Builtin functions */
    double sin(doublereal), tan(doublereal);

    /* Local variables */
    static integer j1, j2;
    static real t1, t2;

    /* Parameter adjustments */
    z_dim1 = *jm;
    z_dim2 = *jm - 0 + 1;
    z_offset = 1 + z_dim1 * (0 + z_dim2);
    z__ -= z_offset;

    /* Function Body */
    i__1 = *jm;
    for (j1 = 1; j1 <= i__1; ++j1) {
	i__2 = *jm;
	for (j2 = 0; j2 <= i__2; ++j2) {
	    t1 = x[j1] - y[j2];
	    t2 = x[j1] + y[j2];
	    z__[j1 + (j2 + z_dim2) * z_dim1] = (sin((*jm << 1) * t1) / tan(t1)
		     + sin((*jm << 1) * t2) / tan(t2)) / (*jm << 1);
/* L10: */
	}
	z__[j1 + z_dim2 * z_dim1] /= 2;
	z__[j1 + (*jm + z_dim2) * z_dim1] /= 2;
/* L11: */
    }
    i__1 = *jm;
    for (j1 = 1; j1 <= i__1; ++j1) {
	z__[j1 + (z_dim2 << 1) * z_dim1] = 0.f;
	i__2 = *jm;
	for (j2 = 1; j2 <= i__2; ++j2) {
	    t1 = x[j1] - y[j2];
	    t2 = x[j1] + y[j2];
	    z__[j1 + (j2 + (z_dim2 << 1)) * z_dim1] = (sin((*jm << 1) * t1) / 
		    sin(t1) - sin((*jm << 1) * t2) / sin(t2)) / (*jm << 1);
/* L20: */
	}
	z__[j1 + (*jm + (z_dim2 << 1)) * z_dim1] /= 2;
/* L21: */
    }
    i__1 = *jm;
    for (j1 = 1; j1 <= i__1; ++j1) {
	i__2 = *jm - 1;
	for (j2 = 0; j2 <= i__2; ++j2) {
	    t1 = x[j1] - y[j2];
	    t2 = x[j1] + y[j2];
	    z__[j1 + (j2 + z_dim2 * 3) * z_dim1] = (sin((*jm << 1) * t1) / 
		    sin(t1) + sin((*jm << 1) * t2) / sin(t2)) / (*jm << 1);
/* L30: */
	}
	z__[j1 + z_dim2 * 3 * z_dim1] /= 2;
/* L31: */
    }
    i__1 = *jm;
    for (j1 = 1; j1 <= i__1; ++j1) {
	z__[j1 + (z_dim2 << 2) * z_dim1] = 0.f;
	i__2 = *jm - 1;
	for (j2 = 1; j2 <= i__2; ++j2) {
	    t1 = x[j1] - y[j2];
	    t2 = x[j1] + y[j2];
	    z__[j1 + (j2 + (z_dim2 << 2)) * z_dim1] = (sin(((*jm << 1) + 1) * 
		    t1) / sin(t1) - sin(((*jm << 1) + 1) * t2) / sin(t2)) / (*
		    jm << 1);
/* L40: */
	}
/* L41: */
    }
    return 0;
} /* shiniz_ */

