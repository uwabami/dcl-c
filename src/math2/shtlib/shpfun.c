/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     CALCULATE LEGENDRE FUNCTIONS */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shpfun_(integer *mm, integer *jm, integer *m, real *fun, 
	real *pm, real *ys, real *yc, real *py, real *r__)
{
    /* System generated locals */
    integer fun_dim1, fun_offset, pm_dim1, pm_offset, py_dim2, py_offset, 
	    i__1, i__2;

    /* Local variables */
    static integer j, n, ip;
    extern /* Subroutine */ int shppma_(integer *, integer *, integer *, real 
	    *, real *, real *, real *);

    /* Parameter adjustments */
    --r__;
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;
    pm_dim1 = *mm + 1 - 0 + 1;
    pm_offset = 0 + pm_dim1 * 0;
    pm -= pm_offset;
    fun_dim1 = *jm - (-(*jm)) + 1;
    fun_offset = -(*jm) + fun_dim1 * *m;
    fun -= fun_offset;

    /* Function Body */
    shppma_(mm, jm, m, &pm[pm_offset], ys, &py[py_offset], &r__[1]);
    j = 0;
    i__1 = *mm;
    for (n = *m; n <= i__1; ++n) {
	fun[j + n * fun_dim1] = pm[n + j * pm_dim1];
/* L10: */
    }
    ip = -1;
    i__1 = *mm;
    for (n = *m; n <= i__1; ++n) {
	ip = -ip;
	i__2 = *jm;
	for (j = 1; j <= i__2; ++j) {
	    fun[j + n * fun_dim1] = pm[n + j * pm_dim1];
	    fun[-j + n * fun_dim1] = pm[n + j * pm_dim1] * ip;
/* L20: */
	}
/* L30: */
    }
    if (*m != 0) {
	i__1 = *mm;
	for (n = *m; n <= i__1; ++n) {
	    i__2 = *jm;
	    for (j = 1; j <= i__2; ++j) {
		fun[j + n * fun_dim1] *= yc[j];
		fun[-j + n * fun_dim1] *= yc[j];
/* L40: */
	    }
/* L50: */
	}
    }
    return 0;
} /* shpfun_ */

