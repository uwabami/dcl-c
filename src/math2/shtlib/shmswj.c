/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* ********************************************************************** */
/*     TRANSFORM SPECTRA INTO WAVE FROM M=M1 TO M=M2 AT A LATITUDE */
/* ********************************************************************** */
/*     Copyright (C) 2000-2016 GFD Dennou Club. All rights reserved. */
/* ----------------------------------------------------------------------- */
/* Subroutine */ int shmswj_(integer *mm, integer *jm, integer *isw, integer *
	j, integer *m1, integer *m2, real *s, real *wj, real *sd, real *pmj, 
	real *ys, real *yc, real *py, real *r__)
{
    /* System generated locals */
    integer wj_offset, py_dim2, py_offset, i__1;

    /* Local variables */
    static integer m;
    extern /* Subroutine */ int shmwjm_(integer *, integer *, integer *, 
	    integer *, integer *, real *, real *, real *, real *, real *, 
	    real *, real *, real *, real *), shmwjz_(integer *, integer *, 
	    integer *, integer *, real *, real *, real *, real *, real *, 
	    real *, real *, real *);

    /* Parameter adjustments */
    --r__;
    wj_offset = -(*mm);
    wj -= wj_offset;
    --s;
    py_dim2 = *jm - 0 + 1;
    py_offset = 1 + 2 * (0 + py_dim2 * 0);
    py -= py_offset;

    /* Function Body */
    if (*m1 > 0) {
	wj[0] = 0.f;
    } else {
	shmwjz_(mm, jm, isw, j, &s[1], wj, sd, pmj, ys, yc, &py[py_offset], &
		r__[1]);
    }
    i__1 = *m2;
    for (m = *m1; m <= i__1; ++m) {
	shmwjm_(mm, jm, isw, j, &m, &s[1], &wj[m], &wj[-m], sd, pmj, ys, yc, &
		py[py_offset], &r__[1]);
/* L20: */
    }
    i__1 = *mm;
    for (m = *m2 + 1; m <= i__1; ++m) {
	wj[m] = 0.f;
	wj[-m] = 0.f;
/* L30: */
    }
    return 0;
} /* shmswj_ */

