/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__51 = 51;
static real c_b10 = .5f;
static logical c_true = TRUE_;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double sqrt(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer n;
    static real x[51], y1[51], y2[51], y3[51];
    static integer iws;
    extern /* Subroutine */ int sgcls_(void), sgfrm_(void), sgopn_(integer *),
	     sgplv_(integer *, real *, real *), sgpmv_(integer *, real *, 
	    real *), sgtnv_(integer *, real *, real *), sgtxv_(real *, real *,
	     char *, ftnlen), sglset_(char *, logical *, ftnlen), sgpwsn_(
	    void), slpvpr_(integer *);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 6, 0, 0, 0 };
    static cilist io___7 = { 0, 5, 0, 0, 0 };


    for (n = 0; n <= 50; ++n) {
	x[n] = (real) n / 50.f;
/* Computing 3rd power */
	r__1 = x[n];
	y1[n] = r__1 * (r__1 * r__1);
/* Computing 2nd power */
	r__1 = x[n];
	y2[n] = r__1 * r__1;
	y3[n] = sqrt(x[n]);
/* L10: */
    }
    s_wsle(&io___6);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___7);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    sgopn_(&iws);
    sgfrm_();
    slpvpr_(&c__1);
    sgplv_(&c__51, x, y1);
    sgpmv_(&c__51, x, y2);
    sgtxv_(&c_b10, &c_b10, "SGTXV", (ftnlen)5);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    sgtnv_(&c__51, x, y3);
    sgcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int kihon1_ () { MAIN__ (); return 0; }
