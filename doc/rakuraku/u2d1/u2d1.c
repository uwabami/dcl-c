/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b7 = 0.f;
static real c_b8 = 360.f;
static real c_b9 = -90.f;
static real c_b10 = 90.f;
static real c_b11 = .2f;
static real c_b12 = .8f;
static real c_b18 = 1.4f;
static real c_b20 = -1.f;
static integer c__4 = 4;
static real c_b24 = .028f;
static real c_b25 = .6f;
static integer c__37 = 37;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1, r__2;

    /* Builtin functions */
    double sin(doublereal), sqrt(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real p[1369]	/* was [37][37] */;
    static integer iws;
    static real alat, alon, slat;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     udgcla_(real *, real *, real *), uddclv_(real *), udcntr_(real *,
	     integer *, integer *, integer *), udsclv_(real *, integer *, 
	    integer *, char *, real *, ftnlen), udsfmt_(char *, ftnlen), 
	    grswnd_(real *, real *, real *, real *), grstrf_(void), usdaxs_(
	    void), grstrn_(integer *), sgpwsn_(void), grsvpt_(real *, real *, 
	    real *, real *);

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 6, 0, 0, 0 };
    static cilist io___8 = { 0, 5, 0, 0, 0 };


    for (j = 1; j <= 37; ++j) {
	for (i__ = 1; i__ <= 37; ++i__) {
	    alon = ((i__ - 1) * 360.f / 36 + 0.f) * .017453277777777776f;
	    alat = ((j - 1) * 180.f / 36 - 90.f) * .017453277777777776f;
	    slat = sin(alat);
/* Computing 2nd power */
	    r__1 = slat;
/* Computing 2nd power */
	    r__2 = slat;
	    p[i__ + j * 37 - 38] = sqrt(1 - r__1 * r__1) * 3 * slat * cos(
		    alon) - (r__2 * r__2 * 3 - 1) * .5f;
/* L10: */
	}
    }
    s_wsle(&io___7);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___8);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b7, &c_b8, &c_b9, &c_b10);
    grsvpt_(&c_b11, &c_b12, &c_b11, &c_b12);
    grstrn_(&c__1);
    grstrf_();
    usdaxs_();
    udsfmt_("(F4.1)", (ftnlen)6);
    udgcla_(&c_b7, &c_b18, &c_b11);
    udsclv_(&c_b20, &c__3, &c__4, "abc", &c_b24, (ftnlen)3);
    uddclv_(&c_b25);
    udcntr_(p, &c__37, &c__37, &c__37);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2d1_ () { MAIN__ (); return 0; }
