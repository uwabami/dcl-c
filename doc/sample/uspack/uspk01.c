/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static integer c__400 = 400;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static real a, b, c__;
    static integer i__;
    static real t, x[400], y[400], dt;
    static integer iws;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     usgrph_(integer *, real *, real *), sgpwsn_(void);

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 6, 0, 0, 0 };
    static cilist io___10 = { 0, 5, 0, 0, 0 };


/* ---------------------------- DATA DEFINITION -------------------------- */
    dt = .0078736591478696747f;
    a = 1e5f;
    b = 1e-4f;
    c__ = 1.f;
    for (i__ = 1; i__ <= 400; ++i__) {
	t = dt * (i__ - 1);
	x[i__ - 1] = a * sin(t * 6.f);
	y[i__ - 1] = b * cos(t * 14.f) + c__;
/* L100: */
    }
/* ----------------------------- GRAPH ----------------------------------- */
    s_wsle(&io___9);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___10);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    usgrph_(&c__400, x, y);
    grcls_();
/* ----------------------------------------------------------------------- */
    return 0;
} /* MAIN__ */

/* Main program alias */ int uspk01_ () { MAIN__ (); return 0; }
