/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b13 = 0.f;
static real c_b14 = 360.f;
static real c_b15 = -90.f;
static real c_b16 = 90.f;
static real c_b17 = .2f;
static real c_b18 = .8f;
static integer c__19 = 19;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    real r__1;

    /* Builtin functions */
    double sin(doublereal), cos(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__, j;
    static real p[361]	/* was [19][19] */;
    static integer iws;
    static real alat, alon, slat;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *);
    static real rmiss;
    extern /* Subroutine */ int glrget_(char *, real *, ftnlen), gllset_(char 
	    *, logical *, ftnlen), udcntr_(real *, integer *, integer *, 
	    integer *), uetone_(real *, integer *, integer *, integer *), 
	    sglset_(char *, logical *, ftnlen), grswnd_(real *, real *, real *
	    , real *), grstrf_(void), usdaxs_(void), grstrn_(integer *), 
	    sgpwsn_(void), grsvpt_(real *, real *, real *, real *);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


    glrget_("RMISS", &rmiss, (ftnlen)5);
    gllset_("LMISS", &c_true, (ftnlen)5);
    for (j = 1; j <= 19; ++j) {
	for (i__ = 1; i__ <= 19; ++i__) {
	    alon = ((i__ - 1) * 360.f / 18 + 0.f) * .01745328888888889f;
	    alat = ((j - 1) * 180.f / 18 - 90.f) * .01745328888888889f;
	    slat = sin(alat);
	    if (i__ == 3 && j == 15 || 9 <= i__ && i__ <= 12 && (7 <= j && j 
		    <= 11)) {
		p[i__ + j * 19 - 20] = rmiss;
	    } else {
/* Computing 2nd power */
		r__1 = slat;
		p[i__ + j * 19 - 20] = cos(alon) * (1 - r__1 * r__1) * sin(
			slat * 6.2831840000000003f) + .05f;
	    }
/* L10: */
	}
/* L20: */
    }
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    sglset_("LSOFTF", &c_true, (ftnlen)6);
    grfrm_();
    grswnd_(&c_b13, &c_b14, &c_b15, &c_b16);
    grsvpt_(&c_b17, &c_b18, &c_b17, &c_b18);
    grstrn_(&c__1);
    grstrf_();
    usdaxs_();
    udcntr_(p, &c__19, &c__19, &c__19);
    uetone_(p, &c__19, &c__19, &c__19);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int u2df06_ () { MAIN__ (); return 0; }
