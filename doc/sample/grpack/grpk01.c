/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static logical c_true = TRUE_;
static real c_b5 = 0.f;
static real c_b6 = 1.f;
static real c_b7 = -.7f;
static real c_b8 = .7f;
static real c_b9 = .2f;
static real c_b10 = .8f;
static real c_b12 = .6f;
static integer c__1 = 1;
static real c_b18 = .5f;
static real c_b21 = .02f;
static integer c__0 = 0;
static integer c__3 = 3;
static real c_b27 = -.5f;
static real c_b31 = .62f;
static real c_b32 = .82f;
static logical c_false = FALSE_;

/* ----------------------------------------------------------------------- */
/* Main program */ int MAIN__(void)
{
    static integer iws;
    extern /* Subroutine */ int grfig_(void), grcls_(void), grfrm_(void), 
	    gropn_(integer *), uzfact_(real *), grswnd_(real *, real *, real *
	    , real *), grstrf_(void), usdaxs_(void), grstrn_(integer *), 
	    grsvpt_(real *, real *, real *, real *), uzlset_(char *, logical *
	    , ftnlen), swcstx_(char *, char *, ftnlen, ftnlen), ussttl_(char *
	    , char *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen), 
	    swlstx_(char *, logical *, ftnlen), sgtxzu_(real *, real *, char *
	    , real *, integer *, integer *, integer *, ftnlen);

    swcstx_("FNAME", "GRPK01", (ftnlen)5, (ftnlen)6);
    swlstx_("LSEP", &c_true, (ftnlen)4);
/*     WRITE(*,*) ' WORKSTATION ID (I)  ? ;' */
/*     CALL SGPWSN */
/*     READ (*,*) IWS */
    iws = 2;
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b5, &c_b6, &c_b7, &c_b8);
    grsvpt_(&c_b9, &c_b10, &c_b9, &c_b12);
    grstrn_(&c__1);
    grstrf_();
    ussttl_("X-AXIS", " ", "Y1-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b18, &c_b5, "PAGE:1, FRAME:1", &c_b21, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    grfig_();
    grswnd_(&c_b5, &c_b6, &c_b27, &c_b18);
    grsvpt_(&c_b9, &c_b10, &c_b31, &c_b32);
    grstrn_(&c__1);
    grstrf_();
    uzlset_("LABELXB", &c_false, (ftnlen)7);
    ussttl_("X-AXIS", " ", "Y2-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b18, &c_b5, "PAGE:1, FRAME:2", &c_b21, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    uzlset_("LABELXB", &c_true, (ftnlen)7);
    grfrm_();
    uzfact_(&c_b18);
    grswnd_(&c_b5, &c_b6, &c_b7, &c_b8);
    grsvpt_(&c_b9, &c_b10, &c_b9, &c_b12);
    grstrn_(&c__1);
    grstrf_();
    ussttl_("X-AXIS", " ", "Y1-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b18, &c_b5, "PAGE:2, FRAME:1", &c_b21, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    grfig_();
    grswnd_(&c_b5, &c_b6, &c_b27, &c_b18);
    grsvpt_(&c_b9, &c_b10, &c_b31, &c_b32);
    grstrn_(&c__1);
    grstrf_();
    uzlset_("LABELXB", &c_false, (ftnlen)7);
    ussttl_("X-AXIS", " ", "Y2-AXIS", " ", (ftnlen)6, (ftnlen)1, (ftnlen)7, (
	    ftnlen)1);
    usdaxs_();
    sgtxzu_(&c_b18, &c_b5, "PAGE:2, FRAME:2", &c_b21, &c__0, &c__0, &c__3, (
	    ftnlen)15);
    uzlset_("LABELXB", &c_true, (ftnlen)7);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int grpk01_ () { MAIN__ (); return 0; }
