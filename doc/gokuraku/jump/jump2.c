/*  -- translated by f2c (version 20160102).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "libtinyf2c.h"

/* Table of constant values */

static doublereal c_b2 = 10.;
static integer c__9 = 9;
static integer c__1 = 1;
static integer c__3 = 3;
static real c_b9 = 1.f;
static real c_b10 = 1e4f;
static real c_b11 = .1f;
static real c_b12 = 1e6f;
static real c_b13 = .2f;
static real c_b14 = .8f;
static integer c__4 = 4;
static integer c__100 = 100;

/* Main program */ int MAIN__(void)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *), exp(doublereal);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_rsle(cilist *), e_rsle(void);

    /* Local variables */
    static integer i__;
    static real r__, x[100], y[100], r0, x2;
    static integer iws;
    static real rexp;
    extern /* Subroutine */ int grcls_(void), grfrm_(void), gropn_(integer *),
	     uulin_(integer *, real *, real *), grswnd_(real *, real *, real *
	    , real *), grstrf_(void), usdaxs_(void), grstrn_(integer *), 
	    sgpwsn_(void), grsvpt_(real *, real *, real *, real *), ussttl_(
	    char *, char *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___8 = { 0, 6, 0, 0, 0 };
    static cilist io___9 = { 0, 5, 0, 0, 0 };


/* -- データ 2 ---- */
    r__ = .2f;
    r0 = 0.f;
    for (i__ = 1; i__ <= 100; ++i__) {
	r__ = r__ * 3.6f * (1.f - r__);
	r0 = r0 + r__ * 4 - 2.58f;
/* Computing 2nd power */
	i__1 = i__ - 50;
	x2 = (real) (i__1 * i__1);
	rexp = i__ * 4.f / 100;
	d__1 = (doublereal) rexp;
	x[i__ - 1] = pow_dd(&c_b2, &d__1);
	d__1 = (doublereal) r0;
	y[i__ - 1] = exp(-x2) * 1e5f + pow_dd(&c_b2, &d__1);
/* L10: */
    }
    y[19] = 1e4f;
    y[39] = 2e3f;
    y[64] = 3e4f;
    y[69] = 500.f;
/* -- グラフ 2 ---- */
    s_wsle(&io___8);
    do_lio(&c__9, &c__1, " WORKSTATION ID (I)  ? ;", (ftnlen)24);
    e_wsle();
    sgpwsn_();
    s_rsle(&io___9);
    do_lio(&c__3, &c__1, (char *)&iws, (ftnlen)sizeof(integer));
    e_rsle();
    gropn_(&iws);
    grfrm_();
    grswnd_(&c_b9, &c_b10, &c_b11, &c_b12);
    grsvpt_(&c_b13, &c_b14, &c_b13, &c_b14);
    grstrn_(&c__4);
    grstrf_();
    ussttl_("FREQUENCY", "/s", "POWER", "m|2\"/s|2\"", (ftnlen)9, (ftnlen)2, (
	    ftnlen)5, (ftnlen)9);
    usdaxs_();
    uulin_(&c__100, x, y);
    grcls_();
    return 0;
} /* MAIN__ */

/* Main program alias */ int jump2_ () { MAIN__ (); return 0; }
